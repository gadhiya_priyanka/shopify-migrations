let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/app.scss', 'public/css')
        .js('resources/assets/js/main-app.js', 'public/js')
        .js('resources/assets/js/project.js', 'public/js')
        .js('resources/assets/js/product.js', 'public/js')
        .js('resources/assets/js/order.js', 'public/js')
        .js('resources/assets/js/form.js', 'public/js')
        .js('resources/assets/js/errors.js', 'public/js')
        .js('resources/assets/js/blog.js', 'public/js')
        .copyDirectory('resources/assets/sass/css', 'public/css')
        .copyDirectory('resources/assets/js/libraries', 'public/js')
        .copyDirectory('resources/assets/fonts', 'public/fonts');