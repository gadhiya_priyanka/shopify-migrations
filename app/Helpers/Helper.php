<?php

namespace App\Helpers;

class Helper
{

    public static function shopifyDateFormate($date)
    {
        return date("c", strtotime($date));
    }

    public static function handleizedString($str)
    {
        $str = str_replace([' ', '/'], ['-', '-'], strtolower(trim($str)));
        return trim(preg_replace('/[^A-Za-z0-9\-]/', '', $str));
    }

    public static function inRange($number, $min, $max, $inclusive = FALSE)
    {
        if (is_int($number) && is_int($min) && is_int($max)) {
            return $inclusive ? ($number >= $min && $number <= $max) : ($number > $min && $number < $max);
        }

        return FALSE;
    }

    public static function shopifyError($str)
    {
        return substr($str, strpos($str, '{"errors"'));
    }

    public static function convertValidEmail($email)
    {
        $domain = self::after('@', $email);
        $domain = substr($domain, strpos($domain, '.') + 1);

        $invalidComDomains = [
            'comI', 'co', 'cm', 'cim', 'comcom', 'ww.com', 'clm', 'c.om', 'combd', 'com0', 'cpm', 'con', 'om', 'son', 'ccom', 'vom', 'scom', 'come', 'commi', 'coml', 'clom', 'xom', 'comm', 'cok', 'comc', 'ca', 'ocm', 'com,', 'comeE', 'comd', 'comlll', 'som'
        ];

        $invalidNetDomains = [
            'nat', 'neg'
        ];

        $invalidEduDomains = [
            'eu', 'esu', 'ed'
        ];

        if (in_array($domain, $invalidComDomains)) {
            return str_replace('.' . $domain, '.com', $email);
        } elseif (in_array($domain, $invalidNetDomains)) {
            return str_replace('.' . $domain, '.net', $email);
        } elseif (in_array($domain, $invalidEduDomains)) {
            return str_replace('.' . $domain, '.edu', $email);
        }

        return $email;
    }

    public static function after($current, $inthat)
    {
        if (!is_bool(strpos($inthat, $current)))
            return trim(substr($inthat, strpos($inthat, $current) + strlen($current)));
    }

    public static function before($current, $inthat)
    {
        return trim(substr($inthat, 0, strpos($inthat, $current)));
    }

    public static function between($current, $that, $inthat)
    {
        return trim(self::before($that, self::after($current, $inthat)));
    }

    public static function addPrefixToArrayKey($prefix, $array)
    {
        return array_combine(
            array_map(function($k) use ($prefix) {
                return ($k) ? $prefix . '-' . $k : $prefix;
            }, array_keys($array)), $array
        );
    }
}
