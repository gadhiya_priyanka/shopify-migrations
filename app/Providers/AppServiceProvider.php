<?php

namespace App\Providers;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //redirect on https
        if (config('app.env') == 'production') {
            $this->app['request']->server->set('HTTPS', true);
        }

        //extra validation 
        Validator::extend('string_contains', function ($attribute, $value, $parameters, $validator) {
            foreach ($parameters as $parameter) {
                if (str_contains($value, $parameter)) {
                    return FALSE;
                }
            }

            return TRUE;
        });

        //resource without wrapping data attribute for api
        Resource::withoutWrapping();

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
