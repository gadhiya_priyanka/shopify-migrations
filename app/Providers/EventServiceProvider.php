<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\RedirectMigrated' => [
            'App\Listeners\RedirectMigrated\NotifyUser',
        ],
        'App\Events\ProductMigrated' => [
            'App\Listeners\ProductMigrated\NotifyUser',
        ],
        'App\Events\CustomerMigrated' => [
            'App\Listeners\CustomerMigrated\NotifyUser',
        ],
        'App\Events\OrderMigrated' => [
            'App\Listeners\OrderMigrated\NotifyUser',
        ],
        'App\Events\ScheduleSaved' => [
            'App\Listeners\SchedulSaved\StartMigration',
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
