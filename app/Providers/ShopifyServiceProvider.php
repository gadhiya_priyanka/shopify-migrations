<?php

namespace App\Providers;

use App\Shopify\Client;
use App\Shopify\CallLimit;
use App\Shopify\ShopifyManager;
use Illuminate\Support\ServiceProvider;

class ShopifyServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind('shopify', ShopifyManager::class);

        $this->app->bind(Client::class, function ($app, $parameters) {  
            $domain = isset($parameters['domain']) ? $parameters['domain'] : $this->getDomain();
            $accessToken = isset($parameters['accessToken']) ? $parameters['accessToken'] : $this->getAccessToken();
//            $callLimit = isset($parameters['callLimit']) ? $parameters['callLimit'] : app(CallLimit::class);
            $callLimit = null;

            return new Client($domain, $accessToken, $callLimit);
        });
    }
    
    protected function getDomain()
    {
        return 'demo-pia.myshopify.com';
    }

    protected function getAccessToken()
    {
        return null;
    }
}
