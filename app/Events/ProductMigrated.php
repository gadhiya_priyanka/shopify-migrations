<?php

namespace App\Events;

use App\Models\ProjectProductSetting as ProductSetting;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ProductMigrated
{

    use Dispatchable,
        InteractsWithSockets,
        SerializesModels;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $productSetting;

    public function __construct(ProductSetting $productSetting)
    {
        $this->productSetting = $productSetting;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
