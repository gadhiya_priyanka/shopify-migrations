<?php

namespace App\Events;

use App\Models\ProjectOrderSetting as OrderSetting;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderMigrated
{

    use Dispatchable,
        InteractsWithSockets,
        SerializesModels;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $orderSetting;

    public function __construct(OrderSetting $orderSetting)
    {
        $this->orderSetting = $orderSetting;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
