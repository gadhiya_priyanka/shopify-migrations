<?php

namespace App\Notifications;

use App\Models\ProjectOrderSetting as OrderSetting;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NotifyOrderSettingMigrated extends Notification
{

    use Queueable;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $orderSetting, $attachedFiles;

    public function __construct(OrderSetting $orderSetting)
    {
        $this->orderSetting = $orderSetting;
        $this->attachedFiles = $this->orderSetting->file_paths['outputs'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $email = (new MailMessage)->subject("Orders Migration")
            ->view('emails.notifications.orders.order-migrated', ['data' => $this->orderSetting]);

        foreach ($this->attachedFiles as $filePath) {
            $email->attach(storage_path($filePath));
        }

        return $email;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => 'orders',
            'model' => OrderSetting::class,
            'model_id' => $this->orderSetting->id,
            'message' => 'Orders of ' . strtoupper($this->orderSetting->project->name) . ' project has been migrated.'
        ];
    }
}
