<?php

namespace App\Notifications;

use App\Models\ProjectRedirectSetting as RedirectSetting;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NotifyRedirectSettingMigrated extends Notification
{

    use Queueable;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $redirectSetting, $attachedFiles;

    public function __construct(RedirectSetting $redirectSetting)
    {
        $this->redirectSetting = $redirectSetting;
        $this->attachedFiles = $this->redirectSetting->file_paths['outputs'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $email = (new MailMessage)->subject("Redirects Migration")
            ->view('emails.notifications.redirects.redirect-migrated', ['data' => $this->redirectSetting]);

        foreach ($this->attachedFiles as $filePath) {
            $email->attach(storage_path($filePath));
        }

        return $email;

        return (new MailMessage)
                ->subject("Redirects Migration")
                ->view('emails.notifications.redirects.redirect-migrated', ['data' => $this->toArray($notifiable)])
                ->attach(storage_path($this->redirectSetting->output_file_path));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => 'redirects',
            'model' => RedirectSetting::class,
            'model_id' => $this->redirectSetting->id,
            'message' => 'Redirects of ' . strtoupper($this->redirectSetting->project->name) . ' project has been migrated.'
        ];
    }
}
