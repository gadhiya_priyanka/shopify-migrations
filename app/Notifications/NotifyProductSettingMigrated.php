<?php

namespace App\Notifications;

use App\Models\ProjectProductSetting as ProductSetting;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NotifyProductSettingMigrated extends Notification
{

    use Queueable;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $productSetting, $attachedFiles;

    public function __construct(ProductSetting $productSetting)
    {
        $this->productSetting = $productSetting;
        $this->attachedFiles = $this->productSetting->file_paths['outputs'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $email = (new MailMessage)->subject("Products Migration")
            ->view('emails.notifications.products.product-migrated', ['data' => $this->productSetting]);

        foreach ($this->attachedFiles as $filePath) {
            $email->attach(storage_path($filePath));
        }

        return $email;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => 'products',
            'model' => ProductSetting::class,
            'model_id' => $this->productSetting->id,
            'message' => 'Products of ' . strtoupper($this->productSetting->project->name) . ' project has been migrated.'
        ];
    }
}
