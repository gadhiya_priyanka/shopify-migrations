<?php

namespace App\Notifications;

use App\Models\ProjectCustomerSetting as CustomerSetting;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NotifyCustomerSettingMigrated extends Notification
{

    use Queueable;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $customerSetting, $attachedFiles;

    public function __construct(CustomerSetting $customerSetting)
    {
        $this->customerSetting = $customerSetting;
        $this->attachedFiles = $this->customerSetting->file_paths['outputs'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $email = (new MailMessage)->subject("Customers Migration")
            ->view('emails.notifications.customers.customer-migrated', ['data' => $this->customerSetting]);

        foreach ($this->attachedFiles as $filePath) {
            $email->attach(storage_path($filePath));
        }

        return $email;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => 'customers',
            'model' => CustomerSetting::class,
            'model_id' => $this->customerSetting->id,
            'message' => 'Customers of ' . strtoupper($this->customerSetting->project->name) . ' project has been migrated.'
        ];
    }
}
