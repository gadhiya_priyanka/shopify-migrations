<?php

namespace App\Http\Requests;

use App\Models\CustomGuzzleHttp;
use App\Models\Platform;
use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    protected $platform;

    public function __construct()
    {
        $request = request();
        $this->platform = Platform::find($request->platform_id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //validation rule
        $validationRules = [];

        $request = request();
        if (isset($request->platform_id)) {

            $settings = $this->platform->setting->shopify_project;

            $validationRules['name'] = 'required|string|max:50';
            $validationRules['shopify_details.api_url'] = 'required|max:100';
            $validationRules['shopify_details.api_key'] = 'required|size:32';
            $validationRules['shopify_details.api_password'] = 'required|size:32';

            //based on platform settings
            foreach ($settings as $setting) {
                switch ($setting) {
                    case 'url':
                        $validationRules['platform_details.url'] = 'required|url|max:100';
                        break;

                    case 'api_url':
                        $validationRules['platform_details.api_url'] = 'required|url|max:100';
                        break;

                    case 'api_key':
                        $validationRules['platform_details.api_key'] = 'required|max:50';
                        break;

                    case 'api_password':
                        $validationRules['platform_details.api_password'] = 'required|max:50';
                        break;

                    case 'db_host':
                        $validationRules['platform_details.db_host'] = 'required|max:50';
                        break;

                    case 'db_port':
                        $validationRules['platform_details.db_port'] = 'required|max:4';
                        break;

                    case 'db_name':
                        $validationRules['platform_details.db_name'] = 'required|max:20';
                        break;

                    case 'db_username':
                        $validationRules['platform_details.db_username'] = 'required|max:50';
                        break;

                    case 'db_password':
                        $validationRules['platform_details.db_password'] = 'required|max:50';
                        break;
                }
            }
        }

        return $validationRules;
    }

    public function withValidator($validator)
    {

        $request = request();
        if ($request->platform_id && !$validator->messages()->getMessages()) {
            $validator->after(function ($validator) use ($request) {
                if (isset($request->platform_details['db_host']) && $error = $this->checkValidDatabase()) {
                    $validator->errors()->add('platform_database', 'Sorry!! We could not access database.');
                }


                if (isset($request->platform_details['api_url']) && $error = $this->checkValidApi('source', $this->platform->slug)) {
                    $validator->errors()->add('platform_api', 'Sorry!! We could not access ' . $this->platform->name . ' api.');
                }

                if ($error = $this->checkValidApi('target', 'shopify')) {
                    $validator->errors()->add('shopify_api', 'Sorry!! We could not access shopify private api.');
                }
            });
        }
    }

    public function checkValidDatabase()
    {
        $request = request();
        $db = CustomGuzzleHttp::connectExternalDatabase($request->platform_details);
        try {
            $db->select('SHOW TABLES');
        } catch (\Exception $e) {
            return TRUE;
        }

        return FALSE;
    }

    public function checkValidApi($type, $slug)
    {
        $request = request();
        $url = config('app.' . $slug . '_test_url');
        $data = ($type == 'target') ? $request->shopify_details : $request->platform_details;
        
        $client = CustomGuzzleHttp::connectExternalAPI($data);
        try {
            $result = $client->request('GET', $url);
        } catch (\Exception $e) {
            return TRUE;
        }
        return FALSE;
    }

    public function messages()
    {
        return [
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'project name',
            //platform details
            'platform_details.url' => 'url',
            'platform_details.api_url' => 'api path',
            'platform_details.api_key' => 'api key',
            'platform_details.api_password' => 'api password',
            'platform_details.db_host' => 'host',
            'platform_details.db_port' => 'port',
            'platform_details.db_name' => 'database name',
            'platform_details.db_username' => 'database username',
            'platform_details.db_password' => 'database password',
            //shopify details
            'shopify_details.api_url' => 'shopify url',
            'shopify_details.api_key' => 'private app key',
            'shopify_details.api_password' => 'private app password',
        ];
    }
}
