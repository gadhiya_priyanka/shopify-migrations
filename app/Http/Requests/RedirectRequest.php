<?php

namespace App\Http\Requests;

use App\Rules\RedirectRule;
use Illuminate\Foundation\Http\FormRequest;

class RedirectRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validationRules = [];
        $request = request();
        if ($request->all()) {
            $validationRules = [
                'project_id' => 'required',
                'file_path' => 'required|max:10000',
            ];
        }
        return $validationRules;
    }

    public function withValidator($validator)
    {
        $request = request();
        if ($request->project_id && !$validator->messages()->getMessages()) {
            $validator->after(function ($validator) use ($request) {
                $isValidate = TRUE;

                if ($request->file_path->getClientOriginalExtension() != 'xlsx') {
                    $validator->errors()->add('file_path', 'The upload file must be a file of type: xlsx.');
                    $isValidate = FALSE;
                }

                if ($isValidate) {
                    $request->validate([
                        'file_path' => [new RedirectRule],
                    ]);
                }
            });
        }
    }

    public function attributes()
    {
        return [
            'project_id' => 'project',
            'file_path' => 'upload file',
        ];
    }
}
