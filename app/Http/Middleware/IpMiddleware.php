<?php

namespace App\Http\Middleware;

use Closure;

class IpMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ips = [
            '127.0.0.1',
            '192.168.0.135',
            '192.168.0.139',
            '165.227.203.89',
        ];

//        dd($request->server());
        if (in_array($request->ip(), $ips)) {

//            header('Access-Control-Allow-Origin:'.$ad);
        }
        return $next($request);
    }
}
