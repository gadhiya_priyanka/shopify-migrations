<?php

namespace App\Http\Controllers;

use App\Models\Blog;

class BlogController extends Controller
{

    public function create()
    {
        return view('pages.blog-create', [
            'blogs' => Blog::all()
        ]);
    }

    public function store()
    {
        $this->validate(request(), [
            'name' => 'required',
            'description' => 'required'
        ]);

        Blog::forceCreate([
            'name' => request('name'),
            'description' => request('description')
        ]);

        return ['message' => 'Blog Created!'];
    }
}
