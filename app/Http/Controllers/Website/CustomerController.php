<?php

namespace App\Http\Controllers\Website;

use App\Models\Schedule;
use App\Models\ShopifyMigration;
use App\Models\Project;
use App\Models\ProjectCustomerSetting as CustomerSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{

    public function __construct()
    {
        set_time_limit(0);
    }

    public function index()
    {
        $projects = \Auth::user()->projects()->with('customerSettings', 'platform')->latest()->get();

        return view('pages.customers', compact('projects'));
    }

    public function view()
    {
        return view('pages.projects');
    }

    public function create(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('pages.customer-create');
        }

        $this->validate($request, $this->validateRules());

        $project = Project::find($request->project_id);

        $mapping = [];
        if ($project->platform->slug == 'cratejoy') {
            $cjRequest = new \App\Http\Controllers\Cratejoy\CratejoyController($project);
            $customerCount = $cjRequest->getCustomerCount();
            $mapping['customer_count'] = $customerCount;
        }

        if ($project->platform->slug == 'magento') {
            $cjRequest = new \App\Http\Controllers\Magento\MagentoController($project);
            $customerCount = $cjRequest->getCustomerCount();
            $mapping['customer_count'] = $customerCount;
        }

        $customerSetting = new CustomerSetting;
        $customerSetting->id = \UUID::uuid4()->toString();
        $customerSetting->mapping = $mapping;
        $customerSetting->file_paths = NULL;
        $customerSetting->import_type = isset($project->platform_details['api_url']) ? "api" : "database";
        $project->customerSettings()->save($customerSetting);

        $shopifyMigration = ShopifyMigration::where('type', 'Customers')->first();

        $schedule = new Schedule;
        $schedule->id = \UUID::uuid4()->toString();
        $schedule->shopify_migration_id = $shopifyMigration->id;
        $schedule->status = $this->getScheduleStatus($customerSetting);
        $customerSetting->schedule()->save($schedule);

        return redirect()->route('customers')->with('message', 'Once customer has been migrate. Mail will be delivered to: ' . \Auth::user()->email);
    }

    public function validateRules()
    {
        return [
            'project_id' => 'required',
        ];
    }

    public function getScheduleStatus($customerSetting)
    {
        $project = $customerSetting->project;

        $customerSetting = $project->customerSettings()
                ->whereDoesntHave('schedule', function ($query) {
                    $query->whereIn('status', ['queued', 'completed']);
                })->get()->toArray();

        return (count($customerSetting) > 1) ? 'queued' : 'todo';
    }
}
