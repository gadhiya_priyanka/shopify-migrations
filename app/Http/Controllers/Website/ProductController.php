<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Shopify\Api\LocationController;
use App\Models\Schedule;
use App\Models\ShopifyMigration;
use App\Models\Project;
use App\Models\ProjectProductSetting;
use App\Models\ProjectProductSetting as ProductSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    public function __construct()
    {
        set_time_limit(0);
    }

    public function index()
    {
        $projects = \Auth::user()->projects()->with('productSettings', 'platform')->latest()->get();

        return view('pages.products', compact('projects'));
    }

    public function view()
    {
        return view('pages.projects');
    }

    public function create(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('pages.product-create');
        }

//        dd($request->all());
        $this->save($request);

        return redirect()->route('products')->with('message', 'Once product has been migrate. Mail will be delivered to: ' . \Auth::user()->email . '. This should take less than 1 hour.');
    }

    public function save(Request $request, ProductSetting $productSetting = NULL)
    {
        $createProductSetting = ($productSetting) ? FALSE : TRUE;

        $this->validate($request, $this->validateRules(), [], $this->attributes());
        
        $project = Project::find($request->project_id);

        $mapping = [];
        $mapping['desc'] = $request->desc;
        if ($project->platform->slug == 'cratejoy') {
            $cjRequest = new \App\Http\Controllers\Cratejoy\CratejoyController($project);
            $productCount = $cjRequest->getProductCount($request->product_type);

            $mapping['product_type'] = $request->product_type;
            $mapping['migrate_invalid_product'] = isset($request->migrate_invalid_product) ? 'true' : 'false';
            $mapping['product_count'] = $productCount;
        }

        if ($project->platform->slug == 'magento') {
            $magentoRequest = new \App\Http\Controllers\Magento\MagentoController($project);
            $productCount = $magentoRequest->getProductCount();
            $mapping['product_count'] = $productCount;
        }
        $mapping['migrate_with_current_handle'] = $request->migrate_with_current_handle;
        $mapping['is_remigrate'] = $request->is_remigrate;

        $location = new LocationController($project->shopify_details);
        $result = $location->get();
        $location = $result['locations'][0];
        $mapping['location_id'] = $location['id'];

        if ($createProductSetting) {
            $productSetting = new ProductSetting;
            $productSetting->id = \UUID::uuid4()->toString();
        }
        $productSetting->mapping = $mapping;
        $productSetting->file_paths = NULL;
        $productSetting->import_type = isset($project->platform_details['api_url']) ? "api" : "database";

        ($createProductSetting) ? $project->productSettings()->save($productSetting) : $productSetting->save();

        if ($createProductSetting) {
            $shopifyMigration = ShopifyMigration::where('type', 'Products')->first();

            $schedule = new Schedule;
            $schedule->id = \UUID::uuid4()->toString();
            $schedule->shopify_migration_id = $shopifyMigration->id;
            $schedule->status = $this->getScheduleStatus($productSetting);
            $productSetting->schedule()->save($schedule);
        }
    }

    public function validateRules()
    {
        return [
            'project_id' => 'required',
            'metafields.*.key' => 'required',
            'metafields.*.value' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'metafields.*.key' => 'metafield key',
            'metafields.*.value' => 'metafield value',
        ];
    }

    public function getScheduleStatus($productSetting)
    {
        $project = $productSetting->project;

        $productSettings = $project->productSettings()
                ->whereDoesntHave('schedule', function ($query) {
                    $query->whereIn('status', ['queued', 'completed']);
                })->get()->toArray();

        return (count($productSettings) > 1) ? 'queued' : 'todo';
    }

    public function update(Request $request, ProductSetting $productSetting)
    {
        if ($request->isMethod('get')) {
            return view('pages.product-create', compact('productSetting'));
        }

        $this->save($request, $productSetting);

        return redirect()->route('products')->with('message', 'Product migration has been updated');
    }

    public function delete(Request $request, ProductSetting $productSetting)
    {
        
    }
}
