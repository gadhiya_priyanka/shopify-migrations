<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Core\ExcelController;
use App\Models\ShopifyMigration;
use App\Models\Schedule;
use App\Models\Project;
use App\Models\ProjectRedirectSetting as RedirectSetting;
use App\Http\Requests\RedirectRequest;
use App\Http\Controllers\Controller;

class RedirectController extends Controller
{

    public function __construct()
    {
        set_time_limit(0);
    }

    public function index()
    {
        $projects = \Auth::user()->projects()->with('redirectSettings')->latest()->get();

        return view('pages.redirects', compact('projects'));
    }

    public function create(RedirectRequest $request)
    {
        if ($request->isMethod('get')) {
            return view('pages.redirect-create');
        }

        $fileName = $request->file('file_path')->getClientOriginalName();
        $storeFileName = time() . '-' . trim(preg_replace('/[^A-Za-z0-9\-.]/', '', str_replace(' ', '-', strtolower(trim($fileName)))));
        $filePath = $request->file('file_path')->storeAs($request->project_id . '/redirects/inputs', $storeFileName);
        $filePath = 'app/' . $filePath;

        $totalUrls = count(ExcelController::view(storage_path($filePath)));
        $project = Project::find($request->project_id);

        $redirectSetting = new RedirectSetting;
        $redirectSetting->id = \UUID::uuid4()->toString();
        $redirectSetting->mapping = [
            'external' => FALSE
        ];
        $redirectSetting->file_paths = [
            'input' => [
                'file_path' => $filePath,
                'file_name' => $fileName,
                'total_urls' => $totalUrls
            ]
        ];
        $project->redirectSettings()->save($redirectSetting);

        $shopifyMigration = ShopifyMigration::where('type', 'Redirects')->first();

        $schedule = new Schedule;
        $schedule->id = \UUID::uuid4()->toString();
        $schedule->shopify_migration_id = $shopifyMigration->id;
        $schedule->status = 'todo';
//        $schedule->status = $this->getScheduleStatus($redirectSetting);
        $redirectSetting->schedule()->save($schedule);

        return redirect()->route('redirects')->with('message', 'Your generated csv file will be delivered by email to: ' . \Auth::user()->email . '. This should take less than 1 hour.');
    }

    public function getScheduleStatus($redirectSetting)
    {
        $project = $redirectSetting->project;

        $redirectSettings = $project->redirectSettings()
                ->whereDoesntHave('schedule', function ($query) {
                    $query->whereIn('status', ['queued', 'completed']);
                })->get()->toArray();
        return (count($redirectSettings) > 1) ? 'queued' : 'todo';
    }

    public function delete(RedirectSetting $redirectSetting)
    {
        if ($redirectSetting->schedule->status == "inprogress") {
            return redirect()->route('redirects')->with('error', 'Sorry!! The process has been in progress. You could not delete redirect.');
        }

        if (isset($redirectSetting->file_paths['input']['file_path'])) {
            \File::delete(storage_path($redirectSetting->file_paths['input']['file_path']));
        }
        if (isset($redirectSetting->file_paths['output']['file_path'])) {
            \File::delete(storage_path($redirectSetting->file_paths['output']['file_path']));
        }
        $redirectSetting->schedule()->delete();
        $redirectSetting->redirects()->delete();
        $redirectSetting->delete();
        return redirect()->route('redirects')->with('message', 'redirect has been deleted successfully.');
    }
}
