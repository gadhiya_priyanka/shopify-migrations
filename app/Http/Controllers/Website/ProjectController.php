<?php

namespace App\Http\Controllers\Website;

use App\Models\CustomGuzzleHttp;
use App\Http\Requests\ProjectRequest;
use App\Models\Platform;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{

    public function __construct()
    {
        set_time_limit(0);
    }

    public function index()
    {
        $projects = \Auth::user()->projects()->latest()->get();
        return view('pages.projects', compact('projects'));
    }

    public function view()
    {
        return view('pages.projects');
    }

    public function create(ProjectRequest $request)
    {
        if ($request->isMethod('get')) {
            return view('pages.project-create');
        }

        $platformDetails = $this->getPlatfromDetails($request);

        $project = new Project;
        $project->id = \UUID::uuid4()->toString();
        $project->platform_id = $request->platform_id;
        $project->name = $request->name;
        $project->platform_details = $platformDetails;
        $project->shopify_details = $request->shopify_details;
        \Auth::user()->projects()->save($project);

        return redirect()->route('projects')->with('message', 'Project have been created successfully.');
    }

    public function update(ProjectRequest $request, Project $project)
    {
        if ($request->isMethod('get')) {
            return view('pages.project-create', compact('project'));
        }

        $platformDetails = $this->getPlatfromDetails($request);

        $project->platform_id = $request->platform_id;
        $project->name = $request->name;
        $project->platform_details = $platformDetails;
        $project->shopify_details = $request->shopify_details;
        \Auth::user()->projects()->save($project);

        return redirect()->route('projects')->with('message', 'Project have been updated successfully.');
    }

    public function delete()
    {
        
    }

    public function getPlatfromDetails($request)
    {
        $platformDetails = $request->platform_details;
        $platform = Platform::find($request->platform_id);
        if ($platform->slug == "wordpress") {
            $db = CustomGuzzleHttp::connectExternalDatabase($request->platform_details);
            $tables = $db->select('SHOW TABLES');

            $key = 'Tables_in_' . $request->platform_details['db_name'];
            $dbtable_prefixs = explode('_', $tables[0]->$key);
            $platformDetails['table_prefix'] = array_first($dbtable_prefixs);
        }
        return $platformDetails;
    }
}
