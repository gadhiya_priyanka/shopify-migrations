<?php

namespace App\Http\Controllers\Website;

use App\Models\Schedule;
use App\Models\ShopifyMigration;
use App\Models\Project;
use App\Models\ProjectOrderSetting as OrderSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{

    public function __construct()
    {
        set_time_limit(0);
    }

    public function index()
    {
        $projects = \Auth::user()->projects()->with('orderSettings', 'platform')->latest()->get();

        return view('pages.orders', compact('projects'));
    }

    public function view()
    {
        return view('pages.projects');
    }

    public function create(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('pages.order-create');
        }

        $this->validate($request, $this->validateRules());

        $project = Project::find($request->project_id);

        $mapping = [];
        if ($project->platform->slug == 'cratejoy') {
            $platformStatuses = \App\Models\Platform::where(['id' => $project->platform->id])->first()->setting->shopify_order;
            $financialStatuses = $platformStatuses['financial_statuses'];
            foreach ($financialStatuses as $financialStatus) {
                $mapping['financial_statuses'][$financialStatus] = $request->financial_status[$financialStatus];
            }

            $fulfillmentStatuses = $platformStatuses['fulfillment_statuses'];
            foreach ($fulfillmentStatuses as $fulfillmentStatus) {
                $mapping['fulfillment_statuses'][$fulfillmentStatus] = $request->fulfillment_status[$fulfillmentStatus];
            }

            $cjRequest = new \App\Http\Controllers\Cratejoy\CratejoyController($project);
            $orderCount = $cjRequest->getOrderCount();
            $mapping['order_count'] = $orderCount;
        }

        if ($project->platform->slug == 'magento') {
            $magentoRequest = new \App\Http\Controllers\Magento\MagentoController($project);
            $orderCount = $magentoRequest->getOrderCount();
            $mapping['order_count'] = $orderCount;
        }

        if ($project->platform->slug == 'other') {
            $mapping['order_count'] = 9303;
        }

        $orderSetting = new OrderSetting;
        $orderSetting->id = \UUID::uuid4()->toString();
        $orderSetting->mapping = $mapping;
        $orderSetting->file_paths = NULL;
        $orderSetting->import_type = isset($project->platform_details['api_url']) ? "api" : "database";
        $project->orderSettings()->save($orderSetting);

        $shopifyMigration = ShopifyMigration::where('type', 'Orders')->first();

        $schedule = new Schedule;
        $schedule->id = \UUID::uuid4()->toString();
        $schedule->shopify_migration_id = $shopifyMigration->id;
//        $schedule->status = $this->getScheduleStatus($orderSetting);
        $schedule->status = 'todo';
        $orderSetting->schedule()->save($schedule);
        
        return redirect()->route('orders')->with('message', 'Once order has been migrate. Mail will be delivered to: ' . \Auth::user()->email);
    }

    public function validateRules()
    {
        return [
            'project_id' => 'required',
        ];
    }

    public function getScheduleStatus($orderSetting)
    {
        $project = $orderSetting->project;

        $orderSetting = $project->orderSettings()
                ->whereDoesntHave('schedule', function ($query) {
                    $query->whereIn('status', ['queued', 'completed']);
                })->get()->toArray();

        return (count($orderSetting) > 1) ? 'queued' : 'todo';
    }
}
