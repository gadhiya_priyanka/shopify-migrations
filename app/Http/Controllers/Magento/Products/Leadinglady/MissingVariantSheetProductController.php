<?php

namespace App\Http\Controllers\Magento\Products\Leadinglady;

use GuzzleHttp\Client;
use App\Http\Controllers\Core\ExcelController;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectProductSetting as ProductSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MissingVariantSheetProductController extends Controller
{
    public $magentoRequest, $shopifyRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function update(ProductSetting $productSetting)
    {
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        $products = \DB::table('recover_products')->get()->toArray();

        foreach ($products as $product) {
            
        }
    }

    public function getShopifyMainProducts()
    {
        $limit = 250;
        $collectionId = 59263877162;
        $url = 'admin/products.json';
        $params = [
            'collection_id' => $collectionId,
            'limit' => $limit
        ];
        $result = $this->shopifyRequest->view($url, $params);
        return $result['products'];
    }

    public function createMagentoProduct(ProductSetting $productSetting)
    {
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $this->magentoRequest = new MagentoController($project);
        $magentoProducts = $this->magentoRequest->getProducts();
        $products = [];

        foreach ($magentoProducts as $magentoProduct) {
            $isBaseProduct = $this->magentoRequest->isBaseProduct($magentoProduct);
            if ($isBaseProduct) {
                $variants = $this->magentoRequest->getVariants($magentoProduct->entity_id);
                $magentoVariantCount = count($variants);
                $visibilityTags = $this->magentoRequest->getProductVisibilityTag($magentoProduct);
                $visible = $this->magentoRequest->getProductValueFromAttributeCode($magentoProduct, 'status');

                $product = [];
                $product['sku'] = $magentoProduct->sku;
                $product['magento_id'] = $magentoProduct->entity_id;
                $product['product_name'] = $this->magentoRequest->getProductValueFromAttributeCode($magentoProduct, 'name');
                $product['product_hanldle'] = $this->magentoRequest->getProductValueFromAttributeCode($magentoProduct, 'url_key');
                $product['magento_variant_count'] = ($magentoVariantCount) ? $magentoVariantCount : '1';
                $product['shopify_variant_count'] = '';
                $product['not-visible-individually'] = ($visibilityTags) ? 'true' : 'false';
                $product['published'] = ($visible == 1) ? 'true' : 'false';
                $products[] = $product;
            }
        }

        $fileName = 'final-magento-products';
        $storePath = '/leading-lady/products';
        ExcelController::create($fileName, $products, $storePath)->download('csv');
    }

    public function updateFinalTimeForAlen(ProductSetting $productSetting)
    {
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $magentoProducts = $this->readCsv();

        $mainProducts = $notVisibleProducts = [];
        foreach ($magentoProducts as $magentoProduct) {
            if ($magentoProduct['not_visible_individually'] == 'true') {
                $notVisibleProducts[] = $magentoProduct;
            } else {
                $mainProducts[] = $magentoProduct;
            }
        }
        $fileName = 'final-main-products';
        $storePath = '/leading-lady/products';
        ExcelController::create($fileName, $mainProducts, $storePath);

        $fileName = 'final-not-visible-products';
        $storePath = '/leading-lady/products';
        ExcelController::create($fileName, $notVisibleProducts, $storePath);
    }

    public function updateSecondTime(ProductSetting $productSetting)
    {
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $this->magentoRequest = new MagentoController($project);
        $magentoProducts = $this->readCsv();
        $gettingErrors = $products = [];
        foreach ($magentoProducts as $magentoProduct) {
            if ($magentoProduct['shopify_variant_count'] == '-') {
                $shopifyProduct = FALSE;
                $url = 'admin/products.json';
                $params = [
                    'handle' => $magentoProduct['product_hanldle']
                ];
                $result = $this->shopifyRequest->view($url, $params);

                if (isset($result['products']) && $result['products']) {
                    $shopifyProduct = $result['products'][0];
                } else {
                    sleep(2);
                    $result = $this->shopifyRequest->view($url, $params);
                    if (isset($result['products']) && $result['products']) {
                        $shopifyProduct = $result['products'][0];
                    } else {
                        $error = [];
                        $error['error'] = $result;
                        $error['id'] = $magentoProduct['magento_id'];
                        $gettingErrors[] = $error;
                    }
                }

//            step 2:get variant count (if shopify product get)
                if ($shopifyProduct) {
                    if ($magentoProduct['magento_variant_count'] > 100) {
                        $proId = $shopifyProduct['id'];

                        $client = new Client([
                            'defaults' => [
                                'headers' => ['content-type' => 'application/json']
                            ],
                        ]);

                        $url = 'https://ll.wlfpt.co/productapi/getdetails?id=' . $proId;
                        $data = $client->request('GET', $url);
                        $appProduct = json_decode($data->getBody(), true);

                        $variantCount = count($appProduct['variants']);
                    } else {
                        $variantCount = count($shopifyProduct['variants']);
                    }
                }

                $product = [];
                $product = $magentoProduct;
                $product['shopify_variant_count'] = (isset($variantCount)) ? $variantCount : '0';
                $product['in_shopify'] = ($shopifyProduct) ? '1' : '0';
                $products[] = $product;
            } else {
                $product = [];
                $product = $magentoProduct;
                $products[] = $product;
            }
        }

        $storePath = '/leading-lady/products';
        $fileName = 'final-magento-vs-shopify-products';
        ExcelController::create($fileName, $products, $storePath);

        $fileName = 'final-products-error';
        ExcelController::create($fileName, $gettingErrors, $storePath);
    }

    public function updateCsvFirstTime(ProductSetting $productSetting)
    {
//        dd("update produc");
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $this->magentoRequest = new MagentoController($project);
        $magentoProducts = $this->readCsv();

        foreach ($magentoProducts as $magentoProduct) {
//            step 1: get shopify product
            $shopifyProduct = FALSE;
            $url = 'admin/products.json';
            $params = [
                'handle' => $magentoProduct['product_hanldle']
            ];
            $result = $this->shopifyRequest->view($url, $params);
            if (isset($result['products']) && $result['products']) {
                $shopifyProduct = $result['products'][0];
            }

//            step 2:get variant count (if shopify product get)
            $variantCount = '-';
            if ($shopifyProduct) {
                if ($magentoProduct['magento_variant_count'] > 100) {
                    $proId = $shopifyProduct['id'];

                    $client = new Client([
                        'defaults' => [
                            'headers' => ['content-type' => 'application/json']
                        ],
                    ]);

                    $url = 'https://ll.wlfpt.co/productapi/getdetails?id=' . $proId;
                    $data = $client->request('GET', $url);
                    $appProduct = json_decode($data->getBody(), true);

                    $variantCount = count($appProduct['variants']);
                } else {
                    $variantCount = count($shopifyProduct['variants']);
                }
            }

            $product = [];
            $product = $magentoProduct;
            $product['shopify_variant_count'] = $variantCount;
            $product['in_shopify'] = ($shopifyProduct) ? '1' : '0';
            $products[] = $product;

            sleep(2);
        }

        $fileName = 'magento-vs-shopify-products';
        $storePath = '/leading-lady/products';
        ExcelController::create($fileName, $products, $storePath)->download('csv');
    }

    public function readCsv()
    {
//        $fileName = 'final-magento-products.csv'; // updateCsvFirstTime
//        $filePath = storage_path('leading-lady/products/') . $fileName;
//        $fileName = 'magento-vs-shopify-products.csv'; //updateSecondTime
//        $filePath = storage_path('leading-lady/products/') . $fileName;
//        $fileName = 'final-magento-vs-shopify-products.csv'; //updateFinalTimeForAlen
//        $filePath = storage_path('leading-lady/products/') . $fileName;
//        dd($filePath);

        $fileName = 'final-main-products.csv';
        $filePath = storage_path('leading-lady/products/') . $fileName;
        return ExcelController::view($filePath);
    }
}
