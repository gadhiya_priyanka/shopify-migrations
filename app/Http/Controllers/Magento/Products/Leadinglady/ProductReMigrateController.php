<?php

namespace App\Http\Controllers\Magento\Products\Leadinglady;

use App\Models\ProjectProduct;
use GuzzleHttp\Client;
use App\Http\Controllers\Core\ExcelController;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectProductSetting as ProductSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductReMigrateController extends Controller
{
    public $magentoRequest, $shopifyRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create(ProductSetting $productSetting)
    {
        dd("create products");
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        $this->magentoRequest = new MagentoController($project);
        //10342 - last record (first migration)
        $options = [];
        $options['from'] = 0;
        $options['size'] = 1;
        $options['search'] = [['row_id', '=', 10993]];
        $migrateProducts = $this->magentoRequest->getProducts($options);
        $migrateProduct = $migrateProducts[0];

        foreach ($migrateProducts as $migrateProduct) {
            \Log::info(['product_id' => $migrateProduct->row_id]);
            $isBaseProduct = $this->magentoRequest->isBaseProduct($migrateProduct);
            if (!$isBaseProduct) {
                $migrateProduct = $this->getBaseProduct($migrateProduct, $options);
                $stopVariable = TRUE;
                if ($migrateProduct) {
                    \Log::info(['stop_product_id' => $migrateProduct->row_id]);
                    $isBaseProduct = TRUE;
                }
            }

            if ($isBaseProduct) {
                $shopifyProduct = [];
                $shopifyProduct = $this->generateShopifyProductObject($migrateProduct);

                dd($shopifyProduct);
//                $shopifyProductImages = $shopifyProduct['images'];
//                unset($shopifyProduct['images']);

                $shopifyProduct = $this->insertProduct($shopifyProduct, $migrateProduct, $productSetting);
//                $this->updateAllVariantImage($shopifyProductImages, $shopifyProduct);
            }

            if (isset($stopVariable)) {
                \Log::info("Script End with stopVariable");
                dd("stop script with stopVariable");
            }
        }

        dd("completed");
    }

    public function insertProduct($shopifyProduct, $migrateProduct, $productSetting, $migrateVariantProduct = [])
    {
        $shopifyProduct = $this->insertProductUsingShopifyApi($shopifyProduct);
        $this->insertProductIntoDatabase($shopifyProduct, $migrateProduct, $productSetting, $migrateVariantProduct);
        return $shopifyProduct;
    }

    public function insertProductUsingShopifyApi($shopifyProduct)
    {
        $url = 'admin/products.json';
        $params = [
            'product' => $shopifyProduct
        ];
        $result = $this->shopifyRequest->create($url, $params);
        if (!isset($result['product'])) {
            \Log::info(["product with error" => $result]);
            dd($result, $shopifyProduct);
        }

        return $result['product'];
    }

    public function insertProductIntoDatabase($importedProduct, $migrateProduct, $productSetting, $migrateVariantProduct)
    {
        $extra = [];
        $extra['product_id'] = $migrateProduct->row_id;
        if ($migrateVariantProduct) {
            $extra['varinat_id'] = $migrateVariantProduct->row_id;
        }
        $extra['type'] = ($migrateVariantProduct) ? 'sub_product' : 'main';

        $projectProduct = new ProjectProduct;
        $projectProduct->id = \UUID::uuid4()->toString();
        $projectProduct->project_id = $productSetting->project->id;
        $projectProduct->product = $importedProduct;
        $projectProduct->extra = $extra;
        $productSetting->products()->save($projectProduct);
    }

    public function updateAllVariantImage($shopifyProductImages, $shopifyProduct)
    {
        $variants = $shopifyProduct['variants'];
        $variantColors = array_map('strtolower', array_column($variants, 'option1'));
        $colors = array_unique($variantColors);

        foreach ($shopifyProductImages as $shopifyProductImage) {
            $alt = $shopifyProductImage['alt'];
            $variantIds = [];

            $validColors = [];
            foreach ($colors as $color) {
                if (str_contains(strtolower($alt), $color)) {
                    $validColors[] = $color;
                }
            }

            foreach ($validColors as $validColor) {
                $variantkeys = [];
                $variantkeys = array_keys($variantColors, $validColor);
                foreach ($variantkeys as $variantkey) {
                    $variantIds[] = $variants[$variantkey]['id'];
                }
            }

            //product image object
            $productImage = [];
            $productImage['src'] = $shopifyProductImage['src'];
            $productImage['alt'] = $shopifyProductImage['alt'];
            $productImage['variant_ids'] = $variantIds;

            $url = '/admin/products/' . $shopifyProduct['id'] . '/images.json';
            $params = [
                'image' => $productImage
            ];

            $updateProduct = $this->shopifyRequest->create($url, $params);
            if (is_string($updateProduct)) {
                sleep(1);
                \Log::info(["second time updateAllVariantImage" => $shopifyProduct['id'], $updateProduct, $productImage]);
                $updateProduct = $this->shopifyRequest->create($url, $params);
            }
        }
    }

    public function getBaseProduct($migrateProduct, $options)
    {
        for ($i = 1; $i <= 1000; $i++) {
            $options['search'] = [['row_id', '>', $migrateProduct->row_id]];

            $migrateProducts = $this->magentoRequest->getProducts($options);
            if (!$migrateProducts) {
                return NULL;
            }
            $migrateProduct = $migrateProducts[0];

            $isBaseProduct = $this->magentoRequest->isBaseProduct($migrateProduct);

            if ($isBaseProduct) {
                return $migrateProduct;
            }
        }
    }

    public function createDeleteVariants(ProductSetting $productSetting)
    {
        dd("create deleted variants");
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $this->magentoRequest = new MagentoController($project);
        $products = \DB::table('recover_products')->get()->toArray();
        foreach ($products as $product) {

            $url = 'admin/products/' . $product->product_id . '/metafields.json';
            $result = $this->shopifyRequest->view($url);
            if (isset($result['metafields'])) {
                $metafields = $result['metafields'];
                foreach ($metafields as $metafield) {
                    if ($metafield['key'] == 'variant_count') {
                        \DB::table('recover_products')->where('id', $product->id)->update([
                            'metafield_counter' => $metafield['value']
                        ]);
                    }
                }
            }
            sleep(1);
        }
        dd("done");
    }

    public function updateMissingVariant(ProductSetting $productSetting)
    {
        dd("resolved delete products");
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $this->magentoRequest = new MagentoController($project);

        $products = \DB::table('recover_products')->get()->toArray();
        $products = array_slice($products, 34);
//        dd($products);
        foreach ($products as $product) {
            \Log::info(['product_id' => $product->product_id]);
            $migrateProduct = $this->magentoRequest->getMagentoProduct($product->magento_id);
            $shopifyProduct = $this->generateShopifyProductObject($migrateProduct);
            $variants = $shopifyProduct['variants'];
//            dd($variants);
            foreach ($variants as $variant) {
                $params = [
                    'variant' => $variant
                ];
                $url = 'admin/products/' . $product->product_id . '/variants.json';
                $result = $this->shopifyRequest->create($url, $params);
                if (is_string($result)) {
                    \Log::info(["error" => $result, 'variant_id' => $variant['sku']]);
                } else {
                    \Log::info(["success" => $result['variant']['id'], 'variant_id' => $variant['sku']]);
                }
                sleep(1);
            }
            sleep(10);
        }
    }

    public function generateShopifyProductObject($migrateProduct)
    {
        //shopify product object

        $visible = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'status');

        $shopifyProduct = [];
        $shopifyProduct['handle'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'url_key'); //url_key => 86
        $shopifyProduct['title'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'name');  //name => 60        
//        dd($shopifyProduct);
//        $shopifyProduct['body_html'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'description'); //description => 61 //short_description => 62
        $shopifyProduct['vendor'] = '';


        $shopifyProduct['published'] = ($visible == 1) ? 'true' : 'false';
        $shopifyProduct['variants'] = $this->magentoRequest->generateProductVariants($migrateProduct);
        $shopifyProduct['options'] = $this->magentoRequest->generateProductOptions(array_first($shopifyProduct['variants']));

        $shopifyProduct['tags'] = $this->magentoRequest->generateProductTags($migrateProduct, $shopifyProduct['variants']);
        $shopifyProduct['product_type'] = (in_array('not-visible-individually', $shopifyProduct['tags'])) ? 'not-visible-individually' : '';
        $shopifyProduct['images'] = $this->magentoRequest->generateProductImages($migrateProduct);

        $shopifyProduct['metafields_global_title_tag'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'meta_title'); //meta_title = 71
        $shopifyProduct['metafields_global_description_tag'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'meta_description'); //meta_description = 73

        $shopifyProduct['metafields'] = $this->magentoRequest->getProductMetaField($migrateProduct, $shopifyProduct);

        return $shopifyProduct;
    }
}
