<?php

namespace App\Http\Controllers\Magento\Products\Leadinglady;

use App\Http\Controllers\Core\ExcelController;
use App\Helpers\Helper;
use App\Models\ProjectProduct;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectProductSetting as ProductSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//for lace => 1455423389738
// MOLDED PADDED SOFTCUP BRA => 1455453601834
class ProductUpdateController extends Controller
{
    public $magentoRequest, $shopifyRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create(ProductSetting $productSetting)
    {
        \Log::useDailyFiles(storage_path() . '/logs/ll/products/delete-product.log');
        \Log::info("script start");
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        for ($i = 1; $i <= 6; $i++) {
            $url = 'admin/products.json';
            $params = [
                'collection_id' => '60259827754',
                'limit' => 250,
                'fields' => 'id'
            ];
            $result = $this->shopifyRequest->view($url, $params);
            $products = $result['products'];

            foreach ($products as $product) {
                \Log::info(["product_id" => $product['id']]);
                $url = 'admin/products/' . $product['id'] . '.json';
                
                $result = $this->shopifyRequest->delete($url);
                if (is_string($result)) {
                    sleep(2);
                    $result = $this->shopifyRequest->delete($url);
                    if (is_string($result)) {
                        \Log::info(["product_delete_error_id" => $result]);
                    }
                }
            }
        }
        \Log::info("script end");
    }

    public function createUpdateCollectionProducts(ProductSetting $productSetting)
    {
        dd("product update controlleasdfr");
        \Log::useDailyFiles(storage_path() . '/logs/ll-merge-product.log');
        \Log::info("script start");
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        $url = 'admin/products.json';
        $params = [
            'collection_id' => '60123676714',
            'limit' => 250
        ];
        $result = $this->shopifyRequest->view($url, $params);
        $products = $result['products'];
        dd($products);
        foreach ($products as $product) {
            \Log::info(["product_id" => $product['id']]);

            $tags = $product['tags'];
            $tags = explode(',', $tags);
            $tags = array_map('trim', $tags);
            foreach ($tags as $key => $tag) {
                if ($tag != "sub-product") {
                    unset($tags[$key]);
                }
            }
            $tags[] = 'molded-padded-seamless-wirefree-full-figure-bra';
            $tags = implode(',', $tags);

            $updateProduct = [];
            $updateProduct['id'] = $product['id'];
            $updateProduct['tags'] = $tags;
            $params = [
                'product' => $updateProduct
            ];
            $url = 'admin/products/' . $product['id'] . '.json';

            $result = $this->shopifyRequest->update($url, $params);
            if (!isset($result['product'])) {
                sleep(2);
                $result = $this->shopifyRequest->update($url, $params);
                if (!isset($result['product'])) {
                    \Log::info(["product_update_error" => $result]);
                }
            }

            $metaField = [];
            $metaField['key'] = 'main_product_handle';
            $metaField['value'] = 'molded-padded-seamless-wirefree-full-figure-bra';
            $metaField['value_type'] = 'string';
            $metaField['namespace'] = 'product_manager';

            $url = 'admin/products/' . $product['id'] . '/metafields.json';
            $params = [
                'metafield' => $metaField
            ];
            $result = $this->shopifyRequest->create($url, $params);
            if (!isset($result['metafield'])) {
                sleep(2);
                $result = $this->shopifyRequest->create($url, $params);
                if (!isset($result['metafield'])) {
                    \Log::info(["product_metafield_error" => $result]);
                }
            }
        }
        \Log::info("script end");
        dd("done");
    }

    public function createCheckAllActiveTask(ProductSetting $productSetting)
    {
        dd("Stop Restart");
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        $filePath = storage_path('leading-lady/products/check-all-active/') . 'll-shopify-error.csv';
        $shopifySkus = ExcelController::view($filePath);

        $filePath = resource_path('assets/leading-lady/gmail/ll-missing-sku.csv');
        $activeSkus = ExcelController::view($filePath);

        $searchSkus = array_column($activeSkus, 'active_upc_sku');

        $tests = [];

        foreach ($shopifySkus as $shopifySku) {
            $key = array_search($shopifySku['upc'], $searchSkus);
            if (is_numeric($key)) {
                
            } else {
                echo $key;
                echo "<br/>";
                $tests[] = $shopifySku['upc'];
            }
        }
        dd($tests);
        dd($array, $unique);
        $result = array_count_values($array);
        dd($result);

        $storePath = 'leading-lady/products/check-all-active';

        $fileName = 'll-shopify-error';
        ExcelController::create($fileName, $variantAreNotFounds, $storePath)->download('csv');
        dd("done");
    }

    public function createMOLDEDProduct(ProductSetting $productSetting)
    {
        \Log::info(["Script Start"]);
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $fileName = 'll-product-insert-part-2.csv';
        $filePath = storage_path('leading-lady/products/insert-product/') . $fileName;
        $exportProducts = ExcelController::view($filePath);
//        dd($exportProducts);
        $exportProducts = array_slice($exportProducts, 1);
        foreach ($exportProducts as $exportProduct) {
            \Log::info(["product_sku" => $exportProduct['variant_sku']]);
            $productOptions = [];
            $productOption = [];
            $productOption['name'] = 'Color';
            $productOptions[] = $productOption;
            $productOption = [];
            $productOption['name'] = 'Size';
            $productOptions[] = $productOption;

            $variants = [];
            $productVariant = [];
            $productVariant['option1'] = $exportProduct['option1_value'];
            $productVariant['option2'] = $exportProduct['option2_value'];

            $productVariant['sku'] = $exportProduct['variant_sku'];
            $productVariant['grams'] = $exportProduct['variant_grams'];

            $productVariant['inventory_quantity'] = $exportProduct['variant_inventory_qty'];
            $productVariant['inventory_management'] = 'shopify';
            $productVariant['inventory_policy'] = 'deny';
            $productVariant['fulfillment_service'] = 'manual';

            $productVariant['price'] = $exportProduct['variant_price'];
            $productVariant['compare_at_price'] = $exportProduct['variant_compare_at_price'];
            $productVariant['requires_shipping'] = 'true';

            $variants[] = $productVariant;

            $shopifyProduct = [];
            $shopifyProduct['handle'] = $exportProduct['handle'];
            $shopifyProduct['title'] = $exportProduct['title'];
            $shopifyProduct['vendor'] = $exportProduct['vendor'];

            $shopifyProduct['published'] = 'true';
            $shopifyProduct['variants'] = $variants;
            $shopifyProduct['options'] = $productOptions;

            $shopifyProduct['tags'] = $exportProduct['tags'];
//            $shopifyProduct['product_type'] = '';
            $shopifyProduct['product_type'] = 'sub_product';
            $shopifyProduct['images'] = [];

            $shopifyProduct['metafields_global_title_tag'] = $exportProduct['seo_title'];
            $shopifyProduct['metafields_global_description_tag'] = $exportProduct['seo_description'];

            $shopifyProduct['metafields'] = $this->getMetaFields($exportProducts);

            $params = [
                'product' => $shopifyProduct
            ];
            $url = 'admin/products.json';

            $result = $this->shopifyRequest->create($url, $params);
            if (!isset($result['product'])) {
                sleep(2);
                $result = $this->shopifyRequest->create($url, $params);
                if (!isset($result['product'])) {
                    \Log::info(["product_error" => $result]);
                } else {
                    \Log::info(["product_success" => $result['product']['id']]);
                }
            } else {
                \Log::info(["product_success" => $result['product']['id']]);
            }
            sleep(1);
        }
        \Log::info(["Script End"]);
    }

    public function createLaceProduct(ProductSetting $productSetting)
    {
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $fileName = 'll-product-insert-part-1.csv';
        $filePath = storage_path('leading-lady/products/insert-product/') . $fileName;
        $exportProducts = ExcelController::view($filePath);

        $productImages = $variants = [];
        foreach ($exportProducts as $exportProduct) {
            $productVariant = [];
            $productVariant['option1'] = $exportProduct['option1_value'];
            $productVariant['option2'] = $exportProduct['option2_value'];


            $productVariant['sku'] = $exportProduct['variant_sku'];
            $productVariant['grams'] = $exportProduct['variant_grams'];

            $productVariant['inventory_quantity'] = $exportProduct['variant_inventory_qty'];
            $productVariant['inventory_management'] = 'shopify';
            $productVariant['inventory_policy'] = 'deny';
            $productVariant['fulfillment_service'] = 'manual';

            $productVariant['price'] = $exportProduct['variant_price'];
            $productVariant['compare_at_price'] = $exportProduct['variant_compare_at_price'];
            $productVariant['requires_shipping'] = 'true';

            $variants[] = $productVariant;

//            if ($exportProduct['variant_image']) {
//                $productImage = [];
//                $image['src'] = $exportProduct['variant_image'];
//                $image['alt'] = '';
//                $productImages[] = $productImage;
//            }
        }

        $productOptions = [];
        $productOption = [];
        $productOption['name'] = 'Color';
        $productOptions[] = $productOption;
        $productOption = [];
        $productOption['name'] = 'Size';
        $productOptions[] = $productOption;

        $shopifyProduct = [];
        $shopifyProduct['handle'] = $exportProducts[0]['handle'];
        $shopifyProduct['title'] = $exportProducts[0]['title'];
        $shopifyProduct['vendor'] = $exportProducts[0]['vendor'];

        $shopifyProduct['published'] = 'true';
        $shopifyProduct['variants'] = $variants;
        $shopifyProduct['options'] = $productOptions;

        $shopifyProduct['tags'] = $exportProducts[0]['tags'];
        $shopifyProduct['product_type'] = '';
        $shopifyProduct['images'] = $productImages;

        $shopifyProduct['metafields_global_title_tag'] = $exportProducts[0]['seo_title'];
        $shopifyProduct['metafields_global_description_tag'] = $exportProducts[0]['seo_description'];

        $shopifyProduct['metafields'] = $this->getMetaFields($exportProducts);

        $url = 'admin/products.json';
        $params = [
            'product' => $shopifyProduct
        ];
        $result = $this->shopifyRequest->create($url, $params);
        dd($result);
        $storePath = 'leading-lady/products/insert-product';

        $fileName = 'll-product-insert';
        ExcelController::create($fileName, $insertProducts, $storePath);
        dd("done");
    }

    public function getMetaFields($exportProducts)
    {
        $productMetaFields = [];
//        $metaField = [];
//        $metaField['key'] = 'style';
//        $metaField['value'] = 5042;
//        $metaField['value_type'] = 'string';
//        $metaField['namespace'] = 'product_manager';
//        $productMetaFields[] = $metaField;
//        $metaField = [];
//        $metaField['key'] = 'description';
//        $metaField['value'] = '-';
//        $metaField['value_type'] = 'string';
//        $metaField['namespace'] = 'product_manager';
//        $productMetaFields[] = $metaField;
//        $metaField = [];
//        $metaField['key'] = 'product_details';
//        $metaField['value'] = '-';
//        $metaField['value_type'] = 'string';
//        $metaField['namespace'] = 'product_manager';
//        $productMetaFields[] = $metaField;
//        $metaField = [];
//        $metaField['key'] = 'quote';
//        $metaField['value'] = '-';
//        $metaField['value_type'] = 'string';
//        $metaField['namespace'] = 'product_manager';
//        $productMetaFields[] = $metaField;
//        $metaField = [];
//        $metaField['key'] = 'fabric';
//        $metaField['value'] = '-';
//        $metaField['value_type'] = 'string';
//        $metaField['namespace'] = 'product_manager';
//        $productMetaFields[] = $metaField;

        $metaField = [];
        $metaField['key'] = 'main_product_handle';
        $metaField['value'] = 'molded-padded-softcup-bra';
        $metaField['value_type'] = 'string';
        $metaField['namespace'] = 'product_manager';
        $productMetaFields[] = $metaField;

//        $metaField = [];
//        $metaField['key'] = 'variant_count';
//        $metaField['value'] = 110;
//        $metaField['value_type'] = 'integer';
//        $metaField['namespace'] = 'product_manager';
//        $productMetaFields[] = $metaField;

        return $productMetaFields;
    }

    public function updateVariants(ProductSetting $productSetting)
    {
        \Log::info(["Script Start"]);
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $fileName = 'll-active-update.csv';
        $filePath = resource_path('assets/leading-lady/products/') . $fileName;
        $activeProductSkus = ExcelController::view($filePath);

        $affectedProducts = $subProducts = $productNotFounds = $variantsNotFoundInDB = [];

        foreach ($activeProductSkus as $activeProductSku) {
            \Log::info(["product_variant_sku" => $activeProductSku['active_upc_sku']]);

            $activeSku = $activeProductSku['active_upc_sku'];
            $variant = \DB::table('leadinglady_product_variants')->where('sku', $activeSku)->first();
            if ($variant) {

                //update product tag and metafields
                $url = '/admin/products/' . $variant->product_id . '.json';
                $result = $this->shopifyRequest->view($url);
                if (!isset($result['product'])) {
                    sleep(2);
                    $result = $this->shopifyRequest->view($url);
                }

                if (isset($result['product'])) {

                    $product = $result['product'];
                    if ($product['product_type'] != 'sub_product') {

                        $affectedProduct = [];
                        $affectedProduct['product_id'] = $variant->product_id;
                        $affectedProduct['variant_id'] = $variant->variant_id;
                        $affectedProduct['product'] = json_encode($product);
                        $affectedProducts[] = $affectedProduct;

                        if ($activeProductSku['add_tags']) {
                            $existingTags = $product['tags'];
                            $existingTags = explode(',', $existingTags);
                            $existingTags = array_map('trim', $existingTags);

                            $newTags = explode(',', $activeProductSku['add_tags']);
                            $newTags = array_map('trim', $newTags);
                            $updatedTags = array_merge($existingTags, $newTags);

                            $updatedProduct = [];
                            $updatedProduct['id'] = $product['id'];
                            $updatedProduct['tags'] = $updatedTags;
                            $params = [
                                'product' => $updatedProduct
                            ];
                            $url = '/admin/products/' . $product['id'] . '.json';
                            $result = $this->shopifyRequest->update($url, $params);
                            if (!isset($result['product'])) {
                                sleep(2);
                                $result = $this->shopifyRequest->update($url, $params);
                                if (!isset($result['product'])) {
                                    \Log::info(["product_update_error" => $result]);
                                }
                            }
                            sleep(1);
                        }

                        $metaField = [];
                        $metaField['key'] = 'style';
                        $metaField['value'] = $activeProductSku['style'];
                        $metaField['value_type'] = 'string';
                        $metaField['namespace'] = 'product_manager';
                        $url = '/admin/products/' . $product['id'] . '/metafields.json';
                        $params = [
                            'metafield' => $metaField
                        ];

                        $result = $this->shopifyRequest->create($url, $params);
                        if (!isset($result['metafield'])) {
                            sleep(2);
                            $result = $this->shopifyRequest->create($url, $params);
                            if (!isset($result['metafield'])) {
                                \Log::info(["product_metafield_error" => $result]);
                            }
                        }
                        sleep(1);
                    } else {
                        $subProduct = [];
                        $subProduct = $activeProductSku;
                        $subProduct['product_id'] = $product['id'];
                        $subProduct['product_title'] = $product['title'];
                        $subProduct['existing_tags'] = $product['tags'];
                        $subProduct['product_json'] = json_encode($product);
                        $subProducts[] = $subProduct;
                    }
                } else {
                    $productNotFound = [];
                    $productNotFound = $activeProductSku;
                    $productNotFound['product_id'] = $variant->product_id;
                    $productNotFound['variant_id'] = $variant->variant_id;
                    $productNotFound['error'] = 'not-found-in-shopify';
                    $productNotFounds[] = $productNotFound;
                }

                //update product variants
                $updateVariant = [];
                $updateVariant['id'] = $variant->variant_id;
                $updateVariant['price'] = $activeProductSku['price'];
                $updateVariant['compare_at_price'] = $activeProductSku['compare_at_price'];

                $url = 'admin/variants/' . $variant->variant_id . '.json';
                $params = [
                    'variant' => $updateVariant
                ];

                $result = $this->shopifyRequest->update($url, $params);
                if (!isset($result['variant'])) {
                    sleep(2);
                    $result = $this->shopifyRequest->update($url, $params);
                    if (!isset($result['variant'])) {
                        \Log::info(["product_variant_error" => $result]);
                    }
                }
                sleep(1);
            } else {
                $variantNotFoundInDB = [];
                $variantNotFoundInDB = $activeProductSku;
                $variantNotFoundInDB['error'] = 'not-found-in-db';
                $variantsNotFoundInDB[] = $variantNotFoundInDB;
            }
        }

        \Log::info(["Generate Csv"]);

        $storePath = 'leading-lady/products/variant-update';

        $fileName = 'll-product-update';
        ExcelController::create($fileName, $affectedProducts, $storePath);

        $fileName = 'll-sub-product';
        ExcelController::create($fileName, $subProducts, $storePath);

        $fileName = 'll-products-not-found-shopify';
        ExcelController::create($fileName, $productNotFounds, $storePath);

        $fileName = 'll-variant-not-found-db';
        ExcelController::create($fileName, $variantsNotFoundInDB, $storePath);

        \Log::info(["Script End"]);
    }

    public function deleteVariants(ProductSetting $productSetting)
    {
        \Log::info(["Script Start"]);
        $fileName = 'll-hidden-variants.csv';
        $filePath = resource_path('assets/leading-lady/products/') . $fileName;
//        dd($filePath);
        $hideProductSkus = ExcelController::view($filePath);
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        $affectedProducts = $proNotFounds = $variantNotFounds = [];

        foreach ($hideProductSkus as $hideProductSku) {
            \Log::info(['product_variant_sku' => $hideProductSku['variant_sku_website']]);

            $deleteSku = $hideProductSku['variant_sku_website'];
            $variant = \DB::table('leadinglady_product_variants')->where('sku', $deleteSku)->first();

            if ($variant) {
                $url = '/admin/products/' . $variant->product_id . '.json';
                $result = $this->shopifyRequest->view($url);
                if (!isset($result['product'])) {
                    sleep(2);
                    $result = $this->shopifyRequest->view($url);
                }

                if (isset($result['product'])) {
                    sleep(1);
                    $product = $result['product'];
                    $affectedProduct = [];
                    $affectedProduct['product_id'] = $variant->product_id;
                    $affectedProduct['variant_id'] = $variant->variant_id;
                    $affectedProduct['product'] = json_encode($product);
                    $affectedProducts[] = $affectedProduct;

                    if (count($product['variants']) == 1) {
                        $url = '/admin/products/' . $variant->product_id . '.json';
                    } else {
                        $url = '/admin/variants/' . $variant->variant_id . '.json';
                    }

                    $result = $this->shopifyRequest->delete($url);
                    if (is_string($result)) {
                        sleep(2);
                        $result = $this->shopifyRequest->delete($url);
                        if (is_string($result)) {
                            \Log::info(['product_delete_error' => $result]);
                        }
                    }
                } else {
                    $proNotFound = [];
                    $proNotFound['product_id'] = $variant->product_id;
                    $proNotFound['variant_id'] = $variant->variant_id;
                    $proNotFounds[] = $proNotFound;
                }
            } else {
                $variantNotFound = [];
                $variantNotFound['product_sku'] = $hideProductSku['variant_sku_website'];
                $variantNotFounds[] = $variantNotFound;
            }
        }

        \Log::info("Generate Csv");

        $storePath = 'leading-lady/products/deleted';

        $fileName = 'll-delete-sku-products';
        ExcelController::create($fileName, $affectedProducts, $storePath);

        $fileName = 'll-products-not-found';
        ExcelController::create($fileName, $proNotFounds, $storePath);

        $fileName = 'll-sku-not-found';
        ExcelController::create($fileName, $variantNotFounds, $storePath);

        \Log::info(["Script End"]);
    }

    public function deleteSaleTags(ProductSetting $productSetting)
    {
//        dd("create products");
        \Log::info(["Script Start"]);
        $project = $productSetting->project;
        $this->magentoRequest = new MagentoController($project);
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        $mainProducts = $this->getMainProducts();
//        dd($mainProducts);
        $shopifyUpdateProducts = [];

        foreach ($mainProducts as $mainProduct) {
            \Log::info(['product_id' => $mainProduct['id']]);

            $tags = $mainProduct['tags'];
            $tags = explode(',', $tags);
            $tags = array_map('trim', $tags);

            //remove sale tag
            $saleKey = array_search('sale', $tags);
            if (is_numeric($saleKey)) {
                unset($tags[$saleKey]);
            }

            //remove clearance tag
            $clearanceKey = array_search('clearance', $tags);
            if (is_numeric($clearanceKey)) {
                unset($tags[$clearanceKey]);
            }

            $tags = implode(',', $tags);

            if (is_numeric($saleKey) || is_numeric($clearanceKey)) {
                $updateProduct = [];
                $updateProduct['id'] = $mainProduct['id'];
                $updateProduct['tags'] = $tags;

                $url = 'admin/products/' . $mainProduct['id'] . '.json';
                $params = [
                    'product' => $updateProduct
                ];

                $result = $this->shopifyRequest->update($url, $params);
                if (!isset($result['product'])) {
                    sleep(2);
                    $result = $this->shopifyRequest->update($url, $params);
                    if (!isset($result['product'])) {
                        \Log::info(['product_update_error' => $result]);
                    }
                }

                if (isset($result['product'])) {

                    $shopifyUpdateProduct = [];
                    $shopifyUpdateProduct['product_id'] = $mainProduct['id'];
                    $shopifyUpdateProduct['tags'] = $mainProduct['tags'];
                    $shopifyUpdateProduct['update_tags'] = $tags;
                    $shopifyUpdateProducts[] = $shopifyUpdateProduct;
                }
                sleep(1);
            }
        }

        \Log::info("Generate Csv");

        $fileName = 'll-update-tag-products';
        $storePath = 'leading-lady/products/updates';
        ExcelController::create($fileName, $shopifyUpdateProducts, $storePath);

        \Log::info(["Script End"]);
    }

    public function getMainProducts()
    {
        $url = 'admin/products.json';
        $params = [
            'limit' => 250,
            'collection_id' => 59263877162,
            'fields' => 'tags, id'
        ];
        $result = $this->shopifyRequest->view($url, $params);
        return $result['products'];
    }
}
