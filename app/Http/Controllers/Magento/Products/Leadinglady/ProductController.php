<?php

namespace App\Http\Controllers\Magento\Products\Leadinglady;

use App\Models\ProjectProduct;
use GuzzleHttp\Client;
use App\Http\Controllers\Core\ExcelController;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectProductSetting as ProductSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public $magentoRequest, $shopifyRequest;

    public function __construct()
    {
        set_time_limit(0);
        //https://leading-lady-inc.myshopify.com/admin/products.json?collection_id=59603386410&limit=250
    }

    public function create81variantProduct(ProductSetting $productSetting)
    {
        dd("shopify create");
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        $url = 'admin/products.json';
        $params = [
            'collection_id' => '59603386410',
            'limit' => 250
        ];
        $result = $this->shopifyRequest->view($url, $params);
        $products = $result['products'];
        foreach ($products as $product) {
            $url = 'admin/products/' . $product['id'] . '.json';
            $result = $this->shopifyRequest->delete($url);
            if (is_string($result)) {
                echo $product['id'];
                echo "<br/>";
                echo $result;
                echo "<br/>";
            }
            sleep(1);
        }

        dd("stop");

        //add original variant
        $project = $productSetting->project;
        $this->magentoRequest = new MagentoController($project);
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $options = [];
        $options['from'] = 0;
        $options['size'] = 1;
        $options['search'] = [['row_id', '=', '5798']];
        $migrateProducts = $this->magentoRequest->getProducts($options);
//        dd($migrateProducts);
        foreach ($migrateProducts as $migrateProduct) {
            $shopifyProduct = $this->generateShopifyProductObject($migrateProduct);
//            dd($shopifyProduct);
            $variants = $shopifyProduct['variants'];
            foreach ($variants as $variant) {
                $url = 'admin/products/1417753362474/variants.json';
                $params = [
                    'variant' => $variant
                ];
                $result = $this->shopifyRequest->create($url, $params);
                sleep(1);
            }

            dd("Stop");
        }
        dd("done");

        $dontDeletes = [
            '1417758081066',
            '1417762308138',
            '1417768927274',
            '1417774694442',
            '1417784459306',
            '1417793601578',
            '1417801105450',
            '1417807527978',
            '1418024222762',
            '1418030940202',
            '1418036838442',
            '1418043785258'
        ];

        foreach ($dontDeletes as $dontDelete) {
            $url = 'admin/products/' . $dontDelete . '.json';
            $result = $this->shopifyRequest->view($url);
            $product = $result['product'];
            $variants = $product['variants'];

            foreach ($variants as $variant) {
                unset($variant['id']);
                unset($variant['product_id']);
                unset($variant['position']);
                unset($variant['created_at']);
                unset($variant['updated_at']);
                unset($variant['image_id']);
                unset($variant['inventory_item_id']);

                $variant['inventory_policy'] = 'deny';
                $variant['inventory_management'] = 'shopify';


                $url = 'admin/products/1417753362474/variants.json';
                $params = [
                    'variant' => $variant
                ];
                $result = $this->shopifyRequest->create($url, $params);
                sleep(1);
            }
        }
        //missing sku - 722286480993, 722286480856
    }

    public function updateSubProudctImage(ProductSetting $productSetting)
    {
        //1420987236394 not image sub product

        \Log::info("script start");
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $products = $this->getSubProducts();
        foreach ($products as $product) {

            $variants = $product['variants'];
            
            foreach ($variants as $variant) {
                $handle = $mainProduct = FALSE;
                if (!$variant['image_id']) {

                    $tags = explode(',', $product['tags']);
                    foreach ($tags as $tag) {
                        if ($tag != "sub_product") {
                            $handle = $tag;
                        }
                    }

                    if ($handle) {
                        sleep(1);
                        $url = '/admin/products.json';
                        $params = [
                            'handle' => $handle
                        ];
                        $result = $this->shopifyRequest->view($url, $params);
                        if ($result['products']) {
                            $mainProduct = $result['products'][0];
                        }
                    }

                    $productImage = [];
                    $productImage['main_product_id'] = ($mainProduct) ? $mainProduct['id'] : '-';
                    $productImage['main_product_title'] = ($mainProduct) ? $mainProduct['title'] : '-';
                    $productImage['sub_product_id'] = $product['id'];
                    $productImage['sub_product_title'] = $product['title'];
                    $productImage['variant_id'] = $variant['id'];
                    $productImage['color_option'] = $variant['option1'];
                    $productImage['published'] = ($product['published_at']) ? 'true' : 'false';
                    $productImages[] = $productImage;
                }
            }
        }

        \Log::info("generate sheet");

        $fileName = 'product-variant-image';
        $data = $productImages;
        $storePath = 'leading-lady/images';
        ExcelController::create($fileName, $data, $storePath);
        \Log::info("script end");
        dd("completed");
    }

    public function getSubProducts()
    {
        $subProducts = [];
        for ($i = 1; $i <= 25; $i++) {
            $url = 'admin/products.json';
            $params = [
                'collection_id' => 59397046314,
                'limit' => 250,
                'page' => $i
            ];
            $result = $this->shopifyRequest->view($url, $params);

            if ($result['products']) {
                $subProducts = array_merge($subProducts, $result['products']);
            } else {
                break;
            }
            sleep(1);
        }

        return $subProducts;
    }

    public function createSeoMetaFields(ProductSetting $productSetting)
    {
        dd("create product");
        \Log::info("script start");

        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        $products = $this->getMainProducts();
        foreach ($products as $product) {
            \Log::info(["product_id" => $product['id']]);

            $url = 'admin/products/' . $product['id'] . '/metafields.json';
            $metaFields = $this->shopifyRequest->view($url);
            $metaFields = $metaFields['metafields'];
            sleep(1);

            foreach ($metaFields as $metaField) {

                if ($metaField['key'] == 'title_tag') {
                    $shopifyMetaField = [];
                    $shopifyMetaField['namespace'] = 'product_manager';
                    $shopifyMetaField['key'] = 'seo_title';
                    $shopifyMetaField['value'] = $metaField['value'];
                    $shopifyMetaField['value_type'] = 'string';
                    $params = [
                        'metafield' => $shopifyMetaField
                    ];
                    $url = 'admin/products/' . $product['id'] . '/metafields.json';
                    $this->shopifyRequest->create($url, $params);
                    sleep(1);
                }

                if ($metaField['key'] == 'description_tag') {
                    $shopifyMetaField = [];
                    $shopifyMetaField['namespace'] = 'product_manager';
                    $shopifyMetaField['key'] = 'seo_description';
                    $shopifyMetaField['value'] = $metaField['value'];
                    $shopifyMetaField['value_type'] = 'string';
                    $params = [
                        'metafield' => $shopifyMetaField
                    ];
                    $url = 'admin/products/' . $product['id'] . '/metafields.json';
                    $this->shopifyRequest->create($url, $params);

                    sleep(1);
                }
            }
        }
        \Log::info("script end");
        dd("completed");
    }

    public function getMainProducts()
    {
        $url = 'admin/products.json';
        $params = [
            'collection_id' => 59263877162,
            'limit' => 250
        ];
        $result = $this->shopifyRequest->view($url, $params);
        return $result['products'];
    }

    public function createNewProducts(ProductSetting $productSetting)
    {
//        dd("shopify create ");
        $project = $productSetting->project;
        $this->magentoRequest = new MagentoController($project);

        //demo store
        $data = [];
        $data['api_key'] = 'c88cad54a110a2b52b657819d2aeb737';
        $data['api_password'] = '9fa89918e2508856c2be712416ad357b';
        $data['api_url'] = 'https://demo-pia.myshopify.com';

        $this->shopifyRequest = new ExternalApiRequestController($data);
        $options = [];
        $options['from'] = 0;
        $options['size'] = 1;
        $options['search'] = [['row_id', '=', '5798']];
        $migrateProducts = $this->magentoRequest->getProducts($options);
//        dd($migrateProducts);
        foreach ($migrateProducts as $migrateProduct) {
            $shopifyProduct = $this->generateShopifyProductObject($migrateProduct);
//            dd($shopifyProduct);
            $url = 'admin/products.json';
            $params = [
                'product' => $shopifyProduct
            ];
            $result = $this->shopifyRequest->create($url, $params);
            if (!isset($result['product'])) {
                dd($result, $shopifyProduct);
            }
            dd("Stop");
        }
        dd("done");

        $dontDelete = [
            '1417758081066',
            '1417762308138',
            '1417768927274',
            '1417774694442',
            '1417784459306',
            '1417793601578',
            '1417801105450',
            '1417807527978',
            '1418024222762',
            '1418030940202',
            '1418036838442',
            '1418043785258'
        ];
        //missing sku - 722286480993, 722286480856
    }

    public function generateShopifyProductObject($migrateProduct)
    {
        //shopify product object

        $visible = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'status'); //status

        $shopifyProduct = [];
        $shopifyProduct['handle'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'url_key'); //url_key => 86
        $shopifyProduct['title'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'name');  //name => 60        
//        dd($shopifyProduct);
//        $shopifyProduct['body_html'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'description'); //description => 61 //short_description => 62
        $shopifyProduct['vendor'] = '';


        $shopifyProduct['published'] = ($visible == 1) ? 'true' : 'false';
        $shopifyProduct['variants'] = $this->magentoRequest->generateProductVariants($migrateProduct);
        $shopifyProduct['options'] = $this->magentoRequest->generateProductOptions(array_first($shopifyProduct['variants']));

        $shopifyProduct['tags'] = $this->magentoRequest->generateProductTags($migrateProduct, $shopifyProduct['variants']);
        $shopifyProduct['product_type'] = (in_array('not-visible-individually', $shopifyProduct['tags'])) ? 'not-visible-individually' : '';
        $shopifyProduct['images'] = $this->magentoRequest->generateProductImages($migrateProduct);

        $shopifyProduct['metafields_global_title_tag'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'meta_title'); //meta_title = 71
        $shopifyProduct['metafields_global_description_tag'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'meta_description'); //meta_description = 73

        $shopifyProduct['metafields'] = $this->magentoRequest->getProductMetaField($migrateProduct, $shopifyProduct);

        return $shopifyProduct;
    }

    public function createTemp(ProductSetting $productSetting)
    {
        dd("shopify create temp");
        $project = $productSetting->project;
        $this->magentoRequest = new MagentoController($project);
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $url = 'https://leading-lady-inc.myshopify.com/admin/products.json';
        $params = [
            'collection_id' => 59603386410,
            'limit' => 250
        ];
        $result = $this->shopifyRequest->view($url, $params);
        $products = $result['products'];
        $invalidProductIds = [];
        foreach ($products as $product) {

            if (count($product['options']) < 2) {
//                $invalidProduct = [];
//                $invalidProduct['id'] = $product['id'];
//                $invalidProduct['option'] = $product['options'];
                $invalidProductIds[] = $product['id'];
            }
        }

        $migrateProducts = \DB::table('product_relations')->where('main_product_id', 1417753362474)->pluck('product_id')->toArray();
        $diff = array_diff($migrateProducts, $invalidProductIds);
        dd($diff, $invalidProductIds, $migrateProducts);
    }

    public function update(ProductSetting $productSetting)
    {
        dd("update variants changes");
//        check
        //12730995277866 inventory 3
//        12730995310634 => 9
        \Log::info("script start");
        $project = $productSetting->project;
        $this->magentoRequest = new MagentoController($project);
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        for ($i = 1; $i <= 35; $i++) {
            \Log::info(["page_id" => $i]);
            $url = 'admin/variants.json';
            $params = [
                'limit' => 250,
                'page' => $i
            ];

            $result = $this->shopifyRequest->view($url, $params);
            $variants = $result['variants'];
//            dd($variants);
            if ($variants) {
                foreach ($variants as $variant) {
                    \Log::info(['variant_id' => $variant['id']]);
                    $newQuantity = $this->magentoRequest->getMagentoProductQuantity($variant);
                    $newWeight = $this->magentoRequest->getWeightOfProduct($variant);

                    $updateVariant = [];
                    $updateVariant['id'] = $variant['id'];
                    $updateVariant['inventory_quantity'] = $newQuantity;
                    $updateVariant['inventory_policy'] = 'deny';
                    $updateVariant['inventory_management'] = 'shopify';
                    $updateVariant['weight'] = $newWeight;

                    $params = [
                        'variant' => $updateVariant
                    ];
                    $url = 'admin/variants/' . $variant['id'] . '.json';
                    $result = $this->shopifyRequest->update($url, $params);
                    if (is_string($result)) {
                        \Log::info(['variant_error_id' => $result]);
                    }
                    sleep(1);
                }
            } else {
                break;
            }

            sleep(10);
        }
        \Log::info(["last_page_id" => $i]);
        \Log::info("script end");
        dd("completed all variants");
    }
}
