<?php

namespace App\Http\Controllers\Magento\Products\Leadinglady;

use App\Models\ProjectProduct;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectProductSetting as ProductSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductControllerBackUp extends Controller
{
    public $magentoRequest, $shopifyRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function update()
    {
        $options = $this->magentoRequest->getAttributeOptions();
        $shopifyOptions = [];
        foreach ($options as $option) {
            $shopifyOption = [];
            $shopifyOption['option'] = $option->value;
            $shopifyOptions[] = $shopifyOption;
        }
        $fileName = 'leading-lady-variants';
        $storePath = '/leading-lady/speacial-products';
        \App\Http\Controllers\Core\ExcelController::create($fileName, $shopifyOptions, $storePath)->download('csv');
        dd("Stop");
    }

    public function create(ProductSetting $productSetting)
    {
        //leading lady migration
//        dd("stop restart");
        \Log::info("Script Start");
//        $schedule = $productSetting->schedule;
//        $schedule->status = 'inprogress';
//        $schedule->save();        
//        sleep(10);

        $project = $productSetting->project;
        $this->magentoRequest = new MagentoController($project);

        $lastProduct = $productSetting->products()->orderBy('extra->product_id', 'desc')->first();

        $options = [];
        $options['from'] = 0;
        $options['size'] = 1;
        if ($lastProduct) {
//            $options['search'] = [['row_id', '>', $lastProduct->extra['product_id']]];
            $options['search'] = [['row_id', '>', 35]];
        }

        $migrateProducts = $this->magentoRequest->getProducts($options);

        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        foreach ($migrateProducts as $migrateProduct) {
            \Log::info(['product_id' => $migrateProduct->row_id]);
            $isBaseProduct = $this->magentoRequest->isBaseProduct($migrateProduct);
            if (!$isBaseProduct) {
                $migrateProduct = $this->getBaseProduct($migrateProduct, $options);
//                dd($migrateProduct);
                $stopVariable = TRUE;
                if ($migrateProduct) {
                    \Log::info(['stop_product_id' => $migrateProduct->row_id]);
                    $isBaseProduct = TRUE;
                }
            }

            if ($isBaseProduct) {
                $shopifyProduct = [];
                $shopifyProduct = $this->generateShopifyProductObject($migrateProduct);
//                dd($shopifyProduct);
                $testVariant = array_first($shopifyProduct['variants']);

                if (!isset($testVariant['option1'])) {

                    if (!isset($testVariant['option1']) && !isset($testVariant['option2'])) {
                        
                    } else {

                        foreach ($shopifyProduct['variants'] as $key => $var) {

                            if (!$var['option1']) {
                                $shopifyProduct['variants'][$key]['option1'] = $shopifyProduct['variants'][$key]['option2'];
                                unset($shopifyProduct['variants'][$key]['option2']);
                            } else {
                                \Log::info("Stop Script wirh variants error");
                                dd("something went wrong in variants", $shopifyProduct['variants']);
                            }
                        }
                    }
                }
//                dd($shopifyProduct);

                $shopifyProductImages = $shopifyProduct['images'];
                unset($shopifyProduct['images']);

                if (count($shopifyProduct['variants']) > 100) {
                    $variants = $shopifyProduct['variants'];

                    $result = $this->setImagesIntoVariant($variants, $shopifyProductImages);
                    $mainImages = $result['main_image'];
                    $variants = $result['variants'];
                    foreach ($variants as $key => $variant) {
//                        \Log::info(['variant_id' => $variant['sku']]);
                        if ($key == 0) {
                            $shopifyMainProduct = $this->generateMainProduct($shopifyProduct);
                            $shopifyMainProduct['images'] = $mainImages;
                            $mainProduct = $this->insertProduct($shopifyMainProduct, $migrateProduct, $productSetting);
                            $this->updateVariantImage($variant, $mainProduct);
                            continue;
                        }

                        $variantProduct = $this->generateVariantProductObject($mainProduct, $variant);
                        $variantProduct = $this->insertProduct($variantProduct, $migrateProduct, $productSetting);
                        $variantProduct['main_product'] = $mainProduct;
                        $this->updateVariantImage($variant, $variantProduct);
                    }
                    \Log::info("Script End with 100+ variants");
                    dd("stop this script");
                    continue;
                }
                $shopifyProduct = $this->insertProduct($shopifyProduct, $migrateProduct, $productSetting);
                $this->updateAllVariantImage($shopifyProductImages, $shopifyProduct);
            }

            if (isset($stopVariable)) {
                \Log::info("Script End with stopVariable");
                dd("stop script with stopVariable");
            }
        }

//        sleep(60);

        if ($migrateProducts) {
//            $schedule->status = 'paused';
//            $schedule->save();
        } else {
//            $schedule->status = 'completed';
//            $schedule->save();
            \Log::info("migration completed");
            dd("complete migration");
        }
        \Log::info("Script End");
    }

    public function getBaseProduct($migrateProduct, $options)
    {
        for ($i = 1; $i <= 1000; $i++) {
            $options['search'] = [['row_id', '>', $migrateProduct->row_id]];

            $migrateProducts = $this->magentoRequest->getProducts($options);
            if (!$migrateProducts) {
                return NULL;
            }
            $migrateProduct = $migrateProducts[0];

            $isBaseProduct = $this->magentoRequest->isBaseProduct($migrateProduct);

            if ($isBaseProduct) {
                return $migrateProduct;
            }
        }
    }

    public function setImagesIntoVariant($variants, $shopifyProductImages)
    {
        $productImages = [];
        $colors = array_map('strtolower', array_column($variants, 'option1'));
        foreach ($shopifyProductImages as $shopifyProductImage) {
            $alt = $shopifyProductImage['alt'];
            $alt = substr($alt, strpos($alt, "Bra ") + 4);

            if (str_contains($alt, '(')) {
                $alt = trim(substr($alt, 0, strpos($alt, "(")));
            }

            $variantkeys = array_keys($colors, strtolower($alt));
            foreach ($variantkeys as $variantkey) {

                $variants[$variantkey]['variant_image'] = [];
                $productImage = [];
                $productImage['src'] = $shopifyProductImage['src'];
                $productImage['alt'] = $shopifyProductImage['alt'];
                $variants[$variantkey]['variant_image'] = $productImage;
            }

            if (!$variantkeys) {
                $productImage = [];
                $productImage['src'] = $shopifyProductImage['src'];
                $productImage['alt'] = $shopifyProductImage['alt'];
                $productImages[] = $productImage;
            }
        }
        $result = [];
        $result['main_image'] = $productImages;
        $result['variants'] = $variants;

        return $result;
    }

    public function insertProduct($shopifyProduct, $migrateProduct, $productSetting)
    {
        $shopifyProduct = $this->insertProductUsingShopifyApi($shopifyProduct);
        $this->insertProductIntoDatabase($shopifyProduct, $migrateProduct, $productSetting);
        return $shopifyProduct;
    }

    public function updateAllVariantImage($shopifyProductImages, $shopifyProduct)
    {
        $variants = $shopifyProduct['variants'];
        $variantColors = array_map('strtolower', array_column($variants, 'option1'));
        $colors = array_unique($variantColors);

        foreach ($shopifyProductImages as $shopifyProductImage) {
            $alt = $shopifyProductImage['alt'];
            $variantIds = [];

            $validColors = [];
            foreach ($colors as $color) {
                if (str_contains(strtolower($alt), $color)) {
                    $validColors[] = $color;
                }
            }

            foreach ($validColors as $validColor) {
                $variantkeys = array_keys($variantColors, $validColor);
                foreach ($variantkeys as $variantkey) {
                    $variantIds[] = $variants[$variantkey]['id'];
                }
            }

            //product image object
            $productImage = [];
            $productImage['src'] = $shopifyProductImage['src'];
            $productImage['alt'] = $shopifyProductImage['alt'];
            $productImage['variant_ids'] = $variantIds;

            $url = '/admin/products/' . $shopifyProduct['id'] . '/images.json';
            $params = [
                'image' => $productImage
            ];

            $updateProduct = $this->shopifyRequest->create($url, $params);
            if (is_string($updateProduct)) {
                sleep(1);
                \Log::info(["second time" => $shopifyProduct['id'], $updateProduct, $productImage]);
                $updateProduct = $this->shopifyRequest->create($url, $params);
            }
        }
    }

    public function updateVariantImage($variant, $variantProduct)
    {
        if (isset($variant['variant_image'])) {
            $shopifyVariants = $variantProduct['variants'];
            $shopifyProductImage = $variant['variant_image'];
            foreach ($shopifyVariants as $shopifyVariant) {

                $variantIds = [];
                $variantIds[] = $shopifyVariant['id'];

                $productImage = [];
                $productImage['src'] = $shopifyProductImage['src'];
                $productImage['alt'] = $shopifyProductImage['alt'];
                $productImage['variant_ids'] = $variantIds;

                $url = '/admin/products/' . $variantProduct['id'] . '/images.json';
                $params = [
                    'image' => $productImage
                ];

                $updateProduct = $this->shopifyRequest->create($url, $params);
                if (is_string($updateProduct)) {
                    sleep(1);
                    \Log::info(["second time updateVariantImage" => $variantProduct['id'], $updateProduct, $productImage]);
                    $updateProduct = $this->shopifyRequest->create($url, $params);
                }
            }
        }
    }

    public function insertProductUsingShopifyApi($shopifyProduct)
    {
        $url = 'admin/products.json';
        $params = [
            'product' => $shopifyProduct
        ];
        $result = $this->shopifyRequest->create($url, $params);
        if (!isset($result['product'])) {
            dd($result, $shopifyProduct);
        }

        return $result['product'];
    }

    public function insertProductIntoDatabase($importedProduct, $migrateProduct, $productSetting)
    {
        $extra = [];
        $extra['product_id'] = $migrateProduct->row_id;

        $projectProduct = new ProjectProduct;
        $projectProduct->id = \UUID::uuid4()->toString();
        $projectProduct->project_id = $productSetting->project->id;
        $projectProduct->product = $importedProduct;
        $projectProduct->extra = $extra;
        $productSetting->products()->save($projectProduct);
    }

    public function generateMainProduct($shopifyProduct)
    {
        $firstVariant = array_first($shopifyProduct['variants']);
        unset($firstVariant['variant_product']);

        $shopifyProduct['variants'] = [];
        $shopifyProduct['variants'][] = $firstVariant;
        return $shopifyProduct;
    }

    public function generateShopifyProductObject($migrateProduct)
    {
        //shopify product object

        $visible = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'status');

        $shopifyProduct = [];
        $shopifyProduct['handle'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'url_key'); //url_key => 86
        $shopifyProduct['title'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'name');  //name => 60        
//        dd($shopifyProduct);
//        $shopifyProduct['body_html'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'description'); //description => 61 //short_description => 62
        $shopifyProduct['vendor'] = '';


        $shopifyProduct['published'] = ($visible == 1) ? 'true' : 'false';
        $shopifyProduct['variants'] = $this->magentoRequest->generateProductVariants($migrateProduct);
        $shopifyProduct['options'] = $this->magentoRequest->generateProductOptions(array_first($shopifyProduct['variants']));

        $shopifyProduct['tags'] = $this->magentoRequest->generateProductTags($migrateProduct, $shopifyProduct['variants']);
        $shopifyProduct['product_type'] = (in_array('not-visible-individually', $shopifyProduct['tags'])) ? 'not-visible-individually' : '';
        $shopifyProduct['images'] = $this->magentoRequest->generateProductImages($migrateProduct);

        $shopifyProduct['metafields_global_title_tag'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'meta_title'); //meta_title = 71
        $shopifyProduct['metafields_global_description_tag'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'meta_description'); //meta_description = 73

        $shopifyProduct['metafields'] = $this->magentoRequest->getProductMetaField($migrateProduct, $shopifyProduct);

        return $shopifyProduct;
    }

    public function generateVariantProductObject($mainProduct, $variant)
    {
        $migrateProduct = $variant['variant_product'];
        unset($variant['variant_product']);

        $visible = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'status');

        $shopifyProduct = [];
        $shopifyProduct['handle'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'url_key'); //url_key => 86
        $shopifyProduct['title'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'name');  //name => 60        
        $shopifyProduct['vendor'] = '';
        $shopifyProduct['product_type'] = 'sub_product';

        $shopifyProduct['published'] = ($visible == 1) ? 'true' : 'false';
        $shopifyProduct['variants'] = [$variant];
        $shopifyProduct['options'] = $this->magentoRequest->generateProductOptions($variant);

        $shopifyProduct['tags'] = $mainProduct['handle'];

        $shopifyProduct['images'] = $this->magentoRequest->generateProductImages($migrateProduct);

        $shopifyProduct['metafields_global_title_tag'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'meta_title'); //meta_title = 71
        $shopifyProduct['metafields_global_description_tag'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'meta_description'); //meta_description = 73        

        return $shopifyProduct;
    }
}

//variant images set
//variant options
