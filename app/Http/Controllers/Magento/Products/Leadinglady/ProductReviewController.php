<?php

namespace App\Http\Controllers\Magento\Products\Leadinglady;

use App\Models\ProjectProductSetting as ProductSetting;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Http\Controllers\Magento\MagentoController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductReviewController extends Controller
{
    public $magentoRequest, $shopifyRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create(ProductSetting $productSetting)
    {
        dd("shopify product review create");
        \Log::info("script start");
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

//        $this->insertAllProductVariants();
//        dd("stop");


        $this->magentoRequest = new MagentoController($project);
        $migrateReviews = $this->magentoRequest->getAllMagentoProductReviews();
//        dd($migrateReviews);
        $shopifyProductReviews = [];
        foreach ($migrateReviews as $migrateReview) {
            $review = [];

            $review = $this->generatProductReviewObject($migrateReview);
            if ($review) {
                $shopifyProductReviews[] = $review;
            }
        }

        $fileName = 'product-ll-reviews';
        $data = $shopifyProductReviews;
        $storePath = 'leading-lady/reviews';
        \App\Http\Controllers\Core\ExcelController::create($fileName, $data, $storePath)->download('csv');
    }

    public function generatProductReviewObject($migrateReview)
    {
        //product id
//        $product = $this->magentoRequest->getMagentoProduct($migrateReview->entity_pk_value);
//        $sku = ($product) ? $product->sku : '';

        $shopifyProduct = \DB::table('project_products')->where(['extra->type' => 'main', 'extra->product_id' => $migrateReview->entity_pk_value])->first();

//        $varinatProduct = \DB::table('leadinglady_product_variants')->where('sku', $sku)->first();

        if ($shopifyProduct) {
            $p = json_decode($shopifyProduct->product, true);

            $productId = $p['id'];
            $productTitle = $p['title'];
            $handle = $p['handle'];
            $productUrl = "https://leading-lady-inc.myshopify.com/products/" . $handle;

            $reviewDetail = $this->magentoRequest->getReviewDetailByReviewId($migrateReview->review_id);
            $reviewTitle = ($reviewDetail) ? $reviewDetail->title : '';
            $reviewAuthor = ($reviewDetail) ? $reviewDetail->nickname : '';
            $reviewBody = ($reviewDetail) ? $reviewDetail->detail : '';

            //rating
            $reviewRating = $this->magentoRequest->getReviewRatingByReviewId($migrateReview->review_id);
            $reviewRating = ($reviewRating) ? "'" . $reviewRating->value . "'" : '';
            //email 
            $customerId = ($reviewDetail) ? $reviewDetail->customer_id : NULL;
            if ($customerId) {
                $customer = $this->magentoRequest->getCustomerById($customerId);

                if ($customer) {
                    $customerEmail = $customer->email;
                }
            }

            //review object
            $review = [];

            $review['product_id'] = $productId;
            $review['product_title'] = $productTitle;
            $review['product_url'] = $productUrl;
            $review['review_content'] = $reviewBody;
            $review['review_score'] = $reviewRating;
            $review['review_title'] = $reviewTitle;
            $review['display_name'] = $reviewAuthor;
            $review['email'] = isset($customerEmail) ? $customerEmail : '';
            $review['product_image_url'] = '';
            $review['date'] = date('Y-m-d', strtotime($migrateReview->created_at));
            $review['product_description'] = '';
            $review['cf_Y__X'] = '';
            $review['published_image_url'] = '';
            $review['unpublished_image_url'] = ($migrateReview->status_id == 1) ? 1 : '0';
            $review['comment_content'] = '';
            $review['comment_public'] = '';
            $review['comment_created_at'] = '';
        }
        return ($shopifyProduct) ? $review : NULL;
    }

    public function insertAllProductVariants()
    {

        for ($i = 19; $i <= 35; $i++) {

            \Log::info(["page_id" => $i]);
            $params = [
                'limit' => 250,
                'page' => $i,
                'fields' => 'id,sku,product_id'
            ];

            $url = 'admin/variants.json';
            $result = $this->shopifyRequest->view($url, $params);
            $variants = $result['variants'];
//            dd($variants);
            foreach ($variants as $variant) {

                $availableParentProduct = $handle = FALSE;
                $product = $parentProduct = [];
                $product = $this->getShopifyProductId($variant['product_id']);
//
                $tags = explode(',', $product['tags']);
                $tags = array_map('trim', $tags);

                if (in_array('sub-product', $tags)) {
                    $availableParentProduct = TRUE;
                }
                if ($availableParentProduct) {
                    $handle = FALSE;
                    foreach ($tags as $tag) {
                        if ($tag != 'sub-product') {
                            $handle = trim($tag);
                        }
                    }

                    if ($handle) {
                        $parentProduct = $this->searchProductByHandle($handle);
                    }
                }

                $productVariant = [];
//                $productVariant['product_id'] = $variant['product_id'];
//                $productVariant['product_title'] = $product['title'];
//                $productVariant['product_handle'] = $product['handle'];
                if ($parentProduct) {
                    $productVariant['parent_product_id'] = $parentProduct['id'];
                    $productVariant['parent_product_title'] = $parentProduct['title'];
                    $productVariant['parent_product_handle'] = $parentProduct['handle'];
                    \DB::table('leadinglady_product_variants')->where('variant_id', $variant['id'])->update($productVariant);
                }
//                $productVariant['variant_id'] = $variant['id'];
//                $productVariant['sku'] = $variant['sku'];
//                dd("done", $variant);
            }

            sleep(1);
        }
        \Log::info("script end");
    }

    public function getShopifyProductId($productId)
    {
        sleep(1);
        $url = 'admin/products/' . $productId . '.json';
        $params = [
            'fields' => 'id,title,handle,tags'
        ];
        $result = $this->shopifyRequest->view($url, $params);
        return $result['product'];
    }

    public function searchProductByHandle($handle)
    {
        sleep(1);
        $url = 'admin/products.json';
        $params = [
            'handle' => $handle,
            'fields' => 'id,title,handle'
        ];

        $result = $this->shopifyRequest->view($url, $params);

        return (isset($result['products'][0])) ? $result['products'][0] : [];
    }
}
