<?php

namespace App\Http\Controllers\Magento\Products\Leadinglady;

use App\Http\Controllers\Core\CommonFunctionsController;
use App\Helpers\Helper;
use App\Models\Project;
use App\Models\CustomGuzzleHttp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MagentoController extends Controller
{
    public $db, $project, $commonFunctions;

    public function __construct(Project $project)
    {
        $this->project = $project;
        $this->db = CustomGuzzleHttp::connectExternalDatabase($project->platform_details);
        $this->commonFunctions = new CommonFunctionsController;
    }

    public function getMagentoDiscounts()
    {
        return $this->db->table('salesrule')->get()->toArray();
    }

    public function getSaleRuleCoupons($couponId)
    {
        return $this->db->table('salesrule_coupon')->where(['rule_id' => $couponId])->get()->toArray();
    }
    
    public function getAnswerOfQue($queId)
    {
        return $this->db->table('aw_pq_answer')->where(['question_id' => $queId])->first();
    }

    public function getAllMagentoProductQAs()
    {
        return $this->db->table('aw_pq_question')->where('visibility', 1)->get()->toArray();
    }

    public function getAllMagentoProductReviews()
    {
        return $this->db->table('review')->get()->toArray();
    }

    public function getSkuFromOption($attributes, $proId)
    {
        $attributeIds = [];
        $values = [];
        foreach ($attributes as $key => $attribute) {
            $attributeIds[] = $key;
            $values[] = $attribute;
        }

        $productIds = $this->db->table('catalog_product_relation')
                ->where('parent_id', $proId)->pluck('child_id')->toArray();

        $product = $this->db->table('catalog_product_entity_int')
                ->whereIn('row_id', $productIds)
                ->whereIn('attribute_id', $attributeIds)
                ->whereIn('value', $values)
                ->select('row_id')
                ->groupBy('row_id')
                ->havingRaw('count(*) = ' . count($attributeIds))
                ->get()->toArray();

        if (count($product) > 1) {
            dd("stop and check");
        }

        return ($product) ? $this->db->table('catalog_product_entity')->where('row_id', $product[0]->row_id)->pluck('sku')->first() : NULL;
    }

    public function getReviewDetailByReviewId($reviewId)
    {
        return $this->db->table('review_detail')->where(['review_id' => $reviewId])->first();
    }

    public function getReviewRatingByReviewId($reviewId)
    {
        return $this->db->table('rating_option_vote')->where(['review_id' => $reviewId])->first();
    }

    public function getMagentoProduct($id)
    {
        return $this->db->table('catalog_product_entity')->where('row_id', $id)->first();
    }

    public function getSwatchImages()
    {
        return $this->db->table('eav_attribute_option')->where('attribute_id', '80')->get()->toArray();
    }

    public function getSwatches($attributeId)
    {
        return $this->db->table('eav_attribute_option')->where('attribute_id', $attributeId)->get()->toArray();
    }

    public function getImageOfSwatch($optionId)
    {
        return $this->db->table('eav_attribute_option_swatch')->where(['option_id' => $optionId, 'type' => '2'])->first();
    }

    public function getImageOfSwatchColor($optionId)
    {
        return $this->db->table('eav_attribute_option_swatch')->where(['option_id' => $optionId, 'type' => '1'])->first();
    }

    public function getCustomers($options = [])
    {
        $search = (isset($options['search'])) ? $options['search'] : [];
        $query = $this->db->table('customer_entity')->where($search);
        if (isset($options['from'])) {
            $query = $query->offset($options['from']);
        }
        if (isset($options['size'])) {
            $query = $query->limit($options['size']);
        }

        return $query->get()->toArray();
    }

    public function getCustomerById($customerId)
    {
        return $this->db->table('customer_entity')->where(['entity_id' => $customerId])->first();
    }

    public function getCustomerAddresses($migrateCustomer)
    {
        return $this->db->table('customer_address_entity')->where(['parent_id' => $migrateCustomer->entity_id])->get()->toArray();
    }

    public function getRegionCodeOfAddress($regionId)
    {
        $region = $this->db->table('directory_country_region')->where(['region_id' => $regionId])->first();
        return ($region) ? $region->code : '';
    }

    public function getCustomerValueFromAttributeCode($migrateCustomer, $attributeCode)
    {
        $query = [
            'attribute_code' => $attributeCode,
            'entity_type_id' => 1
        ];
        $tableNamePrefix = 'customer_entity';
        $attribute = $this->getAttributeFromAttributeCode($query);

        $tableName = $this->getTableNameFromAttribute($attribute, $tableNamePrefix);

        $result = $this->db->table($tableName)->where(['row_id' => $migrateCustomer->row_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => '0'])->first();
        return ($result) ? $result->value : NULL;
    }

    public function getCustomerAddressValueFromAttributeCode($migrateCustomer, $attributeCode)
    {
        $query = [
            'attribute_code' => $attributeCode,
            'entity_type_id' => 2
        ];
        $tableNamePrefix = 'customer_address_entity';
        $attribute = $this->getAttributeFromAttributeCode($query);

        $tableName = $this->getTableNameFromAttribute($attribute, $tableNamePrefix);

        $result = $this->db->table($tableName)->where(['row_id' => $migrateCustomer->row_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => '0'])->first();
        return ($result) ? $result->value : NULL;
    }

    public function getCollections()
    {
        $query = $this->db->table('catalog_category_entity');
        return $query->get()->toArray();
    }

    public function getSpecialProducts()
    {
        return $this->db->table('catalog_product_relation')->select('parent_id', \DB::raw('count(*) as total'))
                ->groupBy('parent_id')
                ->having('total', '>', 100)
                ->get()
                ->toArray();
    }

    public function getProductName($id)
    {
        return $this->db->table('catalog_product_entity_varchar')->where(['row_id' => $id, 'attribute_id' => 60, 'store_id' => '0'])->first();
    }

    public function getProducts($options = [])
    {
        $search = (isset($options['search'])) ? $options['search'] : [];
        $query = $this->db->table('catalog_product_entity')->where($search);
        if (isset($options['from'])) {
            $query = $query->offset($options['from']);
        }
        if (isset($options['size'])) {
            $query = $query->limit($options['size']);
        }

        return $query->get()->toArray();
    }

    public function getOrderTaxLines($magentoOrderId)
    {
        return $this->db->table('sales_order_tax')->where(['order_id' => $magentoOrderId])->get()->toArray();
    }

    public function getOrders($options = [])
    {
        $search = (isset($options['search'])) ? $options['search'] : [];
        $query = $this->db->table('sales_order')->where($search);
        if (isset($options['from'])) {
            $query = $query->offset($options['from']);
        }
        if (isset($options['size'])) {
            $query = $query->limit($options['size']);
        }

        return $query->get()->toArray();
    }

    public function getMagentoProductBySku($sku)
    {
        return $this->db->table('catalog_product_entity')->where(['sku' => $sku])->first();
    }

    public function getChildOrderIds($orderItemId)
    {
        return $this->db->table('sales_order_item')->where(['parent_item_id' => $orderItemId])->first();
    }

    public function getOrderItems($orderId)
    {
        return $this->db->table('sales_order_item')->where(['order_id' => $orderId])->whereNull('parent_item_id')->get()->toArray();
    }

    public function getOrderAddresses($orderAddressId)
    {
        return $this->db->table('sales_order_address')->where(['entity_id' => $orderAddressId])->first();
    }

    public function isBaseProduct($migrateProduct)
    {
        $childProduct = $this->db->table('catalog_product_relation')->where('child_id', $migrateProduct->row_id)->first();
        return ($childProduct) ? FALSE : TRUE;
    }

    public function checkForImportCollection($migrateCollection)
    {
        return ($migrateCollection->parent_id == 0) ? FALSE : TRUE;
    }

    public function getCollectionValueFromAttributeCode($migrateCollection, $attributeCode)
    {
        $query = [
            'attribute_code' => $attributeCode,
            'entity_type_id' => 3
        ];
        $tableNamePrefix = 'catalog_category_entity';
        $attribute = $this->getAttributeFromAttributeCode($query);

        $tableName = $this->getTableNameFromAttribute($attribute, $tableNamePrefix);

        $result = $this->db->table($tableName)->where(['row_id' => $migrateCollection->row_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => '0'])->first();
        return ($result) ? $result->value : NULL;
    }

    public function getMigrateProduct($sku)
    {
        return $this->db->table('catalog_product_entity')->where('sku', $sku)->first();
    }

    public function getProductValueFromAttributeCode($migrateProduct, $attributeCode)
    {
        $query = [
            'attribute_code' => $attributeCode,
            'entity_type_id' => 4
        ];
        $tableNamePrefix = 'catalog_product_entity';
        $attribute = $this->getAttributeFromAttributeCode($query);

        $tableName = $this->getTableNameFromAttribute($attribute, $tableNamePrefix);

        $result = $this->db->table($tableName)->where(['row_id' => $migrateProduct->row_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => '0'])->first();
        return ($result) ? $result->value : NULL;
    }

    public function generateProductTags($migrateProduct, $shopifyVariants)
    {
        $productTags = [];

        //category tag
        $categoryTags = $this->getProductCategoryTag($migrateProduct);
        $productTags = array_merge($productTags, $categoryTags);

        //variant option tag
        $variantOptionTags = $this->getProductVariantOptionTag($shopifyVariants);
        $productTags = array_merge($productTags, $variantOptionTags);

        //price tag
        $priceTags = $this->getProductPriceTag($shopifyVariants);
        $productTags = array_merge($productTags, $priceTags);

        $visibilityTags = $this->getProductVisibilityTag($migrateProduct);
        $productTags = array_merge($productTags, $visibilityTags);

        $productTags[] = 'main';

        $productTags = array_values(array_unique($productTags));

        return $productTags;
    }

    public function getProductVisibilityTag($migrateProduct)
    {
        $visibilityTags = [];
        $visibility = $this->getProductValueFromAttributeCode($migrateProduct, 'visibility');
        if ($visibility == "1") {
            $visibilityTags[] = 'not-visible-individually';
        }

        return $visibilityTags;
    }

    public function getProductCategoryTag($migrateProduct)
    {
        $categoryTags = [];
        $categories = $this->db->table('catalog_category_product')->where('product_id', $migrateProduct->row_id)->pluck('category_id')->toArray();
        foreach ($categories as $category) {
            $tag = $this->db->table('catalog_category_entity_varchar')->where(['store_id' => 0, 'attribute_id' => '35', 'row_id' => $category])->first();
            $tag = $tag->value;

            if (str_contains($tag, 'sale')) {
                $categoryTags[] = $tag;
                $tag = 'sale';
            }

            if (!preg_match('~[0-9]~', $tag)) {
                $categoryTags[] = $tag;
            }
        }

        return $categoryTags;
    }

    public function getProductVariantOptionTag($shopifyVariants)
    {
        $variantOptionTags = [];
        foreach ($shopifyVariants as $shopifyVariant) {
            //option 1 tag
            if (isset($shopifyVariant['option1']) && $shopifyVariant['option1'] != 'Default Title') {
                $tag = Helper::handleizedString($shopifyVariant['option1']);
                $variantOptionTags[] = $tag;
            }

            //option 2 tag
            if (isset($shopifyVariant['option2'])) {
                $tag = Helper::handleizedString($shopifyVariant['option2']);
                $variantOptionTags[] = $tag;

                $size = $shopifyVariant['option2'];
                if (preg_match('~[0-9]~', $size)) {
                    $results = preg_split('/(?<=[0-9])(?=[\/a-z]+)/i', $size);
                    foreach ($results as $result) {
                        $variantOptionTags[] = Helper::handleizedString($result);
                    }
                }
            }

            //option 3 tag
            if (isset($shopifyVariant['option3'])) {
                $tag = Helper::handleizedString($shopifyVariant['option3']);
                $variantOptionTags[] = $tag;
            }
        }

        return $variantOptionTags;
    }

    public function getProductPriceTag($shopifyVariants)
    {
        $priceTags = [];
        $prices = array_column($shopifyVariants, 'price');
        $minPrice = (int) min($prices);
        $maxPrice = (int) max($prices);

        $ranges = $this->getRangeArray();

        //minimum range
        foreach ($ranges as $key => $range) {
            if (Helper::inRange($minPrice, $range['min'], $range['max'], TRUE)) {
                $priceTags[] = $range['tag'];
                $minTagKey = $key;
                break;
            }
        }

        //maximum range
        $ranges = array_slice($ranges, $minTagKey);
        foreach ($ranges as $range) {
            if (Helper::inRange($maxPrice, $range['min'], $range['max'], TRUE)) {
                $priceTags[] = $range['tag'];
                break;
            }
        }

        return $priceTags;
    }

    public function getRangeArray()
    {
        //range array
        $ranges = [];

        $range = [];
        $range['min'] = 0;
        $range['max'] = 9;
        $range['tag'] = 'under-9';
        $ranges[] = $range;

        $range = [];
        $range['min'] = 10;
        $range['max'] = 19;
        $range['tag'] = '10-19';
        $ranges[] = $range;

        $range = [];
        $range['min'] = 20;
        $range['max'] = 29;
        $range['tag'] = '20-29';
        $ranges[] = $range;

        $range = [];
        $range['min'] = 30;
        $range['max'] = 39;
        $range['tag'] = '30-39';
        $ranges[] = $range;

        $range = [];
        $range['min'] = 40;
        $range['max'] = 5000;
        $range['tag'] = '40-higher';
        $ranges[] = $range;
        return $ranges;
    }

    public function getCollectionImage($migrateCollection)
    {
        $prefix = 'https://www.leadinglady.com/media/catalog/category/';
        $imageName = $this->getCollectionValueFromAttributeCode($migrateCollection, 'image');
        if ($imageName) {
            $url = $prefix . $imageName;
            $url = $this->commonFunctions->generateProductImageSrc($url, $this->project->id);
        }
        return (isset($url)) ? $url : NULL;
    }

    public function getCollectionWithHandle($migrateCollection)
    {
        return $this->db->table('catalog_category_entity')
                ->join('catalog_category_entity_varchar', 'catalog_category_entity.row_id', '=', 'catalog_category_entity_varchar.row_id')
                ->where([
                    ['catalog_category_entity.row_id', '<', $migrateCollection->row_id],
                    ['catalog_category_entity_varchar.attribute_id', '=', '35'],
                    ['catalog_category_entity_varchar.store_id', '=', '0']
                ])
                ->pluck('value')->toArray();
    }

    public function getCollectionRules($handle)
    {
        $collectionRules = [];
        $collectionRule = [];
        $collectionRule['column'] = 'tag';
        $collectionRule['relation'] = 'equals';
        $collectionRule['condition'] = $handle;
        $collectionRules[] = $collectionRule;

        $collectionRule = [];
        $collectionRule['column'] = 'tag';
        $collectionRule['relation'] = 'equals';
        $collectionRule['condition'] = 'main';
        $collectionRules[] = $collectionRule;

        $collectionRule = [];
        $collectionRule['column'] = 'type';
        $collectionRule['relation'] = 'not_equals';
        $collectionRule['condition'] = 'sub_product';
        $collectionRules[] = $collectionRule;

        return $collectionRules;
    }

    public function generateProductImages($migrateProduct)
    {
        $prefixSrc = 'https://www.leadinglady.com/media/catalog/product';
        $productImages = [];
        $imageIds = $this->db->table('catalog_product_entity_media_gallery_value_to_entity')->where('row_id', $migrateProduct->row_id)->pluck('value_id')->toArray();
        foreach ($imageIds as $imageId) {
            $src = $this->db->table('catalog_product_entity_media_gallery')->where('value_id', $imageId)->first();
            $alt = $this->db->table('catalog_product_entity_media_gallery_value')->where(['value_id' => $imageId, 'store_id' => 0])->first();
            $result = (isset($alt->label)) ? $alt->label : NULL;

            if (!$result) {
                $alt = $this->db->table('catalog_product_entity_media_gallery_value')->where(['value_id' => $imageId, 'store_id' => 1])->first();
            }

            $result = (isset($alt->label)) ? $alt->label : NULL;
            if (!$result) {
                $alt = $this->db->table('catalog_product_entity_media_gallery_value')->where(['value_id' => $imageId, 'store_id' => 2])->first();
            }

            $imagePath = ($src->value[0] != "/") ? "/" . $src->value : $src->value;
            $url = $prefixSrc . $imagePath;

            $image = [];
            $image['src'] = $this->commonFunctions->generateProductImageSrc($url, $this->project->id);
            $image['alt'] = ($alt) ? $alt->label : '';
            $productImages[] = $image;
        }

        return $productImages;
    }

    public function getProductMetaField($migrateProduct, $shopifyProduct)
    {
        $description = $this->getProductValueFromAttributeCode($migrateProduct, 'description');
        $mainDescription = (str_contains($description, '<p><em>')) ? substr($description, 0, strpos($description, '<p><em>')) : $description;

        $quote = (strpos($description, '<p><em>')) ? substr($description, strpos($description, '<p><em>'), strpos($description, '</em></p>') + 9) : '-';


        $quoteAuthor = '';
        $quoteTitle = '';
        if ($quote != '-') {
            $quote = trim(str_replace(['<p><em>', '</em></p>', '</em></p >'], '', $quote));
            $quoteDescription = Helper::between('"', '"', $quote);
            $quoteTitle = Helper::after(',', $quote);
            $quote = trim(str_replace([$quoteDescription, '"'], '', $quote));
            $quoteAuthor = Helper::before(',', $quote);

            $quote = '<p class="quotes">' . $quoteDescription . '</p><h5>- ' . $quoteAuthor . '</h5><p>' . $quoteTitle . '</p>';
        }

        $productMetaFields = [];
        $metaField = [];
        $metaField['key'] = 'style';
        $metaField['value'] = $this->getProductValueFromAttributeCode($migrateProduct, 'style');
        $metaField['value_type'] = 'string';
        $metaField['namespace'] = 'product_manager';
        $productMetaFields[] = $metaField;

        $metaField = [];
        $metaField['key'] = 'description';
        $metaField['value'] = $mainDescription; //description => 61
        $metaField['value_type'] = 'string';
        $metaField['namespace'] = 'product_manager';
        $productMetaFields[] = $metaField;

        $metaField = [];
        $metaField['key'] = 'product_details';
        $metaField['value'] = '-';
        $metaField['value_type'] = 'string';
        $metaField['namespace'] = 'product_manager';
        $productMetaFields[] = $metaField;

        $metaField = [];
        $metaField['key'] = 'quote';
        $metaField['value'] = $quote; //description => 61
        $metaField['value_type'] = 'string';
        $metaField['namespace'] = 'product_manager';
        $productMetaFields[] = $metaField;

        $metaField = [];
        $metaField['key'] = 'fabric';
        $metaField['value'] = $this->getProductValueFromAttributeCode($migrateProduct, 'short_description'); //short_description => 62
        $metaField['value_type'] = 'string';
        $metaField['namespace'] = 'product_manager';
        $productMetaFields[] = $metaField;

        $metaField = [];
        $metaField['key'] = 'variant_count';
        $metaField['value'] = count($shopifyProduct['variants']);
        $metaField['value_type'] = 'integer';
        $metaField['namespace'] = 'product_manager';
        $productMetaFields[] = $metaField;

        return $productMetaFields;
    }

    public function generateProductOptions($variant)
    {
        $productOptions = [];

        $productOption = [];
        if (isset($variant['option1'])) {
            $productOption['name'] = 'Color';
            $productOptions[] = $productOption;
        }

        if (isset($variant['option2'])) {
            $productOption = [];
            $productOption['name'] = 'Size';
            $productOptions[] = $productOption;
        }

        if (isset($variant['option3'])) {
            $productOption = [];
            $productOption['name'] = 'Pack Size';
            $productOptions[] = $productOption;
        }

        return $productOptions;
    }

    public function getAttributeFromAttributeCode($query)
    {
        $attribute = $this->db->table('eav_attribute')->where($query)->first();
        return $attribute;
    }

    public function getTableNameFromAttribute($attribute, $tableNamePrefix)
    {
        return ($attribute->backend_type == "static") ? $tableNamePrefix : $tableNamePrefix . '_' . $attribute->backend_type;
    }

    public function getVariants($magentoId)
    {
        return $this->db->table('catalog_product_relation')->where('parent_id', $magentoId)->get()->toArray();
    }

    public function generateProductVariants($migrateProduct)
    {
        $productVariants = [];

        $specialVariants = FALSE;
        $productVariant = $this->generateShopifyVariant($migrateProduct, $specialVariants);
        if ($productVariant) {
            $productVariants[] = $productVariant;
        }

        $variants = $this->db->table('catalog_product_relation')->where('parent_id', $migrateProduct->row_id)->get()->toArray();

        if ($variants && count($variants) > (100 - count($productVariants))) {
            $specialVariants = TRUE;
        }
        foreach ($variants as $variant) {
            $productVariant = [];

            $variantDetails = $this->db->table('catalog_product_entity')->where('row_id', $variant->child_id)->first();
            if ($variantDetails->type_id != 'simple') {
                \Log::info($variantDetails);
            }

            $productVariant = $this->generateShopifyVariant($variantDetails, $specialVariants);

            $productVariants[] = $productVariant;
        }

        if (!$variants && !$productVariant) {
            $productVariants[] = $this->getSpecialVariant($migrateProduct);
        }

        return $productVariants;
    }

    public function getSpecialVariant($migrateProduct)
    {
        $productVariant = [];
        $colorId = $this->getProductValueFromAttributeCode($migrateProduct, 'color');
        $sizeId = $this->getProductValueFromAttributeCode($migrateProduct, 'size');
        $specialPrice = $this->getProductValueFromAttributeCode($migrateProduct, 'special_price');
        $upc = $this->getProductValueFromAttributeCode($migrateProduct, 'upc');
        $inventoryDetails = $this->getInventoryDetailsOfProduct($migrateProduct->row_id);
        $allowOutOfStockNotification = $this->getProductValueFromAttributeCode($migrateProduct, 'alp_xnotify_configurable_allow');
        $visible = $this->getProductValueFromAttributeCode($migrateProduct, 'status');

        if (!$colorId && !$sizeId) {
            
        } else {
            $productVariant['option1'] = $this->getValueOfOptions($colorId); //Color
            $productVariant['option2'] = $this->getValueOfOptions($sizeId); //Size
        }

        $productVariant['sku'] = $migrateProduct->sku; //sku => 63
        $productVariant['grams'] = $this->getProductValueFromAttributeCode($migrateProduct, 'weight'); //weight => 69

        $productVariant['inventory_quantity'] = ($visible == 1) ? (int) $inventoryDetails->qty : 0;
        $productVariant['inventory_management'] = 'shopify';
        $productVariant['inventory_policy'] = 'deny';
        $productVariant['fulfillment_service'] = 'manual';

        $productVariant['price'] = ($specialPrice) ? $specialPrice : $this->getProductValueFromAttributeCode($migrateProduct, 'price'); // price => 64    8123                 3008          869   6262
        if ($specialPrice) {
            $productVariant['compare_at_price'] = $this->getProductValueFromAttributeCode($migrateProduct, 'price'); // special_price => 65 // cost => 68 // 124 => msrp
        }
        $productVariant['requires_shipping'] = 'true';

        if ($upc) {
            $productVariant['barcode'] = $upc; //manufacturer =70
        }
        return $productVariant;
    }

    public function generateShopifyVariant($migrateProduct, $specialVariants)
    {
        $colorId = $this->getProductValueFromAttributeCode($migrateProduct, 'color'); // color = 80
        $sizeId = $this->getProductValueFromAttributeCode($migrateProduct, 'size'); // size => 121
        $packSizeId = $this->getProductValueFromAttributeCode($migrateProduct, 'pack_size'); //pack_size = 480

        if (!$colorId && !$sizeId) {
            return FALSE;
        }

        $specialPrice = $this->getProductValueFromAttributeCode($migrateProduct, 'special_price');
        $upc = $this->getProductValueFromAttributeCode($migrateProduct, 'upc');
        $inventoryDetails = $this->getInventoryDetailsOfProduct($migrateProduct->row_id);
        $allowOutOfStockNotification = $this->getProductValueFromAttributeCode($migrateProduct, 'alp_xnotify_configurable_allow');
        $visible = $this->getProductValueFromAttributeCode($migrateProduct, 'status');

        $size = $this->getValueOfOptions($sizeId);

        //generate product variant
        $productVariant = [];

        $productVariant['option1'] = $this->getValueOfOptions($colorId); //Color
        $productVariant['option2'] = $size;
        if ($packSizeId) {
            $productVariant['option3'] = $this->getValueOfOptions($packSizeId); //Size
        }

        $productVariant['sku'] = $migrateProduct->sku; //sku => 63
        $productVariant['grams'] = $this->getProductValueFromAttributeCode($migrateProduct, 'weight'); //weight => 69

        $productVariant['inventory_quantity'] = ($visible == 1) ? (int) $inventoryDetails->qty : 0;
        $productVariant['inventory_management'] = 'shopify';
        $productVariant['inventory_policy'] = 'deny';
        $productVariant['fulfillment_service'] = 'manual';

        $productVariant['price'] = ($specialPrice) ? $specialPrice : $this->getProductValueFromAttributeCode($migrateProduct, 'price'); // price => 64    8123                 3008          869   6262
        if ($specialPrice) {
            $productVariant['compare_at_price'] = $this->getProductValueFromAttributeCode($migrateProduct, 'price'); // special_price => 65 // cost => 68 // 124 => msrp
        }
        $productVariant['requires_shipping'] = 'true';

        if ($upc) {
            $productVariant['barcode'] = $upc; //manufacturer =70
        }

        if ($specialVariants) {
            $productVariant['variant_product'] = $migrateProduct;
        }
        return $productVariant;
    }

    public function getValueOfOptions($optionId)
    {
        $storeId = ($optionId == '443' || $optionId == '444') ? '0' : '1';
        $optionValue = $this->db->table('eav_attribute_option_value')->where(['option_id' => $optionId, 'store_id' => $storeId])->first();

        return ($optionValue) ? $optionValue->value : NULL;
    }

    public function getAttributeOptions()
    {
        return $this->db->table('eav_attribute_option_value')->get()->toArray();
    }

    public function getInventoryDetailsOfProduct($productId)
    {
        $inventoryDetails = $this->db->table('cataloginventory_stock_item')->where(['product_id' => $productId])->first();
        return ($inventoryDetails) ? $inventoryDetails : NULL;
    }

    public function getMagentoProductQuantity($shopifyVariant)
    {
        $magentoProduct = $this->getMagentoProductBySku($shopifyVariant['sku']);
        //not-visible-product
        $isBaseProduct = $this->isBaseProduct($magentoProduct);
        if ($isBaseProduct) {
            $tag = $this->getProductVisibilityTag($magentoProduct);
            if ($tag) {
                return 0;
            }
        }

        $inventoryDetail = $this->db->table('cataloginventory_stock_item')->where(['product_id' => $magentoProduct->row_id])->first();
        return ($inventoryDetail) ? (int) $inventoryDetail->qty : 0;
    }

    public function getWeightOfProduct($shopifyVariant)
    {
        $magentoProduct = $this->getMagentoProductBySku($shopifyVariant['sku']);
        return $this->getProductValueFromAttributeCode($magentoProduct, 'weight');
    }

    public function getBaseProducts()
    {
        $mainProducts = $this->db->table('catalog_product_entity')->pluck('row_id')->toArray();
        $childProducts = $this->db->table('catalog_product_relation')->pluck('child_id')->toArray();
        $baseProducts = array_diff($mainProducts, $childProducts);
        return $baseProducts;
    }
}

