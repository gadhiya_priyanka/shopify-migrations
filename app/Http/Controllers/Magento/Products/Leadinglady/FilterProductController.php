<?php

namespace App\Http\Controllers\Magento\Products\Leadinglady;

use Image;
use App\Helpers\Helper;
use App\Models\ProjectProduct;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectProductSetting as ProductSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterProductController extends Controller
{
    public $magentoRequest, $shopifyRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function test(ProductSetting $productSetting)
    {
        $project = $productSetting->project;
        $this->magentoRequest = new MagentoController($project);
        $products = $this->magentoRequest->getBaseProducts();
        dd($products);
    }

    public function swatchImage(ProductSetting $productSetting)
    {
//        dd($productSetting, "in swatchImage");
//        echo 'eav_attribute_option'; // main
//        echo 'eav_attribute_option_swatch';

        $prefix = 'https://www.leadinglady.com/media/attribute/swatch';
        $project = $productSetting->project;
        $this->magentoRequest = new MagentoController($project);
        $swatchImages = $this->magentoRequest->getSwatchImages();

        foreach ($swatchImages as $swatchImage) {
            $optionId = $swatchImage->option_id;
            $optionName = $this->magentoRequest->getValueOfOptions($optionId);
            if (!$optionName) {
                $optionName = $optionId;
            }

            $optionName = Helper::handleizedString($optionName);
            $image = $this->magentoRequest->getImageOfSwatch($optionId);
            $url = ($image) ? $image->value : NULL;
            if ($url) {
                $imagePath = $prefix . $url;
//                dd($imagePath);
                $extenstion = Helper::after('.', $url);
                $storagePath = 'app/leading-lady/swatches-with-id/' . $optionName . '-' . $optionId . '.' . $extenstion;
//                $storagePath = 'app/leading-lady/swatches/' . $optionName . '.' . $extenstion;

                $path = storage_path($storagePath);

                Image::make($imagePath)->save($path);
            }
        }
    }

    public function filterValues(ProductSetting $productSetting)
    {
        dd("filter");
        $project = $productSetting->project;
        $this->magentoRequest = new MagentoController($project);
        //color
        $swatchColors = $this->magentoRequest->getSwatches(80);
        $swatchSizes = $this->magentoRequest->getSwatches(121);
        $swatchStyles = $this->magentoRequest->getCollections();

        $colors = $optionNames = [];
        foreach ($swatchColors as $swatchColor) {
            $optionId = $swatchColor->option_id;
            $optionName = $this->magentoRequest->getValueOfOptions($optionId);

            if (!in_array($optionName, $optionNames)) {
                $color = [];
                $color['name'] = $optionName;
                $colors[] = $color;
                $optionNames[] = $optionName;
            }
        }

        $sizes = $optionNames = [];
        foreach ($swatchSizes as $swatchSize) {
            $optionId = $swatchSize->option_id;
            $optionName = $this->magentoRequest->getValueOfOptions($optionId);

            if (!in_array($optionName, $optionNames)) {
                $size = [];
                $size['name'] = $optionName;
                $sizes[] = $size;
                $optionNames[] = $optionName;
            }
        }

        $styles = $optionNames = [];
        foreach ($swatchStyles as $swatchStyle) {
            $isImport = $this->magentoRequest->checkForImportCollection($swatchStyle);
            $handle = $this->magentoRequest->getCollectionValueFromAttributeCode($swatchStyle, 'url_key');
            if (!preg_match('~[0-9]~', $handle)) {
                if ($isImport) {
                    $style = [];
                    $style['name'] = $this->magentoRequest->getCollectionValueFromAttributeCode($swatchStyle, 'name');
                    $style['handle'] = $this->magentoRequest->getCollectionValueFromAttributeCode($swatchStyle, 'url_key');
                    $styles[] = $style;
                }
            }
        }

        $storePath = '/leading-lady/swatches';
        $fileName = 'color-swatch';
        \App\Http\Controllers\Core\ExcelController::create($fileName, $colors, $storePath);
        $fileName = 'size-swatch';
        \App\Http\Controllers\Core\ExcelController::create($fileName, $sizes, $storePath);
        $fileName = 'style-swatch';
        \App\Http\Controllers\Core\ExcelController::create($fileName, $styles, $storePath);
        dd("completed");
    }

    public function sizeFilterValues(ProductSetting $productSetting)
    {
        dd("size filter");
        $project = $productSetting->project;
        $this->magentoRequest = new MagentoController($project);
        $swatchSizes = $this->magentoRequest->getSwatches(121);

        $bandSizes = $cupSizes = $commonSizes = $optionNames = [];
        foreach ($swatchSizes as $swatchSize) {
            $optionId = $swatchSize->option_id;
            $optionName = $this->magentoRequest->getValueOfOptions($optionId);

            if (!in_array($optionName, $optionNames)) {

                if (preg_match('~[0-9]~', $optionName)) {
                    $results = preg_split('/(?<=[0-9])(?=[\/a-z]+)/i', $optionName);
                    foreach ($results as $key => $result) {
                        if ($key == 0) {
                            if (!in_array($result, array_column($bandSizes, 'name'))) {
                                $bandSize = [];
                                $bandSize['name'] = $result;
                                $bandSizes[] = $bandSize;
                            }
                        } else {
                            if (!in_array($result, array_column($cupSizes, 'name'))) {
                                $cupSize = [];
                                $cupSize['name'] = $result;
                                $cupSizes[] = $cupSize;
                            }
                        }
                    }
                } else {

                    $commonSize = [];
                    $commonSize['name'] = $optionName;
                    $commonSizes[] = $commonSize;
                }
                $optionNames[] = $optionName;
            }
        }

        $storePath = '/leading-lady/filters';
        $fileName = 'common-size-swatch';
        \App\Http\Controllers\Core\ExcelController::create($fileName, $commonSizes, $storePath);
        $fileName = 'band-size-swatch';
        \App\Http\Controllers\Core\ExcelController::create($fileName, $bandSizes, $storePath);
        $fileName = 'cup-size-swatch';
        \App\Http\Controllers\Core\ExcelController::create($fileName, $cupSizes, $storePath);
        dd("completed");
    }

    public function update(ProductSetting $productSetting)
    {
//        dd("update product");
        $project = $productSetting->project;
        $this->magentoRequest = new MagentoController($project);
        $options = [];
        $options['from'] = 0;
        $options['size'] = 1;
        $options['search'] = [['row_id', '=', '5324']];
        $migrateProducts = $this->magentoRequest->getProducts($options);
//        dd($migrateProducts);
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        foreach ($migrateProducts as $migrateProduct) {
            $shopifyProduct = $this->generateShopifyProductObject($migrateProduct);

            $shopifyProductImages = $shopifyProduct['images'];

            $shopifyProductImages = $shopifyProduct['images'];
            $variants = $shopifyProduct['variants'];

            $variants = array_splice($variants, 252);

            $productId = '1414173556778';
            $mainProduct = $this->getMainProduct($productId);
//            dd($shopifyProduct, $variants, $mainProduct);
            foreach ($variants as $key => $variant) {
//                        \Log::info(['variant_id' => $variant['sku']]);
//                if ($key == 0) {
//                    $shopifyMainProduct = $this->generateMainProduct($shopifyProduct);
//                    $mainProduct = $this->insertProduct($shopifyMainProduct, $migrateProduct, $productSetting);
//                    $this->updateMainVariantImage($mainProduct);
//                    continue;
//                }

                $variantProduct = $this->generateVariantProductObject($mainProduct, $variant);
                $migrateVariantProduct = $variant['variant_product'];
                $variantProduct = $this->insertProduct($variantProduct, $migrateProduct, $productSetting, $migrateVariantProduct);
                $variantProduct['main_product'] = $mainProduct;
                $this->updateVariantImage($variantProduct);
//                dd("done");
            }
            \Log::info("Script End with 100+ variants");
            dd("stop this script");
            continue;
        }
    }

    public function getMainProduct($productId)
    {
        $url = '/admin/products/' . $productId . '.json';
        $result = $this->shopifyRequest->view($url);
        return $result['product'];
    }

    public function getSpecialProducts()
    {
        $options = $this->magentoRequest->getAttributeOptions();
        $shopifyOptions = [];
        foreach ($options as $option) {
            $shopifyOption = [];
            $shopifyOption['option'] = $option->value;
            $shopifyOptions[] = $shopifyOption;
        }
        $fileName = 'leading-lady-variants';
        $storePath = '/leading-lady/speacial-products';
        \App\Http\Controllers\Core\ExcelController::create($fileName, $shopifyOptions, $storePath)->download('csv');
        dd("Stop");
    }

    public function create(ProductSetting $productSetting)
    {
        dd("create product");

        \Log::info("Script Start");

        $project = $productSetting->project;
        $this->magentoRequest = new MagentoController($project);

        $lastProduct = $productSetting->products()->orderBy('extra->product_id', 'desc')->first();

        $options = [];
        $options['from'] = 0;
        $options['size'] = 1;
        if ($lastProduct) {
            $options['search'] = [['row_id', '>', $lastProduct->extra['product_id']]];
//            $options['search'] = [['row_id', '>', 36]];
        }

        $migrateProducts = $this->magentoRequest->getProducts($options);

        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        foreach ($migrateProducts as $migrateProduct) {
            \Log::info(['product_id' => $migrateProduct->row_id]);
            $isBaseProduct = $this->magentoRequest->isBaseProduct($migrateProduct);
            if (!$isBaseProduct) {
                $migrateProduct = $this->getBaseProduct($migrateProduct, $options);
//                dd($migrateProduct);
                $stopVariable = TRUE;
                if ($migrateProduct) {
                    \Log::info(['stop_product_id' => $migrateProduct->row_id]);
                    $isBaseProduct = TRUE;
                }
            }

            if ($isBaseProduct) {
                $shopifyProduct = [];
                $shopifyProduct = $this->generateShopifyProductObject($migrateProduct);
                $testVariant = array_first($shopifyProduct['variants']);

                if (!isset($testVariant['option1'])) {

                    if (!isset($testVariant['option1']) && !isset($testVariant['option2'])) {
                        
                    } else {

                        foreach ($shopifyProduct['variants'] as $key => $var) {

                            if (!$var['option1']) {
                                $shopifyProduct['variants'][$key]['option1'] = $shopifyProduct['variants'][$key]['option2'];
                                unset($shopifyProduct['variants'][$key]['option2']);
                            } else {
                                \Log::info("Stop Script wirh variants error");
                                dd("something went wrong in variants", $shopifyProduct['variants']);
                            }
                        }
                    }
                }

                $shopifyProductImages = $shopifyProduct['images'];
                if (count($shopifyProduct['variants']) > 100) {
                    $variants = $shopifyProduct['variants'];

                    foreach ($variants as $key => $variant) {
//                        \Log::info(['variant_id' => $variant['sku']]);
                        if ($key == 0) {
                            $shopifyMainProduct = $this->generateMainProduct($shopifyProduct);
                            $mainProduct = $this->insertProduct($shopifyMainProduct, $migrateProduct, $productSetting);
                            $this->updateMainVariantImage($mainProduct);
                            continue;
                        }

                        $variantImage = $this->getVariantImage($variant, $mainProduct);
                        $variant['image'] = $variantImage;
                        $variantProduct = $this->generateVariantProductObject($mainProduct, $variant);
                        $migrateVariantProduct = $variant['variant_product'];
                        $variantProduct = $this->insertProduct($variantProduct, $migrateProduct, $productSetting, $migrateVariantProduct);
                        if ($variantImage) {
                            $this->updateVariantImage($variantProduct, $variantImage);
                        }
                    }
                    \Log::info("Script End with 100+ variants");
                    dd("stop this script");
                    continue;
                }
                unset($shopifyProduct['images']);
                $shopifyProduct = $this->insertProduct($shopifyProduct, $migrateProduct, $productSetting);
                $this->updateAllVariantImage($shopifyProductImages, $shopifyProduct);
            }

            if (isset($stopVariable)) {
                \Log::info("Script End with stopVariable");
                dd("stop script with stopVariable");
            }
        }

//        sleep(60);

        if ($migrateProducts) {
//            $schedule->status = 'paused';
//            $schedule->save();
        } else {
//            $schedule->status = 'completed';
//            $schedule->save();
            \Log::info("migration completed");
            dd("complete migration");
        }
        \Log::info("Script End");
    }

    public function getBaseProduct($migrateProduct, $options)
    {
        for ($i = 1; $i <= 1000; $i++) {
            $options['search'] = [['row_id', '>', $migrateProduct->row_id]];

            $migrateProducts = $this->magentoRequest->getProducts($options);
            if (!$migrateProducts) {
                return NULL;
            }
            $migrateProduct = $migrateProducts[0];

            $isBaseProduct = $this->magentoRequest->isBaseProduct($migrateProduct);

            if ($isBaseProduct) {
                return $migrateProduct;
            }
        }
    }

    public function updateMainVariantImage($mainProduct)
    {
        $images = $mainProduct['images'];
        $firstVariant = array_first($mainProduct['variants']);

        foreach ($images as $image) {
            if (str_contains(strtolower($image['alt']), strtolower($firstVariant['option1']))) {
                $imageId = $image['id'];

                $productImage = [];
                $productImage['id'] = $imageId;
                $productImage['variant_ids'] = [
                    $firstVariant['id']
                ];

                $url = '/admin/products/' . $mainProduct['id'] . '/images/' . $imageId . '.json';
                $params = [
                    'image' => $productImage
                ];

                $updateProduct = $this->shopifyRequest->update($url, $params);
                if (is_string($updateProduct)) {
                    sleep(1);
                    \Log::info(["second time updateMainVariantImage" => $mainProduct['id'], $updateProduct, $productImage]);
                    $updateProduct = $this->shopifyRequest->update($url, $params);
                }
                break;
            }
        }
    }

    public function insertProduct($shopifyProduct, $migrateProduct, $productSetting, $migrateVariantProduct = [])
    {
        $shopifyProduct = $this->insertProductUsingShopifyApi($shopifyProduct);
        $this->insertProductIntoDatabase($shopifyProduct, $migrateProduct, $productSetting, $migrateVariantProduct);
        return $shopifyProduct;
    }

    public function updateAllVariantImage($shopifyProductImages, $shopifyProduct)
    {
        $variants = $shopifyProduct['variants'];
        $variantColors = array_map('strtolower', array_column($variants, 'option1'));
        $colors = array_unique($variantColors);

        foreach ($shopifyProductImages as $shopifyProductImage) {
            $alt = $shopifyProductImage['alt'];
            $variantIds = [];

            $validColors = [];
            foreach ($colors as $color) {
                if (str_contains(strtolower($alt), $color)) {
                    $validColors[] = $color;
                }
            }

            foreach ($validColors as $validColor) {
                $variantkeys = [];
                $variantkeys = array_keys($variantColors, $validColor);
                foreach ($variantkeys as $variantkey) {
                    $variantIds[] = $variants[$variantkey]['id'];
                }
            }

            //product image object
            $productImage = [];
            $productImage['src'] = $shopifyProductImage['src'];
            $productImage['alt'] = $shopifyProductImage['alt'];
            $productImage['variant_ids'] = $variantIds;

            $url = '/admin/products/' . $shopifyProduct['id'] . '/images.json';
            $params = [
                'image' => $productImage
            ];

            $updateProduct = $this->shopifyRequest->create($url, $params);
            if (is_string($updateProduct)) {
                sleep(1);
                \Log::info(["second time updateAllVariantImage" => $shopifyProduct['id'], $updateProduct, $productImage]);
                $updateProduct = $this->shopifyRequest->create($url, $params);
            }
        }
    }

    public function getVariantImage($variant, $mainProduct)
    {
        $images = $variantProduct['main_product']['images'];
        $firstVariant = $variant;
        foreach ($images as $image) {
            if (str_contains(strtolower(trim($image['alt'])), strtolower(trim($firstVariant['option1'])))) {
                $variantImage = $image;
                break;
            }
        }

        if (!isset($variantImage)) {
            foreach ($images as $image) {
                if (str_contains(strtolower($image['src']), strtolower(trim($firstVariant['option1'])))) {
                    $variantImage = $image;
                    break;
                }
            }
        }

        if (!isset($variantImage)) {
            return NULL;
        }
    }

    public function updateVariantImage($variantProduct, $variantImage)
    {
        $firstVariant = array_first($variantProduct['variants']);

        $productImage = [];
        $productImage['src'] = $variantImage['src'];
        $productImage['alt'] = $variantImage['alt'];
        $productImage['variant_ids'] = [
            $firstVariant['id']
        ];
        $url = '/admin/products/' . $variantProduct['id'] . '/images.json';
        $params = [
            'image' => $productImage
        ];

        $updateProduct = $this->shopifyRequest->create($url, $params);
        if (is_string($updateProduct)) {
            sleep(1);
            \Log::info(["second time updateVariantImage" => $variantProduct['id'], $updateProduct, $productImage]);
            $updateProduct = $this->shopifyRequest->create($url, $params);
        }
    }

    public function insertProductUsingShopifyApi($shopifyProduct)
    {
        $url = 'admin/products.json';
        $params = [
            'product' => $shopifyProduct
        ];
        $result = $this->shopifyRequest->create($url, $params);
        if (!isset($result['product'])) {
            dd($result, $shopifyProduct);
        }

        return $result['product'];
    }

    public function insertProductIntoDatabase($importedProduct, $migrateProduct, $productSetting, $migrateVariantProduct)
    {
        $extra = [];
        $extra['product_id'] = $migrateProduct->row_id;
        if ($migrateVariantProduct) {
            $extra['varinat_id'] = $migrateVariantProduct->row_id;
        }
        $extra['type'] = ($migrateVariantProduct) ? 'sub_product' : 'main';

        $projectProduct = new ProjectProduct;
        $projectProduct->id = \UUID::uuid4()->toString();
        $projectProduct->project_id = $productSetting->project->id;
        $projectProduct->product = $importedProduct;
        $projectProduct->extra = $extra;
        $productSetting->products()->save($projectProduct);
    }

    public function generateMainProduct($shopifyProduct)
    {
        $firstVariant = array_first($shopifyProduct['variants']);
        unset($firstVariant['variant_product']);

        $shopifyProduct['variants'] = [];
        $shopifyProduct['variants'][] = $firstVariant;
        return $shopifyProduct;
    }

    public function generateShopifyProductObject($migrateProduct)
    {
        //shopify product object

        $visible = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'status');

        $shopifyProduct = [];
        $shopifyProduct['handle'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'url_key'); //url_key => 86
        $shopifyProduct['title'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'name');  //name => 60        
//        dd($shopifyProduct);
//        $shopifyProduct['body_html'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'description'); //description => 61 //short_description => 62
        $shopifyProduct['vendor'] = '';


        $shopifyProduct['published'] = ($visible == 1) ? 'true' : 'false';
        $shopifyProduct['variants'] = $this->magentoRequest->generateProductVariants($migrateProduct);
        $shopifyProduct['options'] = $this->magentoRequest->generateProductOptions(array_first($shopifyProduct['variants']));

        $shopifyProduct['tags'] = $this->magentoRequest->generateProductTags($migrateProduct, $shopifyProduct['variants']);
        $shopifyProduct['product_type'] = (in_array('not-visible-individually', $shopifyProduct['tags'])) ? 'not-visible-individually' : '';
        $shopifyProduct['images'] = $this->magentoRequest->generateProductImages($migrateProduct);

        $shopifyProduct['metafields_global_title_tag'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'meta_title'); //meta_title = 71
        $shopifyProduct['metafields_global_description_tag'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'meta_description'); //meta_description = 73

        $shopifyProduct['metafields'] = $this->magentoRequest->getProductMetaField($migrateProduct, $shopifyProduct);

        return $shopifyProduct;
    }

    public function generateVariantProductObject($mainProduct, $variant)
    {
        $migrateProduct = $variant['variant_product'];
        unset($variant['variant_product']);

        $visible = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'status');

        $shopifyProduct = [];
        $shopifyProduct['handle'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'url_key'); //url_key => 86
        $shopifyProduct['title'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'name');  //name => 60        
        $shopifyProduct['vendor'] = '';
        $shopifyProduct['product_type'] = 'sub_product';

        $shopifyProduct['published'] = ($visible == 1) ? 'true' : 'false';
        $shopifyProduct['variants'] = [$variant];
        $shopifyProduct['options'] = $this->magentoRequest->generateProductOptions($variant);

        $shopifyProduct['tags'] = $mainProduct['handle'];

        if ($variant['image']) {
            $shopifyProduct['metafields'] = [
                [
                    'key' => 'image_url',
                    'value' => $variant['image']['src'],
                    'value_type' => 'string',
                    'namespace' => 'product_manager'
                ]
            ];
        }

        $shopifyProduct['metafields_global_title_tag'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'meta_title'); //meta_title = 71
        $shopifyProduct['metafields_global_description_tag'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'meta_description'); //meta_description = 73        

        return $shopifyProduct;
    }
}

//variant images set
//variant options
