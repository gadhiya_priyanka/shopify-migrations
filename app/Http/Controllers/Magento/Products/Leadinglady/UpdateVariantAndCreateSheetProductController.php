<?php

namespace App\Http\Controllers\Magento\Products\Leadinglady;

use App\Http\Controllers\Core\ExcelController;
use Image;
use App\Helpers\Helper;
use App\Models\ProjectProduct;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectProductSetting as ProductSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UpdateVariantAndCreateSheetProductController extends Controller
{
    public $magentoRequest, $shopifyRequest, $mainProducts;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function update(ProductSetting $productSetting)
    {
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $mainProducts = $this->getMainProducts();
        $invalidProducts = [];
        $counter = 0;
        foreach ($mainProducts as $mainProduct) {
            if (count($mainProduct['variants']) == 1) {
                $counter++;
            }
            //89 100 under
            $productId = $mainProduct['id'];
//            $metaFields = $this->getMetafieldsOfProductId($productId);
//
//            foreach ($metaFields as $metaField) {
//                if ($metaField['key'] == 'variant_count') {
//                    if ($metaField['value'] < 100) {
////                        if (count($mainProduct['variants']) != $metaField['value']) {
////                            $product = [];
////                            $product['id'] = $productId;
////                            $product['title'] = $mainProduct['title'];
////                            $product['shopify_variant_count'] = count($mainProduct['variants']);
////                            $product['metafield_variant_count'] = $metaField['value'];
////                            $invalidProducts[] = $product;
////                        }
//                    }
//                }
//            }
        }
        dd($counter);

        $fileName = 'invalid-product';
        $storePath = 'leading-lady/products';
        ExcelController::create($fileName, $invalidProducts, $storePath)->download('csv');
    }

    public function updateVariant(ProductSetting $productSetting)
    {
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $this->magentoRequest = new MagentoController($project);

        for ($i = 1; $i <= 31; $i++) {

            $page = $i; //30
            $projectName = 'leading-lady';
            $folderName = 'variants';
            $logFileName = 'variant-' . $page . '.log';
            \Log::useDailyFiles(storage_path() . '/logs/' . $projectName . '/' . $folderName . '/' . $logFileName);
            \Log::info("script start");

            $params = [
                'page' => $page,
                'limit' => 250,
                'fields' => 'id,product_id,sku'
            ];
            $url = 'admin/variants.json';
            $result = $this->shopifyRequest->view($url, $params);
            $variants = $result['variants'];
            $errors = [];

            foreach ($variants as $variant) {
                \Log::info(["variant_id" => $variant['id']]);

                $variantId = $variant['id'];
                $sku = $variant['sku'];
                $migrateProduct = $this->magentoRequest->getMigrateProduct($sku);
                $grams = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'weight');

                $url = '/admin/variants/' . $variantId . '.json';
                $params = [
                    'variant' => [
                        'grams' => $grams,
                        'inventory_management' => 'shopify',
                        'inventory_policy' => 'deny'
                    ]
                ];
                $updateVariant = $this->shopifyRequest->update($url, $params);
                if (!isset($updateVariant['variant'])) {
                    sleep(2);
                    $updateVariant = $this->shopifyRequest->update($url, $params);
                    if (!isset($updateVariant['variant'])) {
                        \Log::info(["second time variant error" => $updateVariant, $variant]);
                        $error = [];
                        $error = $variant;
                        $error['error'] = $updateVariant;
                        $errors[] = $error;
                    }
                }
            }

            \Log::info("generate csv");

            $fileName = 'error-variant-shipping-' . $page;
            $storePath = 'app/leading-lady/shipping';
            ExcelController::create($fileName, $errors, $storePath);
            \Log::info("script end");
            sleep(10);
        }
        \Log::info("for loop end");
    }

    public function update11(ProductSetting $productSetting)
    {
//        dd("Update Images");
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $fileName = 'variant-image-update.csv';
        $filePath = resource_path('assets/leading-lady/') . $fileName;
//        dd($filePath);
        $products = $this->readCsv($filePath);
        $errors = [];
        foreach ($products as $product) {

            $variantId = (int) $product['variant_id'];
            $subProductId = (int) $product['sub_product_id'];
            $productId = (int) $product['main_product_id'];

            \Log::info(["product_id" => $subProductId]);
            $variantIds = [];
            $variantIds[] = $variantId;

            $productImage = [];
            $productImage['src'] = $product['image_url'];
            $productImage['variant_ids'] = $variantIds;

            $url = '/admin/products/' . $subProductId . '/images.json';
            $params = [
                'image' => $productImage
            ];
            $updateProduct = $this->shopifyRequest->create($url, $params);
            if (is_string($updateProduct)) {
                sleep(1);
                \Log::info(["second time update image" => $product['image_url'], $updateProduct, $productImage]);
                $updateProduct = $this->shopifyRequest->create($url, $params);
                if (is_string($updateProduct)) {
                    \Log::info(["second time error" => $updateProduct, $productImage]);
                    $error = [];
                    $error = $product;
                    $error['error'] = $updateProduct;
                    $errors[] = $error;
                }
            }
            dd("done", $product);
        }

        $fileName = 'error-variant-image';
        $storePath = 'app/leading-lady/products';
        ExcelController::create($fileName, $data, $storePath);
    }

    public function updateTemp(ProductSetting $productSetting)
    {

        $product = $productSetting->products()->where('extra->product_id', '5282')->pluck('product')->toArray();
        dd($product);

//        dd("update");
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $products = [];
        $productVariants = $this->getProductVariants();

        foreach ($productVariants as $productVariant) {

            if (!$productVariant['image_id']) {

                $subProductId = $productVariant['product_id'];
                $subProduct = $this->getProduct($subProductId);
                if ($subProduct['product_type'] != "sub_product") {
                    $mainProductId = $subProductId;
                } else {
                    $mainProduct = $this->getSearchProductByHandle(trim($subProduct['tags']));
                    $mainProductId = $mainProduct['id'];
                }

                $product = [];
                $product['main_product_id'] = $mainProductId;
                $product['sub_product_id'] = ($mainProductId != $subProductId) ? $subProductId : '-';
                $product['variant_id'] = $productVariant['id'];
                $product['image_url'] = '';
                $products[] = $product;
            }
        }

        $fileName = 'variant_image_remainig';
        $storePath = '/leading-lady/variants';
        ExcelController::create($fileName, $products, $storePath)->download('csv');
        dd("Stop");
    }

    public function updateImageMetafield()
    {
        dd("update");
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $fileName = 'variant-image-update.csv';
        $filePath = resource_path('assets/leading-lady/') . $fileName;
//        dd($filePath);
        $products = $this->readCsv($filePath);
        $errors = [];

        foreach ($products as $product) {

            $findMetaField = FALSE;
            $subProductId = (int) $product['sub_product_id'];
            $variantId = (int) $product['variant_id'];
            \Log::info(["product_id" => $subProductId]);

            $url = '/admin/products/' . $subProductId . '/metafields.json';
            $result = $this->shopifyRequest->view($url);
            $metaFileds = $result['metafields'];
            foreach ($metaFileds as $metaFiled) {
                if ($metaFiled['key'] == 'image_url') {
                    $findMetaField = TRUE;
                    $url = '/admin/products/' . $subProductId . '/metafields/' . $metaFiled['id'] . '.json';
                    $params = [
                        'metafield' => [
                            'value' => $product['image_url'],
                            'value_type' => 'string',
                            'namespace' => 'product_manager'
                        ]
                    ];
                    $updateMetaFiled = $this->shopifyRequest->update($url, $params);
                    if (!isset($updateMetaFiled['metafield'])) {
                        sleep(2);
                        $updateMetaFileds = $this->shopifyRequest->update($url, $params);
                        if (!isset($updateMetaFiled['metafield'])) {
                            \Log::info(["metafield error update" => $updateMetaFiled, $product]);
                            $error = [];
                            $error = $product;
                            $error['error'] = $updateMetaFiled;
                            $error['type'] = 'update';
                            $errors[] = $error;
                        }
                    }
                }
            }

            if (!$findMetaField) {
                $url = '/admin/products/' . $subProductId . '/metafields.json';
                $params = [
                    'metafield' => [
                        'key' => 'image_url',
                        'value' => $product['image_url'],
                        'value_type' => 'string',
                        'namespace' => 'product_manager'
                    ]
                ];
                $createMetaField = $this->shopifyRequest->create($url, $params);
                if (!isset($createMetaField['metafield'])) {
                    sleep(2);
                    $createMetaField = $this->shopifyRequest->update($url, $params);
                    if (!isset($createMetaField['metafield'])) {
                        \Log::info(["metafield error create" => $createMetaField, $product]);
                        $error = [];
                        $error = $product;
                        $error['error'] = $createMetaField;
                        $error['type'] = 'create';
                        $errors[] = $error;
                    }
                }
            }
        }

        $fileName = 'error-variant-metafield';
        $storePath = 'app/leading-lady/metafields';
        ExcelController::create($fileName, $errors, $storePath);
    }

    public function generateSheetForVariant(ProductSetting $productSetting)
    {
        $project = $productSetting->project;
//        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
//        $products = [];
//        $productVariants = $this->getProductVariants();
//
//        foreach ($productVariants as $productVariant) {
//
//            if (!$productVariant['image_id']) {
//
//                $subProductId = $productVariant['product_id'];
//                $subProduct = $this->getProduct($subProductId);
//                if ($subProduct['product_type'] != "sub_product") {
//                    $mainProductId = $subProductId;
//                } else {
//                    $mainProduct = $this->getSearchProductByHandle(trim($subProduct['tags']));
//                    $mainProductId = $mainProduct['id'];
//                }
//
//                $product = [];
//                $product['main_product_id'] = $mainProductId;
//                $product['sub_product_id'] = ($mainProductId != $subProductId) ? $subProductId : '-';
//                $product['variant_id'] = $productVariant['id'];
//                $product['image_url'] = '';
//                $products[] = $product;
//            }
//        }
//
//        $fileName = 'variant_image';
//        $storePath = '/leading-lady/variants';
//        ExcelController::create($fileName, $products, $storePath);
//        dd("Stop");
    }

    public function updateUnder100Variant()
    {
        foreach ($reMigrateProducts as $reMigrateProduct) {
            dd("inventory 0 please do it first");
            $productId = $reMigrateProduct->product_id;
            $updateProductId = $reMigrateProduct->main_product_id;
            $product = $this->getProductById($productId);
            $updateProduct = $this->getProductById($updateProductId);

            //add variant in main product
            $variant = array_first($product['variants']);
            $lists = [
                'created_at',
                'updated_at',
                'id',
                'product_id',
                'position',
                'image_id',
                'inventory_item_id',
                'old_inventory_quantity'
            ];
            foreach ($lists as $list) {
                unset($variant[$list]);
            }
            $variant['inventory_policy'] = 'deny';
            $variant['inventory_management'] = 'shopify';
            $variant['inventory_quantity'] = 0;

            $url = '/admin/products/' . $updateProductId . '/variants.json';
            $params = [
                'variant' => $variant
            ];
            $updateMainProduct = $this->shopifyRequest->create($url, $params);
            if (!isset($updateMainProduct['variant'])) {
                dd($updateMainProduct);
            }

            //update variant tags
            $tags = [];
            $existingTags = explode(',', $updateProduct['tags']);
            $existingTags = array_map('trim', $existingTags);
            $tags = array_merge($tags, $existingTags);
            $newTags = explode(',', $product['tags']);
            $newTags = array_map('trim', $newTags);

            foreach ($newTags as $newTag) {
                if (!in_array($newTag, $existingTags)) {
                    if ($newTag != 'not-visible-individually') {
                        $tags[] = $newTag;
                    }
                }
            }

            $url = '/admin/products/' . $updateProductId . '.json';

            $params = [
                'product' => [
                    'id' => $updateProductId,
                    'tags' => $tags
                ]
            ];
            $updateTags = $this->shopifyRequest->update($url, $params);
            if (!isset($updateTags['product'])) {
                dd($updateTags);
            }

            //update metafield variant count
            $url = '/admin/products/' . $updateProductId . '/metafields.json';
            $result = $this->shopifyRequest->view($url);
            if (!isset($result['metafields'])) {
                dd($result, "metafieds", $reMigrateProduct);
            }

            $metaFields = $result['metafields'];
            $metaKeys = array_keys(array_column($metaFields, 'key'), 'variant_count');
            $metaField = [];
            if ($metaKeys) {
                $metaField = $metaFields[$metaKeys[0]];
            }

            if (isset($metaField)) {
                $variantCount = $metaField['value'];
                $variantCount = $variantCount + 1;
                $metaFieldId = $metaField['id'];
                $params = [
                    'metafield' => [
                        'id' => $metaFieldId,
                        'value' => $variantCount,
                        'value_type' => 'integer'
                    ]
                ];
                $url = '/admin/products/' . $updateProductId . '/metafields/' . $metaFieldId . '.json';
                $result = $this->shopifyRequest->update($url, $params);
                if (!isset($result['metafield'])) {
                    dd($result, "metafield", $reMigrateProduct, $metaField);
                }
            }

            \DB::table('product_relations')->where('id', $reMigrateProduct->id)->update([
                'status' => 'solved'
            ]);
            dd("stop", $reMigrateProduct);
        }
    }

    public function matchString(ProductSetting $productSetting)
    {
        dd("stop restart match sring");
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $this->mainProducts = $this->getMainProducts();

        $reMigrateProducts = \DB::table('product_relations')->whereNull('main_product_id')->where([['status', 'pending']])->get()->toArray();
//        dd($reMigrateProducts);
        foreach ($reMigrateProducts as $reMigrateProduct) {
            $mainProduct = [];
            $mainProduct = $this->getMainProduct($reMigrateProduct->product_title);
            if ($mainProduct) {
                \DB::table('product_relations')->where('id', $reMigrateProduct->id)->update([
                    'main_product_id' => $mainProduct['id'],
                    'main_product_title' => $mainProduct['title'],
                    'main_product_handle' => $mainProduct['handle'],
                    'variant_count' => count($mainProduct['variants']),
                ]);
            }
        }

        dd("done");
    }

    public function alenShopifyProduct()
    {
        dd("stop restart unpublished product");
        $project = $productSetting->project;
        $this->magentoRequest = new MagentoController($project);
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $mainProducts = $this->getMainProducts();

        $shopifyProducts = [];
        foreach ($mainProducts as $mainProduct) {
            $variantCount = $this->getVariantCount($mainProduct);

            $shopifyProduct = [];
            $shopifyProduct['id'] = $mainProduct['id'];
            $shopifyProduct['title'] = $mainProduct['title'];
            $shopifyProduct['visibility'] = 'false';
            $shopifyProduct['variant_count'] = $variantCount;
            $shopifyProducts[] = $shopifyProduct;
        }

        $storePath = '/leading-lady/products';
        $data = $shopifyProducts;
        $fileName = 'unpublished-products';
        ExcelController::create($fileName, $data, $storePath);
        dd("done");
    }

    public function counter(ProductSetting $productSetting)
    {
        dd("stop restart counter sring");
        $project = $productSetting->project;
        $filePath = storage_path('leading-lady/products/') . 'unpublished-products.csv';
        $shopifyProducts = $this->readCsv($filePath);
        $filePath = storage_path('leading-lady/products/') . 'magento-products.csv';
        $magetoProducts = $this->readCsv($filePath);
        $filePath = storage_path('leading-lady/products/') . 'magento-products-1.csv';
        $magetoProduct = $this->readCsv($filePath);
        $magetoProducts = array_merge($magetoProducts, $magetoProduct);

        $migratedProducts = [];
        foreach ($shopifyProducts as $shopifyProduct) {
            $count = $this->getVariantsCount($magetoProducts, $shopifyProduct);

            $product = $shopifyProduct;
            $product['magento_variant_count'] = ($count) ? $count : "0";
            $migratedProducts[] = $product;
        }

        $storePath = '/leading-lady/products';
        $data = $migratedProducts;
        $fileName = 'unpublished-migrated-products';
        ExcelController::create($fileName, $data, $storePath);
        dd("done");
    }

    public function getVariantsCount($magetoProducts, $shopifyProduct)
    {
        $arrayKeys = array_keys(array_column($magetoProducts, 'title'), $shopifyProduct['title']);
        if ($arrayKeys) {
            return $magetoProducts[$arrayKeys[0]]['variant_count'];
        }
    }

    public function magentoProducts()
    {
        $project = $productSetting->project;
        $this->magentoRequest = new MagentoController($project);
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $products = $this->magentoRequest->getProducts();

        $magentoProducts = [];
        foreach ($products as $product) {
            $isBaseProduct = $this->magentoRequest->isBaseProduct($migrateProduct);
            if ($isBaseProduct) {
                $variants = $this->db->table('catalog_product_relation')->where('parent_id', $migrateProduct->row_id)->get()->toArray();

                $magentoProduct = [];
                $magentoProduct['id'] = $magentoProduct->row_id;
                $magentoProduct['title'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'name');
                $magentoProduct['handle'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'url_key');
                $magentoProduct['variant_count'] = count($variants);
            }
        }
    }

    public function getVariantCount($mainProduct)
    {
        $url = 'admin/products/' . $mainProduct['id'] . '/metafields.json';
        $metafileds = $this->shopifyRequest->view($url);
        $metafileds = $metafileds['metafields'];
        foreach ($metafileds as $metafiled) {
            if ($metafiled['key'] == "variant_count") {
                return $metafiled['value'];
                break;
            }
        }
    }

    public function alenCollection()
    {
        $project = $productSetting->project;
        $this->magentoRequest = new MagentoController($project);
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $fileName = 'collection.csv';
        $filePath = storage_path('/leading-lady/collections/') . $fileName;
        $shopifyCollections = $this->readCsv($filePath);

        $shopifyCollections = array_unique(array_column($shopifyCollections, 'title'));
//        dd($shopifyCollections);
        $magentoCollections = $this->magentoRequest->getCollections();
//        dd($magentoCollections);

        $existInShopifyCollections = $onlyInMagentos = [];
        foreach ($magentoCollections as $magentoCollection) {

            if ($magentoCollection) {
                $title = $this->magentoRequest->getCollectionValueFromAttributeCode($magentoCollection, 'name');
                if (in_array($title, $shopifyCollections)) {
                    $shopify = [];
                    $shopify['id'] = $magentoCollection->entity_id;
                    $shopify['title'] = $title;
                    $existInShopifyCollections[] = $shopify;
                } else {
                    $magento = [];
                    $magento['id'] = $magentoCollection->entity_id;
                    $magento['title'] = $title;
                    $onlyInMagentos[] = $magento;
                }
            }
        }
        $storePath = '/leading-lady/shopify-collections';
        $data = $existInShopifyCollections;
        $fileName = 'shopify-collection';
        ExcelController::create($fileName, $data, $storePath);

        $storePath = '/leading-lady/magento-collections';
        $data = $onlyInMagentos;
        $fileName = 'magento-collection';
        ExcelController::create($fileName, $data, $storePath);
        dd("done");
    }

    public function readCsv($filePath)
    {
        return ExcelController::view($filePath);
    }

    public function counterShopifyCollection()
    {
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $url = '/admin/smart_collections.json';
        $params = [
            'limit' => 250
        ];
        $result = $this->shopifyRequest->view($url, $params);
        $smartCollections = $result['smart_collections'];
        $shopifyCollections = [];
        foreach ($smartCollections as $smartCollection) {
            $collectionId = $smartCollection['id'];
            $productCount = $this->getCollectionProductCount($collectionId);

            $collection = [];
            $collection['id'] = $collectionId;
            $collection['title'] = $smartCollection['title'];
            $collection['handle'] = $smartCollection['handle'];
            $collection['product_count'] = $productCount;

            $shopifyCollections[] = $collection;
        }

        $storePath = '/leading-lady/collections';
        $data = $shopifyCollections;
        $fileName = 'collection';
        ExcelController::create($fileName, $data, $storePath);
        dd("done");
    }

    public function getCollectionProductCount($collectionId)
    {
        $url = 'admin/products/count.json';
        $params = [
            'collection_id' => $collectionId
        ];
        $result = $this->shopifyRequest->view($url, $params);
        return (isset($result['count'])) ? $result['count'] : NULL;
    }

    public function updateCodeLast(ProductSetting $productSetting)
    {
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        $reMigrateProducts = \DB::table('product_relations')->whereNotNull('main_product_id')->where([['main_product_id', '=', '1417753362474'], ['status', 'pending']])->limit(5)->get()->toArray();
//        dd($reMigrateProducts);
        foreach ($reMigrateProducts as $reMigrateProduct) {
            $productId = $reMigrateProduct->product_id;
            $updateProductId = $reMigrateProduct->main_product_id;

            $product = $this->getProductById($productId);
            $updateProduct = $this->getProductById($updateProductId);

            //update sub products
            $chageProduct = [];
            $chageProduct['product_type'] = 'sub_product';
            $chageProduct['tags'] = $updateProduct['handle'] . ', sub-product';
            $chageProduct['published'] = true;
            $params = [
                'product' => $chageProduct
            ];
            $url = '/admin/products/' . $productId . '.json';
            $result = $this->shopifyRequest->update($url, $params);
            if (!isset($result['product'])) {
                dd($result, $chageProduct, $reMigrateProduct);
            }

            //update sub product variants
            $variant = array_first($product['variants']);
            $updateVariant = [
                'id' => $variant['id'],
                'inventory_policy' => 'deny',
                'inventory_management' => 'shopify',
                'inventory_quantity' => 0
            ];

            $url = '/admin/products/' . $productId . '/variants/' . $variant['id'] . '.json';
            $params = [
                'variant' => $updateVariant
            ];
            $updateMainProduct = $this->shopifyRequest->update($url, $params);
            if (!isset($updateMainProduct['variant'])) {
                dd($updateMainProduct, $updateVariant, $reMigrateProduct);
            }

            //delete metafields in sub products
            $metaFields = $this->getMetafieldsOfProductId($productId);
            foreach ($metaFields as $metaField) {
                $url = '/admin/products/' . $productId . '/metafields/' . $metaField['id'] . '.json';
                $this->shopifyRequest->delete($url);
            }

            //update main product tag
            $tags = [];
            $existingTags = explode(',', $updateProduct['tags']);
            $existingTags = array_map('trim', $existingTags);
            $tags = array_merge($tags, $existingTags);
            $newTags = explode(',', $product['tags']);
            $newTags = array_map('trim', $newTags);

            foreach ($newTags as $newTag) {
                if (!in_array($newTag, $existingTags)) {
                    if ($newTag != 'not-visible-individually') {
                        $tags[] = $newTag;
                    }
                }
            }

            $chageProduct = [];
            $chageProduct['tags'] = $tags;
            $params = [
                'product' => $chageProduct
            ];
            $url = '/admin/products/' . $updateProductId . '.json';
            $result = $this->shopifyRequest->update($url, $params);
            if (!isset($result['product'])) {
                dd($result, $reMigrateProduct, $chageProduct);
            }

            \DB::table('product_relations')->where('id', $reMigrateProduct->id)->update([
                'status' => 'solved'
            ]);
            sleep(4);

//            dd("stop", $reMigrateProduct);
        }

        dd("loop is completed", $reMigrateProducts);
    }

    public function update100upVariants(ProductSetting $productSetting)
    {
        dd("stop");
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
//        $this->mainProducts = $this->getMainProducts();
//        $notVisibleProducts = $this->getNotVisibleProducts();
        $reMigrateProducts = \DB::table('product_relations')->whereNotNull('main_product_id')->where([['variant_count', '>', '100'], ['status', 'pending']])->get()->toArray();
//        dd($reMigrateProducts);
        foreach ($reMigrateProducts as $reMigrateProduct) {
            $productId = $reMigrateProduct->product_id;
            $updateProductId = $reMigrateProduct->main_product_id;
            $product = $this->getProductById($productId);
            $updateProduct = $this->getProductById($updateProductId);

            //update sub products
            $chageProduct = [];
            $chageProduct['product_type'] = 'sub_product';
            $chageProduct['tags'] = $updateProduct['handle'] . ', sub-product';
            $chageProduct['published'] = true;
            $params = [
                'product' => $chageProduct
            ];
            $url = '/admin/products/' . $productId . '.json';
            $result = $this->shopifyRequest->update($url, $params);
            if (!isset($result['product'])) {
                dd($result, $chageProduct, $reMigrateProduct);
            }

            //update sub product variants
            $variant = array_first($product['variants']);
            $updateVariant = [
                'id' => $variant['id'],
                'inventory_policy' => 'deny',
                'inventory_management' => 'shopify',
                'inventory_quantity' => 0
            ];

            $url = '/admin/products/' . $productId . '/variants/' . $variant['id'] . '.json';
            $params = [
                'variant' => $updateVariant
            ];
            $updateMainProduct = $this->shopifyRequest->update($url, $params);
            if (!isset($updateMainProduct['variant'])) {
                dd($updateMainProduct, $updateVariant, $reMigrateProduct);
            }

            //delete metafields in sub products
            $metaFields = $this->getMetafieldsOfProductId($productId);
            foreach ($metaFields as $metaField) {
                $url = '/admin/products/' . $productId . '/metafields/' . $metaField['id'] . '.json';
                $this->shopifyRequest->delete($url);
            }


            //update main product
            $tags = [];
            $existingTags = explode(',', $updateProduct['tags']);
            $existingTags = array_map('trim', $existingTags);
            $tags = array_merge($tags, $existingTags);
            $newTags = explode(',', $product['tags']);
            $newTags = array_map('trim', $newTags);

            foreach ($newTags as $newTag) {
                if (!in_array($newTag, $existingTags)) {
                    if ($newTag != 'not-visible-individually') {
                        $tags[] = $newTag;
                    }
                }
            }

            $chageProduct = [];
            $chageProduct['tags'] = $tags;
            $params = [
                'product' => $chageProduct
            ];
            $url = '/admin/products/' . $updateProductId . '.json';
            $this->shopifyRequest->update($url, $params);

            //update main product metafields
            $url = '/admin/products/' . $updateProductId . '/metafields.json';
            $result = $this->shopifyRequest->view($url);
            if (!isset($result['metafields'])) {
                dd($result, "metafieds", $reMigrateProduct);
            }

            $metaFields = $result['metafields'];
            $metaKeys = array_keys(array_column($metaFields, 'key'), 'variant_count');
            $metaField = [];
            if ($metaKeys) {
                $metaField = $metaFields[$metaKeys[0]];
            }

            if (isset($metaField)) {
                $variantCount = $metaField['value'];
                $variantCount = $variantCount + 1;
                $metaFieldId = $metaField['id'];
                $params = [
                    'metafield' => [
                        'id' => $metaFieldId,
                        'value' => $variantCount,
                        'value_type' => 'integer'
                    ]
                ];
                $url = '/admin/products/' . $updateProductId . '/metafields/' . $metaFieldId . '.json';
                $result = $this->shopifyRequest->update($url, $params);
                if (!isset($result['metafield'])) {
                    dd($result, "metafield", $reMigrateProduct, $metaField);
                }
            }
            \DB::table('product_relations')->where('id', $reMigrateProduct->id)->update([
                'status' => 'solved'
            ]);
            sleep(4);
//            dd("stop", $reMigrateProduct);
        }


        dd("done", $reMigrateProducts);
    }

    public function getMetafieldsOfProductId($productId)
    {
        $url = '/admin/products/' . $productId . '/metafields.json';
        $result = $this->shopifyRequest->view($url);
        if (!isset($result['metafields'])) {
            dd($result, "metafieds", $productId);
        }

        return $result['metafields'];
    }

    public function deleteProduct($productId)
    {
        $url = '/admin/products/' . $productId . '.json';
        $this->shopifyRequest->delete($url);
    }

    public function getSearchProductByHandle($handle)
    {
        $url = '/admin/products.json';
        $params = [
            'handle' => $handle,
            'fields' => 'product_type,id,tags'
        ];
        $result = $this->shopifyRequest->view($url, $params);
        return ($result['products']) ? $result['products'][0] : NULL;
    }

    public function getProductById($productId)
    {
        $url = '/admin/products/' . $productId . '.json';

        $result = $this->shopifyRequest->view($url);
        return $result['product'];
    }

    public function getProduct($productId)
    {
        $url = '/admin/products/' . $productId . '.json';
        $params = [
            'fields' => 'product_type,id,tags'
        ];
        $result = $this->shopifyRequest->view($url, $params);
        return $result['product'];
    }

    public function getProductVariants()
    {
        $shopifyVariants = [];
        $url = '/admin/variants.json';
        for ($i = 1; $i < 50; $i++) {
            $params = [
                'limit' => 250,
                'fields' => 'product_id,id,sku,image_id',
                'page' => $i
            ];
            $result = $this->shopifyRequest->view($url, $params);
            $variants = $result['variants'];

            if ($variants) {
                $shopifyVariants = array_merge($shopifyVariants, $variants);
//                foreach ($variants as $variant) {
//                    \DB::table('product_variants')->insert([
//                        'product_id' => $variant['product_id'],
//                        'variant_id' => $variant['id'],
//                        'sku' => $variant['sku']
//                        ]
//                    );
//                }
            } else {
                break;
            }
        }

        return $shopifyVariants;
    }

    public function update1(ProductSetting $productSetting)
    {
//        dd("update product");
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        $this->mainProducts = $this->getMainProducts();
        $notVisibleProducts = $this->getNotVisibleProducts();


        foreach ($notVisibleProducts as $notVisibleProduct) {
            $mainProduct = $this->getMainProduct($notVisibleProduct['title']);

            $productRelation = [];
            $productRelation['product_id'] = $notVisibleProduct['id'];
            $productRelation['product_title'] = $notVisibleProduct['title'];
            $productRelation['product_handle'] = $notVisibleProduct['handle'];
            $productRelation['main_product_id'] = ($mainProduct) ? $mainProduct['id'] : NULL;
            $productRelation['main_product_title'] = ($mainProduct) ? $mainProduct['title'] : NULL;
            $productRelation['main_product_handle'] = ($mainProduct) ? $mainProduct['handle'] : NULL;
            $productRelation['variant_count'] = ($mainProduct) ? count($mainProduct['variants']) : NULL;
            $productRelation['status'] = 'pending';

            \DB::table('product_relations')->insert($productRelation);
        }
        dd("done");
    }

    public function getMainProduct($title)
    {
        $mainProducts = $this->mainProducts;

        foreach ($mainProducts as $mainProduct) {
            $sim = similar_text($title, $mainProduct['title'], $perc);
            $perc = (int) $perc;
//            if($mainProduct['id'] == "1418575446058"){
//                dd($perc, $sim, $title, $mainProduct);
//            }
            if ($perc >= 65) {
                $product = $mainProduct;
                break;
            }
        }

        return (isset($product)) ? $product : NULL;
    }

    public function getMainProducts()
    {
        $shopifyMainProducts = [];

        $url = 'admin/products.json';
        for ($i = 1; $i < 50; $i++) {
            $params = [
                'limit' => 250,
//                'fields' => 'product_id,id,sku',
                'collection_id' => 59263877162,
//                'published_status' => 'unpublished',
                'page' => $i
            ];
            $result = $this->shopifyRequest->view($url, $params);
            $products = $result['products'];

            if ($products) {
                $shopifyMainProducts = array_merge($shopifyMainProducts, $products);
            } else {
                break;
            }
        }
        return $shopifyMainProducts;
    }

    public function getNotVisibleProducts()
    {
        $shopifyNotVisibleProducts = [];

        $url = 'admin/products.json';
        for ($i = 1; $i < 50; $i++) {
            $params = [
                'limit' => 250,
//                'fields' => 'product_id,id,sku',
                'collection_id' => 59141554218,
                'page' => $i
            ];
            $result = $this->shopifyRequest->view($url, $params);
            $products = $result['products'];

            if ($products) {
                $shopifyNotVisibleProducts = array_merge($shopifyNotVisibleProducts, $products);
            } else {
                break;
            }
        }
        return $shopifyNotVisibleProducts;
    }
}
