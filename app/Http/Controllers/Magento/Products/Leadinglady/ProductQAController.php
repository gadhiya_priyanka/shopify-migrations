<?php

namespace App\Http\Controllers\Magento\Products\Leadinglady;

use App\Models\ProjectProductSetting as ProductSetting;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Http\Controllers\Magento\MagentoController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductQAController extends Controller
{
    public $magentoRequest, $shopifyRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create(ProductSetting $productSetting)
    {
//        dd("quest-ans create");
        $project = $productSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $this->magentoRequest = new MagentoController($project);
        $migrateQAs = $this->magentoRequest->getAllMagentoProductQAs();

        $yotpoQAs = [];
        foreach ($migrateQAs as $migrateQA) {
            $yotpoQA = $this->generateYotpoQAObject($migrateQA);
            $yotpoQAs[] = $yotpoQA;
        }

        $fileName = 'll-qa-visible-1';
        $storePath = 'leading-lady/products/qa';
        \App\Http\Controllers\Core\ExcelController::create($fileName, $yotpoQAs, $storePath)->download('csv');
    }

    public function generateYotpoQAObject($migrateQA)
    {
        $answer = $this->magentoRequest->getAnswerOfQue($migrateQA->entity_id);
        $product = \DB::table('project_products')->where(['extra->type' => 'main', 'extra->product_id' => $migrateQA->product_id])->pluck('product')->first();
        $product = json_decode($product, true);

        $yotpoQA = [];
        $yotpoQA['question_id'] = '';
        $yotpoQA['question_content'] = $migrateQA->content;
        $yotpoQA['question_user_email'] = $migrateQA->author_email;
        $yotpoQA['question_user_name'] = $migrateQA->author_name;
        $yotpoQA['question_created_at'] = date('Y-m-d', strtotime($migrateQA->created_at));
        $yotpoQA['product_sku'] = $product['id'];
        $yotpoQA['product_title'] = $product['title'];
        $yotpoQA['product_description'] = '';
        $yotpoQA['product_url'] = 'https://leading-lady-inc.myshopify.com/products/' . $product['handle'];
        $yotpoQA['product_image_url'] = '';
        $yotpoQA['answer_content'] = ($answer) ? $answer->content : '';
        $yotpoQA['answer_user_email'] = ($answer) ? $answer->author_email : '';
        $yotpoQA['answer_user_name'] = ($answer) ? $answer->author_name : '';
        $yotpoQA['answer_created_at'] = ($answer) ? date('Y-m-d', strtotime($answer->created_at)) : '';
        return $yotpoQA;
    }
}
