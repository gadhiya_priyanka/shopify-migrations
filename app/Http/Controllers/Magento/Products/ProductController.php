<?php

namespace App\Http\Controllers\Magento\Products;

use App\Helpers\Helper;
use App\Events\ProductMigrated;
use App\Models\Schedule;
use App\Models\ProjectProduct;
use App\Http\Controllers\Shopify\Api\ProductController as ShopifyProductController;
use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectProductSetting as ProductSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public $magentoRequest, $productSetting, $pageId;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function index()
    {
        
    }

    public function update(ProductSetting $productSetting)
    {
        dd("update magento product controller");
        \Log::info("Script Magento Product Update Start");
        $project = $productSetting->project;
        $this->productSetting = $productSetting;
        $this->magentoRequest = new MagentoController($project);

        $shopifyRequest = new ShopifyProductController($project->shopify_details);

        $migrateProducts = $this->magentoRequest->getProducts();

        $migrateProducts = array_slice($migrateProducts, 2206);

        foreach ($migrateProducts as $migrateProduct) {
            $magentoProId = $migrateProduct->entity_id;
            \Log::info(["mg_product_id" => $magentoProId]);
            $isBaseProduct = $this->magentoRequest->isBaseProduct($magentoProId);
            if (!$isBaseProduct) {
                continue;
            }

            $metaKeyword = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'meta_keyword');
            $searchProduct = $productSetting->products()->where(['extra->product_id' => $magentoProId])->orderBy('created_at', 'asc')->first();
            if ($searchProduct) {
                $searchProduct = $searchProduct->toArray();
            } else {
                \Log::info(["product_not_found_id" => $magentoProId]);
                continue;
            }

            $productId = $searchProduct['product']['id'];
            $metaField = [];
            $metaField['namespace'] = 'migration_app';
            $metaField['key'] = 'meta_keyword';
            $metaField['value'] = ($metaKeyword) ?: '-';
            $metaField['value_type'] = 'string';

            $result = $shopifyRequest->createMetafield($productId, $metaField);
            if (!isset($result['metafield'])) {
                \Log::info(["error_product_id" => [$magentoProId, $productId, $result]]);
            }
        }
        \Log::info("Script Magento Product Update End");
        dd("the end");
    }

    public function create(ProductSetting $productSetting)
    {
        dd("add metafields for extra infromations", "need to check for custom options", "check for weight then start the script");
        $schedule = $productSetting->schedule;
        $schedule->status = 'inprogress';
        $schedule->save();

        $project = $productSetting->project;
        $this->productSetting = $productSetting;
        $this->magentoRequest = new MagentoController($project);
        $shopifyRequest = new ShopifyProductController($project->shopify_details);

        $lastProduct = $productSetting->products()->orderBy('extra->product_id', 'desc')->first();
        $options = [];
        $options = [];
        $options['from'] = 0;
        $options['size'] = 100;
        if ($lastProduct) {
            $options['search'] = [['entity_id', '>', $lastProduct->extra['product_id']]];
        }

        $migrateProducts = $this->magentoRequest->getProducts($options);

        foreach ($migrateProducts as $migrateProduct) {
            \Log::info(["product_id" => $migrateProduct->entity_id]);
            $isBaseProduct = $this->magentoRequest->isBaseProduct($migrateProduct->entity_id);
            if (!$isBaseProduct) {
                continue;
            }
            $foundBaseProduct = TRUE;
            $shopifyProduct = $this->getShopifyProductObject($migrateProduct);
            $shopifyVariants = $shopifyProduct['variants'];
            $shopifyImages = array_column($shopifyProduct['images'], 'src');

            //unset images and quantity and added in new array
            $newShopifyVariants = [];
            foreach ($shopifyVariants as $key => $shopifyVariant) {
                $title = NULL;

                if (isset($shopifyVariant['option1'])) {
                    $title = $shopifyVariant['option1'];
                }
                if (isset($shopifyVariant['option2'])) {
                    $title = $title . ' / ' . $shopifyVariant['option2'];
                }
                if (isset($shopifyVariant['option3'])) {
                    $title = $title . ' / ' . $shopifyVariant['option3'];
                }

                $newShopifyVariant = [];
                $newShopifyVariant['title'] = ($title) ? $title : 'Default Title';
                $newShopifyVariant['inventory_quantity'] = $shopifyVariant['inventory_quantity'];
                $newShopifyVariant['image'] = $shopifyVariant['image'];
                $newShopifyVariants[] = $newShopifyVariant;

                if ($shopifyVariant['image']) {
                    \Log::info([$migrateProduct->entity_id => [$newShopifyVariant['title'], $shopifyVariant['image']]]);
                    foreach ($shopifyVariant['image'] as $imaage) {
                        if (!in_array($imaage['src'], $shopifyImages)) {
                            $shopifyProduct['images'][] = $imaage;
                        }
                    }
                }

                unset($shopifyProduct['variants'][$key]['inventory_quantity']);
                unset($shopifyProduct['variants'][$key]['image']);
            }

            //insert in shopify
            $result = $shopifyRequest->create($shopifyProduct);
            \Log::info(["shopify_id" => $result['product']['id']]);
            //insert in database
            $this->insertProductInDB($result, $migrateProduct);

            //update quantites
            $importedProduct = $result['product'];

            $variantTitles = array_column($newShopifyVariants, 'title');
            $variantTitles = array_map('trim', $variantTitles);

            foreach ($importedProduct['variants'] as $variant) {
                $searchKey = array_search(trim($variant['title']), $variantTitles);
                if (is_numeric($searchKey)) {
                    $variantInventory = [];
                    $variantInventory['available'] = $newShopifyVariants[$searchKey]['inventory_quantity'];
                    $variantInventory['location_id'] = '9081946227';
                    $variantInventory['inventory_item_id'] = $variant['inventory_item_id'];
                    $result = $shopifyRequest->setInventoryLevel($variantInventory);

                    if (!isset($result['inventory_level'])) {
                        \Log::info(["failed_inventory_update" => [$variant['id'], $result]]);
                    }
                } else {
                    \Log::info([$variant['title'], $newShopifyVariants, $variantTitles, $importedProduct['variants']]);
                }
            }

            //update images
            if ($importedProduct['image']) {
                $updateFeaturedImage = [];
                $updateFeaturedImage['id'] = $importedProduct['image']['id'];
                $updateFeaturedImage['variant_ids'] = array_column($importedProduct['variants'], 'id');
                $result = $shopifyRequest->updateImages($updateFeaturedImage, $importedProduct['id']);

                if (!isset($result['image'])) {
                    \Log::info(["failed_images_update" => [$importedProduct['id'], $result]]);
                }
            }
        }

        if (!isset($foundBaseProduct)) {
            $migrateProducts = [];
        }

        if ($migrateProducts) {
            $schedule = $productSetting->schedule;
            $schedule->status = 'paused';
            $schedule->save();
        } else {
            event(new ProductMigrated($productSetting));
            $schedule = $productSetting->schedule;
            $schedule->status = 'completed';
            $schedule->save();
        }
    }

    public function insertProductInDB($result, $migrateProduct, $isOrderProduct = FALSE)
    {
        $extra = [];
        $extra['type'] = (isset($result['product'])) ? 'success' : 'error';

        $extra['product_id'] = ($isOrderProduct) ? $migrateProduct->product_id : $migrateProduct->entity_id;

        $export = [];
        $export['product_id'] = ($isOrderProduct) ? $migrateProduct->product_id : $migrateProduct->entity_id;
        if (isset($result['product'])) {
            $export['shopify_id'] = $result['product']['id'];
        } else {
            $export['error'] = Helper::shopifyError($result);
        }

        $projectProduct = new ProjectProduct;
        $projectProduct->id = \UUID::uuid4()->toString();
        $projectProduct->project_id = $this->productSetting->project->id;
        $projectProduct->product = (isset($result['product'])) ? $result['product'] : ['error' => $result];
        $projectProduct->extra = $extra;
        $projectProduct->export = $export;

        $this->productSetting->products()->save($projectProduct);
    }

    public function getShopifyProductObject($migrateProduct)
    {
        $visible = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'status');
        $visibility = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'visibility');
        $shortDescription = '<div class="short_description">' . $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'short_description') . '</div>';
        $mainDescription = '<div class="main_description">' . $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'description') . '</div>';
        $description = $shortDescription . $mainDescription;

        $metaDescription = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'meta_description');
        if (!$metaDescription) {
            $metaDescription = strip_tags($mainDescription);
        }

        //meta title
        $metaTitle = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'meta_title');


        $shopifyProduct = [];
//        if ($this->productSetting->mapping['migrate_with_old_handle']) {
        $shopifyProduct['handle'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'url_key');
//        }

        $shopifyProduct['title'] = $this->magentoRequest->getProductValueFromAttributeCode($migrateProduct, 'name');
        $shopifyProduct['body_html'] = $description;

        $shopifyProduct['vendor'] = '';
        $shopifyProduct['product_type'] = ($visibility == "1") ? 'not-visible-individually' : '';

        $shopifyProduct['options'] = $this->magentoRequest->generateProductOptions($migrateProduct->entity_id);
        $shopifyProduct['variants'] = $this->magentoRequest->generateProductVariants($migrateProduct);
        $shopifyProduct['images'] = $this->magentoRequest->generateProductImages($migrateProduct);

        $shopifyProduct['tags'] = $this->magentoRequest->generateProductTags($migrateProduct, $shopifyProduct['variants'], $shopifyProduct['options']);
        $shopifyProduct['published'] = ($visible == 1) ? 'true' : 'false';

        //There is No SEO description.
        $shopifyProduct['metafields_global_title_tag'] = ($metaTitle) ? $metaTitle : $shopifyProduct['title'];
        $shopifyProduct['metafields_global_description_tag'] = $metaDescription;

        return $shopifyProduct;
    }
}
