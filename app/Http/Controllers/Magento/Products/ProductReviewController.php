<?php

namespace App\Http\Controllers\Magento\Products;

use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectProductSetting as ProductSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductReviewController extends Controller
{

    public function __construct()
    {
        set_time_limit(0);
    }
    public $magentoRequest, $productSetting, $products;

    public function create(ProductSetting $productSetting)
    {
        $project = $productSetting->project;
        $this->productSetting = $productSetting;
        $this->products = $this->productSetting->projects()->products();
        $this->magentoRequest = new MagentoController($project);
        $migrateReviews = $this->magentoRequest->getAllMagentoProductReviews();

        $shopifyProductReviews = [];

        foreach ($migrateReviews as $migrateReview) {
            $shopifyProductReview = $this->getShopifyProductReviewObject($migrateReview);
            if ($shopifyProductReview) {
                $shopifyProductReviews[] = $shopifyProductReview;
            }
        }

        \App\Http\Controllers\Core\ExcelController::create('product-review', $shopifyProductReviews, 'projects/hanz-de-fuko', 'csv')->download('csv');
    }

    public function getShopifyProductReviewObject($migrateReview)
    {
        $shopifyProduct = $this->products->where(['extra->product_id' => $migrateReview->entity_pk_value])->first();

        $shopifyProductReview = [];
        if ($shopifyProduct) {
            $reviewDetail = $this->magentoRequest->getReviewDetailByReviewId($migrateReview->review_id);
//            $reviewRating = $this->magentoRequest->getReviewRatingByReviewId($migrateReview->review_id);
//            $reviewRating = ($reviewRating) ? "'" . $reviewRating->value . "'" : '';


            $shopifyProductReview['product_handle'] = $shopifyProduct->product['handle'];
            $shopifyProductReview['state'] = ($migrateReview->status_id == 1) ? 'published' : 'unpublished';
            $shopifyProductReview['rating'] = '0';
            $shopifyProductReview['title'] = ($reviewDetail) ? $reviewDetail->title : '';
            $shopifyProductReview['author'] = ($reviewDetail) ? $reviewDetail->nickname : '';
            $shopifyProductReview['email'] = 'harin.dhingra4u@gmail.com';
            $shopifyProductReview['location'] = '';
            $shopifyProductReview['body'] = ($reviewDetail) ? $reviewDetail->detail : '';
            $shopifyProductReview['reply'] = '';
            $shopifyProductReview['created_at'] = $migrateReview->created_at;
            $shopifyProductReview['replied_at'] = '';
        }
        return $shopifyProductReview;
    }
}
