<?php

namespace App\Http\Controllers\Magento\Customers;

use App\Http\Controllers\Core\ExcelController;
use App\Helpers\Helper;
use App\Models\ProjectCustomer;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectCustomerSetting as CustomerSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class llCustomerController extends Controller
{
    public $magentoRequest, $shopifyRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function view(CustomerSetting $customerSetting)
    {
        $successCustomers = $failedCustomers = [];
        $successCustomers = $customerSetting->customers()->where('extra->type', 'success')->pluck('extra')->toArray();
        $failedCustomers = $customerSetting->customers()->where('extra->type', 'error')->pluck('extra')->toArray();

        $storePath = '/app/leading-lady/customers';
        $fileName = 'success-customers-26309';
        ExcelController::create($fileName, $successCustomers, $storePath);
        $fileName = 'failed-customers-26309';
        ExcelController::create($fileName, $failedCustomers, $storePath);
    }

    public function create(CustomerSetting $customerSetting)
    {
        dd("in create customer magento");
        //please check notification then do migrations
        \Log::info("Script Start");
        $project = $customerSetting->project;
        $this->magentoRequest = new MagentoController($project);

        $lastCustomer = $customerSetting->customers()->orderBy('extra->customer_id', 'desc')->first();
        $options = [];
        $options['from'] = 0;
        $options['size'] = 350;
        if ($lastCustomer) {
            $options['search'] = [['entity_id', '>', $lastCustomer->extra['customer_id']]];
        }

        $shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        $migrateCustomers = $this->magentoRequest->getCustomers($options);
//        dd($migrateCustomers);
        foreach ($migrateCustomers as $migrateCustomer) {

            $convertedEmail = $successCustomer = $failedCustomer = FALSE;

            \Log::info(["customer_id" => $migrateCustomer->entity_id]);
            $shopifyCustomer = [];
            $shopifyCustomer = $this->generateShopifyCustomerObject($migrateCustomer);

            $params = [];
            $params = [
                'customer' => $shopifyCustomer
            ];
            $url = '/admin/customers.json';
            $result = $shopifyRequest->create($url, $params);
            if (is_string($result)) {
                $failedCustomer = TRUE;
                $error = Helper::shopifyError($result);
                if (str_contains($error, "email")) {
                    $convertedEmail = Helper::convertValidEmail($shopifyCustomer['email']);
                    $params['customer']['email'] = $convertedEmail;

                    $result = $shopifyRequest->create($url, $params);
                    if (isset($result['customer'])) {
                        $successCustomer = TRUE;
                        $failedCustomer = FALSE;
                    }
                }
            } else {
                $successCustomer = TRUE;
            }

            if ($successCustomer) {
                $importedCustomer = $result['customer'];
                \Log::info(["success" => $importedCustomer['id']]);

                $extra = [];
                $extra['type'] = 'success';
                $extra['success'] = $importedCustomer['id'];
                $extra['email'] = $shopifyCustomer['email'];
                $extra['convert_email'] = ($convertedEmail) ? $importedCustomer['email'] : $shopifyCustomer['email'];
                $extra['customer_id'] = $migrateCustomer->entity_id;

                $this->insertIntoDatabase($extra, $customerSetting, $result);
            }

            if ($failedCustomer) {
                \Log::info(["error" => $error]);

                $extra = [];
                $extra['type'] = 'error';
                $extra['error'] = $error;
                $extra['email'] = $shopifyCustomer['email'];
                $extra['convert_email'] = ($convertedEmail) ? $convertedEmail : $shopifyCustomer['email'];
                $extra['customer_id'] = $migrateCustomer->entity_id;

                $this->insertIntoDatabase($extra, $customerSetting, $result);
            }
        }

        \Log::info("Script End");
    }

    public function insertIntoDatabase($extra, $customerSetting, $result)
    {
        $projectCustomer = new ProjectCustomer;
        $projectCustomer->id = \UUID::uuid4()->toString();
        $projectCustomer->project_id = $customerSetting->project->id;
        $projectCustomer->customer = (is_string($result) ? [] : $result['customer']);
        $projectCustomer->extra = $extra;
        $customerSetting->customers()->save($projectCustomer);
    }

    public function generateShopifyCustomerObject($migrateCustomer)
    {
        $result = $this->generateShopifyCustomerAddressAndTag($migrateCustomer);
        $shopifyCustomer = [];
        $shopifyCustomer['email'] = $migrateCustomer->email;
        $shopifyCustomer['first_name'] = $migrateCustomer->firstname;
        $shopifyCustomer['last_name'] = $migrateCustomer->lastname;
        $shopifyCustomer['tags'] = $result['tags'];
        $shopifyCustomer['addresses'] = $result['addresses'];

        return $shopifyCustomer;
    }

    public function generateShopifyCustomerAddressAndTag($migrateCustomer)
    {
        $shopifyAddresses = [];

        $tags = [];
        $customerAddresses = $this->magentoRequest->getCustomerAddresses($migrateCustomer);
        foreach ($customerAddresses as $customerAddress) {
            $shopifyAddress = [];
            $shopifyAddress['first_name'] = $customerAddress->firstname;
            $shopifyAddress['last_name'] = $customerAddress->lastname;
            $shopifyAddress['address1'] = $customerAddress->street;
//            $shopifyAddress['address2'] = $customerAddress;
            $shopifyAddress['company'] = $customerAddress->company;            
            $shopifyAddress['city'] = $customerAddress->city;
            $shopifyAddress['zip'] = $customerAddress->postcode;
            $shopifyAddress['phone'] = $customerAddress->telephone;
            if ($customerAddress->country_id) {
                $shopifyAddress['province_code'] = $this->magentoRequest->getRegionCodeOfAddress($customerAddress->region_id);
            }
            $shopifyAddress['country_code'] = $customerAddress->country_id;
            $shopifyAddresses[] = $shopifyAddress;

            $tag = ($customerAddress->fax) ? 'fax-' . $customerAddress->fax : NULL;
            if ($tag) {
                $tags[] = $tag;
            }
        }

        $result = [];
        $result['tags'] = $tags;
        $result['addresses'] = $shopifyAddresses;
        return $result;
    }
}
