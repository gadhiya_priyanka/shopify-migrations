<?php

namespace App\Http\Controllers\Magento\Customers;

use App\Events\CustomerMigrated;
use App\Http\Controllers\Core\ExcelController;
use App\Helpers\Helper;
use App\Models\ProjectCustomer;
use App\Http\Controllers\Shopify\Api\CustomerController as ShopifyCustomerController;
use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectCustomerSetting as CustomerSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    public $magentoRequest, $shopifyRequest;

    public function __construct()
    {
        //version 1.9
        set_time_limit(0);
    }

    public function error(CustomerSetting $customerSetting)
    {
        $project = $customerSetting->project;
        $this->magentoRequest = new MagentoController($project);

        $customers = $customerSetting->failedCustomers()->pluck('export')->toArray();

        $exportErrorCustomers = [];
        foreach ($customers as $customer) {

            if (!str_contains($customer['error'], 'has already been taken')) {
                $migrateCustomer = $this->magentoRequest->getCustomerById($customer['customer_id']);
                $shopifyCustomer = $this->getShopifyCustomerObject($migrateCustomer);

                $exportErrorCustomer = [];
                $exportErrorCustomer['customer_id'] = $customer['customer_id'];
                $exportErrorCustomer['error'] = $customer['error'];
                if (str_contains($customer['error'], 'email')) {
                    $exportErrorCustomer['details'] = "Email : " . $shopifyCustomer['email'];
                } else {
                    $exportErrorCustomer['details'] = '';
                }
                $exportErrorCustomer['passing_data'] = json_encode($shopifyCustomer);
                $exportErrorCustomers[] = $exportErrorCustomer;
            }
        }

        ExcelController::create('failed_customer', $exportErrorCustomers, 'storage/projects/hanz-de-fuko', 'xlsx')->download('xlsx');
    }

    public function create(CustomerSetting $customerSetting)
    {
        $schedule = $customerSetting->schedule;
        $schedule->status = 'inprogress';
        $schedule->save();

        //please check notification then do migrations        
        $project = $customerSetting->project;
        $this->magentoRequest = new MagentoController($project);
        $shopifyRequest = new ShopifyCustomerController($project->shopify_details);

        $lastCustomer = $customerSetting->customers()->orderBy('extra->customer_id', 'desc')->first();
        $options = [];
        $options['from'] = 0;
        $options['size'] = 100;
        if ($lastCustomer) {
            $options['search'] = [['entity_id', '>', $lastCustomer->extra['customer_id']]];
        }

        $migrateCustomers = $this->magentoRequest->getCustomers($options);

        foreach ($migrateCustomers as $migrateCustomer) {
            $shopifyCustomer = $this->getShopifyCustomerObject($migrateCustomer);
            $result = $shopifyRequest->create($shopifyCustomer);
            if (is_string($result)) {
                $result = strtolower($result);
                if (str_contains($result, "email")) {
                    $convertedEmail = Helper::convertValidEmail($shopifyCustomer['email']);
                    $shopifyCustomer['email'] = $convertedEmail;
                    $result = $shopifyRequest->create($shopifyCustomer);
                }
            }

            $this->insertCustomerInDB($result, $shopifyCustomer, $migrateCustomer, $customerSetting);
        }

        if ($migrateCustomers) {
            $schedule = $customerSetting->schedule;
            $schedule->status = 'paused';
            $schedule->save();
        } else {
            event(new CustomerMigrated($customerSetting));
            $schedule = $customerSetting->schedule;
            $schedule->status = 'completed';
            $schedule->save();
        }
    }

    public function insertCustomerInDB($result, $shopifyCustomer, $migrateCustomer, $customerSetting)
    {
        $extra = [];
        $extra['type'] = (isset($result['customer'])) ? 'success' : 'error';
        $extra['customer_id'] = $migrateCustomer->entity_id;
        if (isset($result['customer'])) {
            $extra['email'] = $migrateCustomer->email;
            $extra['migrated_email'] = $result['customer']['email'];
        } else {
            $extra['email'] = $migrateCustomer->email;
        }

        $export = [];
        $export['customer_id'] = $migrateCustomer->entity_id;
        if (isset($result['customer'])) {
            $export['shopify_id'] = $result['customer']['id'];
        } else {
            $export['error'] = Helper::shopifyError($result);
            $export['email'] = $migrateCustomer->email;
            $export['customer_details'] = $shopifyCustomer;
        }

        $projectCustomer = new ProjectCustomer;
        $projectCustomer->id = \UUID::uuid4()->toString();
        $projectCustomer->project_id = $customerSetting->project->id;
        $projectCustomer->customer = (isset($result['customer'])) ? $result['customer'] : ['error' => $result];
        $projectCustomer->extra = $extra;
        $projectCustomer->export = $export;

        $customerSetting->customers()->save($projectCustomer);
    }

    public function getShopifyCustomerObject($migrateCustomer)
    {
        $customerId = $migrateCustomer->entity_id;
        $result = $this->generateShopifyCustomerAddressAndTag($migrateCustomer);
        $shopifyCustomer = [];
//        $shopifyCustomer['email'] = 'mahesh+' . $customerId . '@wolfpointagency.com';
        $shopifyCustomer['email'] = $migrateCustomer->email;
        $shopifyCustomer['first_name'] = $this->magentoRequest->getCustomerValueFromAttributeCode($migrateCustomer, 'firstname');
        $shopifyCustomer['last_name'] = $this->magentoRequest->getCustomerValueFromAttributeCode($migrateCustomer, 'lastname');
        $shopifyCustomer['tags'] = implode(',', $result['tags']);
        $shopifyCustomer['addresses'] = $result['addresses'];

        return $shopifyCustomer;
    }

    public function generateShopifyCustomerAddressAndTag($migrateCustomer)
    {
        $tags = [];

        $groupTag = $this->magentoRequest->getCustomerGroup($migrateCustomer->group_id);
        if ($groupTag) {
            $tags[] = 'group-' . $groupTag;
        }

        $customerIdTag = $migrateCustomer->entity_id;
        if ($customerIdTag) {
            $tags[] = 'magento-' . $customerIdTag;
        }

        $shopifyAddresses = [];

        $customerAddresses = $this->magentoRequest->getCustomerAddresses($migrateCustomer);

        foreach ($customerAddresses as $customerAddress) {
            $countryId = $this->magentoRequest->getCustomerAddressValueFromAttributeCode($migrateCustomer, 'country_id');
            $fax = $this->magentoRequest->getCustomerAddressValueFromAttributeCode($migrateCustomer, 'fax');
            $shopifyAddress = [];
            $shopifyAddress['first_name'] = $this->magentoRequest->getCustomerAddressValueFromAttributeCode($migrateCustomer, 'firstname');
            $shopifyAddress['last_name'] = $this->magentoRequest->getCustomerAddressValueFromAttributeCode($migrateCustomer, 'lastname');
            $shopifyAddress['address1'] = $this->magentoRequest->getCustomerAddressValueFromAttributeCode($migrateCustomer, 'street');
//            $shopifyAddress['address2'] = $customerAddress;
            $shopifyAddress['company'] = $this->magentoRequest->getCustomerAddressValueFromAttributeCode($migrateCustomer, 'company');
            $shopifyAddress['city'] = $this->magentoRequest->getCustomerAddressValueFromAttributeCode($migrateCustomer, 'city');
            $shopifyAddress['zip'] = $this->magentoRequest->getCustomerAddressValueFromAttributeCode($migrateCustomer, 'postcode');
            $shopifyAddress['phone'] = $this->magentoRequest->getCustomerAddressValueFromAttributeCode($migrateCustomer, 'telephone');

            if ($countryId) {
                $regionId = $this->magentoRequest->getCustomerAddressValueFromAttributeCode($migrateCustomer, 'region_id');
                $shopifyAddress['province_code'] = $this->magentoRequest->getRegionCodeOfAddress($regionId);
            }
            $shopifyAddress['country_code'] = $countryId;

            $shopifyAddresses[] = $shopifyAddress;

            $tag = ($fax) ? 'fax-' . $fax : NULL;
            if ($tag) {
                $tags[] = $tag;
            }
        }

        $result = [];
        $result['tags'] = $tags;
        $result['addresses'] = $shopifyAddresses;
        return $result;
    }
}
