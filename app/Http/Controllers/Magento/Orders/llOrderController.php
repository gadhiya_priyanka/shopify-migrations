<?php

namespace App\Http\Controllers\Magento\Orders;

use App\Models\ProjectOrder;
use App\Models\ProjectProduct;
use App\Helpers\Helper;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectOrderSetting as OrderSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class llOrderController extends Controller
{
    public $magentoRequest, $shopifyRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function delete($orderSetting)
    {
        dd("stop delete");
        $repeatedOrders = $orderSetting->orders()->select('id', 'order->id', 'extra->order_id')->skip($size)->take(2000)->latest()->get()->toArray();
        $magentoOrderIds = array_column($repeatedOrders, "`extra`->'$.\"order_id\"'");

        $dublicate = array_count_values($magentoOrderIds);
        $data = array_keys($dublicate, 2);
        $project = $orderSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        \Log::info("Script Start");
//        for ($i = 24013; $i <= 41522; $i++) {
        for ($i = 50000; $i <= 60000; $i++) {
            \Log::info(["magento_order_id" => $i]);

            $orders = $orderSetting->orders()->where(['extra->order_id' => $i])->latest()->get()->toArray();

            if (count($orders) > 1) {
                if (count($orders) == 2) {
                    if (!isset($orders[0]['order']['error'])) {
                        $deleteOrderId = $orders[0]['order']['id'];
                        \Log::info(["delete_order_id" => $deleteOrderId]);
                        $url = 'admin/orders/' . $deleteOrderId . '.json';
                        $result = $this->shopifyRequest->delete($url);
                        if (is_string($result)) {
                            sleep(2);
                            $result = $this->shopifyRequest->delete($url);
                            if (is_string($result)) {
                                \Log::info(["error_delete_order_id" => $deleteOrderId . '-' . $result]);
                            }
                        }
                    } else {
                        \Log::info(["delete_order_id" => $orders[0]['order']['error']]);
                    }
                } else {
                    \Log::info(["order_status" => "More than 2"]);
                    \Log::info(["orders" => $orders]);
                    \Log::info(["order_count" => count($orders)]);
                    dd($orders, count($orders));
                }
            } else {
                if (count($orders) == 1) {
                    \Log::info(["order_status" => "only one order-" . count($orders)]);
                } else {
                    \Log::info(["order_status" => "Not found-" . count($orders)]);
                }
            }
        }
        \Log::info("Script End");
    }

    public function create(OrderSetting $orderSetting)
    {


        dd("Stop crate order script");
        $repeatedOrders = $orderSetting->orders()->where('extra->type', 'error')->latest()->pluck('extra')->toArray();
        dd($repeatedOrders);
        $magentoOrderIds = array_column($repeatedOrders, 'order_id');
        $dublicate = array_count_values($magentoOrderIds);
        $removeFromSuccess = array_keys($dublicate, 2);
        dd($removeFromSuccess);
        
        $dublicate = array_count_values($magentoOrderIds);
        $data = array_keys($dublicate, 3);
        dd($data);
        $successOrders = $orderSetting->orders()->where('extra->type', 'success')->latest()->pluck('extra')->toArray();

        $magentoOrderIds = array_column($successOrders, 'order_id');
//        dd($magentoOrderIds);
        $dublicate = array_count_values($magentoOrderIds);
        $removeFromSuccess = array_keys($dublicate, 2);
        foreach ($removeFromSuccess as $value) {
            $orderKeys = array_keys($magentoOrderIds, $value);
            if ($orderKeys) {
                unset($successOrders[$orderKeys[0]]);
            }
        }
        
//        $failedOrders = $orderSetting->orders()->where('extra->type', 'error')->select('extra')->get()->toArray();

        $fileName = 'success-orders';
        $storePath = storage_path('leading-lady/orders');

        \App\Http\Controllers\Core\ExcelController::create($fileName, $successOrders, $storePath)->download('csv');
        dd($successOrders);
//        dd($orderSetting);
//        \Log::useDailyFiles(storage_path() . '/logs/ll/orders/deleted.log');
//        \Log::info("Script Start");

        $project = $orderSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);


        //83186

        $this->delete($orderSetting);
        dd("Please firsts inset all variants, check create date in order table, remove email of mahesh, test and start");
        \Log::useDailyFiles(storage_path() . '/logs/ll/orders/laravel.log');
        \Log::info("Script Start");
        $project = $orderSetting->project;
        $this->productSetting = $project->productSettings()->first();

        $this->magentoRequest = new MagentoController($project);

        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        $lastOrder = $orderSetting->orders()->orderBy('extra->order_id', 'desc')->first();
//        dd($lastOrder);
        $options = [];
        $options['from'] = 0;
        $options['size'] = 100;
        if ($lastOrder) {
            $options['search'] = [['entity_id', '>', $lastOrder->extra['order_id']]];
        }

        $migrateOrders = $this->magentoRequest->getOrders($options);
//        dd($migrateOrders);
        foreach ($migrateOrders as $migrateOrder) {

            $searchOrder = $orderSetting->orders()->where('extra->order_id', $migrateOrder->entity_id)->first();
            if ($searchOrder) {
                \Log::info(['stoped_script_order_id' => $migrateOrder->entity_id]);
                \Log::info("Stop Order Script");
                dd("Stop Order Script");
            }
            \Log::info(['order_id' => $migrateOrder->entity_id]);

            $convertedEmail = $successOrder = $failedOrder = FALSE;

            $shopifyOrder = [];
            $shopifyOrder = $this->generateShopifyOrderObject($migrateOrder);
//            dd($shopifyOrder);
            $url = '/admin/orders.json';
            $params = [
                'order' => $shopifyOrder
            ];

            $result = $this->shopifyRequest->create($url, $params);

            if (is_string($result)) {
                $failedOrder = TRUE;
                $error = Helper::shopifyError($result);
                if (str_contains($error, '502 Bad Gateway')) {
                    sleep(2);
                    $result = $this->shopifyRequest->create($url, $params);
                    if (isset($result['order'])) {
                        $successOrder = TRUE;
                        $failedOrder = FALSE;
                    } else {
                        $error = Helper::shopifyError($result);
                    }
                }

                if ($failedOrder && str_contains($error, "email")) {
                    sleep(2);
                    $convertedEmail = Helper::convertValidEmail($shopifyOrder['email']);
                    $params['order']['email'] = $convertedEmail;

                    $result = $shopifyRequest->create($url, $params);
                    if (isset($result['order'])) {
                        $successOrder = TRUE;
                        $failedOrder = FALSE;
                    } else {
                        $error = Helper::shopifyError($result);
                    }
                }
            } else {
                $successOrder = TRUE;
            }

            if ($successOrder) {
                $importedOrder = $result['order'];
                \Log::info(["success" => $importedOrder['id']]);

                $extra = [];
                $extra['type'] = 'success';
                $extra['order_id'] = $migrateOrder->entity_id;
                $extra['success'] = $importedOrder['id'];
                $extra['email'] = $shopifyOrder['email'];
                $extra['convert_email'] = ($convertedEmail) ? $importedOrder['email'] : $shopifyOrder['email'];

                $this->insertIntoDatabase($extra, $orderSetting, $result);

                if ($migrateOrder->status == "canceled") {
                    sleep(1);
                    $url = '/admin/orders/' . $importedOrder['id'] . '/cancel.json';

                    $params = [];
                    if ($migrateOrder->total_refunded) {
                        $params['amount'] = $migrateOrder->total_refunded;
                    }

                    $resultCancelOrder = $this->shopifyRequest->create($url, $params);
                    if (is_string($resultCancelOrder)) {
                        \Log::info(["error order cancel $migrateOrder->entity_id" => $resultCancelOrder]);
                    } else {
                        $cancelOrderId = $resultCancelOrder['order']['id'];
                        \Log::info(["success order cancel $migrateOrder->entity_id" => $cancelOrderId]);

                        $cancelOrders[] = ['cancel odrer id' => $cancelOrderId];
                    }
                }
            }

            if ($failedOrder) {
                \Log::info(["error" => $error]);

                $extra = [];
                $extra['type'] = 'error';
                $extra['order_id'] = $migrateOrder->entity_id;
                $extra['error'] = $error;
                $extra['email'] = $shopifyOrder['email'];
                $extra['convert_email'] = ($convertedEmail) ? $convertedEmail : $shopifyOrder['email'];

                $this->insertIntoDatabase($extra, $orderSetting, $result);
            }
        }
        \Log::info("Script End");
    }

    public function insertIntoDatabase($extra, $orderSetting, $result)
    {
        $projectOrder = new ProjectOrder;
        $projectOrder->id = \UUID::uuid4()->toString();
        $projectOrder->project_id = $this->productSetting->project->id;
        $projectOrder->order = (isset($result['order'])) ? $result['order'] : ['error' => $result];
        $projectOrder->extra = $extra;
        $orderSetting->orders()->save($projectOrder);
    }

    public function getAllVarinats()
    {
        for ($i = 1; $i <= 35; $i++) {
            $params = [
                'limit' => 250,
                'page' => $i,
                'fields' => 'id,product_id,sku'
            ];

            $url = 'admin/variants.json';
            $result = $this->shopifyRequest->view($url, $params);
            $variants = $result['variants'];
            if ($variants) {
                foreach ($variants as $variant) {
                    $productVariant = [];
                    $productVariant['product_id'] = $variant['product_id'];
                    $productVariant['variant_id'] = $variant['id'];
                    $productVariant['sku'] = $variant['sku'];

                    \DB::table('leadinglady_product_variants')->insert($productVariant);
                }
            } else {
                break;
            }
        }
    }

    public function generateShopifyOrderObject($migrateOrder)
    {
        $lineItems = $this->getLineItems($migrateOrder);
        $discountCodes = $this->getDiscountCodes($migrateOrder);
        $financialStatus = $this->getFinancialStatus($migrateOrder);
        $tags = $this->getTags($migrateOrder);
        $shippingLines = $this->getShippingLines($migrateOrder);
        $taxLines = $this->getTaxLines($migrateOrder);
        $billingAddress = $this->getAddress($migrateOrder->billing_address_id);
        $shippingAddress = $this->getAddress($migrateOrder->shipping_address_id);
//        $date = date("Y-m-d H:i:s", strtotime('+4 hours', strtotime($migrateOrder->created_at)));
        $processedAt = Helper::shopifyDateFormate($migrateOrder->created_at);
        $transactions = $this->getTransactions($migrateOrder);

        $shopifyOrder = [];
        $shopifyOrder['email'] = $migrateOrder->customer_email;
//        $shopifyOrder['email'] = 'mahesh@wolfpointagency.com';
//        $shopifyOrder['email'] = 'akshay@wolfpointagency.com';
        $shopifyOrder['billing_address'] = $billingAddress;
        $shopifyOrder['shipping_address'] = $shippingAddress;
        $shopifyOrder['line_items'] = $lineItems;
        $shopifyOrder['discount_codes'] = $discountCodes;
        if ($migrateOrder->status != "canceled") {
            $shopifyOrder['fulfillment_status'] = 'fulfilled';
        }
        $shopifyOrder['tags'] = $tags;
        $shopifyOrder['shipping_lines'] = $shippingLines;
        $shopifyOrder['tax_lines'] = $taxLines;
        $shopifyOrder['created_at'] = $processedAt;
        $shopifyOrder['transactions'] = $transactions;
        return $shopifyOrder;
    }

    public function getTransactions($migrateOrder)
    {
        $transactions = [];
        if ($migrateOrder->grand_total > 0) {
            $transaction = [];
            $transaction['status'] = 'success';
            $transaction['amount'] = $migrateOrder->grand_total;
            $transactions[] = $transaction;
        }
        return $transactions;
    }

    public function getTaxLines($migrateOrder)
    {
        $taxLines = [];

        $orderTaxLines = $this->magentoRequest->getOrderTaxLines($migrateOrder->entity_id);
        foreach ($orderTaxLines as $orderTaxLine) {
            $taxLine = [];
            if ($orderTaxLine->title) {
                $taxLine['title'] = $orderTaxLine->title;
            }
            $taxLine['price'] = $orderTaxLine->amount;
            $taxLine['rate'] = $orderTaxLine->percent;

            $taxLines[] = $taxLine;
        }
        return $taxLines;
    }

    public function getShippingLines($migrateOrder)
    {
        $shippingLines = [];
        if ($migrateOrder->shipping_amount > 0 && $migrateOrder->shipping_description) {
            $shippingLine = [];
            $shippingLine['code'] = 'Shipping Charge';
            $shippingLine['title'] = $migrateOrder->shipping_description;
            $shippingLine['source'] = 'shopify';
            $shippingLine['price'] = $migrateOrder->shipping_amount;
            $shippingLines[] = $shippingLine;
        }

        return $shippingLines;
    }

    public function getTags($migrateOrder)
    {
        $tags = [];
        $tags[] = 'state-' . $migrateOrder->state;
        $tags[] = 'status-' . $migrateOrder->status;
        $tags[] = 'migrated-oder';
        return $tags;
    }

    public function getFinancialStatus($migrateOrder)
    {
        //financial status
        $pendingStatus = [
            'holded',
            'pending'
        ];

        $paidStatus = [
            'canceled',
            'closed',
            'complete',
            'processing'
        ];

        if (in_array($migrateOrder->status, $paidStatus)) {
            $financialStatus = 'paid';
        } elseif (in_array($migrateOrder->status, $pendingStatus)) {
            $financialStatus = 'pending';
        } else {
            $financialStatus = NULL;
        }

        return $financialStatus;
    }

    public function getDiscountCodes($migrateOrder)
    {
        $discountCodes = [];

        if ($migrateOrder->coupon_code) {
            $discountCode = [];
            $discountCode['amount'] = abs($migrateOrder->discount_amount);
            $discountCode['code'] = $migrateOrder->coupon_code;
            $discountCode['type'] = 'fixed_amount';
            $discountCodes[] = $discountCode;
        }

        return $discountCodes;
    }

    public function getLineItems($migrateOrder)
    {
        $lineItems = [];
        $orderItems = $this->magentoRequest->getOrderItems($migrateOrder->entity_id);
//        dd($orderItems);
        foreach ($orderItems as $key => $orderItem) {
            $variantId = NULL;

            //get product variants            
            $childOrderItem = $this->magentoRequest->getChildOrderIds($orderItem->item_id);
            $productSku = ($childOrderItem) ? $childOrderItem->sku : $orderItem->sku;

//            $variantId = ($productSku == '722286294163') ? '12666600947754' : \DB::table('leadinglady_product_variants')->where('sku', $productSku)->pluck('variant_id')->first();
            $variantId = ($productSku == '722286294163') ? '12666600947754' : \DB::table('current_available_sku')->where('sku', $productSku)->pluck('variant_id')->first();

            if ($variantId) {
                $lineItem = [];
                $lineItem['variant_id'] = $variantId;
                $lineItem['quantity'] = (int) $orderItem->qty_ordered;
                $lineItems[] = $lineItem;
            } else {

                $shopifyProduct = $this->insertProductInStore($orderItem);
                $lineItem = [];
                $lineItem['variant_id'] = $shopifyProduct['variants'][0]['id'];
                $lineItem['quantity'] = (int) $orderItem->qty_ordered;
                $lineItems[] = $lineItem;
            }
        }
//        dd($lineItems);
        return $lineItems;
    }

    public function insertProductInStore($orderItem)
    {
        $childOrderItem = $this->magentoRequest->getChildOrderIds($orderItem->item_id);
        $shopifyProduct = [];

        $product = unserialize($orderItem->product_options);

        $productOptions = [];
        if (isset($product['attributes_info'])) {
            $variants = $product['attributes_info'];
            $productOptions = $this->getProductOptions($variants);
        }

        if (!$productOptions) {
            $information = $product['info_buyRequest'];
            if (isset($product['super_attribute'])) {
                $superAttributes = $product['super_attribute'];
                foreach ($superAttributes as $key => $superAttribute) {

                    $productOption = [];
                    $productOption['name'] = ($key == 80) ? 'Color' : (($key == 121) ? 'Size' : 'Pack Size');
                    $productOptions[] = $productOption;
                }
            }
        }

        //product options
        $productVariants = $this->generateProductVariants($orderItem);

        $shopifyProduct['title'] = (isset($product['simple_name'])) ? $product['simple_name'] : $orderItem->name;
        $shopifyProduct['options'] = $productOptions;
        $shopifyProduct['variants'] = $productVariants;
        $shopifyProduct['product_type'] = 'order-product';
        $shopifyProduct['tags'] = 'order-product';
        $shopifyProduct['published'] = false;

        $url = '/admin/products.json';
        $params = [
            'product' => $shopifyProduct
        ];
        $result = $this->shopifyRequest->create($url, $params);
        $importedProduct = $result['product'];

        //insert in leadinglady_product_variants
        $productVariant = [];
        $productVariant['product_id'] = $importedProduct['id'];
        $productVariant['product_title'] = $importedProduct['title'];
        $productVariant['product_handle'] = $importedProduct['handle'];
        $productVariant['variant_id'] = $importedProduct['variants'][0]['id'];
        $productVariant['sku'] = $importedProduct['variants'][0]['sku'];
//        \DB::table('leadinglady_product_variants')->insert($productVariant);
        \DB::table('current_available_sku')->insert($productVariant);

        $extra = [];
        $extra['type'] = 'order';
        $extra['product_id'] = $orderItem->product_id;

        if ($childOrderItem) {
            $extra['variant_id'] = $childOrderItem->product_id;
        }
        $extra['order_item_id'] = ($childOrderItem) ? $childOrderItem->item_id : $orderItem->item_id;

        //insert into products
        $projectProduct = new ProjectProduct;
        $projectProduct->id = \UUID::uuid4()->toString();
        $projectProduct->project_id = $this->productSetting->project->id;
        $projectProduct->product = $importedProduct;
        $projectProduct->extra = $extra;
        $this->productSetting->products()->save($projectProduct);
        sleep(1);
        return $importedProduct;
    }

    public function generateProductVariants($orderItem)
    {
        $product = unserialize($orderItem->product_options);

        $productVariants = $productVariant = [];

        //generate product variant
        if (isset($product['attributes_info'])) {
            $variants = $product['attributes_info'];
            foreach ($variants as $key => $variant) {
                $newKey = $key + 1;
                $productVariant['option' . $newKey] = $variant['value'];
            }
        }

        if (!$productVariant) {
            $productVariant['option1'] = 'Default Title';
        }

        $productVariant['sku'] = (isset($product['simple_sku'])) ? $product['simple_sku'] : $orderItem->sku;
        $productVariant['grams'] = $orderItem->weight;
        $productVariant['fulfillment_service'] = 'manual';
        $productVariant['price'] = $orderItem->price;
        $productVariant['requires_shipping'] = 'true';
        $productVariants[] = $productVariant;
        return $productVariants;
    }

    public function getProductOptions($variants)
    {
        $productOptions = [];
        foreach ($variants as $variant) {

            //generate product option
            $productOption = [];
            $productOption['name'] = $variant['label'];
            $productOptions[] = $productOption;
        }

        return $productOptions;
    }

    public function getAddress($addressId)
    {
        $customerAddress = [];
        $address = $this->magentoRequest->getOrderAddresses($addressId);
        if ($address) {
            $customerAddress['first_name'] = $address->firstname;
            $customerAddress['last_name'] = $address->lastname;
            $customerAddress['address1'] = $address->street;
//            $billingAddress['address2'] = '';
            if ($address->company) {
                $customerAddress['company'] = $address->company;
            }
            $customerAddress['city'] = $address->city;
            $customerAddress['zip'] = $address->postcode;
            $customerAddress['phone'] = $address->telephone;
            if ($address->country_id) {
                $customerAddress['province_code'] = $this->magentoRequest->getRegionCodeOfAddress($address->region_id);
            }
            $customerAddress['country_code'] = $address->country_id;
        }

        return $customerAddress;
    }
}
