<?php

namespace App\Http\Controllers\Magento\Orders;

use App\Events\OrderMigrated;
use App\Models\Schedule;
use App\Helpers\Helper;
use App\Http\Controllers\Shopify\Api\ProductController as ShopifyProductController;
use App\Http\Controllers\Magento\Products\ProductController;
use App\Http\Controllers\Shopify\Api\OrderController as ShopifyOrderController;
use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectOrder;
use App\Models\ProjectOrderSetting as OrderSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public $magentoRequest, $magentoProduct, $variants, $skus, $shopifyProductRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function delete(OrderSetting $orderSetting)
    {
        dd("order delete script");
        \Log::info("Script Start");
        $project = $orderSetting->project;
        $shopifyRequest = new ShopifyOrderController($project->shopify_details);

        $result = $shopifyRequest->get();

        $orders = $result['orders'];
        foreach ($orders as $order) {
            $shopifyRequest->delete($order['id']);
        }
        \Log::info("Script End");
        dd("Deleted");
    }

    public function create(OrderSetting $orderSetting)
    {
        //test id 563,4
//        dd("Stop crate order script, first test the script using id 4, check disable notifications");
        $schedule = $orderSetting->schedule;
        $schedule->status = 'inprogress';
        $schedule->save();

        $project = $orderSetting->project;
        $this->orderSetting = $orderSetting;
        $products = $project->products()->pluck('product')->toArray();

        $variants = array_column($products, 'variants');
        $this->variants = collect($variants)->collapse()->all();
        $this->skus = array_map('strtolower', array_column($this->variants, 'sku'));

        $this->magentoRequest = new MagentoController($project);
        $shopifyRequest = new ShopifyOrderController($project->shopify_details);

        //migrate product which finds in orders
        $this->magentoProduct = new ProductController;
        $this->magentoProduct->productSetting = $project->productSettings()->first();
        $this->magentoProduct->magentoRequest = $this->magentoRequest;

        //shopify product api controller
        $this->shopifyProductRequest = new ShopifyProductController($project->shopify_details);

        $lastOrder = $orderSetting->orders()->orderBy('extra->order_id', 'desc')->first();
        $options = [];
        $options['from'] = 0;
        $options['size'] = 100;
        if ($lastOrder) {
            $options['search'] = [['entity_id', '>', $lastOrder->extra['order_id']]];
        }

//        $options['search'] = [['entity_id', '>', '1534']];

        $migrateOrders = $this->magentoRequest->getOrders($options);

        foreach ($migrateOrders as $migrateOrder) {
            $shopifyOrder = $this->getShopifyOrderObject($migrateOrder);

            $result = $shopifyRequest->create($shopifyOrder);
            if (is_string($result)) {
                $result = strtolower($result);
                if (str_contains($result, "email")) {
                    $convertedEmail = Helper::convertValidEmail($shopifyOrder['email']);
                    $shopifyOrder['email'] = $convertedEmail;
                    $result = $shopifyRequest->create($shopifyOrder);
                }
            }

            $this->insertOrderInDB($result, $shopifyOrder, $migrateOrder);

            if (is_array($result) && $migrateOrder->status == "canceled") {
                $orderId = $result['order']['id'];
                $cancelOrder = [];
                if ($migrateOrder->total_refunded) {
                    $cancelOrder['amount'] = $migrateOrder->total_refunded;
                }
                $resultCancelOrder = $shopifyRequest->cancel($orderId, $cancelOrder);

                $fulFilledOrder = $shopifyRequest->fulfillments($result['order']['id']);
            }
        }

        if ($migrateOrders) {
            $schedule = $orderSetting->schedule;
            $schedule->status = 'paused';
            $schedule->save();
        } else {
            event(new OrderMigrated($orderSetting));
            $schedule = $orderSetting->schedule;
            $schedule->status = 'completed';
            $schedule->save();
        }
    }

    public function insertOrderInDB($result, $shopifyOrder, $migrateOrder)
    {
        $extra = [];
        $extra['type'] = (isset($result['order'])) ? 'success' : 'error';
        $extra['order_id'] = $migrateOrder->entity_id;

        $export = [];
        $export['order_id'] = $migrateOrder->entity_id;
        $export['order_no'] = $migrateOrder->increment_id;
        if (isset($result['order'])) {
            $export['shopify_id'] = $result['order']['id'];
            $export['shopify_no'] = $result['order']['name'];
        } else {
            $export['error'] = Helper::shopifyError($result);
            $export['customer_email'] = $migrateOrder->customer_email;
            $export['use_email'] = $shopifyOrder['email'];
        }

        $projectOrder = new ProjectOrder;
        $projectOrder->id = \UUID::uuid4()->toString();
        $projectOrder->project_id = $this->orderSetting->project->id;
        $projectOrder->order = (isset($result['order'])) ? $result['order'] : ['error' => $result];
        $projectOrder->extra = $extra;
        $projectOrder->export = $export;

        $this->orderSetting->orders()->save($projectOrder);
    }

    public function getShopifyOrderObject($migrateOrder)
    {
        $lineItems = $this->getLineItems($migrateOrder);
        $discountCodes = $this->getDiscountCodes($migrateOrder);
        $tags = $this->getTags($migrateOrder);
        $shippingLines = $this->getShippingLines($migrateOrder);
        $taxLines = $this->getTaxLines($migrateOrder);
        $billingAddress = $this->getAddress($migrateOrder->billing_address_id);
        $shippingAddress = $this->getAddress($migrateOrder->shipping_address_id);
//        $date = date("Y-m-d H:i:s", strtotime('+4 hours', strtotime($migrateOrder->created_at)));
        $processedAt = Helper::shopifyDateFormate($migrateOrder->created_at);
        $transactions = $this->getTransactions($migrateOrder);

        $shopifyOrder = [];
        $shopifyOrder['email'] = $migrateOrder->customer_email;
//        $shopifyOrder['email'] = 'mahesh@wolfpointagency.com';
//        $shopifyOrder['email'] = 'akshay@wolfpointagency.com';
        $shopifyOrder['billing_address'] = $billingAddress;
        $shopifyOrder['shipping_address'] = $shippingAddress;
        $shopifyOrder['line_items'] = $lineItems;
        $shopifyOrder['discount_codes'] = $discountCodes;
        $shopifyOrder['financial_status'] = 'paid';
        if ($migrateOrder->status != "canceled") {
            $shopifyOrder['fulfillment_status'] = 'fulfilled';
        }
        $shopifyOrder['tags'] = $tags;
        $shopifyOrder['shipping_lines'] = $shippingLines;
        $shopifyOrder['tax_lines'] = $taxLines;
        $shopifyOrder['created_at'] = $processedAt;
        $shopifyOrder['transactions'] = $transactions;
        return $shopifyOrder;
    }

    public function getTransactions($migrateOrder)
    {
        $transactions = [];
        if ($migrateOrder->grand_total > 0) {
            $transaction = [];
            $transaction['status'] = 'success';
            $transaction['amount'] = $migrateOrder->grand_total;
            $transactions[] = $transaction;
        }
        return $transactions;
    }

    public function getTaxLines($migrateOrder)
    {
        $taxLines = [];

        $orderTaxLines = $this->magentoRequest->getOrderTaxLines($migrateOrder->entity_id);
        foreach ($orderTaxLines as $orderTaxLine) {
            $taxLine = [];
            if ($orderTaxLine->title) {
                $taxLine['title'] = $orderTaxLine->title;
            }
            $taxLine['price'] = $orderTaxLine->amount;
            $taxLine['rate'] = $orderTaxLine->percent;

            $taxLines[] = $taxLine;
        }
        return $taxLines;
    }

    public function getShippingLines($migrateOrder)
    {
        $shippingLines = [];
        if ($migrateOrder->shipping_amount > 0 && $migrateOrder->shipping_description) {
            $shippingLine = [];
            $shippingLine['code'] = 'Shipping Charge';
            $shippingLine['title'] = $migrateOrder->shipping_description;
            $shippingLine['source'] = 'shopify';
            $shippingLine['price'] = $migrateOrder->shipping_amount;
            $shippingLines[] = $shippingLine;
        }

        return $shippingLines;
    }

    public function getTags($migrateOrder)
    {
        $tags = [];
        $tags[] = 'state-' . $migrateOrder->state;
        $tags[] = 'status-' . $migrateOrder->status;
        $tags[] = 'magento-b-' . $migrateOrder->entity_id;
        $tags[] = 'magento-f-' . $migrateOrder->increment_id;
        return $tags;
    }

    public function getFinancialStatus($migrateOrder)
    {
        //financial status
        $pendingStatus = [
            'holded',
            'pending'
        ];

        $paidStatus = [
            'canceled',
            'closed',
            'complete',
            'processing'
        ];

        if (in_array($migrateOrder->status, $paidStatus)) {
            $financialStatus = 'paid';
        } elseif (in_array($migrateOrder->status, $pendingStatus)) {
            $financialStatus = 'pending';
        } else {
            $financialStatus = NULL;
        }

        return $financialStatus;
    }

    public function getDiscountCodes($migrateOrder)
    {
        $discountCodes = [];

        if ($migrateOrder->coupon_code) {
            $discountCode = [];
            $discountCode['amount'] = abs($migrateOrder->discount_amount);
            $discountCode['code'] = $migrateOrder->coupon_code;
            $discountCode['type'] = 'fixed_amount';
            $discountCodes[] = $discountCode;
        }

        return $discountCodes;
    }

    public function getLineItems($migrateOrder)
    {
        $lineItems = [];

        $products = $this->magentoRequest->getOrderItems($migrateOrder->entity_id);

        foreach ($products as $product) {
            $childProduct = $this->magentoRequest->getChildOrderIds($product->item_id);
            $productSku = ($childProduct) ? $childProduct->sku : $product->sku;

            $key = array_search(strtolower($productSku), $this->skus);

            if (!is_numeric($key)) {
                $migrateProduct = ($childProduct) ? $childProduct : $product;
                $shopifyProduct = $this->insertProductInStore($product, $childProduct);
                $result = $this->shopifyProductRequest->create($shopifyProduct);
                $this->magentoProduct->insertProductInDB($result, $migrateProduct, TRUE);

                $variants = $result['product']['variants'];

                foreach ($variants as $variant) {
                    $this->variants[] = $variant;
                    $this->skus[] = strtolower($variant['sku']);
                }

                $key = array_search(strtolower($productSku), $this->skus);
            }

            $lineItem = [];
            $lineItem['variant_id'] = $this->variants[$key]['id'];
            $lineItem['quantity'] = (int) $product->qty_ordered;
            $lineItems[] = $lineItem;
        }

        return $lineItems;
    }

    public function insertProductInStore($product, $childProduct)
    {
        $productOptions = unserialize($product->product_options);

        $shopifyProductOptions = $this->getShopifyProductOptions($productOptions);
        $productVariants = $this->generateProductVariants($product, $childProduct, $productOptions);

        $productId = ($childProduct) ? $childProduct->product_id : $product->product_id;

        $productTags = [];
        $productTags[] = 'order-product';
        $productTags[] = 'magento-order-product-' . $productId;

        $shopifyProduct = [];
        $shopifyProduct['title'] = ($childProduct) ? $childProduct->name : $product->name;
        $shopifyProduct['options'] = $shopifyProductOptions;
        $shopifyProduct['variants'] = $productVariants;
        $shopifyProduct['product_type'] = 'order-product';
        $shopifyProduct['tags'] = implode(',', $productTags);
        $shopifyProduct['published'] = false;
        return $shopifyProduct;
    }

    public function getShopifyProductOptions($productOptions)
    {
        $shopifyProductOptions = [];
        if (isset($productOptions['options'])) {
            $variants = $productOptions['options'];
            $shopifyProductOptions = $this->getProductOptions($variants);
        }
        return $shopifyProductOptions;
    }

    public function generateProductVariants($product, $childProduct, $productOptions)
    {
        $productVariants = $productVariant = [];

        //generate product variant
        if (isset($productOptions['options'])) {
            $variants = $productOptions['options'];
            $test = [];
            foreach ($variants as $key => $variant) {
                if (!in_array(trim($variant['label']), $test)) {
                    $newKey = $key + 1;
                    $productVariant['option' . $newKey] = $variant['value'];
                    $test[] = trim($variant['label']);
                }
            }
        }

        $productVariant['sku'] = ($childProduct) ? $childProduct->sku : $product->sku;
        $productVariant['weight'] = ($childProduct) ? $childProduct->weight : $product->weight;
        $productVariant['fulfillment_service'] = 'manual';
        $productVariant['price'] = $product->price;
        $productVariant['requires_shipping'] = 'true';
        $productVariants[] = $productVariant;
        return $productVariants;
    }

    public function getProductOptions($variants)
    {
        $productOptions = [];
        $test = [];
        foreach ($variants as $variant) {
            if (!in_array(trim($variant['label']), $test)) {
                //generate product option
                $productOption = [];
                $productOption['name'] = $variant['label'];
                $productOptions[] = $productOption;
                $test[] = trim($variant['label']);
            }
        }

        return $productOptions;
    }

    public function getAddress($addressId)
    {
        $customerAddress = [];
        $address = $this->magentoRequest->getOrderAddresses($addressId);
        if ($address) {
            $customerAddress['first_name'] = $address->firstname;
            $customerAddress['last_name'] = $address->lastname;
            $customerAddress['address1'] = $address->street;
//            $billingAddress['address2'] = '';
            if ($address->company) {
                $customerAddress['company'] = $address->company;
            }
            $customerAddress['city'] = $address->city;
            $customerAddress['zip'] = $address->postcode;
            $customerAddress['phone'] = $address->telephone;
            if ($address->country_id) {
                $customerAddress['province_code'] = $this->magentoRequest->getRegionCodeOfAddress($address->region_id);
            }
            $customerAddress['country_code'] = $address->country_id;
        }

        return $customerAddress;
    }
}
