<?php

namespace App\Http\Controllers\Magento\Orders;

use App\Helpers\Helper;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectOrderSetting as OrderSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderOriginalController extends Controller
{
    public $magentoRequest, $shopifyRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create(OrderSetting $orderSetting)
    {
//        dd("Please firsts inset all variants, check create date in order table");

        $project = $orderSetting->project;
        $this->magentoRequest = new MagentoController($project);
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

//        $this->getAllVarinats();
//        dd("stop inserted all variants");

        $lastOrder = $orderSetting->orders()->orderBy('extra->order_id', 'desc')->first();
//        dd($lastOrder);
        $options = [];
        $options['from'] = 0;
        $options['size'] = 10;
        if ($lastOrder) {
            $options['search'] = [['entity_id', '>', $lastProduct->extra['order_id']]];
        }

        $migrateOrders = $this->magentoRequest->getOrders($options);

        foreach ($migrateOrders as $migrateOrder) {
            $convertedEmail = $successOrder = $failedOrder = FALSE;

            $shopifyOrder = [];
            $shopifyOrder = $this->generateShopifyOrderObject($migrateOrder);
//            dd($shopifyOrder);
            $url = '/admin/orders.json';
            $params = [
                'order' => $shopifyOrder
            ];

            $result = $this->shopifyRequest->create($url, $params);
//            dd($result);
//            if (is_string($result)) {
//                $failedOrder = TRUE;
//                $error = Helper::shopifyError($result);
//                if (str_contains($error, '502 Bad Gateway')) {
//                    sleep(2);
//                    $result = $this->shopifyRequest->create($url, $params);
//                    if (isset($result['order'])) {
//                        $successOrder = TRUE;
//                        $failedOrder = FALSE;
//                    } else {
//                        $error = Helper::shopifyError($result);
//                    }
//                }
//
//                if ($failedOrder && str_contains($error, "email")) {
//                    sleep(2);
//                    $convertedEmail = Helper::convertValidEmail($shopifyOrder['email']);
//                    $params['order']['email'] = $convertedEmail;
//
//                    $result = $shopifyRequest->create($url, $params);
//                    if (isset($result['order'])) {
//                        $successOrder = TRUE;
//                        $failedOrder = FALSE;
//                    } else {
//                        $error = Helper::shopifyError($result);
//                    }
//                }
//            } else {
//                $successOrder = TRUE;
//            }
            $successOrder = TRUE;
            if ($successOrder) {
                $importedOrder = $result['order'];
                \Log::info(["success" => $importedOrder['id']]);

                $extra = [];
                $extra['type'] = 'success';
                $extra['success'] = $importedOrder['id'];
                $extra['email'] = $shopifyOrder['email'];
                $extra['convert_email'] = ($convertedEmail) ? $importedOrder['email'] : $shopifyOrder['email'];
                $extra['order_id'] = $migrateOrder->entity_id;

//                $this->insertIntoDatabase($extra, $orderSetting, $result);
//                if (isset($shopifyOrder['refunds'])) {
//                    $url = '/admin/orders/' . $importedOrder['id'] . '/refunds.json';
//                    $params = [
//                        'refunds' => $shopifyOrder['refunds']
//                    ];
//                    $resultRefundOrder = $this->shopifyRequest->create($url, $params);
//                    if (is_string($resultRefundOrder)) {
//                        \Log::info(["error order refund $migrateOrder->entity_id" => $resultRefundOrder]);
//                    } else {
//                        $refundOrderId = $resultRefundOrder['order']['id'];
//                        \Log::info(["success order refund $migrateOrder->entity_id" => $refundOrderId]);
//
//                        $refundOrders[] = ['refund odrer id' => $refundOrderId];
//                    }
//                }
                if ($migrateOrder->status == "canceled") {
                    sleep(2);
                    $url = '/admin/orders/' . $importedOrder['id'] . '/cancel.json';


                    $params = [];
                    if ($migrateOrder->total_refunded) {
                        $params['amount'] = $migrateOrder->total_refunded;
                    }

                    $resultCancelOrder = $this->shopifyRequest->create($url, $params);
                    dd($resultCancelOrder);
                    if (is_string($resultCancelOrder)) {
                        \Log::info(["error order cancel $migrateOrder->entity_id" => $resultCancelOrder]);
                    } else {
                        $cancelOrderId = $resultCancelOrder['order']['id'];
                        \Log::info(["success order cancel $migrateOrder->entity_id" => $cancelOrderId]);

                        $cancelOrders[] = ['cancel odrer id' => $cancelOrderId];
                    }
                }

                dd("done");
            }
            dd("its over");

            if ($failedOrder) {
                \Log::info(["error" => $error]);

                $extra = [];
                $extra['type'] = 'error';
                $extra['error'] = $error;
                $extra['email'] = $shopifyOrder['email'];
                $extra['convert_email'] = ($convertedEmail) ? $convertedEmail : $shopifyOrder['email'];
                $extra['order_id'] = $migrateOrder->entity_id;

                $this->insertIntoDatabase($extra, $orderSetting, $result);
            }

            dd("yeah");
        }
        dd("Stop");
//        $migrateProducts = $this->magentoRequest->getProducts($options);
    }

    public function getAllVarinats()
    {
        for ($i = 1; $i <= 35; $i++) {
            $params = [
                'limit' => 250,
                'page' => $i,
                'fields' => 'id,product_id,sku'
            ];

            $url = 'admin/variants.json';
            $result = $this->shopifyRequest->view($url, $params);
            $variants = $result['variants'];
            if ($variants) {
                foreach ($variants as $variant) {
                    $productVariant = [];
                    $productVariant['product_id'] = $variant['product_id'];
                    $productVariant['variant_id'] = $variant['id'];
                    $productVariant['sku'] = $variant['sku'];

                    \DB::table('leadinglady_product_variants')->insert($productVariant);
                }
            } else {
                break;
            }
        }
    }

    public function generateShopifyOrderObject($migrateOrder)
    {
        $lineItems = $this->getLineItems($migrateOrder);
        $discountCodes = $this->getDiscountCodes($migrateOrder);
        $financialStatus = $this->getFinancialStatus($migrateOrder);
        $tags = $this->getTags($migrateOrder);
        $shippingLines = $this->getShippingLines($migrateOrder);
        $taxLines = $this->getTaxLines($migrateOrder);
        $billingAddress = $this->getAddress($migrateOrder->billing_address_id);
        $shippingAddress = $this->getAddress($migrateOrder->shipping_address_id);
//        $date = date("Y-m-d H:i:s", strtotime('+4 hours', strtotime($migrateOrder->created_at)));
        $processedAt = Helper::shopifyDateFormate($migrateOrder->created_at);
        $transactions = $this->getTransactions($migrateOrder);

        $shopifyOrder = [];
//        $shopifyOrder['email'] = $migrateOrder->customer_email;
        $shopifyOrder['email'] = 'mahesh@wolfpointagency.com';
        $shopifyOrder['billing_address'] = $billingAddress;
        $shopifyOrder['shipping_address'] = $shippingAddress;
        $shopifyOrder['line_items'] = $lineItems;
        $shopifyOrder['discount_codes'] = $discountCodes;
        if ($migrateOrder->status != "canceled") {
            $shopifyOrder['fulfillment_status'] = 'fulfilled';
        }
        $shopifyOrder['tags'] = $tags;
        $shopifyOrder['shipping_lines'] = $shippingLines;
        $shopifyOrder['tax_lines'] = $taxLines;
        $shopifyOrder['created_at'] = $processedAt;
        $shopifyOrder['transactions'] = $transactions;
        return $shopifyOrder;
    }

    public function getTransactions($migrateOrder)
    {
        $transactions = [];
        $transaction = [];
        $transaction['status'] = 'success';
        $transaction['amount'] = $migrateOrder->grand_total;
        $transactions[] = $transaction;

        return $transactions;
    }

    public function getTaxLines($migrateOrder)
    {
        $taxLines = [];

        $orderTaxLines = $this->magentoRequest->getOrderTaxLines($migrateOrder->entity_id);
        foreach ($orderTaxLines as $orderTaxLine) {
            $taxLine = [];
            if ($orderTaxLine->title) {
                $taxLine['title'] = $orderTaxLine->title;
            }
            $taxLine['price'] = $orderTaxLine->amount;
            $taxLine['rate'] = $orderTaxLine->percent;

            $taxLines[] = $taxLine;
        }
        return $taxLines;
    }

    public function getShippingLines($migrateOrder)
    {
        $shippingLines = [];
        if ($migrateOrder->shipping_amount > 0 && $migrateOrder->shipping_description) {
            $shippingLine = [];
            $shippingLine['code'] = 'Shipping Charge';
            $shippingLine['title'] = $migrateOrder->shipping_description;
            $shippingLine['source'] = 'shopify';
            $shippingLine['price'] = $migrateOrder->shipping_amount;
            $shippingLines[] = $shippingLine;
        }

        return $shippingLines;
    }

    public function getTags($migrateOrder)
    {
        $tags = [];
        $tags[] = 'state-' . $migrateOrder->state;
        $tags[] = 'status-' . $migrateOrder->status;
        $tags[] = 'migrated-oder';
        return $tags;
    }

    public function getFinancialStatus($migrateOrder)
    {
        //financial status
        $pendingStatus = [
            'holded',
            'pending'
        ];

        $paidStatus = [
            'canceled',
            'closed',
            'complete',
            'processing'
        ];

        if (in_array($migrateOrder->status, $paidStatus)) {
            $financialStatus = 'paid';
        } elseif (in_array($migrateOrder->status, $pendingStatus)) {
            $financialStatus = 'pending';
        } else {
            $financialStatus = NULL;
        }

        return $financialStatus;
    }

    public function getDiscountCodes($migrateOrder)
    {
        $discountCodes = [];

        if ($migrateOrder->discount_amount > 0 && $migrateOrder->coupon_code) {
            $discountCode = [];
            $discountCode['amount'] = $migrateOrder->discount_amount;
            $discountCode['code'] = $migrateOrder->coupon_code;
            $discountCode['type'] = 'fixed_amount';
            $discountCodes[] = $discountCode;
        }

        return $discountCodes;
    }

    public function getLineItems($migrateOrder)
    {
        $lineItems = [];
        $orderItems = $this->magentoRequest->getOrderItems($migrateOrder->entity_id);

        foreach ($orderItems as $key => $orderItem) {
            $variantId = NULL;

            //get product variants            
            $childOrderItem = $this->magentoRequest->getChildOrderIds($orderItem->item_id);
            $productSku = ($childOrderItem) ? $childOrderItem->sku : $orderItem->sku;
            
            $lineItemProduct = $this->magentoRequest->getMagentoProductBySku($productSku);
            if (!$lineItemProduct) {
                $options = unserialize($orderItem->product_options);
                $attributes = $options['info_buyRequest']['super_attribute'];
                $variantSku = $this->magentoRequest->getSkuFromOption($attributes, $orderItem->product_id);

                $lineItemProduct = $this->magentoRequest->getMagentoProductBySku($variantSku);
            }

            if ($lineItemProduct) {
//                products/1417753362474
                if ($lineItemProduct->sku == '722286294163') {
                    $variantId = '12666600947754';
                } else {
                    $variantId = \DB::table('leadinglady_product_variants')->where('sku', $lineItemProduct->sku)->pluck('variant_id')->first();
                }
            }

            if ($variantId) {
                $lineItem = [];
                $lineItem['variant_id'] = $variantId;
                $lineItem['quantity'] = (int) $orderItem->qty_ordered;
                $lineItems[] = $lineItem;
            } else {
                $options = unserialize($orderItem->product_options);
            }
        }
        return $lineItems;
    }

    public function getAddress($addressId)
    {
        $customerAddress = [];
        $address = $this->magentoRequest->getOrderAddresses($addressId);
        if ($address) {
            $customerAddress['first_name'] = $address->firstname;
            $customerAddress['last_name'] = $address->lastname;
            $customerAddress['address1'] = $address->street;
//            $billingAddress['address2'] = '';
            if ($address->company) {
                $customerAddress['company'] = $address->company;
            }
            $customerAddress['city'] = $address->city;
            $customerAddress['zip'] = $address->postcode;
            $customerAddress['phone'] = $address->telephone;
            if ($address->country_id) {
                $customerAddress['province_code'] = $this->magentoRequest->getRegionCodeOfAddress($address->region_id);
            }
            $customerAddress['country_code'] = $address->country_id;
        }

        return $customerAddress;
    }
}
