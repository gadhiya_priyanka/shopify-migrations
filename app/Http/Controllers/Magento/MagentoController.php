<?php

namespace App\Http\Controllers\Magento;

use App\Http\Controllers\Core\CommonFunctionsController;
use App\Helpers\Helper;
use App\Models\Project;
use App\Models\CustomGuzzleHttp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MagentoController extends Controller
{
    public $db, $project, $commonFunctions;

    public function __construct(Project $project)
    {
        $this->project = $project;
        $this->db = CustomGuzzleHttp::connectExternalDatabase($project->platform_details);
        $this->commonFunctions = new CommonFunctionsController;
    }

    public function getMagentoDiscounts()
    {
        return $this->db->table('salesrule')->get()->toArray();
    }

    public function getSaleRuleCoupons($couponId)
    {
        return $this->db->table('salesrule_coupon')->where(['rule_id' => $couponId])->get()->toArray();
    }

    public function getAnswerOfQue($queId)
    {
        return $this->db->table('aw_pq_answer')->where(['question_id' => $queId])->first();
    }

    public function getAllMagentoProductQAs()
    {
        return $this->db->table('aw_pq_question')->where('visibility', 1)->get()->toArray();
    }

    public function getAllMagentoProductReviews()
    {
        return $this->db->table('review')->get()->toArray();
    }

    public function getReviewDetailByReviewId($reviewId)
    {
        return $this->db->table('review_detail')->where(['review_id' => $reviewId])->first();
    }

    public function getReviewRatingByReviewId($reviewId)
    {
        return $this->db->table('rating_option_vote')->where(['review_id' => $reviewId])->first();
    }

    public function getMagentoProduct($id)
    {
        return $this->db->table('catalog_product_entity')->where('row_id', $id)->first();
    }

    public function getSwatchImages()
    {
        return $this->db->table('eav_attribute_option')->where('attribute_id', '80')->get()->toArray();
    }

    public function getSwatches($attributeId)
    {
        return $this->db->table('eav_attribute_option')->where('attribute_id', $attributeId)->get()->toArray();
    }

    public function getImageOfSwatch($optionId)
    {
        return $this->db->table('eav_attribute_option_swatch')->where(['option_id' => $optionId, 'type' => '2'])->first();
    }

    public function getImageOfSwatchColor($optionId)
    {
        return $this->db->table('eav_attribute_option_swatch')->where(['option_id' => $optionId, 'type' => '1'])->first();
    }

    public function getCustomers($options = [])
    {
        $search = (isset($options['search'])) ? $options['search'] : [];
        $query = $this->db->table('customer_entity')->where($search);
        if (isset($options['from'])) {
            $query = $query->offset($options['from']);
        }
        if (isset($options['size'])) {
            $query = $query->limit($options['size']);
        }

        return $query->get()->toArray();
    }

    public function getCustomerCount()
    {
        return $this->db->table('customer_entity')->count();
    }

    public function getCustomerById($customerId)
    {
        return $this->db->table('customer_entity')->where(['entity_id' => $customerId])->first();
    }

    public function getCustomerGroup($groupId)
    {
        $group = $this->db->table('customer_group')->where(['customer_group_id' => $groupId])->first();
        return ($group) ? Helper::handleizedString($group->customer_group_code) : NULL;
    }

    public function getCustomerAddresses($migrateCustomer)
    {
        return $this->db->table('customer_address_entity')->where(['parent_id' => $migrateCustomer->entity_id])->get()->toArray();
    }

    public function getRegionCodeOfAddress($regionId)
    {
        $region = $this->db->table('directory_country_region')->where(['region_id' => $regionId])->first();
        return ($region) ? $region->code : '';
    }

    public function getCustomerValueFromAttributeCode($migrateCustomer, $attributeCode)
    {
        $query = [
            'attribute_code' => $attributeCode,
            'entity_type_id' => 1
        ];
        $tableNamePrefix = 'customer_entity';
        $attribute = $this->getAttributeFromAttributeCode($query);

        $tableName = $this->getTableNameFromAttribute($attribute, $tableNamePrefix);

        $result = $this->db->table($tableName)->where(['entity_id' => $migrateCustomer->entity_id, 'attribute_id' => $attribute->attribute_id])->first();
        return ($result) ? $result->value : NULL;
    }

    public function getCustomerAddressValueFromAttributeCode($migrateCustomer, $attributeCode)
    {
        $query = [
            'attribute_code' => $attributeCode,
            'entity_type_id' => 2
        ];
        $tableNamePrefix = 'customer_address_entity';
        $attribute = $this->getAttributeFromAttributeCode($query);

        $tableName = $this->getTableNameFromAttribute($attribute, $tableNamePrefix);

        $result = $this->db->table($tableName)->where(['entity_id' => $migrateCustomer->entity_id, 'attribute_id' => $attribute->attribute_id])->first();
        return ($result) ? $result->value : NULL;
    }

    public function getCollections()
    {
        return $this->db->table('catalog_category_entity')->get()->toArray();
    }

    public function getSpecialProducts()
    {
        return $this->db->table('catalog_product_relation')->select('parent_id', \DB::raw('count(*) as total'))
                ->groupBy('parent_id')
                ->having('total', '>', 100)
                ->get()
                ->toArray();
    }

    public function getProductName($id)
    {
        return $this->db->table('catalog_product_entity_varchar')->where(['row_id' => $id, 'attribute_id' => 60, 'store_id' => '0'])->first();
    }

    public function getProducts($options = [])
    {
        $search = (isset($options['search'])) ? $options['search'] : [];
        $query = $this->db->table('catalog_product_entity')->where($search);
        if (isset($options['from'])) {
            $query = $query->offset($options['from']);
        }
        if (isset($options['size'])) {
            $query = $query->limit($options['size']);
        }

        return $query->get()->toArray();
    }

    public function getProductCount()
    {
        return $this->db->table('catalog_product_entity')->count();
    }

    public function getOrderTaxLines($magentoOrderId)
    {
        return $this->db->table('sales_order_tax')->where(['order_id' => $magentoOrderId])->get()->toArray();
    }

    public function getOrders($options = [])
    {
        $search = (isset($options['search'])) ? $options['search'] : [];
        $query = $this->db->table('sales_flat_order')->where($search);
        if (isset($options['from'])) {
            $query = $query->offset($options['from']);
        }
        if (isset($options['size'])) {
            $query = $query->limit($options['size']);
        }

        return $query->get()->toArray();
    }

    public function getOrderCount()
    {
        return $this->db->table('sales_flat_order')->count();
    }

    public function getMagentoProductBySku($sku)
    {
        return $this->db->table('catalog_product_entity')->where(['sku' => $sku])->first();
    }

    public function getChildOrderIds($orderItemId)
    {
        return $this->db->table('sales_flat_order_item')->where(['parent_item_id' => $orderItemId])->first();
    }

    public function getOrderItems($orderId)
    {
        return $this->db->table('sales_flat_order_item')->where(['order_id' => $orderId])->whereNull('parent_item_id')->get()->toArray();
    }

    public function getOrderAddresses($orderAddressId)
    {
        return $this->db->table('sales_flat_order_address')->where(['entity_id' => $orderAddressId])->first();
    }

    public function isBaseProduct($migrateProductId)
    {
        $childProduct = $this->db->table('catalog_product_relation')->where('child_id', $migrateProductId)->first();
        return ($childProduct) ? FALSE : TRUE;
    }

    public function checkForImportCollection($migrateCollection)
    {
        return ($migrateCollection->parent_id == 0) ? FALSE : TRUE;
    }

    public function getCollectionValueFromAttributeCode($migrateCollection, $attributeCode)
    {
        $query = [
            'attribute_code' => $attributeCode,
            'entity_type_id' => 3
        ];
        $tableNamePrefix = 'catalog_category_entity';
        $attribute = $this->getAttributeFromAttributeCode($query);

        $tableName = $this->getTableNameFromAttribute($attribute, $tableNamePrefix);

        $result = $this->db->table($tableName)->where(['entity_id' => $migrateCollection->entity_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => '0'])->first();
        if (!$result) {
            $result = $this->db->table($tableName)->where(['entity_id' => $migrateCollection->entity_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => '1'])->first();
        }
        return ($result) ? $result->value : NULL;
    }

    public function getMigrateProduct($sku)
    {
        return $this->db->table('catalog_product_entity')->where('sku', $sku)->first();
    }

    public function getProductValueFromAttributeCode($migrateProduct, $attributeCode)
    {
        $query = [
            'attribute_code' => $attributeCode,
            'entity_type_id' => 4
        ];
        $tableNamePrefix = 'catalog_product_entity';
        $attribute = $this->getAttributeFromAttributeCode($query);

        $tableName = $this->getTableNameFromAttribute($attribute, $tableNamePrefix);
        $result = $this->db->table($tableName)->where(['entity_id' => $migrateProduct->entity_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => '0'])->first();
        return ($result) ? $result->value : NULL;
    }

    public function generateProductTags($migrateProduct, $shopifyVariants, $shopifyOptions)
    {
        $productTags = [];

        //category tag
        $categoryTags = $this->getProductCategoryTag($migrateProduct);
        $productTags = array_merge($productTags, $categoryTags);

        //variant option tag
        $variantOptionTags = $this->getProductVariantOptionTag($shopifyVariants, $shopifyOptions);
        $productTags = array_merge($productTags, $variantOptionTags);

        //price tag
        $priceTags = $this->getProductPriceTag($shopifyVariants);
        $productTags = array_merge($productTags, $priceTags);

        //magento tag
        $productTags[] = 'magento-' . $migrateProduct->entity_id;

        //visibility tag
        $visibilityTags = $this->getProductVisibilityTag($migrateProduct);
        $productTags = array_merge($productTags, $visibilityTags);

        $productTags = array_values(array_unique($productTags));

        return $productTags;
    }

    public function getProductVisibilityTag($migrateProduct)
    {
        $visibilityTags = [];
        $visibility = $this->getProductValueFromAttributeCode($migrateProduct, 'visibility');
        if ($visibility == "1") {
            $visibilityTags[] = 'not-visible-individually';
        }

        return $visibilityTags;
    }

    public function getProductCategoryTag($migrateProduct)
    {
        $categoryTags = [];
        $categories = $this->db->table('catalog_category_product')->where('product_id', $migrateProduct->entity_id)->get()->toArray();

        foreach ($categories as $category) {
            $tag = $this->getCategoryValueFromAttributeCode($category, 'url_key');
            $categoryTags[] = $tag;
        }

        return $categoryTags;
    }

    public function getCategoryValueFromAttributeCode($migrateCategory, $attributeCode)
    {
        $query = [
            'attribute_code' => $attributeCode,
            'entity_type_id' => 3
        ];
        $tableNamePrefix = 'catalog_category_entity';
        $attribute = $this->getAttributeFromAttributeCode($query);

        $tableName = $this->getTableNameFromAttribute($attribute, $tableNamePrefix);
        $result = $this->db->table($tableName)->where(['entity_id' => $migrateCategory->category_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => '0'])->first();
        if (!$result) {
            $result = $this->db->table($tableName)->where(['entity_id' => $migrateCategory->category_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => '1'])->first();
        }
        return ($result) ? $result->value : NULL;
    }

    public function getProductVariantOptionTag($shopifyVariants, $shopifyOptions)
    {
        $variantOptionTags = [];
        foreach ($shopifyVariants as $shopifyVariant) {

            //option 1 tag
            if (isset($shopifyOptions[0])) {
                if (!str_contains(strtolower($shopifyOptions[0]['name']), "size")) {
                    $tag = Helper::handleizedString($shopifyVariant['option1']);
                    $variantOptionTags[] = $tag;
                }
            }

            if (isset($shopifyOptions[1])) {
                if (!str_contains(strtolower($shopifyOptions[1]['name']), "size")) {
                    $tag = Helper::handleizedString($shopifyVariant['option2']);
                    $variantOptionTags[] = $tag;
                }
            }

            if (isset($shopifyOptions[2])) {
                if (!str_contains(strtolower($shopifyOptions[2]['name']), "size")) {
                    $tag = Helper::handleizedString($shopifyVariant['option3']);
                    $variantOptionTags[] = $tag;
                }
            }
        }

        return $variantOptionTags;
    }

    public function getProductPriceTag($shopifyVariants)
    {
        $priceTags = [];
        $prices = array_column($shopifyVariants, 'price');
        $minPrice = (int) min($prices);
        $maxPrice = (int) max($prices);

        $ranges = $this->getRangeArray();

        //minimum range
        foreach ($ranges as $key => $range) {
            if (Helper::inRange($minPrice, $range['min'], $range['max'], TRUE)) {
                $priceTags[] = $range['tag'];
                $minTagKey = $key;
                break;
            }
        }

        //maximum range
        $ranges = array_slice($ranges, $minTagKey);
        foreach ($ranges as $range) {
            if (Helper::inRange($maxPrice, $range['min'], $range['max'], TRUE)) {
                $priceTags[] = $range['tag'];
                break;
            }
        }

        return $priceTags;
    }

    public function getRangeArray()
    {
        //range array
        $ranges = [];

        $range = [];
        $range['min'] = 0;
        $range['max'] = 9;
        $range['tag'] = 'under-9';
        $ranges[] = $range;

        $range = [];
        $range['min'] = 10;
        $range['max'] = 19;
        $range['tag'] = '10-19';
        $ranges[] = $range;

        $range = [];
        $range['min'] = 20;
        $range['max'] = 29;
        $range['tag'] = '20-29';
        $ranges[] = $range;

        $range = [];
        $range['min'] = 30;
        $range['max'] = 39;
        $range['tag'] = '30-39';
        $ranges[] = $range;

        $range = [];
        $range['min'] = 40;
        $range['max'] = 5000;
        $range['tag'] = '40-higher';
        $ranges[] = $range;
        return $ranges;
    }

    public function getCollectionImage($migrateCollection)
    {
        $prefix = 'https://www.piccardmeds4pets.com/upgrade/media/catalog/category/';
        $imageName = $this->getCollectionValueFromAttributeCode($migrateCollection, 'image');
        if ($imageName) {
            $url = $prefix . $imageName;
            $url = $this->commonFunctions->generateProductImageSrc($url, $this->project->id);
        }
        return (isset($url)) ? $url : NULL;
    }

    public function getCollectionWithHandle($migrateCollection)
    {
        return $this->db->table('catalog_category_entity')
                ->join('catalog_category_entity_varchar', 'catalog_category_entity.entity_id', '=', 'catalog_category_entity_varchar.entity_id')
                ->where([
                    ['catalog_category_entity.entity_id', '<', $migrateCollection->entity_id],
                    ['catalog_category_entity_varchar.attribute_id', '=', '43'],
                    ['catalog_category_entity_varchar.store_id', '=', '0']
                ])
                ->pluck('value')->toArray();
    }

    public function getCollectionRules($handle)
    {
        $collectionRules = [];
        $collectionRule = [];
        $collectionRule['column'] = 'tag';
        $collectionRule['relation'] = 'equals';
        $collectionRule['condition'] = $handle;
        $collectionRules[] = $collectionRule;
        return $collectionRules;
    }

    public function generateProductImages($migrateProduct)
    {
        $prefixSrc = 'https://www.piccardmeds4pets.com/upgrade/media/catalog/product';
        $productImages = [];

        $baseImage = $this->getProductValueFromAttributeCode($migrateProduct, 'image');

        $images = $this->db->table('catalog_product_entity_media_gallery')->where(['entity_id' => $migrateProduct->entity_id])->get()->toArray();
        foreach ($images as $image) {
            $productImage = [];
            $imagePath = ($image->value[0] != "/") ? "/" . $image->value : $image->value;
            $url = $prefixSrc . $imagePath;
            $url = $this->commonFunctions->generateProductImageSrc($url, $this->project->id);

            if ($url) {

                $productImage['src'] = $url;
                $productImage['alt'] = $this->getProductValueFromAttributeCode($migrateProduct, 'name');

                $position = $this->db->table('catalog_product_entity_media_gallery_value')->where(['value_id' => $image->value_id])->pluck('position')->first();
                if ($image->value == $baseImage) {
                    if (isset($positions) && in_array(1, $positions)) {
                        $searchKey = array_search('1', $positions);
                        if (is_numeric($searchKey)) {
                            $productImages[$searchKey]['position'] = $position;
                        }
                    }

                    $productImage['position'] = 1;
                } else {
                    $productImage['position'] = $position;
                    $positions[] = $productImage['position'];
                }
                $productImages[] = $productImage;
            }
        }

        return $productImages;
    }

    public function getProductMetaField($migrateProduct, $shopifyProduct)
    {
        $description = $this->getProductValueFromAttributeCode($migrateProduct, 'description');
        $mainDescription = (str_contains($description, '<p><em>')) ? substr($description, 0, strpos($description, '<p><em>')) : $description;

        $quote = (strpos($description, '<p><em>')) ? substr($description, strpos($description, '<p><em>'), strpos($description, '</em></p>') + 9) : '-';


        $quoteAuthor = '';
        $quoteTitle = '';
        if ($quote != '-') {
            $quote = trim(str_replace(['<p><em>', '</em></p>', '</em></p >'], '', $quote));
            $quoteDescription = Helper::between('"', '"', $quote);
            $quoteTitle = Helper::after(',', $quote);
            $quote = trim(str_replace([$quoteDescription, '"'], '', $quote));
            $quoteAuthor = Helper::before(',', $quote);

            $quote = '<p class="quotes">' . $quoteDescription . '</p><h5>- ' . $quoteAuthor . '</h5><p>' . $quoteTitle . '</p>';
        }

        $productMetaFields = [];
        $metaField = [];
        $metaField['key'] = 'style';
        $metaField['value'] = $this->getProductValueFromAttributeCode($migrateProduct, 'style');
        $metaField['value_type'] = 'string';
        $metaField['namespace'] = 'product_manager';
        $productMetaFields[] = $metaField;

        $metaField = [];
        $metaField['key'] = 'description';
        $metaField['value'] = $mainDescription; //description => 61
        $metaField['value_type'] = 'string';
        $metaField['namespace'] = 'product_manager';
        $productMetaFields[] = $metaField;

        $metaField = [];
        $metaField['key'] = 'product_details';
        $metaField['value'] = '-';
        $metaField['value_type'] = 'string';
        $metaField['namespace'] = 'product_manager';
        $productMetaFields[] = $metaField;

        $metaField = [];
        $metaField['key'] = 'quote';
        $metaField['value'] = $quote; //description => 61
        $metaField['value_type'] = 'string';
        $metaField['namespace'] = 'product_manager';
        $productMetaFields[] = $metaField;

        $metaField = [];
        $metaField['key'] = 'fabric';
        $metaField['value'] = $this->getProductValueFromAttributeCode($migrateProduct, 'short_description'); //short_description => 62
        $metaField['value_type'] = 'string';
        $metaField['namespace'] = 'product_manager';
        $productMetaFields[] = $metaField;

        $metaField = [];
        $metaField['key'] = 'variant_count';
        $metaField['value'] = count($shopifyProduct['variants']);
        $metaField['value_type'] = 'integer';
        $metaField['namespace'] = 'product_manager';
        $productMetaFields[] = $metaField;

        return $productMetaFields;
    }

    public function getProductAttributeName($attributeId)
    {
        $attribute = $this->db->table('eav_attribute')->where(['attribute_id' => $attributeId])->first();
        return ($attribute) ? $attribute->frontend_label : NULL;
    }

    public function generateProductOptions($migrationId)
    {
        $options = $this->db->table('catalog_product_super_attribute')->where(['product_id' => $migrationId])->get()->toArray();

        $productOptions = [];
        foreach ($options as $option) {
            $name = $this->getProductAttributeName($option->attribute_id);
            if ($name) {
                $productOption['name'] = ucwords($name);
                $productOptions[] = $productOption;
            }
        }
        return $productOptions;
    }

    public function getAttributeFromAttributeCode($query)
    {
        $attribute = $this->db->table('eav_attribute')->where($query)->first();
        return $attribute;
    }

    public function getTableNameFromAttribute($attribute, $tableNamePrefix)
    {
        return ($attribute->backend_type == "static") ? $tableNamePrefix : $tableNamePrefix . '_' . $attribute->backend_type;
    }

    public function getVariants($magentoId)
    {
        return $this->db->table('catalog_product_relation')->where('parent_id', $magentoId)->get()->toArray();
    }

    public function generateProductVariants($migrateProduct)
    {
        $productVariants = [];
        //child product's variant
        $variants = $this->db->table('catalog_product_relation')->where('parent_id', $migrateProduct->entity_id)->get()->toArray();

        if (!$variants) {
            //parent product variant
            $productVariant = $this->generateShopifyVariant($migrateProduct, $migrateProduct->entity_id, TRUE);
            if ($productVariant) {
                $productVariants[] = $productVariant;
            }
        }

        foreach ($variants as $variant) {
            $productVariant = [];

            $variantDetails = $this->db->table('catalog_product_entity')->where('entity_id', $variant->child_id)->first();
            $productVariant = $this->generateShopifyVariant($variantDetails, $migrateProduct->entity_id);

            $productVariants[] = $productVariant;
        }
        return $productVariants;
    }

    public function generateShopifyVariant($migrateProduct, $superProductMigrationId, $baseProduct = FALSE)
    {
        //product options
        $productOptions = [];
        $options = $this->db->table('catalog_product_super_attribute')->where(['product_id' => $superProductMigrationId])->get()->toArray();
        foreach ($options as $option) {
            $optionAttributeId = $option->attribute_id;
            $attribute = $this->db->table('eav_attribute')->where(['attribute_id' => $optionAttributeId])->pluck('attribute_code')->first();
            $value = $this->getProductValueFromAttributeCode($migrateProduct, $attribute);
            if ($value) {
                $optionValue = $this->db->table('eav_attribute_option_value')->where(['option_id' => $value, 'store_id' => '0'])->first();
                if (!$optionValue) {
                    $optionValue = $this->db->table('eav_attribute_option_value')->where(['option_id' => $value, 'store_id' => '1'])->first();
                }
                $productOptions[] = trim($optionValue->value);
            }
        }

        //compare at price
        $specialPrice = $this->getProductValueFromAttributeCode($migrateProduct, 'special_price');
        $specialToDate = $this->getProductValueFromAttributeCode($migrateProduct, 'special_to_date');
        if ($specialPrice && $specialToDate) {
            if (date('Y-m-d', strtotime($specialToDate)) <= date('Y-m-d')) {
                $specialPrice = NULL;
            }
        }

//        $upc = $this->getProductValueFromAttributeCode($migrateProduct, 'upc');
        $inventoryDetails = $this->getInventoryDetailsOfProduct($migrateProduct->entity_id);
        $visible = $this->getProductValueFromAttributeCode($migrateProduct, 'status');

        $productVariant = [];
        foreach ($productOptions as $key => $productOption) {
            $index = $key + 1;
            $productVariant['option' . $index] = $productOption;
        }

        $productVariant['sku'] = $migrateProduct->sku;
        $productVariant['weight'] = $this->getProductValueFromAttributeCode($migrateProduct, 'weight');
        $productVariant['weight_unit'] = 'lb';
        $productVariant['inventory_quantity'] = ($visible == 1) ? (int) $inventoryDetails->qty : 0; //inventory level
        $productVariant['inventory_management'] = 'shopify';
        $productVariant['inventory_policy'] = 'deny';
        $productVariant['fulfillment_service'] = 'manual';

        $productVariant['price'] = ($specialPrice) ? $specialPrice : $this->getProductValueFromAttributeCode($migrateProduct, 'price');
        if ($specialPrice) {
            $productVariant['compare_at_price'] = $this->getProductValueFromAttributeCode($migrateProduct, 'price');
        }

        $productVariant['requires_shipping'] = 'true';
        $productVariant['image'] = ($baseProduct) ? [] : $this->generateProductImages($migrateProduct);
        return $productVariant;
    }

    public function getValueOfOptions($optionId)
    {
        $storeId = ($optionId == '443' || $optionId == '444') ? '0' : '1';
        $optionValue = $this->db->table('eav_attribute_option_value')->where(['option_id' => $optionId, 'store_id' => $storeId])->first();

        return ($optionValue) ? $optionValue->value : NULL;
    }

    public function getAttributeOptions()
    {
        return $this->db->table('eav_attribute_option_value')->get()->toArray();
    }

    public function getInventoryDetailsOfProduct($productId)
    {
        $inventoryDetails = $this->db->table('cataloginventory_stock_item')->where(['product_id' => $productId])->first();
        return ($inventoryDetails) ? $inventoryDetails : NULL;
    }

    public function getMagentoProductQuantity($shopifyVariant)
    {
        $magentoProduct = $this->getMagentoProductBySku($shopifyVariant['sku']);
        //not-visible-product
        $isBaseProduct = $this->isBaseProduct($magentoProduct);
        if ($isBaseProduct) {
            $tag = $this->getProductVisibilityTag($magentoProduct);
            if ($tag) {
                return 0;
            }
        }

        $inventoryDetail = $this->db->table('cataloginventory_stock_item')->where(['product_id' => $magentoProduct->row_id])->first();
        return ($inventoryDetail) ? (int) $inventoryDetail->qty : 0;
    }

    public function getWeightOfProduct($shopifyVariant)
    {
        $magentoProduct = $this->getMagentoProductBySku($shopifyVariant['sku']);
        return $this->getProductValueFromAttributeCode($magentoProduct, 'weight');
    }

    public function getBaseProducts()
    {
        $mainProducts = $this->db->table('catalog_product_entity')->pluck('row_id')->toArray();
        $childProducts = $this->db->table('catalog_product_relation')->pluck('child_id')->toArray();
        $baseProducts = array_diff($mainProducts, $childProducts);
        return $baseProducts;
    }
}
