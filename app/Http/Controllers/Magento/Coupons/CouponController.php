<?php

namespace App\Http\Controllers\Magento\Coupons;

use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectProductSetting as ProductSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CouponController extends Controller
{
    public $magentoRequest, $shopifyRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create(ProductSetting $productSetting)
    {
//        dd("shopify create", $productSetting);
        $project = $productSetting->project;
        $this->magentoRequest = new MagentoController($project);
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        $migrateRules = $this->magentoRequest->getMagentoDiscounts();

        $exceptionalRules = [];
        $shopifyPriceRules = $shopifyCoupons = $failEntries = $successEntries = $failCouponEntries = $successCouponEntries = [];
        foreach ($migrateRules as $migrateRule) {
            $migrateRuleCoupons = $this->magentoRequest->getSaleRuleCoupons($migrateRule->rule_id);

            if (!$migrateRuleCoupons) {
                $exceptionalRules[] = [
                    "rule_name" => $migrateRule->name
                ];
            }

            $shopifyPriceRule = $this->generateShopifyCoupon($migrateRule);
            $url = '/admin/price_rules.json';
            $params = [
                'price_rule' => $shopifyPriceRule
            ];
            $resultPriceRule = $this->shopifyRequest->create($url, $params);
            if (is_string($resultPriceRule)) {
                $errors = $this->getErrorMessage($resultPriceRule);
                $failEntry = [];
                $failEntry['rule'] = $migrateRule->rule_id;
                $failEntry['rule name'] = $migrateRule->name;
                $failEntry['error'] = $errors;
                $failEntries[] = $failEntry;

                \Log::info(["error rule $migrateRule->rule_id" => $resultPriceRule]);
            } else {
                $priceRuleId = $resultPriceRule['price_rule']['id'];
                $successEntry = [];
                $successEntry['rule'] = $migrateRule->rule_id;
                $successEntry['rule name'] = $migrateRule->name;
                $successEntry['success'] = $priceRuleId;
                $successEntries[] = $successEntry;

                \Log::info(["success rule $migrateRule->rule_id" => $priceRuleId]);

                if ($migrateRuleCoupons) {
                    sleep(1);
                    foreach ($migrateRuleCoupons as $migrateRuleCoupon) {
                        $batchCoupon = [];
                        $batchCoupon['code'] = $migrateRuleCoupon->code;
                        if ($migrateRuleCoupon->usage_limit) {
                            $batchCoupon['usage_count'] = $migrateRuleCoupon->usage_limit;
                        }
                        $batchCoupons[] = $batchCoupon;
                    }

                    $params = [
                        'discount_codes' => $batchCoupons
                    ];

                    $url = '/admin/price_rules/' . $priceRuleId . '/batch.json';
                    $resultBatch = $this->shopifyRequest->create($url, $params);
                    if (is_string($resultBatch)) {
                        $errors = $this->getErrorMessage($resultBatch);

                        $failEntry = [];
                        $failEntry['rule'] = $migrateRule->rule_id;
                        $failEntry['rule name'] = $migrateRule->name;
                        $failEntry['error'] = $errors;
                        $failBatchEntries[] = $failEntry;

                        \Log::info(["error batch $migrateRule->rule_id - $migrateRule->rule_id" => $resultBatch]);
                    } else {

                        $batchId = $resultBatch['discount_code_creation']['id'];
                        $successEntry = [];
                        $successEntry['rule'] = $migrateRule->rule_id;
                        $successEntry['rule name'] = $migrateRule->name;
                        $successEntry['success'] = $batchId;
                        $successBatchEntries[] = $successEntry;

                        \Log::info(["success batch $migrateRule->rule_id - $migrateRule->rule_id" => $batchId]);
                    }
                }
            }
            dd("done");
            sleep(1);
        }
    }

    public function generateShopifyCoupon($migrateRule)
    {
//        dd($migrateRule);
        $shopifyPriceRule = [];
        $shopifyPriceRule['title'] = $migrateRule->name;
        $shopifyPriceRule['target_type'] = ($migrateRule->apply_to_shipping == 1) ? 'shipping_line' : 'line_item';
        $shopifyPriceRule['target_selection'] = 'all';
        $shopifyPriceRule['allocation_method'] = ($shopifyPriceRule['target_type'] == 'shipping_line') ? 'each' : 'across';
        $shopifyPriceRule['value_type'] = ($shopifyPriceRule['target_type'] == 'shipping_line' || $migrateRule->simple_action == 'by_percent') ? 'percentage' : 'fixed_amount';
        $shopifyPriceRule['value'] = ($shopifyPriceRule['target_type'] == 'shipping_line') ? '-100' : '-' . $migrateRule->discount_amount;
//        $shopifyPriceRule['usage_limit'] = '';

        $shopifyPriceRule['once_per_customer'] = ($migrateRule->uses_per_customer == 1) ? true : false;
        $shopifyPriceRule['customer_selection'] = 'all';

        $conditions = unserialize($migrateRule->conditions_serialized);

        if (isset($conditions['conditions'])) {
            $conditions = $conditions['conditions'];

            foreach ($conditions as $condition) {
                if ($condition['attribute'] == 'base_subtotal') {

                    if ($shopifyPriceRule['target_type'] == "line_item") {
                        $shopifyPriceRule['prerequisite_subtotal_range'] = [
                            'greater_than_or_equal_to' => $condition['value']
                        ];
                    } elseif ($shopifyPriceRule['target_type'] == "shipping_line") {
                        $shopifyPriceRule['prerequisite_shipping_price_range'] = [
                            'less_than_or_equal_to' => $condition['value']
                        ];
                    }
                } elseif ($condition['attribute'] == 'country_id') {

                    $countryKey = array_keys(array_column($this->countries, 'code'), $condition['value']);

                    if ($countryKey && $shopifyPriceRule['target_type'] == "shipping_line") {
                        $shopifyPriceRule['entitled_country_ids'] = [
                            $this->countries[$countryKey[0]]['id']
                        ];
                        $shopifyPriceRule['target_selection'] = 'entitled';
                    }
                }
            }
        }

        $shopifyPriceRule['starts_at'] = ($migrateRule->from_date) ? date("c", (strtotime($migrateRule->from_date))) : date("c", time());

        if ($migrateRule->to_date && !$migrateRule->is_active) {
            $shopifyPriceRule['ends_at'] = date("c", (strtotime($migrateRule->to_date)));
        }

        return $shopifyPriceRule;
    }

    public function getErrorMessage($str)
    {
        return substr($str, strpos($str, '{"errors"'));
    }
}
