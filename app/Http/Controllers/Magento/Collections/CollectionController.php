<?php

namespace App\Http\Controllers\Magento\Collections;

use App\Models\ProjectCollection;
use App\Http\Controllers\Shopify\Api\CollectionController as ShopifyCollectionController;
use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectCollectionSetting as CollectionSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CollectionController extends Controller
{
    public $magentoRequest, $shopifyRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create(CollectionSetting $collectionSetting)
    {
        \Log::info("Script Magento Collection Start");

        $project = $collectionSetting->project;
        $this->magentoRequest = new MagentoController($project);
        $shopifyRequest = new ShopifyCollectionController($project->shopify_details);

        $migrateCollections = $this->magentoRequest->getCollections();        
        foreach ($migrateCollections as $migrateCollection) {
            \Log::info(["collection_id" => $migrateCollection->entity_id]);
            //handle
            $handle = $this->magentoRequest->getCollectionValueFromAttributeCode($migrateCollection, 'url_key');
            $handle = $this->getValidCollectionHandle($migrateCollection, $handle);
            //name
            $name = $this->magentoRequest->getCollectionValueFromAttributeCode($migrateCollection, 'name');
            //description
            $description = $this->magentoRequest->getCollectionValueFromAttributeCode($migrateCollection, 'description');
            //image
//            $url = $this->magentoRequest->getCollectionImage($migrateCollection);
//            if ($url) {
//                $image = [];
//                $image['src'] = $url;
//                $image['alt'] = $name;
//            }
            //rules
            $collectionRules = $this->magentoRequest->getCollectionRules($handle);

            //collection object
            $shopifyCollection = [];
            $shopifyCollection['title'] = $name;
            $shopifyCollection['handle'] = $handle;
            $shopifyCollection['body_html'] = $description;
//            if ($url) {
//                $shopifyCollection['image'] = $image;
//            }
            $shopifyCollection['rules'] = $collectionRules;
            $shopifyCollection['disjunctive'] = false;

            $result = $shopifyRequest->createSmartCollection($shopifyCollection);
            $this->insertCollectionInDB($result, $migrateCollection, $collectionSetting);
//            dd("done");
        }
        \Log::info("Script Magento Collection End");
    }

    public function insertCollectionInDB($result, $migrateCollection, $collectionSetting)
    {
        $extra = [];
        $extra['type'] = (isset($result['smart_collection'])) ? 'success' : 'error';
        $extra['collection_id'] = $migrateCollection->entity_id;

        $export = [];
        $export['collection_id'] = $migrateCollection->entity_id;
        if (isset($result['smart_collection'])) {
            $export['shopify_id'] = $result['smart_collection']['id'];
        } else {
            $export['error'] = Helper::shopifyError($result);
        }

        $projectCollection = new ProjectCollection;
        $projectCollection->id = \UUID::uuid4()->toString();
        $projectCollection->project_id = $collectionSetting->project->id;
        $projectCollection->collection = (isset($result['smart_collection'])) ? $result['smart_collection'] : ['error' => $result];
        $projectCollection->extra = $extra;
        $projectCollection->export = $export;

        $collectionSetting->collections()->save($projectCollection);
    }

    public function getValidCollectionHandle($migrateCollection, $handle)
    {
        $handles = $this->magentoRequest->getCollectionWithHandle($migrateCollection);
        $handleKeys = array_keys($handles, $handle);
        if ($handleKeys) {
            $handle = $handle . '-' . count($handleKeys);
        }

        return $handle;
    }

    public function insertCollection($shopifyCollection, $migrateCollection, $collectionSetting)
    {
        $shopifyCollection = $this->insertCollectionUsingShopifyApi($shopifyCollection);
        $this->insertCollectionIntoDatabase($shopifyCollection, $migrateCollection, $collectionSetting);
        return $shopifyCollection;
    }

    public function insertCollectionUsingShopifyApi($shopifyCollection)
    {
        $url = 'admin/smart_collections.json';
        $params = [
            'smart_collection' => $shopifyCollection
        ];
        $result = $this->shopifyRequest->create($url, $params);
        if (!isset($result['smart_collection'])) {
            \Log::info(["reason for fail" => $result, "collection" => $shopifyCollection]);
            return NULL;
            dd($result, $shopifyCollection);
        }

        return $result['smart_collection'];
    }

    public function insertCollectionIntoDatabase($shopifyCollection, $migrateCollection, $collectionSetting)
    {
        $extra = [];
        $extra['collection_id'] = $migrateCollection->row_id;

        $projectCollection = new ProjectCollection;
        $projectCollection->id = \UUID::uuid4()->toString();
        $projectCollection->project_id = $collectionSetting->project->id;
        $projectCollection->collection = $shopifyCollection;
        $projectCollection->extra = $extra;
        $collectionSetting->collections()->save($projectCollection);
    }

    public function update(CollectionSetting $collectionSetting)
    {
        //ed76c1cf-3722-11e8-92d7-54bef7662a73
        $project = $collectionSetting->project;
        $this->magentoRequest = new MagentoController($project);

        $specialProducts = $this->magentoRequest->getSpecialProducts();
        $products = [];
        foreach ($specialProducts as $specialProduct) {
            $product = [];
            $productname = $this->magentoRequest->getProductName($specialProduct->parent_id);
            $product['Name'] = $productname->value;
            $product['Count'] = $specialProduct->total;
            $products[] = $product;
        }

        $fileName = 'leading-lady';
        $storePath = '/leading-lady/speacial-products';
        \App\Http\Controllers\Core\ExcelController::create($fileName, $products, $storePath)->download('csv');
    }
}
