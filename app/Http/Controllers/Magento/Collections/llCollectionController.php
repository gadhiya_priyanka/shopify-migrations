<?php

namespace App\Http\Controllers\Magento\Collections;

use App\Models\ProjectCollection;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectCollectionSetting as CollectionSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class llCollectionController extends Controller
{
    public $magentoRequest, $shopifyRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create(CollectionSetting $collectionSetting)
    {
        \Log::info("Script Start");
        //leading lady migration
        $project = $collectionSetting->project;
        $this->magentoRequest = new MagentoController($project);
        $migrateCollections = $this->magentoRequest->getCollections();
//        dd($migrateCollections);

        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        foreach ($migrateCollections as $migrateCollection) {
            \Log::info(["collection_id" => $migrateCollection->row_id]);

            $isImport = $this->magentoRequest->checkForImportCollection($migrateCollection);

            if ($isImport) {
                //handle
                $handle = $this->magentoRequest->getCollectionValueFromAttributeCode($migrateCollection, 'url_key');

                if (!preg_match('~[0-9]~', $handle)) {

                    $handle = $this->getValidCollectionHandle($migrateCollection, $handle);
                    //name
                    $name = $this->magentoRequest->getCollectionValueFromAttributeCode($migrateCollection, 'name');
                    //description
                    $description = $this->magentoRequest->getCollectionValueFromAttributeCode($migrateCollection, 'description');
                    //image
                    $url = $this->magentoRequest->getCollectionImage($migrateCollection);
                    if ($url) {
                        $image = [];
                        $image['src'] = $url;
                        $image['alt'] = $name;
                    }

                    //rules
                    $collectionRules = $this->magentoRequest->getCollectionRules($handle);

                    //collection object
                    $shopifyCollection = [];
                    $shopifyCollection['title'] = $name;
                    $shopifyCollection['handle'] = $handle;
                    $shopifyCollection['body_html'] = $description;
                    if ($url) {
                        $shopifyCollection['image'] = $image;
                    }
                    $shopifyCollection['rules'] = $collectionRules;
                    $shopifyCollection['disjunctive'] = false;

                    $this->insertCollection($shopifyCollection, $migrateCollection, $collectionSetting);
                }
            }
        }
        \Log::info("Script End");
    }

    public function getValidCollectionHandle($migrateCollection, $handle)
    {
        $handles = $this->magentoRequest->getCollectionWithHandle($migrateCollection);
        $handleKeys = array_keys($handles, $handle);
        if ($handleKeys) {
            $handle = $handle . '-' . count($handleKeys);
        }

        return $handle;
    }

    public function insertCollection($shopifyCollection, $migrateCollection, $collectionSetting)
    {
        $shopifyCollection = $this->insertCollectionUsingShopifyApi($shopifyCollection);
        $this->insertCollectionIntoDatabase($shopifyCollection, $migrateCollection, $collectionSetting);
        return $shopifyCollection;
    }

    public function insertCollectionUsingShopifyApi($shopifyCollection)
    {
        $url = 'admin/smart_collections.json';
        $params = [
            'smart_collection' => $shopifyCollection
        ];
        $result = $this->shopifyRequest->create($url, $params);
        if (!isset($result['smart_collection'])) {
            \Log::info(["reason for fail" => $result, "collection" => $shopifyCollection]);
            return NULL;
            dd($result, $shopifyCollection);
        }

        return $result['smart_collection'];
    }

    public function insertCollectionIntoDatabase($shopifyCollection, $migrateCollection, $collectionSetting)
    {
        $extra = [];
        $extra['collection_id'] = $migrateCollection->row_id;

        $projectCollection = new ProjectCollection;
        $projectCollection->id = \UUID::uuid4()->toString();
        $projectCollection->project_id = $collectionSetting->project->id;
        $projectCollection->collection = $shopifyCollection;
        $projectCollection->extra = $extra;
        $collectionSetting->collections()->save($projectCollection);
    }

    public function update(CollectionSetting $collectionSetting)
    {
        //ed76c1cf-3722-11e8-92d7-54bef7662a73
        $project = $collectionSetting->project;
        $this->magentoRequest = new MagentoController($project);

        $specialProducts = $this->magentoRequest->getSpecialProducts();
        $products = [];
        foreach ($specialProducts as $specialProduct) {
            $product = [];
            $productname = $this->magentoRequest->getProductName($specialProduct->parent_id);
            $product['Name'] = $productname->value;
            $product['Count'] = $specialProduct->total;
            $products[] = $product;
        }

        $fileName = 'leading-lady';
        $storePath = '/leading-lady/speacial-products';
        \App\Http\Controllers\Core\ExcelController::create($fileName, $products, $storePath)->download('csv');
    }
}
