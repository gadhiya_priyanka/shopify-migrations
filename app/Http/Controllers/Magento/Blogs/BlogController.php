<?php

namespace App\Http\Controllers\Magento\Blogs;

use App\Http\Controllers\Core\ExcelController;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Http\Controllers\Magento\MagentoController;
use App\Models\ProjectCollectionSetting as CollectionSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public $magentoRequest, $shopifyRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create(CollectionSetting $collectionSetting)
    {
        dd("blog create");
        \Log::info("start script");
        $project = $collectionSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $shopifyArticles = $this->getArticles();
//        dd($shopifyArticles);
        $specialArticles = [];
        foreach ($shopifyArticles as $shopifyArticle) {

            $wrongSeoArticle = FALSE;
            $isSeoPresent = FALSE;

            $url = 'admin/blogs/' . $shopifyArticle['blog_id'] . '/articles/' . $shopifyArticle['id'] . '/metafields.json';
            $result = $this->shopifyRequest->view($url);
            $metafields = $result['metafields'];
            foreach ($metafields as $metafield) {
                if ($metafield['key'] == "seo_title" || $metafield['key'] == 'seo_description') {
                    if ($metafield['key'] == 'seo_title' && $metafield['value'] == "-") {
                        $wrongSeoArticle = TRUE;
                        break;
                    }

                    if ($metafield['key'] == 'seo_description' && $metafield['value'] == "-") {
                        $wrongSeoArticle = TRUE;
                        break;
                    }
                    $isSeoPresent = TRUE;
                }
            }

            if ($isSeoPresent == FALSE || $wrongSeoArticle == TRUE) {
                $specialArticle = [];
                $specialArticle['blog_id'] = $shopifyArticle['blog_id'];
                $specialArticle['article_id'] = $shopifyArticle['id'];
                $specialArticle['article_title'] = $shopifyArticle['title'];
                $specialArticles[] = $specialArticle;
            }
            sleep(2);
        }

        \Log::info("generate file");
        $storePath = 'leading-lady/articles';
        $fileName = 'special-articles';
        $data = $specialArticles;
        ExcelController::create($fileName, $data, $storePath);

        \Log::info("end script");
        dd("done");
    }

    public function update(CollectionSetting $collectionSetting)
    {
//        dd("blog update");
        \Log::info("start script");
        $project = $collectionSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);
        $fileName = 'amys-corner-export-2018-april-10-2047.csv';
        $filePath = resource_path('assets/leading-lady/blogs/') . $fileName;
        $articles = $this->readCsv($filePath);
        $articleIds = array_column($articles, 'id');

        $migratedBlogs = \DB::table('leading_lady_blogs')->whereNotNull('shopify_blog_id')->get()->toArray();

        foreach ($migratedBlogs as $migratedBlog) {

            \Log::info(["article_id" => $migratedBlog->shopify_blog_id]);

            $arraykey = array_search($migratedBlog->blog_id, $articleIds);
            $seoTitle = ($articles[$arraykey]['yoast_wpseo_title']) ? $articles[$arraykey]['yoast_wpseo_title'] : '-';
            $seoDescription = ($articles[$arraykey]['yoast_wpseo_metadesc']) ? $articles[$arraykey]['yoast_wpseo_metadesc'] : '-';

            $metaField = [];
            $metaField['namespace'] = 'blog';
            $metaField['key'] = 'seo_title';
            $metaField['value'] = $seoTitle;
            $metaField['value_type'] = 'string';
            $url = 'admin/articles/' . $migratedBlog->shopify_blog_id . '/metafields.json';
            $params = [
                'metafield' => $metaField,
            ];

            $result = $this->shopifyRequest->create($url, $params);
            if (!isset($result['metafield'])) {
                sleep(2);
                $result = $this->shopifyRequest->create($url, $params);
                if (!isset($result['metafield'])) {
                    \Log::info(["article_title_error" => $result]);
                }
            }

            $metaField = [];
            $metaField['namespace'] = 'blog';
            $metaField['key'] = 'seo_description';
            $metaField['value'] = $seoDescription;
            $metaField['value_type'] = 'string';

            $url = 'admin/articles/' . $migratedBlog->shopify_blog_id . '/metafields.json';
            $params = [
                'metafield' => $metaField,
            ];

            $result = $this->shopifyRequest->create($url, $params);
            if (!isset($result['metafield'])) {
                sleep(2);
                $result = $this->shopifyRequest->create($url, $params);
                if (!isset($result['metafield'])) {
                    \Log::info(["article_desc_error" => $result]);
                }
            }
            sleep(1);
        }

        \Log::info("end script");
        dd("done");
    }

    public function getArticles()
    {
        $shopifyArticles = [];
        for ($i = 1; $i <= 6; $i++) {
            $url = '/admin/articles.json';

            $params = [
                'limit' => 250,
                'fields' => 'id,blog_id,title',
                'page' => $i
            ];

            $result = $this->shopifyRequest->view($url, $params);
            if ($result['articles']) {
                $shopifyArticles = array_merge($shopifyArticles, $result['articles']);
            } else {
                break;
            }

            sleep(1);
        }

        return $shopifyArticles;
    }

    public function updateResolved(CollectionSetting $collectionSetting)
    {
        \Log::info("start script");
        $project = $collectionSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        $fileName = 'leading_lady_blogs_resolve.csv';
        $filePath = resource_path('assets/leading-lady/blogs/') . $fileName;
        $sheetArticles = $this->readCsv($filePath);

        $fileName = 'amys-corner-export-2018-april-10-2047.csv';
        $filePath = resource_path('assets/leading-lady/blogs/') . $fileName;
        $masterArticles = $this->readCsv($filePath);
        $articleIds = array_column($masterArticles, 'id');
//        dd($articleIds, $sheetArticles, $masterArticles);
        $failedArticles = $blankSeoTitles = $blankSeoDescriptions = [];

        foreach ($sheetArticles as $sheetArticle) {

            $article = [];

            if ($sheetArticle['shopify_blog_id'] != "NULL") {

                $arrayKeys = array_keys($articleIds, $sheetArticle['blog_id']);
                if ($arrayKeys) {
                    if (count($arrayKeys) > 1) {
                        dd($arrayKeys, $sheetArticle);
                    }
                    $article = $masterArticles[$arrayKeys[0]];
                }

                $seoTitle = ($article['yoast_wpseo_title']) ? $article['yoast_wpseo_title'] : '-';
                $seoDescription = ($article['yoast_wpseo_metadesc']) ? $article['yoast_wpseo_metadesc'] : '-';

                $metaField = [];
                $metaField['namespace'] = 'blog';
                $metaField['key'] = 'seo_title';
                $metaField['value'] = $seoTitle;
                $metaField['value_type'] = 'string';

                $url = '/admin/blogs/20648427562/articles/' . $sheetArticle['shopify_blog_id'] . '/metafields.json';
                $params = [
                    'metafield' => $metaField,
                ];
                $result = $this->shopifyRequest->create($url, $params);
                if (!isset($result['metafield'])) {
                    \Log::info(["failed_article" => $article['id'], "error" => "seo-title-" . $result]);
                    $error = [];
                    $error = $article;
                    $error['error'] = $result;
                    $error['error_type'] = 'seo_title';
                    $failedArticles[] = $error;
                }
                sleep(1);

                $metaField = [];
                $metaField['namespace'] = 'blog';
                $metaField['key'] = 'seo_description';
                $metaField['value'] = $seoDescription;
                $metaField['value_type'] = 'string';

                $url = '/admin/blogs/20648427562/articles/' . $sheetArticle['shopify_blog_id'] . '/metafields.json';
                $params = [
                    'metafield' => $metaField,
                ];
                $result = $this->shopifyRequest->create($url, $params);
                if (!isset($result['metafield'])) {
                    \Log::info(["failed_article" => $article['id'], "error" => "seo-description-" . $result]);
                    $error = [];
                    $error = $article;
                    $error['error'] = $result;
                    $error['error_type'] = 'seo_description';
                    $failedArticles[] = $error;
                }

                if ($seoTitle == "-") {
                    $blankSeoTitles[] = $article;
                }

                if ($seoDescription == "-") {
                    $blankSeoDescriptions[] = $article;
                }
            }

            unset($sheetArticle['id']);
            unset($sheetArticle['created_at']);
            unset($sheetArticle['updated_at']);
            if ($sheetArticle['shopify_blog_id'] == "NULL") {
                unset($sheetArticle['shopify_blog_id']);
            }

            \DB::table('leading_lady_blogs_resolve')->insert($sheetArticle);
            sleep(2);
        }

        \Log::info("generate file");
        $storePath = 'leading-lady/blogs';

        $fileName = 'amys-corner-recover-failed';
        $data = $failedArticles;
        ExcelController::create($fileName, $data, $storePath);

        $fileName = 'amys-corner-recover-blank-title';
        $data = $blankSeoTitles;
        ExcelController::create($fileName, $data, $storePath);

        $fileName = 'amys-corner-recover-blank-desc';
        $data = $blankSeoDescriptions;
        ExcelController::create($fileName, $data, $storePath);
        \Log::info("end script");
        dd("done");
    }

    public function updateBlogByWpSheet(CollectionSetting $collectionSetting)
    {
//        dd("blog update");
//        \Log::info("start script");

        $project = $collectionSetting->project;
        $this->shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        $fileName = 'leading-lady-blogs-all.csv';
        $filePath = resource_path('assets/leading-lady/blogs/') . $fileName;
        $articles = $this->readCsv($filePath);
        dd($articles);
        $failedArticles = $blankSeoTitles = $blankSeoDescriptions = [];

        foreach ($articles as $article) {

            $handle = $article['slug'];
            $handle = str_replace("%e2%80%9c", '', $handle);
            $handle = str_replace("%e2%80%9d", '', $handle);
            $handle = str_replace("%e2%80%99", '-', $handle);
            $shopifyArticle = $this->searchArticle($handle);
            sleep(1);

            if ($shopifyArticle) {
                $seoTitle = ($article['yoast_wpseo_title']) ? $article['yoast_wpseo_title'] : '-';
                $seoDescription = ($article['yoast_wpseo_metadesc']) ? $article['yoast_wpseo_metadesc'] : '-';

                $metaField = [];
                $metaField['namespace'] = 'blog';
                $metaField['key'] = 'seo_title';
                $metaField['value'] = $seoTitle;
                $metaField['value_type'] = 'string';

                $url = '/admin/blogs/' . $shopifyArticle['blog_id'] . '/articles/' . $shopifyArticle['id'] . '/metafields.json';
                $params = [
                    'metafield' => $metaField,
                ];
                $result = $this->shopifyRequest->create($url, $params);
                if (!isset($result['metafield'])) {
                    \Log::info(["failed_article" => $article['id'], "error" => "seo-title-" . $result]);
                    $error = [];
                    $error = $article;
                    $error['error'] = $result;
                    $error['error_type'] = 'seo_title';
                    $failedArticles[] = $error;
                }
                sleep(1);

                $metaField = [];
                $metaField['namespace'] = 'blog';
                $metaField['key'] = 'seo_description';
                $metaField['value'] = $seoDescription;
                $metaField['value_type'] = 'string';

                $url = '/admin/blogs/' . $shopifyArticle['blog_id'] . '/articles/' . $shopifyArticle['id'] . '/metafields.json';
                $params = [
                    'metafield' => $metaField,
                ];
                $result = $this->shopifyRequest->create($url, $params);
                if (!isset($result['metafield'])) {
                    \Log::info(["failed_article" => $article['id'], "error" => "seo-description-" . $result]);
                    $error = [];
                    $error = $article;
                    $error['error'] = $result;
                    $error['error_type'] = 'seo_description';
                    $failedArticles[] = $error;
                }

                if ($seoTitle == "-") {
                    $blankSeoTitles[] = $article;
                }

                if ($seoDescription == "-") {
                    $blankSeoDescriptions[] = $article;
                }
            }

            $updateArticle = [];
            $updateArticle['blog_id'] = $article['id'];
            $updateArticle['blog_title'] = $article['blog_title'];
            $updateArticle['blog_magento_slug'] = $article['slug'];
            $updateArticle['blog_handle'] = $handle;

            if ($shopifyArticle) {
                $updateArticle['shopify_blog_id'] = $shopifyArticle['id'];
                $updateArticle['shopify_blog_title'] = $shopifyArticle['title'];
                $updateArticle['shopify_blog_handle'] = $shopifyArticle['handle'];
            }

            \DB::table('all_leadinglady_blogs')->insert($updateArticle);
            sleep(2);
        }

        \Log::info("generate file");
        $storePath = 'leading-lady/blogs';

        $fileName = 'full-figure-failed';
        $data = $failedArticles;
        ExcelController::create($fileName, $data, $storePath);

        $fileName = 'full-figure-blank-title';
        $data = $blankSeoTitles;
        ExcelController::create($fileName, $data, $storePath);

        $fileName = 'full-figure-blank-desc';
        $data = $blankSeoDescriptions;
        ExcelController::create($fileName, $data, $storePath);
        \Log::info("start end");
        dd("done");
    }

    public function searchArticle($handle)
    {
        $url = 'admin/articles.json';
        $params = [
            'handle' => $handle
        ];
        $result = $this->shopifyRequest->view($url, $params);
        if (isset($result['articles'])) {
            $articles = ($result['articles']) ? $result['articles'][0] : NULL;
        } else {
            \Log::info(["article-error" => $result, 'article_hanlde' => $handle]);
            $articles = NULL;
        }

        return $articles;
    }

    public function readCsv($filePath)
    {
        return ExcelController::view($filePath);
    }
}
