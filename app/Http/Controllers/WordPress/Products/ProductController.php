<?php

namespace App\Http\Controllers\WordPress\Products;

use App\Helpers\Helper;
use App\Http\Controllers\WordPress\WordPressController;
use App\Models\ShopifyProduct;
use App\Models\ProjectProductSetting as ProductSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    protected $wpRequest;
    protected $productSettings;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create(ProductSetting $product)
    {
        $this->product;
        $this->productSettings = $product;

        $this->wpRequest = new WordPressController($this->productSettings->project->platform_details);
        $migrateProducts = $this->wpRequest->getPosts('product');
        $migrateProducts = array_slice($migrateProducts, 9);


//        dd($migrateProducts);
//        $results = $this->productSettings->shopifyProducts()->where('product->Title', 'AUTO-DRAFT')->get();
//        $results = $this->productSettings->shopifyProducts()->where('product->Title', $migrateProduct->post_name)->get();
//        dd($results->isEmpty());
//        
        foreach ($migrateProducts as $migrateProduct) {
            $isVariant = FALSE;
            $product = $this->generateShopifyProductObject($migrateProduct, $isVariant);
            dd($product);
            
            $shopifyProduct = new ShopifyProduct;
            $shopifyProduct->id = \UUID::uuid4()->toString();
            $shopifyProduct->project_id = $this->productSettings->project->id;
            $shopifyProduct->product = $product;
            $this->productSettings->shopifyProducts()->save($shopifyProduct);
        }
        dd("completed");
    }

    public function generateShopifyProductObject($migrateProduct, $isVariant)
    {

        $variants = $this->wpRequest->getProductVariants($migrateProduct);
        dd($variants);
        $productTags = [];
        $productTags = $this->wpRequest->getTermValueOfPost($migrateProduct, ['product_tag', 'product_cat']);

        $product = [];

        $product['Handle'] = $this->getProductHandle($migrateProduct);
        $product['Title'] = ($isVariant) ? '' : $migrateProduct->post_title;
        $product['Body (HTML)'] = ($isVariant) ? '' : (($migrateProduct->post_content) ? $migrateProduct->post_content : $migrateProduct->post_excerpt);
        $product['Vendor'] = ($isVariant) ? '' : ((isset($this->productSettings->mapping->vendor)) ? $this->productSettings->mapping->vendor : $this->productSettings->project->name);
        $product['Type'] = ($isVariant) ? '' : ((isset($this->productSettings->mapping->type)) ? $this->productSettings->mapping->type : '');
        $product['Tags'] = ($isVariant) ? '' : implode(',', $productTags);
        $product['Published'] = ($isVariant) ? '' : (($migrateProduct['post_status'] == "publish") ? 'true' : 'false');

        $product['Option1 Name'] = ($isVariant) ? '' : '';
        $product['Option1 Value'] = '';
        $product['Option2 Name'] = ($isVariant) ? '' : '';
        $product['Option2 Value'] = '';
        $product['Option3 Name'] = ($isVariant) ? '' : '';
        $product['Option3 Value'] = '';

        $product['Variant SKU'] = '';
        $product['Variant Grams'] = '';
        $product['Variant Inventory Tracker'] = '';
        $product['Variant Inventory Qty'] = '';
        $product['Variant Inventory Policy'] = '';
        $product['Variant Fulfillment Service'] = '';
        $product['Variant Price'] = '';
        $product['Variant Compare At Price'] = '';
        $product['Variant Requires Shipping'] = '';
        $product['Variant Taxable'] = '';
        $product['Variant Barcode'] = '';

        $product['Image Src'] = '';
        $product['Image Position'] = '';
        $product['Image Alt Text'] = '';

        $product['Gift Card'] = ($isVariant) ? '' : '';

        $product['SEO Title'] = ($isVariant) ? '' : '';
        $product['SEO Description'] = ($isVariant) ? '' : '';

        $product['Google Shopping / Google Product Category'] = ($isVariant) ? '' : '';
        $product['Google Shopping / Gender'] = ($isVariant) ? '' : '';
        $product['Google Shopping / Age Group'] = ($isVariant) ? '' : '';
        $product['Google Shopping / MPN'] = ($isVariant) ? '' : '';
        $product['Google Shopping / AdWords Grouping'] = ($isVariant) ? '' : '';
        $product['Google Shopping / AdWords Labels'] = ($isVariant) ? '' : '';
        $product['Google Shopping / Condition'] = ($isVariant) ? '' : '';
        $product['Google Shopping / Custom Product'] = ($isVariant) ? '' : '';
        $product['Google Shopping / Custom Label 0'] = ($isVariant) ? '' : '';
        $product['Google Shopping / Custom Label 1'] = ($isVariant) ? '' : '';
        $product['Google Shopping / Custom Label 2'] = ($isVariant) ? '' : '';
        $product['Google Shopping / Custom Label 3'] = ($isVariant) ? '' : '';
        $product['Google Shopping / Custom Label 4'] = ($isVariant) ? '' : '';
        $product['Variant Image'] = '';
        $product['Variant Weight Unit'] = '';
        $product['Variant Tax Code'] = '';


        return $product;
    }

    public function getProductHandle($migrateProduct)
    {
        $productHandle = ($migrateProduct->post_name) ? $migrateProduct->post_name : Helper::handleizedString($migrateProduct->post_title);

        $results = $this->productSettings->shopifyProducts()->where('product->Title', $migrateProduct->post_title)->get()->toArray();
        if ($results) {
            if ($results[0]['product']['Handle'] == $productHandle) {
                $productHandle = $productHandle . '-' . count($results);
            }
        }

        return $productHandle;
    }
}
