<?php

namespace App\Http\Controllers\WordPress;

use App\Models\CustomGuzzleHttp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WordPressController extends Controller
{
    public $db, $prefix;

    public function __construct($dbDetails)
    {
        $this->db = CustomGuzzleHttp::connectExternalDatabase($dbDetails);
        $this->prefix = $dbDetails['table_prefix'] . '_';
    }

    public function getPublishArticles($from = NULL, $size = NULL)
    {
        $type = 'post';
        $options = [];
        $options['status'] = 'publish';
        if ($from) {
            $options['from'] = $from;
        }
        if ($size) {
            $options['size'] = $size;
        }

        return $this->getPosts($type, $options);
    }

    public function getPosts($type, $options = [])
    {

        $search = [];
        $search[$this->getTableName('posts') . '.post_type'] = $type;

        if (isset($options['status'])) {
            $search[$this->getTableName('posts') . '.post_status'] = $options['status'];
        }

        $query = $this->db->table($this->getTableName('posts'))
//                          ->join($this->getTableName('postmeta'), $this->getTableName('posts') . '.ID', '=', $this->getTableName('postmeta') . '.post_id')
            ->where($search);
        if (isset($options['from'])) {
            $query = $query->offset($options['from']);
        }

        if (isset($options['size'])) {
            $query = $query->limit($options['size']);
        }

        return $query->get()->toArray();
    }

    public function getAuthorOfArticle($article)
    {
        $table = $this->getTableName('users');
        $user = $this->db->table($table)->where('id', $article->post_author)->first();
        return $user->display_name;
    }

    public function getImageOfArticle($article)
    {
        $table = $this->getTableName('postmeta');
        $thumbnail = $this->db->table($table)->where(['post_id' => $article->ID, 'meta_key' => '_thumbnail_id'])->first();

        $table = $this->getTableName('posts');
        $article = $this->db->table($table)->where(['ID' => $thumbnail->meta_value, 'post_type' => 'attachment'])->first();

        $image = [];
        $image['src'] = ($article) ? $article->guid : '';
        return $image;
    }

    public function getArticleCategory($article)
    {
        $table = $this->getTableName('postmeta');
        $articleMetaCategory = $this->db->table($table)->where(['post_id' => $article->ID, 'meta_key' => '_yoast_wpseo_primary_category'])->first();

        $table = $this->getTableName('terms');
        $articleCategory = $this->db->table($table)->where(['term_id' => $articleMetaCategory->meta_value])->first();

        return $articleCategory;
    }

//    public function getTagsOfArticle($article)
//    {
//        $tags = [];
//
//        $table = $this->prefix . 'term_relationships';
//        $terms = $this->db->table($table)->where('object_id', $article->ID)->get();
//
//        foreach ($terms as $term) {
//            $table = $this->prefix . 'term_taxonomy';
//            $taxonomy = $this->db->table($table)->where('term_id', $term->term_taxonomy_id)->first();
//
//            if ($taxonomy->taxonomy == "post_tag") {
//
//                $table = $this->prefix . 'terms';
//
//                $tag = $this->db->table($table)->where('term_id', $term->term_taxonomy_id)->first();
//                $tags[] = $tag->name;
//            }
//        }
//
//        return implode(',', $tags);
//    }

    public function getTermValueOfPost($post, $taxonomies)
    {
        //product tag = product_tag
        //artical tag = post_tag
        $values = $this->db->table($this->getTableName('posts'))
                ->join($this->getTableName('term_relationships'), $this->getTableName('posts') . '.ID', '=', $this->getTableName('term_relationships') . '.object_id')
                ->join($this->getTableName('term_taxonomy'), $this->getTableName('term_relationships') . '.term_taxonomy_id', '=', $this->getTableName('term_taxonomy') . '.term_id')
                ->join($this->getTableName('terms'), $this->getTableName('term_relationships') . '.term_taxonomy_id', '=', $this->getTableName('terms') . '.term_id')
                ->where([$this->getTableName('posts') . '.ID' => $post->ID])
                ->whereIn($this->getTableName('term_taxonomy') . '.taxonomy', $taxonomies)
                ->pluck($this->getTableName('terms') . '.slug')->toArray();

        return $values;
    }

    public function getProductVariants($product)
    {
        $variants = $this->db->table($this->getTableName('postmeta'))->where(['post_id' => $product->ID, 'meta_key' => '_product_attributes'])->first();

        dd($variants->meta_key);
    }

    public function getTableName($name)
    {
        return $this->prefix . $name;
    }

    public function getMetaDataOfPost($postId)
    {
        $postMetaData = $this->db->table($this->getTableName('postmeta'))->where('post_id', $postId)->get()->toArray();
        return $postMetaData;
    }

    public function getMetaValueFromKey($postId, $key)
    {
        $metaValue = $this->db->table($this->getTableName('postmeta'))->where(['post_id' => $postId, 'meta_key' => $key])->first();
        return ($metaValue) ? $metaValue->meta_value : NULL;
    }
}
