<?php

namespace App\Http\Controllers\WordPress\Blogs;

use Helper;
use App\Http\Controllers\Shopify\ShopifyRequestController;
use App\Http\Controllers\Shopify\ShopifyController;
use App\Http\Controllers\WordPress\WordPressRequestController;
use App\Models\CustomGuzzleHttp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public $wp;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create()
    {
        
        dd("Stop restart");
        $this->wp = new WordPressRequestController;
        $articles = $this->wp->getPublishArticles();
//        $articles = array_slice($articles, 3);

        $shopifyArticles = [];
        foreach ($articles as $article) {
            $shopifyArticle = [];
            $shopifyArticle = $this->generateShopifyArticleObject($article);

            $project = \App\Models\Project::find('f6b460a5-9eae-46ac-a4ab-ad8f0e044d37');

            $url = 'admin/blogs/' . $shopifyArticle['blog_id'] . '/articles.json';
            $shopifyRequest = new ShopifyRequestController($project);

            $params = [
                'article' => $shopifyArticle
            ];

            $result = $shopifyRequest->create($url, $params);
            if (!isset($result['article'])) {
                dd($article, $result, $shopifyArticle);
            }
        }
        dd("done");
    }

    public function generateShopifyArticleObject($article)
    {

        $category = $this->wp->getArticleCategory($article);

        $shopifyArticle = [];
        $shopifyArticle['blog_id'] = $this->getShopifyBlogId($category);
        $shopifyArticle['title'] = $article->post_title;
        $shopifyArticle['handle'] = $article->post_name;
        $shopifyArticle['body_html'] = $article->post_content;
        $shopifyArticle['author'] = $this->wp->getAuthorOfArticle($article);
        $shopifyArticle['image'] = $this->wp->getImageOfArticle($article);
        $shopifyArticle['published'] = true;
        $shopifyArticle['published_at'] = Helper::shopifyDateFormate($article->post_date_gmt);
        $shopifyArticle['tags'] = $this->wp->getTagsOfArticle($article);
//        $shopifyArticle['template_suffix'] = '';
        return $shopifyArticle;
    }

    public function getShopifyBlogId($category)
    {
        $project = \App\Models\Project::find('f6b460a5-9eae-46ac-a4ab-ad8f0e044d37');
        $searchType = 'blogs';
        $search = $category->slug;
//        $search = 'news';
        $fieldName = 'handle';

        $shopify = new ShopifyController();
        $blog = $shopify->shopifySearch($project, $searchType, $search, $fieldName);

        if ($blog) {
            return $blog[0]['id'];
        }

        $shopifyBlog = $this->generateShopifyBlogObject($category);

        $url = '/admin/blogs.json';
        $shopifyRequest = new ShopifyRequestController($project);

        $params = [
            'blog' => $shopifyBlog
        ];

        $result = $shopifyRequest->create($url, $params);

        return $result['blog']['id'];
//        $shopifyBlog['commentable'] = '';
//        $shopifyBlog['tags'] = '';
//
//        $shopifyBlog['template_suffix'] = '';
//        $shopifyBlog['feedburner'] = '';
//        $shopifyBlog['feedburner_location'] = '';
//        $shopifyBlog['metafield'] = '';
    }

    public function generateShopifyBlogObject($category)
    {
        $shopifyBlog = [];
        $shopifyBlog['title'] = $category->name;
        $shopifyBlog['handle'] = $category->slug;
        return $shopifyBlog;
    }
}
