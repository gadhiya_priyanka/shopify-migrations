<?php

namespace App\Http\Controllers\WordPress\Coupons;

use App\Helpers\Helper;
use App\Http\Controllers\Shopify\ShopifyRequestController;
use App\Models\Project;
use App\Http\Controllers\WordPress\WordPressRequestController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CouponController extends Controller
{

    public function __construct()
    {
        
    }

    public function create()
    {

        dd("wp coupons");
        $project = Project::find('dd5003c3-065c-416a-93ee-ff18e6a259f0');
        $shopifyRequest = new ShopifyRequestController($project->shopify_details);

//        $priceRules = [
//            '287570427984',
//        ];
//        foreach ($priceRules as $priceRule) {
//            $priceRuleId = $priceRule;
//            $url = '/admin/price_rules/' . $priceRuleId . '.json';
//            $deletePriceRule = $shopifyRequest->delete($url);
//        };
//
//        dd($deletePriceRule);

        $this->wpRequest = new WordPressRequestController($project->platform_details);

        $options = [];
        $options['status'] = 'publish';
        $migrateCoupons = $this->wpRequest->getPosts('shop_coupon', $options);
//        dd($migrateCoupons);
        $shopifyRequest = new ShopifyRequestController($project->shopify_details);

        $shopifyCoupons = $shopifyPriceRules = [];
        foreach ($migrateCoupons as $migrateCoupon) {

            $metaData = $this->wpRequest->getMetaDataOfPost($migrateCoupon->ID);
            $shopifyPriceRule = $this->generateShopifyPriceRuleObject($migrateCoupon, $metaData);
//            dd($shopifyPriceRule);
            $url = 'admin/price_rules.json';
            $params = [
                'price_rule' => $shopifyPriceRule
            ];

            $resultPriceRule = $shopifyRequest->create($url, $params);
//            dd($resultPriceRule);
            if (!isset($resultPriceRule['price_rule'])) {
                dd($resultPriceRule, $migrateCoupon);
            }
            $shopifyPriceRules[] = $resultPriceRule['price_rule'];

            $shopifyCoupon = $this->generateShopifyCouponObject($resultPriceRule, $migrateCoupon, $metaData);

            $url = 'admin/price_rules/' . $resultPriceRule['price_rule']['id'] . '/discount_codes.json';
            $params = [
                'discount_code' => $shopifyCoupon
            ];

            $resultCoupon = $shopifyRequest->create($url, $params);
            $shopifyCoupons[] = $resultCoupon;
        }

        dd($shopifyCoupons, $shopifyPriceRules);
    }

    public function generateShopifyPriceRuleObject($migrateCoupon, $metaData)
    {
        $freeShipping = $this->wpRequest->getMetaValueFromKey($migrateCoupon->ID, 'free_shipping');
        $discountType = $this->wpRequest->getMetaValueFromKey($migrateCoupon->ID, 'discount_type');
        $couponAmount = $this->wpRequest->getMetaValueFromKey($migrateCoupon->ID, 'coupon_amount');
        $usageLimitPerUser = $this->wpRequest->getMetaValueFromKey($migrateCoupon->ID, 'usage_limit_per_user');
        $maximumAmount = $this->wpRequest->getMetaValueFromKey($migrateCoupon->ID, 'maximum_amount');
        $minimumAmount = $this->wpRequest->getMetaValueFromKey($migrateCoupon->ID, 'minimum_amount');
        $expiryDate = $this->wpRequest->getMetaValueFromKey($migrateCoupon->ID, 'expiry_date');

//        $targeType = ($freeShipping == "yes") ? 'shipping_line' : 'line_item';
//        $targeType = ($couponAmount == "100") ? 'shipping_line' : 'line_item';
        $targeType = 'line_item';
        $valueType = ($discountType == "percent" ) ? 'percentage' : 'fixed_amount';
        $value = '-' . $couponAmount;
        $oncePerCustomer = ($usageLimitPerUser) ? 'true' : 'false';

        //Real PriceRule Object
        $shopifyPriceRule = [];
        $shopifyPriceRule['title'] = $migrateCoupon->post_title;
        $shopifyPriceRule['target_type'] = $targeType;
        $shopifyPriceRule['target_selection'] = 'all';
        $shopifyPriceRule['allocation_method'] = 'across';
        $shopifyPriceRule['value_type'] = $valueType;
        $shopifyPriceRule['value'] = $value;
        $shopifyPriceRule['once_per_customer'] = $oncePerCustomer;
//        $shopifyPriceRule['usage_limit'] = $metaData->usage_limit_per_user;
        $shopifyPriceRule['customer_selection'] = 'all';
//        $shopifyPriceRule['prerequisite_saved_search_ids'] = '';
//        $shopifyPriceRule['prerequisite_customer_ids'] = '';
        if ($minimumAmount) {
            $shopifyPriceRule['prerequisite_subtotal_range'] = [
                'greater_than_or_equal_to' => $minimumAmount
            ];
        }
//        if ($maximumAmount) {
//            $shopifyPriceRule['prerequisite_shipping_price_range'] = [
//                'less_than_or_equal_to' => $maximumAmount
//            ];
//        }

//        $shopifyPriceRule['entitled_product_ids'] = '';
//        $shopifyPriceRule['entitled_variant_ids'] = '';
//        $shopifyPriceRule['entitled_collection_ids'] = '';
//        $shopifyPriceRule['entitled_country_ids'] = '';
        $shopifyPriceRule['starts_at'] = Helper::shopifyDateFormate(date('Y-m-d'));
        if ($expiryDate) {

            if (date("Y-m-d", strtotime($expiryDate)) < date('Y-m-d')) {
                $shopifyPriceRule['ends_at'] = Helper::shopifyDateFormate(date('Y-m-d'));
            } else {
                $shopifyPriceRule['ends_at'] = Helper::shopifyDateFormate($expiryDate);
            }
        }

        return $shopifyPriceRule;
    }

    public function generateShopifyCouponObject($shopifyPriceRule, $migrateCoupon, $metaData)
    {
        $usageCount = $this->wpRequest->getMetaValueFromKey($migrateCoupon->ID, 'usage_count');

        //Real Coupon Object
        $shopifyCoupon = [];
        $shopifyCoupon['code'] = $migrateCoupon->post_name;
        $shopifyCoupon['usage_count'] = $usageCount;
        $shopifyCoupon['price_rule_id'] = $shopifyPriceRule['price_rule']['id'];

        return $shopifyCoupon;
    }

    public function delete()
    {
        $shopifyRequest = new ShopifyRequestController($project->shopify_details);

        $priceRuleId = '127227985952';
        $url = '/admin/price_rules/' . $priceRuleId . '.json';
        $deletePriceRule = $shopifyRequest->delete($url);
        dd($deletePriceRule);
    }
}
