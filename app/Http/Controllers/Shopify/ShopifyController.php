<?php

namespace App\Http\Controllers\Shopify;

use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShopifyController extends Controller
{

    public function __construct()
    {
        
    }

    public function getProducts()
    {
        
    }

    public function getBlog(Project $project, $id)
    {
        $blogs = [];
        $url = '/admin/blogs/' . $id . '.json';
        $result = $this->requestToShopify($project, $url);

        $blogs = $result['blog'];

        return $blogs;
    }

    public function getCollections(Project $project)
    {
        $collections = [];
        $url = 'admin/custom_collections.json';
        $result = $this->requestToShopify($project, $url);
        $collections = $result['custom_collections'];

        $url = 'admin/smart_collections.json';
        $result = $this->requestToShopify($project, $url);
        $collections = array_merge($collections, $result['smart_collections']);

        return $collections;
    }

    public function getSearchCollections(Project $project, $search, $with = NULL)
    {
        $searchResult = $collections = [];

        $searchType = 'custom_collections';
        $collections = $this->shopifySearch($project, $searchType, $search, 'handle');

        if (!$collections) {
            $searchType = 'smart_collections';
            $collections = $this->shopifySearch($project, $searchType, $search, 'handle');
        }

        if (!$collections) {
            $search = str_replace('-', ' ', $search);
            $searchType = 'custom_collections';
            $collections = $this->shopifySearch($project, $searchType, $search, 'title');
        }

        if (!$collections) {
            $searchType = 'smart_collections';
            $collections = $this->shopifySearch($project, $searchType, $search, 'title');
        }

        $searchResult['collections'] = $collections;

        if ($with) {
            $with = explode(',', $with);
            $with = array_map('trim', $with);

            if (in_array('links', $with)) {
                $links = [];
                foreach ($collections as $collection) {
                    $links[] = $this->generateCollectionUrl($collection['handle']);
                }

                $searchResult['links'] = $links;
                return $searchResult;
            }
        }

        return $searchResult;
    }

    public function getSearchPages(Project $project, $search, $with = NULL)
    {
        $searchResult = $pages = [];

        $searchType = 'pages';
        $pages = $this->shopifySearch($project, $searchType, $search, 'handle');

        if (!$pages) {
            $search = str_replace('-', ' ', $search);
            $pages = $this->shopifySearch($project, $searchType, $search, 'title');
        }

        $searchResult['pages'] = $pages;

        if ($with) {
            $with = explode(',', $with);
            $with = array_map('trim', $with);

            if (in_array('links', $with)) {
                $links = [];
                foreach ($pages as $page) {
                    $links[] = $this->generatePageUrl($page['handle']);
                }

                $searchResult['links'] = $links;
                return $searchResult;
            }
        }

        return $searchResult;
    }

    public function getSearchProducts(Project $project, $search, $with = NULL)
    {
        $searchResult = $products = [];

        $searchType = 'products';
        $products = $this->shopifySearch($project, $searchType, $search, 'handle');

        if (!$products) {
            $search = str_replace('-', ' ', $search);
            $products = $this->shopifySearch($project, $searchType, $search, 'title');
        }

        $searchResult['products'] = $products;

        if ($with) {
            $with = explode(',', $with);
            $with = array_map('trim', $with);

            if (in_array('links', $with)) {
                $links = [];
                foreach ($products as $product) {
                    $links[] = $this->generateProductUrl($product['handle']);
                }

                $searchResult['links'] = $links;
                return $searchResult;
            }
        }

        return $searchResult;
    }

    public function getSearchBlogs(Project $project, $search, $with = NULL)
    {
        $searchResult = $blogs = [];

        $searchType = 'blogs';
        $blogs = $this->shopifySearch($project, $searchType, $search, 'handle');

        $searchResult['blogs'] = $blogs;

        if ($with) {
            $with = explode(',', $with);
            $with = array_map('trim', $with);

            if (in_array('links', $with)) {
                $links = [];
                foreach ($blogs as $blog) {
                    $links[] = $this->generateBlogUrl($blog['handle']);
                }

                $searchResult['links'] = $links;
                return $searchResult;
            }
        }

        return $searchResult;
    }

    public function getSearchArticles(Project $project, $search, $with = NULL)
    {
        $searchResult = $articles = [];

        $searchType = 'articles';
        $articles = $this->shopifySearch($project, $searchType, $search, 'handle');

        $searchResult['articles'] = $articles;

        if ($with) {
            $with = explode(',', $with);
            $with = array_map('trim', $with);

            if (in_array('links', $with)) {
                $links = [];
                foreach ($articles as $article) {
                    $blog = $this->getBlog($project, $article['blog_id']);
                    $links[] = $this->generateArticleUrl($blog['handle'], $article['handle']);
                }

                $searchResult['links'] = $links;
                return $searchResult;
            }
        }
        return $searchResult;
    }

    public function shopifySearch(Project $project, $searchType, $search, $fieldName)
    {
        $url = '/admin/' . $searchType . '.json';
        $params = [
            $fieldName => $search,
            'fields' => 'id, title, handle, blog_id'
        ];

        $result = $this->requestToShopify($project, $url, $params);

        return (is_string($result)) ? [] : $result[$searchType];
    }

    public function requestToShopify($project, $url, $params = [])
    {
        $shopifyDetails = $project->shopify_details;
        $shopifyRequest = new ExternalApiRequestController($shopifyDetails);
        $result = $shopifyRequest->view($url, $params);
        if (is_string($result)) {
            sleep(1);
            \Log::info(["Shopify Controller Error $url" => $result]);
            $result = $shopifyRequest->view($url, $params);
        }
        return $result;
    }

    public function generateCollectionUrl($handle)
    {
        return $this->generateUrl('collections', $handle);
    }

    public function generatePageUrl($handle)
    {
        return $this->generateUrl('pages', $handle);
    }

    public function generateProductUrl($handle)
    {
        return $this->generateUrl('products', $handle);
    }

    public function generateBlogUrl($handle)
    {
        return $this->generateUrl('blogs', $handle);
    }

    public function generateArticleUrl($handle, $articleHandle)
    {
        return $this->generateUrl('blogs', $handle, $articleHandle);
    }

    public function generateUrl($urlType, $handle, $articleHandle = NULL)
    {
        $url = '/' . $urlType . '/' . $handle;
        $url = ($articleHandle) ? $url . '/' . $articleHandle : $url;
        return $url;
    }
}
