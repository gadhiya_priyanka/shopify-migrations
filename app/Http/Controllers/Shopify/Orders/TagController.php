<?php

namespace App\Http\Controllers\Shopify\Orders;

use App\Http\Controllers\Core\ExcelController;
use App\Http\Controllers\Core\ExternalApiRequestController;
use Exception;
use App\Models\CustomGuzzleHttp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends Controller
{

    public function __construct()
    {
        set_time_limit(0);
        dd("please turn off notification first, Shopify Order Tags controller");
    }

    public function create()
    {
        \Log::info("Script Start");
        $basic = 'https://90357ec8836f4ed019c860cd51680f1b:087cdf62249a344f994a3679856ea783@fuego-box.myshopify.com/admin/';
        $basePath = $basic . 'customers/search.json';
        $tempRequest = CustomGuzzleHttp::connectTest();

        $filePath = storage_path('/projects/fuegobox/task-16664/box1/box-1-customers.xlsx');
        $customers = ExcelController::view($filePath);

        $data = [];
        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);

        $updateOrderErrors = $updateOrders = $updatedCustomers = $updateCustomerErrors = [];
        $customerNotFounds = [];
        foreach ($customers as $customer) {
            $customerId = (int) $customer['customer_id'];
            $email = $customer['customer_email'];
            \Log::info(["customer_email" => $email]);
            $url = 'admin/customers/' . $customerId . '.json';
            $searchCustomer = $shopifyRequest->view($url);
            $searchCustomer = $searchCustomer['customer'];

            $tags = $searchCustomer['tags'];
            $tags = explode(',', $tags);
            $tags = array_map('trim', $tags);

            $updateCustomerFound = FALSE;
            foreach ($tags as $tag) {
                if ($tag == "CJ Import") {
                    \Log::info(["updateCustomers" => $searchCustomer['email']]);
                    $updateCustomerFound = TRUE;
                    break;
                }
            }

            if ($updateCustomerFound) {
                foreach ($tags as $key => $tag) {
                    if ($tag == "Box1") {
                        unset($tags[$key]);
                    }
                }

                $tags[] = 'Box4';
                $updateCustomer = [];
                $updateCustomer['id'] = $customerId;
                $updateCustomer['tags'] = implode(',', $tags);
                $params = [];
                $params['customer'] = $updateCustomer;

                $update = $shopifyRequest->update($url, $params);
                if (isset($update['customer'])) {
                    \Log::info(['update_customer' => $update['customer']['id']]);
                    $updatedCustomer = $customer;
                    $updatedCustomer['search_email'] = $searchCustomer['email'];
                    $updatedCustomer['email'] = $email;
                    $updatedCustomer['tags'] = $tags;
                    $updatedCustomer['old_tags'] = $searchCustomer['tags'];
                    $updatedCustomers[] = $updatedCustomer;
                } else {
                    \Log::info(['failed_customer' => [$customerId, $update]]);
                    $eror = $customer;
                    $updateCustomerErrors[] = $eror;
                }

                //tag of customer's last order update

                $url = $basic . 'customers/' . $searchCustomer['id'] . '/orders.json?status=any&page=1&limit=250';
                try {
                    $result = $tempRequest->request('GET', $url);
                } catch (Exception $ex) {
                    sleep(1);
                    try {
                        $result = $tempRequest->request('GET', $url);
                    } catch (Exception $ex) {
                        break;
                    }
                }
                $result = json_decode($result->getBody(), TRUE);

                $orders = $result['orders'];
                $subscriptionOrder = FALSE;
                foreach ($orders as $order) {
                    $orderTags = [];
                    $orderTags = $order['tags'];
                    $orderTags = explode(',', $orderTags);
                    $orderTags = array_map('trim', $orderTags);

                    $tagFound = FALSE;
                    //find subscription order
                    foreach ($orderTags as $orderTag) {
                        if (str_contains(strtolower($orderTag), 'subscription')) {
                            $tagFound = TRUE;
                            break;
                        }
                    }

                    if ($tagFound) {
                        $subscriptionOrder = TRUE;
                        $orderUpdateTags = [];

                        foreach ($orderTags as $key => $orderTag) {
                            if ($orderTag == "Box1") {
                                unset($orderTags[$key]);
                            }
                        }
                        $orderTags[] = 'Box4';
                        $orderUpdateTags = implode(',', $orderTags);

                        $updateOrder = [];
                        $updateOrder['id'] = $order['id'];
                        $updateOrder['tags'] = $orderUpdateTags;
                        $url = '/admin/orders/' . $order['id'] . '.json';
                        $params = [
                            'order' => $updateOrder
                        ];
                        $result = $shopifyRequest->update($url, $params);
                        if (!isset($result['order'])) {
                            $eror = $customer;
                            $updateOrderErrors[] = $eror;
                            \Log::info(["order_update_error" => [$email, $result]]);
                        } else {
                            \Log::info(["order_update_success" => $email]);
                            $successOrder = $updateOrder;
                            $successOrder['search_email'] = $searchCustomer['email'];
                            $successOrder['email'] = $email;
                            $successOrder['tags'] = $orderUpdateTags;
                            $successOrder['old_tags'] = $order['tags'];
                            $updateOrders[] = $successOrder;
                        }
                        break;
                    }
                }
            }
        }

        \Log::info("Generate CSV");
        $createFileFolder = 'projects/fuegobox/task-16664';
        $storePath = $createFileFolder . '/box4';
        ExcelController::create('box-4-order-error', $updateOrderErrors, $storePath, 'xlsx');

        $createFileFolder = 'projects/fuegobox/task-16664';
        $storePath = $createFileFolder . '/box4';
        ExcelController::create('box-4-updated-orders', $updateOrders, $storePath, 'xlsx');

        $createFileFolder = 'projects/fuegobox/task-16664';
        $storePath = $createFileFolder . '/box4';
        ExcelController::create('box-4-updated-customers', $updatedCustomers, $storePath, 'xlsx');

        $createFileFolder = 'projects/fuegobox/task-16664';
        $storePath = $createFileFolder . '/box4';
        ExcelController::create('box-4-customers-error', $updateCustomerErrors, $storePath, 'xlsx');

        \Log::info("Script End");
    }

    public function update()
    {
        dd("stop");
        \Log::info("Script Start");

        $basic = 'https://90357ec8836f4ed019c860cd51680f1b:087cdf62249a344f994a3679856ea783@fuego-box.myshopify.com/admin/';
        $basePath = $basic . 'customers/search.json';
        $tempRequest = CustomGuzzleHttp::connectTest();

        $filePath = resource_path('assets/projects/fuego-box/task-16623/customers_export.csv');
        $customers = ExcelController::view($filePath);

        $data = [];
        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);

        $updateCustomers = $exceptions = [];
        foreach ($customers as $customer) {
            $email = $customer['email'];
            \Log::info(["customer_email" => $email]);
            $customerTags = $customer['tags'];
            $customerTags = explode(',', $customerTags);
            $customerTags = array_map('trim', $customerTags);

            $subscriptionCustomer = FALSE;
            $boxCounter = 0;
            foreach ($customerTags as $key => $customerTag) {
                if ($customerTag == 'Box1') {
                    unset($customerTags[$key]);
                    $subscriptionCustomer = TRUE;
                }
                if (str_contains(strtolower($customerTag), 'box')) {
                    $boxCounter++;
                }
            }

            if ($subscriptionCustomer) {
                if ($boxCounter > 1) {
                    $exceptions[] = $customer;
                } else {
                    $url = $basePath . '?query=email:' . $email;
                    try {
                        $result = $tempRequest->request('GET', $url);
                    } catch (Exception $ex) {
                        sleep(1);
                        try {
                            $result = $tempRequest->request('GET', $url);
                        } catch (Exception $ex) {
                            \Log::info(["Getting Error catch" => $ex]);
                            break;
                        }
                    }

                    $result = json_decode($result->getBody(), TRUE);
                    $customers = $result['customers'];
                    if ($customers) {
                        $searchCustomer = $customers[0];

                        $updateTags = $customerTags;
                        $updateTags[] = 'Box2';
                        $updateTags = implode(',', $updateTags);

                        $updateCustomer = [];
                        $updateCustomer['id'] = $searchCustomer['id'];
                        $updateCustomer['tags'] = $updateTags;

                        $url = '/admin/customers/' . $searchCustomer['id'] . '.json';
                        $params = [
                            'customer' => $updateCustomer
                        ];

                        $result = $shopifyRequest->update($url, $params);
                        if (!isset($result['customer'])) {
                            \Log::info(["error_customer" => [$searchCustomer['id'], $searchCustomer['email'], $result]]);
                        } else {
                            \Log::info(["success_customer" => [$searchCustomer['id'], $searchCustomer['email']]]);
                            $updateCustomer['old_tags'] = $customer['tags'];
                            $updateCustomers[] = $updateCustomer;
                        }
                    }
                }
            }
        }

        \Log::info("Generate Csv");
        $fileName = 'customers_export';
        $storePath = 'projects/fuegobox/task-16623';
        ExcelController::create($fileName, $updateCustomers, $storePath, 'csv');

        $fileName = 'exceptions_customers';
        $storePath = 'projects/fuegobox/task-16623';
        ExcelController::create($fileName, $exceptions, $storePath, 'csv');
        \Log::info("Script End");

        dd("script end");
        foreach ($orders as $order) {
            $orderId = (int) $order['id'];
            \Log::info(["order_id" => $orderId]);
            $tags = $order['tags'];
            $orderTags = explode(',', $tags);
            $orderTags = array_map('trim', $orderTags);
            $subscriptionOrder = FALSE;

            $boxCounter = 0;
            foreach ($orderTags as $key => $orderTag) {
                if ($orderTag == 'Box1') {
                    unset($orderTags[$key]);
                    $subscriptionOrder = TRUE;
                }
                if (str_contains(strtolower($orderTag), 'box')) {
                    $boxCounter++;
                }
            }
            if ($subscriptionOrder) {
                if ($boxCounter > 1) {
                    $exceptions[] = $order;
                } else {
                    $updateTags = $orderTags;
                    $updateTags[] = 'Box2';
                    $updateTags = implode(',', $updateTags);

                    $updateOrder = [];
                    $updateOrder['id'] = $orderId;
                    $updateOrder['tags'] = $updateTags;

                    $url = '/admin/orders/' . $orderId . '.json';
                    $params = [
                        'order' => $updateOrder
                    ];

                    $result = $shopifyRequest->update($url, $params);
                    if (!isset($result['order'])) {
                        \Log::info(['failed_order' => [$orderId, $result]]);
                    } else {
                        \Log::info(['success_order' => [$orderId]]);
                        $updateOrder['old_tags'] = $order['tags'];
                        $updateOrders[] = $updateOrder;
                    }
                }
            }
        }

        \Log::info("Generate Csv");
        $fileName = 'customers_export';
        $storePath = 'projects/fuegobox/task-16623';
        ExcelController::create($fileName, $updateOrders, $storePath, 'csv');

        $fileName = 'reveiver-orders_export';
        $storePath = 'projects/fuegobox/task-16623';
        ExcelController::create($fileName, $updateOrders, $storePath, 'csv');
        \Log::info("Script End");

        dd("please stop");
//        $basic = 'https://90357ec8836f4ed019c860cd51680f1b:087cdf62249a344f994a3679856ea783@fuego-box.myshopify.com/admin/';
//        $basePath = $basic . 'customers/search.json';
//        $tempRequest = CustomGuzzleHttp::connectTest();

        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
//        $data['db_password'] = 'localhost';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);
        $orders = $db->table('orders')->get()->toArray();
        $orderids = array_column($orders, 'order_id');
        $orders = array_unique($orderids);
        foreach ($orders as $order) {
            $order = $db->table('orders')->where(['order_id' => $order])->first();
            $newOrder = [];
            $newOrder['order_id'] = $order->order_id;
            $newOrder['customer_id'] = $order->customer_id;
            $newOrder['customer_email'] = $order->customer_email;
            $newOrder['order_email'] = $order->order_email;
            $newOrder['order_count'] = $order->order_count;
            $newOrder['subscription_order_count'] = $order->subscription_order_count;
            $order = $db->table('unique_orders')->insert($newOrder);
        }
        dd($orders);

        $data = [];
        $data['api_key'] = '3f662a612b0d40447af32d5cdb2dadfe';
        $data['api_password'] = 'd92fcada3895ecd18c325180baa3d053';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $api = new ExternalApiRequestController($data);

        $orders = ExcelController::view($filePath);
        $orders = array_slice($orders, 2858);

        foreach ($orders as $order) {
            \Log::info(["order_id" => $order['id']]);
            $tags = $order['tags'];
            $orderTags = explode(',', $tags);
            $orderTags = array_map('trim', $orderTags);
            $subscriptionOrder = FALSE;
            foreach ($orderTags as $orderTag) {
                if (str_contains(strtolower($orderTag), 'subscription')) {
                    $subscriptionOrder = TRUE;
                    break;
                }
            }

            if ($subscriptionOrder) {
                $email = $order['email'];
                if (trim($email) == 'jsmith@gandcautobody.com') {
                    $email = 'ctsweee@gmail.com';
                }

                if (trim($email) == 'mrisirius2020@gmail.com') {
                    $email = 'danielleyoungberg627@gmail.com';
                }

                $url = $basePath . '?query=email:' . $email;
                try {
                    $result = $tempRequest->request('GET', $url);
                } catch (Exception $ex) {
                    sleep(1);
                    try {
                        $result = $tempRequest->request('GET', $url);
                    } catch (Exception $ex) {
                        break;
                    }
                }

                $result = json_decode($result->getBody(), TRUE);
                $customers = $result['customers'];

                $customer = $customers[0];
                $url = 'admin/customers/' . $customer['id'] . '/orders.json';
                $params = [
                    'status' => 'any',
                    'page' => 1,
                    'limit' => 250
                ];
                $result = $api->view($url, $params);
                $customerOrders = $result['orders'];

                $subscriptionOrderCount = 0;
                foreach ($customerOrders as $customerOrder) {
                    $tags = $customerOrder['tags'];
                    $orderTags = explode(',', $tags);
                    $orderTags = array_map('trim', $orderTags);
                    foreach ($orderTags as $orderTag) {
                        if (str_contains(strtolower($orderTag), 'subscription')) {
                            $subscriptionOrderCount++;
                            break;
                        }
                    }
                }

                $subscriberCustomer = [];
                $subscriberCustomer['order_id'] = (int) $order['id'];
                $subscriberCustomer['customer_id'] = $customer['id'];
                $subscriberCustomer['customer_email'] = $customer['email'];
                $subscriberCustomer['order_email'] = $order['email'];
                $subscriberCustomer['order_count'] = $customer['orders_count'];
                $subscriberCustomer['subscription_order_count'] = $subscriptionOrderCount;
                $db->table('orders')->insert($subscriberCustomer);
            }
        }

        \Log::info("Script End");
        dd("done script");


        $url = 'admin/customers.json';

        $params = [
            'limit' => 250,
            'page' => $pageId
        ];
        $result = $api->view($url, $params);
        $customers = $result['customers'];
        foreach ($customers as $customer) {
            $subscriptionOrderCount = 0;
            if ($customer['orders_count'] > 0) {
                $url = 'admin/customers/' . $customer['id'] . '/orders.json';
                $params = [
                    'status' => 'any',
                    'page' => 1,
                    'limit' => 250
                ];
                $result = $api->view($url, $params);
                $orders = $result['orders'];
                foreach ($orders as $order) {
                    $tags = $order['tags'];
                    $orderTags = explode(',', $tags);
                }
            }

            $subscriberCustomer = [];
            $subscriberCustomer['order_id'] = $subscriberBoxOrder;
            $subscriberCustomer['customer_id'] = $customer['id'];
            $subscriberCustomer['order_count'] = $customer['orders_count'];
            $subscriberCustomer['subscription_order_count'] = $subscriptionOrderCount;
            $subscriberCustomer['page_id'] = $subscriptionOrderCount;
            $db->table('orders')->insert($subscriberCustomer);
        }
        dd($result);
    }
}
