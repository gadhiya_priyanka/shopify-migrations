<?php

namespace App\Http\Controllers\Shopify\Core;

use App\Helpers\Helper;
use App\Models\ProjectRedirect;
use App\Models\ProjectRedirectSetting as RedirectSetting;
use App\Http\Controllers\Core\CommonFunctionsController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RedirectController extends Controller
{
    public $redirectSetting;

    public function __construct(RedirectSetting $redirectSetting)
    {
        set_time_limit(0);
        $this->redirectSetting = $redirectSetting;
    }

    public function create($shopifyRedirect)
    {
        //add prefix url
        $multiTargetUrls = Helper::addPrefixToArrayKey('target', $shopifyRedirect['target']);

        $extra = $shopifyRedirect['extra'];
        $extra['type'] = ($extra['type']) ?: (($shopifyRedirect['target'][0]) ? 'success' : 'error');
        unset($shopifyRedirect['extra']);

        $export = $shopifyRedirect;
        $export = array_merge($export, $multiTargetUrls);

        $redirect = new ProjectRedirect;
        $redirect->id = \UUID::uuid4()->toString();
        $redirect->project_id = $this->redirectSetting->project->id;
        $redirect->redirect = $shopifyRedirect;
        $redirect->extra = $extra;
        $redirect->export = $export;
        $this->redirectSetting->redirects()->save($redirect);
    }
}
