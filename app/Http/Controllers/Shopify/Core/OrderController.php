<?php

namespace App\Http\Controllers\Shopify\Core;

use App\Helpers\Helper;
use App\Models\ProjectOrderSetting as OrderSetting;
use App\Models\ProjectOrder;
use App\Http\Controllers\Shopify\Api\CustomerController as ShopifyCustomerController;
use App\Http\Controllers\Shopify\Api\OrderController as ShopifyOrderController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public $orderSetting, $shopifyRequest, $shopifyCustomerRequest;

    public function __construct(OrderSetting $orderSetting)
    {
        set_time_limit(0);
        $this->orderSetting = $orderSetting;
        $project = $orderSetting->project;
        $this->shopifyRequest = new ShopifyOrderController($project->shopify_details);
        $this->shopifyCustomerRequest = new ShopifyCustomerController($project->shopify_details);
    }

    public function create($shopifyOrder, $migrateOrder = [])
    {
        $details = [];
        $details['extra'] = $shopifyOrder['extra'];
        $details['export'] = $shopifyOrder['export'];
        $details['email'] = $shopifyOrder['email'];
//        $this->shopifyCustomerRequest->create($shopifyOrder['customer']);
        unset($shopifyOrder['extra'], $shopifyOrder['export'], $shopifyOrder['customer']);

        $result = $this->shopifyRequest->create($shopifyOrder);
        if (!isset($result['order'])) {
            $remigrate = FALSE;
            if (str_contains(strtolower($result), 'email')) {
                $shopifyOrder['email'] = Helper::convertValidEmail($shopifyOrder['email']);
                $remigrate = TRUE;
            }
            if (str_contains(strtolower($result), 'billing_address')) {
                unset($shopifyOrder['billing_address']);
                $remigrate = TRUE;
            }
            if ($remigrate) {
                $result = $this->shopifyRequest->create($shopifyOrder);
            }
        }
        $this->insertOrderInDB($result, $migrateOrder, $details);

        unset($shopifyOrder, $migrateOrder, $details);
    }

    public function insertOrderInDB($result, $migrateOrder, $details)
    {
        $extra = $details['extra'];
        $extra['type'] = (isset($result['order'])) ? 'success' : 'error';

        $export = $details['export'];
        if (isset($result['order'])) {
            $export['shopify_id'] = $result['order']['id'];
            $export['shopify_no'] = $result['order']['name'];
        } else {
            $export['error'] = Helper::shopifyError($result);
        }
        $export['customer_email'] = $details['email'];

        $projectOrder = new ProjectOrder;
        $projectOrder->id = \UUID::uuid4()->toString();
        $projectOrder->project_id = $this->orderSetting->project->id;
        $projectOrder->order = (isset($result['order'])) ? $result['order'] : ['error' => $result];
        $projectOrder->extra = $extra;
        $projectOrder->export = $export;

        $this->orderSetting->orders()->save($projectOrder);
        unset($migrateOrder, $result, $details);
    }
}
