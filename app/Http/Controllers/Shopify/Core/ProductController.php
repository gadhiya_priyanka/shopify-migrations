<?php

namespace App\Http\Controllers\Shopify\Core;

use App\Helpers\Helper;
use App\Models\ProjectProduct;
use App\Http\Controllers\Shopify\Api\ProductController as ShopifyProductController;
use App\Models\ProjectProductSetting as ProductSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public $productSetting, $shopifyRequest;

    public function __construct(ProductSetting $productSetting)
    {
        set_time_limit(0);
        $this->productSetting = $productSetting;
        $project = $productSetting->project;
        $this->shopifyRequest = new ShopifyProductController($project->shopify_details);
    }

    public function create($shopifyProduct, $migrateProduct)
    {
        //product variant inventory 
        $newShopifyVariants = [];
        if (isset($migrateProduct['product_type']) && $migrateProduct['product_type'] != "subscription") {
            $shopifyVariants = $shopifyProduct['variants'];
            foreach ($shopifyVariants as $key => $shopifyVariant) {
                $newShopifyVariant = [];
                $newShopifyVariant['inventory_quantity'] = $shopifyVariant['inventory_quantity'];
                $newShopifyVariants[] = $newShopifyVariant;
                unset($shopifyProduct['variants'][$key]['inventory_quantity']);
            }
        }

        //insert product into shopify store
        $details = [];
        $details['extra'] = $shopifyProduct['extra'];
        $details['export'] = $shopifyProduct['export'];

        unset($shopifyProduct['extra'], $shopifyProduct['export']);
        $result = $this->shopifyRequest->create($shopifyProduct);

        //inset product insert in database
        $this->insertProductInDB($result, $migrateProduct, $details);

        if (isset($result['product']) && isset($migrateProduct['product_type']) && $migrateProduct['product_type'] != "subscription") {
            $migratedProduct = $result['product'];

            foreach ($migratedProduct['variants'] as $key => $migratedVariant) {
                if ($migratedVariant['inventory_management']) {
                    $variantInventory = [];
                    $variantInventory['available'] = $newShopifyVariants[$key]['inventory_quantity'];
                    $variantInventory['location_id'] = trim($this->productSetting->mapping['location_id']);
                    $variantInventory['inventory_item_id'] = $migratedVariant['inventory_item_id'];
                    $result = $this->shopifyRequest->setInventoryLevel($variantInventory);
                    if (!isset($result['inventory_level'])) {
                        \Log::info(["failed_inventory_update" => [$migratedVariant['id'], $result]]);
                    }
                }
            }
        }
        unset($shopifyProduct, $migrateProduct, $result, $details, $newShopifyVariants, $newShopifyVariant);
    }

    public function insertProductInDB($result, $migrateProduct, $details)
    {
        $extra = $details['extra'];
        $extra['type'] = (isset($result['product'])) ? 'success' : 'error';

        $export = $details['export'];
        if (isset($result['product'])) {
            $export['shopify_id'] = $result['product']['id'];
        } else {
            $export['error'] = Helper::shopifyError($result);
        }

        $projectProduct = new ProjectProduct;
        $projectProduct->id = \UUID::uuid4()->toString();
        $projectProduct->project_id = $this->productSetting->project->id;
        $projectProduct->product = (isset($result['product'])) ? $result['product'] : ['error' => $result];
        $projectProduct->extra = $extra;
        $projectProduct->export = $export;

        $this->productSetting->products()->save($projectProduct);
    }
}
