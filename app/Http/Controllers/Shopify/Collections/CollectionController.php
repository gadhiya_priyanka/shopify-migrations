<?php

namespace App\Http\Controllers\Shopify\Collections;

use App\Models\ProjectCollection;
use App\Models\ProjectCollectionSetting as CollectionSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CollectionController extends Controller
{

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create(CollectionSetting $collectionSetting)
    {
        dd("stop");
        $schedule = $collectionSetting->schedule;
        $schedule->status = 'inprogress';
        $schedule->save();

        \Log::info("Check log for Shopify\Collections\CollectionController create");

        $projectCollection = new ProjectCollection;
        $projectCollection->id = \UUID::uuid4()->toString();
        $projectCollection->project_id = $collectionSetting->project->id;
        $projectCollection->collection = [];
        $projectCollection->extra = [];
        $collectionSetting->collections()->save($projectCollection);

        sleep(5);

        $collections = $collectionSetting->collections()->get()->toArray();
        if (count($collections) >= 100) {
            $schedule->status = 'completed';
            $schedule->save();
            \Log::info("End Script");
        } else {

            $schedule->status = 'paused';
            $schedule->save();
            \Log::info("Continue");
        }
    }
}
