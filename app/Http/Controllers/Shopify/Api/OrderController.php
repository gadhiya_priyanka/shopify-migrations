<?php

namespace App\Http\Controllers\Shopify\Api;

use App\Http\Controllers\Core\ExternalApiRequestController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public $shopifyRequest;

    public function __construct($apiData)
    {
        $this->shopifyRequest = new ExternalApiRequestController($apiData);
    }

    public function create($shopifyOrder)
    {
        $url = '/admin/orders.json';
        $params = [
            'order' => $shopifyOrder
        ];
        return $this->shopifyRequest->create($url, $params);
    }

    public function get($page = 1)
    {
        $url = '/admin/orders.json';
        $params = [
            'limit' => 250,
            'status' => 'any',
            'page' => $page
        ];
        return $this->shopifyRequest->view($url, $params);
    }

    public function cancel($id, $params = [])
    {
        $url = '/admin/orders/' . $id . '/cancel.json';
        return $this->shopifyRequest->create($url, $params);
    }

    public function delete($id)
    {
        $url = '/admin/orders/' . $id . '.json';
        return $this->shopifyRequest->delete($url);
    }

    public function fulfillments($id)
    {
        $url = '/admin/orders/' . $id . '/fulfillments.json';
        $params = [
            'fulfillment' => [
                'order_id' => $id
            ]
        ];

        return $this->shopifyRequest->create($url, $params);
    }
}
