<?php

namespace App\Http\Controllers\Shopify\Api;

use App\Http\Controllers\Core\ExternalApiRequestController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductVariantController extends Controller
{
    public $shopifyRequest;

    public function __construct($apiData)
    {
        $this->shopifyRequest = new ExternalApiRequestController($apiData);
    }
    
    public function search($query)
    {
        $url = '/admin/api/graphql.json';
        
        $query = '{shop{productVariants(query:"'.$query.'",first:2){edges{node{id,price,sku,compareAtPrice,title}}}}}';
        return $this->shopifyRequest->graphQL($url, $query);
    }
}
