<?php

namespace App\Http\Controllers\Shopify\Api;

use App\Http\Controllers\Core\ExternalApiRequestController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationController extends Controller
{
    public $shopifyRequest;

    public function __construct($apiData)
    {
        $this->shopifyRequest = new ExternalApiRequestController($apiData);
    }

    public function get($page = 1)
    {
        $url = '/admin/locations.json';
        $params = [
            'limit' => 250,
            'status' => 'any',
            'page' => $page
        ];
        return $this->shopifyRequest->view($url, $params);
    }
}
