<?php

namespace App\Http\Controllers\Shopify\Api;

use App\Http\Controllers\Core\ExternalApiRequestController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public $shopifyRequest;

    public function __construct($apiData)
    {
        $this->shopifyRequest = new ExternalApiRequestController($apiData);
    }

    public function create($shopifyProduct)
    {
        $url = '/admin/products.json';
        $params = [
            'product' => $shopifyProduct
        ];
        return $this->shopifyRequest->create($url, $params);
    }

    public function get($id, $query)
    {
        $url = "/admin/products/{$id}.json";
        $params = [
            'query' => $query
        ];
        return $this->shopifyRequest->view($url, $params);
    }

    public function setInventoryLevel($variantInventory)
    {
        $url = '/admin/inventory_levels/set.json';
        return $this->shopifyRequest->create($url, $variantInventory);
    }

    public function updateImages($image, $productId)
    {
        $url = '/admin/products/' . $productId . '/images/' . $image['id'] . '.json';
        $params = [
            'image' => $image
        ];

        return $this->shopifyRequest->update($url, $params);
    }

    public function update($productId, $shopifyProduct)
    {
        $url = '/admin/products/' . $productId . '.json';
        $params = [
            'product' => $shopifyProduct
        ];
        return $this->shopifyRequest->update($url, $params);
    }

    public function createMetafield($productId, $metaField)
    {
        $url = '/admin/products/' . $productId . '/metafields.json';
        $params = [
            'metafield' => $metaField
        ];
        return $this->shopifyRequest->create($url, $params);
    }
}
