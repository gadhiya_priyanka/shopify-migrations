<?php

namespace App\Http\Controllers\Shopify\Api;

use App\Http\Controllers\Core\ExternalApiRequestController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    public $shopifyRequest;

    public function __construct($apiData)
    {
        $this->shopifyRequest = new ExternalApiRequestController($apiData);
    }

    public function create($shopifyCustomer)
    {
        $url = '/admin/customers.json';
        $params = [
            'customer' => $shopifyCustomer
        ];
        return $this->shopifyRequest->create($url, $params);
    }

    public function delete($customerId)
    {
        $url = '/admin/customers/' . $customerId . '.json';
        return $this->shopifyRequest->delete($url);
    }
}
