<?php

namespace App\Http\Controllers\Shopify\Api;

use App\Http\Controllers\Core\ExternalApiRequestController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CollectionController extends Controller
{
    public $shopifyRequest;
    
    public function __construct($apiData)
    {
        $this->shopifyRequest = new ExternalApiRequestController($apiData);
    }

    public function createSmartCollection($shopifyCollection)
    {
        $url = '/admin/smart_collections.json';
        $params = [
            'smart_collection' => $shopifyCollection
        ];
        return $this->shopifyRequest->create($url, $params);
    }
}
