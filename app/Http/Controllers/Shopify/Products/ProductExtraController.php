<?php

namespace App\Http\Controllers\Shopify\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductExtraController extends Controller
{

    public function __construct()
    {
        set_time_limit(0);
    }

    public function addMetafields()
    {
//        dd("here for metfa afgdd");
        \Log::info("Script Start");
        $project = \App\Models\Project::find('c4d9ee34-8312-4caa-9d9f-75dbd113efe3');
        $shopifyRequest = new \App\Http\Controllers\Core\ExternalApiRequestController($project->shopify_details);

        $url = '/admin/products.json';
        $params = [
            'collection_id' => 80803692662
        ];

        $result = $shopifyRequest->view($url, $params);
        $products = $result['products'];
        foreach ($products as $product) {

            \Log::info(['product_id' => $product['id']]);
            $url = 'admin/products/' . $product['id'] . '/metafields.json';
            $params = [
                'metafield' => [
                    'namespace' => 'description',
                    'key' => 'video_image',
                    'value' => 'https://cdn.shopify.com/s/files/1/0078/8391/5382/files/terra-fragrance-collage-collection_4b98bed1-9e45-49ed-bb58-5a3b2b8f547f.jpg?8928179796859379074',
                    'value_type' => 'string'
                ]
            ];

            $result = $shopifyRequest->create($url, $params);
            if (!isset($result['metafield'])) {
                \Log::info([$product['id'] => $result]);
            }
        }
        \Log::info("Script End");

        dd("Stop");
    }
}
