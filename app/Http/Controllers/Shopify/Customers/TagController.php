<?php

namespace App\Http\Controllers\Shopify\Customers;

use Exception;
use App\Models\Project;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Models\CustomGuzzleHttp;
use App\Http\Controllers\Core\ExcelController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends Controller
{

    public function __construct()
    {
        set_time_limit(0);
//        dd("please turn off notification first, Shopify Customer Tags controller");
    }

    public function searchCustomer()
    {
        $data = [];
        $data['api_key'] = 'c88cad54a110a2b52b657819d2aeb737';
        $data['api_password'] = '9fa89918e2508856c2be712416ad357b';
        $data['api_url'] = 'https://demo-pia.myshopify.com';

        $email = 'mahesh@wolfpointagency.com';
        $url = '/admin/customers/search.json';
        $params = [
            'query' => 'email:' . $email
        ];
        $request = new Core\ExternalApiRequestController($data);
        $customers = $request->view($url, $params);
        dd($customers);
    }

    public function cjReportOfCustomerdb()
    {
        $lastCustomer = \DB::table('customers')->where(['mark' => 1])->orderBy('page_id', 'desc')->first();
        $pageId = ($lastCustomer) ? $lastCustomer->page_id + 1 : 1;
        \Log::info("Script start $pageId");
        $dbCustomers = \DB::table('customers')->where(['page_id' => $pageId])->get()->toArray();
//
//        $data = [];
//        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
//        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
//        $data['api_url'] = 'https://fuego-box.myshopify.com';
//        $shopifyRequest = new ExternalApiRequestController($data);

        $basic = 'https://90357ec8836f4ed019c860cd51680f1b:087cdf62249a344f994a3679856ea783@fuego-box.myshopify.com/admin';
        $basePath = $basic . '/customers/search.json';
        $tempRequest = CustomGuzzleHttp::connectTest();
        foreach ($dbCustomers as $dbCustomer) {

            $url = $basePath . '?query=email:' . $dbCustomer->email;
            try {
                $result = $tempRequest->request('GET', $url);
            } catch (Exception $ex) {
                sleep(1);
                try {
                    $result = $tempRequest->request('GET', $url);
                } catch (Exception $ex) {
                    \Log::info(["temp request error $pageId" => $ex]);
                    break;
                }
            }

            $result = json_decode($result->getBody(), TRUE);

            $customers = $result['customers'];
            if (isset($customers[0])) {
                $spCustomer = $customers[0];

                $updateCustomer = [];
                $updateCustomer['exist_in_sp'] = 1;
                $updateCustomer['sp_customer_id'] = $spCustomer['id'];
                $updateCustomer['sp_email'] = $spCustomer['email'];
                $updateCustomer['sp_name'] = $spCustomer['first_name'] . ' ' . $spCustomer['last_name'];
                $updateCustomer['sp_num_orders'] = $spCustomer['orders_count'];
                $updateCustomer['sp_tags'] = $spCustomer['tags'];
                $updateCustomer['mark'] = 1;
                \DB::table('customers')->where(['customer_id' => $dbCustomer->customer_id])->update($updateCustomer);
            } else {
                $updateCustomer = [];
                $updateCustomer['exist_in_sp'] = 0;
                $updateCustomer['mark'] = 1;
                \DB::table('customers')->where(['customer_id' => $dbCustomer->customer_id])->update($updateCustomer);
            }
        }

        \Log::info("Script end $pageId");
        dd("done");
    }

    public function report()
    {
        dd("report completed");
        $exportCustomers = [];
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';

        $db = CustomGuzzleHttp::connectExternalDatabase($data);
        $customers = $db->table('customer_tag_report')->where('sheet_name', 'cj-with-box1-tags')->orderBy('id', 'desc')->get()->ToArray();
        foreach ($customers as $customer) {
            $export = json_decode($customer->export, true);
            $exportCustomer = [];
            $exportCustomer['customer_id'] = $export['customer_id'];
            $exportCustomer['email'] = $export['email'];
            $exportCustomer['customer_all_tags'] = $customer->customer_tags;
            $exportCustomer['customer_box_tags'] = $export['tags'];
            $exportCustomer['order_count'] = $customer->order_count;
            $exportCustomers[] = $exportCustomer;
        }
        ExcelController::create('migrated-customer-with-only-box1-tag', $exportCustomers, 'projects/fuegobox/task-17312', 'xlsx')->download('xlsx');
    }

    public function reportInDatabase()
    {
//        dd("report fuegobox");
        \Log::info("Start Script");
        $project = Project::find('cdc4705d-50c0-4142-9d30-e06ab30c2d76');
        $shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';

        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $lastProduct = $db->table('customer_tag_report')->orderBy('id', 'desc')->first();
        $pageId = ($lastProduct) ? $lastProduct->page_id + 1 : 1;
        $limit = $pageId + 4; //3+4 = 7
        for ($i = $pageId; $i <= $limit; $i++) {
            \Log::info("Start Script" . $i);
            $url = 'admin/customers.json';
            $params = [];
            $params['limit'] = 250;
            $params['page'] = $i;
            $result = $shopifyRequest->view($url, $params);
            $shopifyCustomers = $result['customers'];

            foreach ($shopifyCustomers as $shopifyCustomer) {
                $tags = $shopifyCustomer['tags'];
                $exploadTags = explode(',', $tags);
                $trimedtags = array_map('trim', $exploadTags);
                $lowercaseTags = array_map('strtolower', $trimedtags);

                $implodTags = implode(',', $lowercaseTags);

                $boxTags = [];
                foreach ($lowercaseTags as $key => $lowercaseTag) {
                    if (str_contains($lowercaseTag, 'box')) {
                        $boxTags[] = trim($exploadTags[$key]);
                    }
                }

                $implodBoxTags = implode(', ', $boxTags);

                $customer = [];
                $customer['customer_id'] = $shopifyCustomer['id'];
                $customer['customer_email'] = $shopifyCustomer['email'];
                $customer['customer_tags'] = $shopifyCustomer['tags'];
                $customer['order_count'] = $shopifyCustomer['orders_count'];
                $customer['page_id'] = $i;

                $export = [];
                $export['customer_id'] = $shopifyCustomer['id'];
                $export['email'] = $shopifyCustomer['email'];

                if (count($boxTags) > 1) {
                    $export['tags'] = $implodBoxTags;

                    $customer['sheet_name'] = 'cj-multi-box-tags';
                } else if (count($boxTags) == 1) {
                    $export['tags'] = $implodBoxTags;

                    if (in_array('cj import', $lowercaseTags) || in_array('cj-import', $lowercaseTags)) {

                        if (strtolower($boxTags[0]) == 'box1') {
                            $customer['sheet_name'] = 'cj-with-box1-tags';
                        } else {
                            $customer['sheet_name'] = 'cj-with-other-tags';
                        }
                    } else {

                        if (strtolower($boxTags[0]) == 'box1') {
                            $customer['sheet_name'] = 'without-with-box1-tags';
                        } else {
                            $customer['sheet_name'] = 'without-with-other-tags';
                        }
                    }
                } else {

                    $export['tags'] = $shopifyCustomer['tags'];

                    if (in_array('cj import', $lowercaseTags) || in_array('cj-import', $lowercaseTags)) {

                        $customer['sheet_name'] = 'cj-no-box-tags';
                    } else {
                        $customer['sheet_name'] = 'without-no-box-tags';
                    }
                }
                $customer['export'] = json_encode($export);

                $db->table('customer_tag_report')->insert($customer);
            }
            \Log::info("End Script" . $i);
        }
        \Log::info("End Script");
    }

    public function create()
    {
        dd("modification please turn off notification customer first");
        \Log::info("script start");
        $filePath = resource_path('/assets/projects/fuego-box/tagging-customer/Shipment-Fuego-Script.xlsx');
        $beforeUpdatedCustomers = ExcelController::view($filePath);
        $beforeEmails = array_column($beforeUpdatedCustomers, 'customer_email');
        $beforeUniqueEmails = array_count_values($beforeEmails);
        $beforeUniqueEmails = array_unique($beforeEmails);

        $filePath = resource_path('/assets/projects/fuego-box/task-16664/Fuego-Shipment-Update-Prepaid-Migrations.csv');
        $updateCustomers = ExcelController::view($filePath);
//        dd($updateCustomers);
        $emails = array_column($updateCustomers, 'customer_email');
        $uniqueEmails = array_count_values($emails);
        $updateEmails = array_unique($emails);

        $needForUpdates = array_diff($updateEmails, $beforeUniqueEmails);
//        dd($needForUpdates, $uniqueEmails); //2634

        $basic = 'https://90357ec8836f4ed019c860cd51680f1b:087cdf62249a344f994a3679856ea783@fuego-box.myshopify.com/admin/';
        $basePath = $basic . 'customers/search.json';

        $tempRequest = CustomGuzzleHttp::connectTest();

        $data = [];
        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);

        $customerNotFounds = $updateCustomerErrors = $updatedCustomers = $updateOrderErrors = $updateOrders = $orderNotFounds = [];
        foreach ($uniqueEmails as $key => $uniqueEmail) {
            $email = $key;
            if (in_array($email, $needForUpdates)) {
                \Log::info(["customer_email" => $email]);

                $url = $basePath . '?query=email:' . $email;
                try {
                    $result = $tempRequest->request('GET', $url);
                } catch (Exception $ex) {
                    sleep(1);
                    try {
                        $result = $tempRequest->request('GET', $url);
                    } catch (Exception $ex) {
                        break;
                    }
                }

                $result = json_decode($result->getBody(), TRUE);
                $customers = $result['customers'];
                if ($customers) {
                    \Log::info(["customer_update_email" => $email]);
                    $searchCustomer = $customers[0];
                    $customerTags = $searchCustomer['tags'];
                    $customerTags = explode(',', $customerTags);
                    $customerTags = array_map('trim', $customerTags);

                    foreach ($customerTags as $key => $customerTag) {
                        if (str_contains($customerTag, 'Box')) {
                            unset($customerTags[$key]);
                        }
                    }
                    $tags = $customerTags;
                    $tagging = [];
                    $keys = array_keys($emails, $email);
                    if ($keys) {
                        foreach ($keys as $key) {
                            $tagging[] = $updateCustomers[$key]['tags'];
                            $tags[] = $updateCustomers[$key]['tags'];
                        }
                    }
                    $tags = implode(',', $tags);

                    //customer tag update
                    $newCustomer = [];
                    $newCustomer['id'] = $searchCustomer['id'];
                    $newCustomer['tags'] = $tags;
                    $url = 'admin/customers/' . $searchCustomer['id'] . '.json';
                    $params = [
                        'customer' => $newCustomer
                    ];

                    $result = $shopifyRequest->update($url, $params);
                    if (!isset($result['customer'])) {
                        $eror = [];
                        $eror['email'] = $email;
                        $updateCustomerErrors[] = $eror;
                        \Log::info(["customer_update_error" => [$email, $result]]);
                    } else {
                        \Log::info(["customer_update_success" => $email]);
                        $updatedCustomer = $newCustomer;
                        $updatedCustomer['search_email'] = $searchCustomer['email'];
                        $updatedCustomer['email'] = $email;
                        $updatedCustomer['tags'] = $tags;
                        $updateCustomers['old_tags'] = $searchCustomer['tags'];
                        $updatedCustomers[] = $updatedCustomer;
                    }
                    //tag of customer's last order update

                    $url = $basic . 'customers/' . $searchCustomer['id'] . '/orders.json?status=any&page=1&limit=250';
                    try {
                        $result = $tempRequest->request('GET', $url);
                    } catch (Exception $ex) {
                        sleep(1);
                        try {
                            $result = $tempRequest->request('GET', $url);
                        } catch (Exception $ex) {
                            break;
                        }
                    }
                    $result = json_decode($result->getBody(), TRUE);

                    $orders = $result['orders'];
                    $subscriptionOrder = FALSE;
                    foreach ($orders as $order) {
                        $orderTags = [];
                        $orderTags = $order['tags'];
                        $orderTags = explode(',', $orderTags);
                        $orderTags = array_map('trim', $orderTags);

                        $tagFound = FALSE;
                        //find subscription order
                        foreach ($orderTags as $orderTag) {
                            if (str_contains(strtolower($orderTag), 'subscription')) {
                                $tagFound = TRUE;
                                break;
                            }
                        }

                        if ($tagFound) {
                            $subscriptionOrder = TRUE;
                            $orderUpdateTags = [];
                            foreach ($orderTags as $key => $orderTag) {
                                if (str_contains($orderTag, 'Box')) {
                                    unset($orderTags[$key]);
                                }
                            }

                            $orderUpdateTags = array_merge($orderTags, $tagging);
                            $orderUpdateTags = implode(',', $orderUpdateTags);

                            $updateOrder = [];
                            $updateOrder['id'] = $order['id'];
                            $updateOrder['tags'] = $orderUpdateTags;
                            $url = '/admin/orders/' . $order['id'] . '.json';
                            $params = [
                                'order' => $updateOrder
                            ];
                            $result = $shopifyRequest->update($url, $params);
                            if (!isset($result['order'])) {
                                $eror = [];
                                $eror['email'] = $email;
                                $updateOrderErrors[] = $eror;
                                \Log::info(["order_update_error" => [$email, $result]]);
                            } else {
                                \Log::info(["order_update_success" => $email]);
                                $successOrder = $updateOrder;
                                $successOrder['search_email'] = $searchCustomer['email'];
                                $successOrder['email'] = $email;
                                $successOrder['tags'] = $orderUpdateTags;
                                $successOrder['old_tags'] = $order['tags'];
                                $updateOrders[] = $successOrder;
                            }
                            break;
                        }
                    }

                    if (!$subscriptionOrder) {
                        $tempOrder = [];
                        $tempOrder['email'] = $email;
                        $tempOrder['tags'] = implode(',', $tagging);
                        $orderNotFounds[] = $tempOrder;
                    }
                } else {
                    $m = [];
                    $m['email'] = $email;
                    $customerNotFounds[] = $m;
                }
            }
        }

        \Log::info("Generate CSV");
        $createFileFolder = 'projects/fuegobox/task-16664';

        $fileName = 'customer-not-found';
        $storePath = $createFileFolder . '/error';
        ExcelController::create($fileName, $customerNotFounds, $storePath, 'xlsx');

        $fileName = 'customer-update-error';
        $storePath = $createFileFolder . '/error';
        ExcelController::create($fileName, $updateCustomerErrors, $storePath, 'xlsx');

        $fileName = 'customer-updated';
        $storePath = $createFileFolder . '/success';
        ExcelController::create($fileName, $updatedCustomers, $storePath, 'xlsx');

        $fileName = 'order-updated';
        $storePath = $createFileFolder . '/success';
        ExcelController::create($fileName, $updateOrders, $storePath, 'xlsx');

        $fileName = 'order-error-updated';
        $storePath = $createFileFolder . '/error';
        ExcelController::create($fileName, $updateOrderErrors, $storePath, 'xlsx');

        $fileName = 'order-not-found';
        $storePath = $createFileFolder . '/error';
        ExcelController::create($fileName, $orderNotFounds, $storePath, 'xlsx');

        \Log::info("End Script");
    }

    public function updateTag($page)
    {
        $limit = 1000;
        $skip = ($page - 1) * $limit;
        $task = 'task-18316';
        $fileName = 'FuegoCustomerBoxTagUpdate.csv';
        $logFileName = 'log-file-' . $page . '.log';
        \Log::useDailyFiles(storage_path() . '/logs/fuego-box/' . $task . '/' . $logFileName);
        \Log::info("script start $task $page");
        $filePath = resource_path('/assets/projects/fuego-box/' . $task . '/' . $fileName);
        $customers = ExcelController::view($filePath, $limit, $skip);

        //request in shopify
        $data = [];
        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);

        $shopifyCustomers = [];

        $basic = 'https://90357ec8836f4ed019c860cd51680f1b:087cdf62249a344f994a3679856ea783@fuego-box.myshopify.com/admin/';
        $basePath = $basic . 'customers/search.json';
        $tempRequest = CustomGuzzleHttp::connectTest();

        foreach ($customers as $customer) {
            $email = $customer['email'];
            $url = $basePath . '?query=email:' . $email;
            try {
                $result = $tempRequest->request('GET', $url);
            } catch (Exception $ex) {
                sleep(1);
                try {
                    $result = $tempRequest->request('GET', $url);
                } catch (Exception $ex) {
                    break;
                }
            }

            $result = json_decode($result->getBody(), TRUE);
            $resultCustomers = $result['customers'];
            $resultCustomer = $resultCustomers[0];
            \Log::info(['before_customer_id' => $resultCustomer['id'], 'tags' => $resultCustomer['tags']]);

            $tags = $resultCustomer['tags'];
            $tags = explode(',', $tags);
            $tags = array_map('trim', $tags);
            foreach ($tags as $key => $tag) {
                if (str_contains($tag, 'Box')) {
                    unset($tags[$key]);
                }
            }
            $tags[] = $customer['new_box_tag'];

            $params = [
                'customer' => [
                    'tags' => implode(',', $tags)
                ]
            ];
            $url = 'admin/customers/' . $resultCustomer['id'] . '.json';
            $updateCustomer = $shopifyRequest->update($url, $params);

            \Log::info(['after_customer_id' => $updateCustomer['customer']['id'], 'tags' => $updateCustomer['customer']['tags']]);
        }
        \Log::info("script end $task $page");
    }

    public function updateTagOrderWithCustomer()
    {
        $page = '-update-tag-2018-08-23';
        $task = 'task-18164';
        $fileName = 'FuegoAugust20Fix.csv';
        \Log::info("script start $task $page");
//        $skip = ($page - 1) * 1000;
        $filePath = resource_path('/assets/projects/fuego-box/' . $task . '/' . $fileName);

        $orders = ExcelController::view($filePath);
//        dd($orders);
        //request in shopify
        $data = [];
        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);

        //search customers
//        $basic = 'https://90357ec8836f4ed019c860cd51680f1b:087cdf62249a344f994a3679856ea783@fuego-box.myshopify.com/admin/';
//        $basePath = $basic . 'customers/search.json';
//        $tempRequest = CustomGuzzleHttp::connectTest();
        $shopifyOrders = [];
        foreach ($orders as $order) {
//            $email = $order['email'];

            $url = 'admin/orders/' . $order['order_id'] . '.json';
            $result = $shopifyRequest->view($url);
            $resultOrder = $result['order'];

            \Log::info(['order_id' => $resultOrder['id']]);
            //update orders tags
            $tags = [];
            $tags = $resultOrder['tags'];
            $tags = explode(',', $tags);
            $tags = array_map('trim', $tags);
            foreach ($tags as $key => $tag) {
                if (str_contains($tag, 'Box')) {
                    unset($tags[$key]);
                }
            }

            $tags[] = $order['new_order_tag'];
            $params = [
                'order' => [
                    'id' => $resultOrder['id'],
                    'tags' => implode(',', $tags)
                ]
            ];
            $url = 'admin/orders/' . $resultOrder['id'] . '.json';
            $result = $shopifyRequest->update($url, $params);
            $updatedOrder = $result['order'];

            //update customer tags
            $tags = [];
            $tags = $updatedOrder['customer']['tags'];
            $tags = explode(',', $tags);
            $tags = array_map('trim', $tags);
            foreach ($tags as $key => $tag) {
                if (str_contains($tag, 'Box')) {
                    unset($tags[$key]);
                }
            }

            $tags[] = $order['new_customer_tag'];
            $customerId = $updatedOrder['customer']['id'];
            $params = [
                'customer' => [
                    'id' => $customerId,
                    'tags' => implode(',', $tags)
                ]
            ];

            $url = 'admin/customers/' . $customerId . '.json';
            $result = $shopifyRequest->update($url, $params);
            $resultCustomers = $result['customer'];
            \Log::info(['customer_id' => $resultCustomers['id']]);
        }

//        \Log::info("generat sheet");
//        ExcelController::create("update-both-$page", $shopifyOrders, 'projects/fuegobox/' . $task, 'xlsx');
        \Log::info("script end $task $page");
        dd("stop");
    }

    public function updateTagReportAlen()
    {
        $page = 'second-time-requested';
        $task = 'task-18164';
        $fileName = 'Fuego Aug21-22 Rebill Fixes.csv';
        \Log::info("script start $task $page");
//        $skip = ($page - 1) * 1000;
        $filePath = resource_path('/assets/projects/fuego-box/' . $task . '/' . $fileName);
        $orders = ExcelController::view($filePath);

        //request in shopify
        $data = [];
        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);

        $shopifyOrders = [];
        foreach ($orders as $order) {
            $email = $order['email'];
            $orderId = (int) $order['id'];
            $url = 'admin/orders/' . $orderId . '.json';
            $result = $shopifyRequest->view($url);
            $resultOrder = $result['order'];
            \Log::info(["order_id" => $resultOrder['id']]);
            $customer = $resultOrder['customer'];
            $url = 'admin/customers/' . $customer['id'] . '/orders.json';
            $params = [
                'status' => 'any'
            ];
            $result = $shopifyRequest->view($url, $params);
            $resultOrders = $result['orders'];

            $month = NULL;
            $finalOrder = NULL;
            $checkMonth = NULL;
            $alenTags = $alenOrderId = NULL;
            $myTags = $myOrderId = NULL;
            foreach ($resultOrders as $newOrder) {

                if ($finalOrder) {
                    $alenTags = $newOrder['tags'];
                    $alenOrderId = $newOrder['id'];
                    $finalOrder = NULL;
                }

                $month = (int) date('m', strtotime($newOrder['processed_at']));
                if ($checkMonth && $month <= $checkMonth) {
                    $myTags = $newOrder['tags'];
                    $myOrderId = $newOrder['id'];
                    break;
                }

                if ($newOrder['id'] == $orderId) {
                    $finalOrder = TRUE;
                    $month = (int) date('m', strtotime($newOrder['processed_at']));
                    $checkMonth = $month - 1;
                }
            }

            $shopify = [];
            $shopify = $order;
            $shopify['customer_tags'] = $resultOrder['customer']['tags'];
            $shopify['last_order_id'] = ($alenOrderId) ? $alenOrderId : '';
            $shopify['last_order_tags'] = ($alenTags) ? $alenTags : '';
            $shopify['last_month_order_id'] = ($myOrderId) ? $myOrderId : '';
            $shopify['last_month_order_tags'] = ($myTags) ? $myTags : '';
            $shopify['is_last_order'] = ($finalOrder && !$alenTags) ? '1' : '0';
            $shopify['customer_order'] = $customer['orders_count'];

            $shopifyOrders[] = $shopify;
        }

        \Log::info("generat sheet");
        ExcelController::create("alen-update-$page", $shopifyOrders, 'projects/fuegobox/' . $task, 'xlsx')->download('xlsx');
        \Log::info("script end $task $page");
        dd("stop");
    }

    public function update()
    {
        #16664
        \Log::info("script start");
        dd("original please turn off notification customer first");

        $filePath = resource_path('/assets/projects/fuego-box/tagging-customer/Shipment-Fuego-Script.xlsx');
        $beforeUpdatedCustomers = ExcelController::view($filePath);
        $beforeEmails = array_column($beforeUpdatedCustomers, 'customer_email');
        $beforeUniqueEmails = array_count_values($beforeEmails);
        $beforeUniqueEmails = array_unique($beforeEmails);

        $filePath = resource_path('/assets/projects/fuego-box/task-16664/Fuego-Shipment-Update-Prepaid-Migrations.csv');
        $updateCustomers = ExcelController::view($filePath);
        $emails = array_column($updateCustomers, 'customer_email');
        $uniqueEmails = array_count_values($emails);
        $uniqueEmails = array_unique($emails);

//        $customers = array_diff($beforeUniqueEmails, $uniqueEmails);
        $customers = array_diff($uniqueEmails, $beforeUniqueEmails);

        $data = [];
        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);
        $updateCustomerErrors = $updatedCustomers = $updateOrderErrors = $updateOrders = $orderNotFounds = [];

        $basic = 'https://90357ec8836f4ed019c860cd51680f1b:087cdf62249a344f994a3679856ea783@fuego-box.myshopify.com/admin/';
        $basePath = $basic . 'customers/search.json';

        foreach ($uniqueEmails as $key => $uniqueEmail) {
            $email = $key;
            \Log::info(["customer_email" => $email]);
            $tempRequest = CustomGuzzleHttp::connectTest();
            $url = $basePath . '?query=email:' . $email;
            try {
                $result = $tempRequest->request('GET', $url);
            } catch (Exception $ex) {
                sleep(1);
                try {
                    $result = $tempRequest->request('GET', $url);
                } catch (Exception $ex) {
                    break;
                }
            }

            $result = json_decode($result->getBody(), TRUE);
            $customers = $result['customers'];
            if ($customers) {
                \Log::info(["customer_update_email" => $email]);
                $searchCustomer = $customers[0];
                $customerTags = $searchCustomer['tags'];
                $customerTags = explode(',', $customerTags);
                $customerTags = array_map('trim', $customerTags);

                foreach ($customerTags as $key => $customerTag) {
                    if (str_contains($customerTag, 'Box')) {
                        unset($customerTags[$key]);
                    }
                }
                $tags = $customerTags;
                $tagging = [];
                $keys = array_keys($emails, $email);
                if ($keys) {
                    foreach ($keys as $key) {
                        $tagging[] = $updateCustomers[$key]['tag'];
                        $tags[] = $updateCustomers[$key]['tag'];
                    }
                }
                $tags = implode(',', $tags);

                //customer tag update
                $newCustomer = [];
                $newCustomer['id'] = $searchCustomer['id'];
                $newCustomer['tags'] = $tags;
                $url = 'admin/customers/' . $searchCustomer['id'] . '.json';
                $params = [
                    'customer' => $newCustomer
                ];

                $result = $shopifyRequest->update($url, $params);
                if (!isset($result['customer'])) {
                    $eror = [];
                    $eror[$key] = $email;
                    $updateCustomerErrors[] = $eror;
                    \Log::info(["customer_update_error" => [$email, $result]]);
                } else {
                    \Log::info(["customer_update_success" => $email]);
                    $updatedCustomer = $newCustomer;
                    $updatedCustomer['search_email'] = $searchCustomer['email'];
                    $updatedCustomer['email'] = $email;
                    $updatedCustomers[] = $updatedCustomer;
                }
                //tag of customer's last order update

                $url = $basic . 'customers/' . $searchCustomer['id'] . '/orders.json?status=any&page=1&limit=250';
                try {
                    $result = $tempRequest->request('GET', $url);
                } catch (Exception $ex) {
                    sleep(1);
                    try {
                        $result = $tempRequest->request('GET', $url);
                    } catch (Exception $ex) {
                        break;
                    }
                }
                $result = json_decode($result->getBody(), TRUE);

                $orders = $result['orders'];
                $subscriptionOrder = FALSE;
                foreach ($orders as $order) {
                    $orderTags = [];
                    $orderTags = $order['tags'];
                    $orderTags = explode(',', $orderTags);
                    $orderTags = array_map('trim', $orderTags);

                    $tagFound = FALSE;
                    //find subscription order
                    foreach ($orderTags as $orderTag) {
                        if (str_contains(strtolower($orderTag), 'subscription')) {
                            $tagFound = TRUE;
                            break;
                        }
                    }

                    if ($tagFound) {
                        $subscriptionOrder = TRUE;
                        $orderUpdateTags = [];
                        foreach ($orderTags as $key => $orderTag) {
                            if (str_contains($orderTag, 'Box')) {
                                unset($orderTags[$key]);
                            }
                        }

                        $orderUpdateTags = array_merge($orderTags, $tagging);
                        $orderUpdateTags = implode(',', $orderUpdateTags);

                        $updateOrder = [];
                        $updateOrder['id'] = $order['id'];
                        $updateOrder['tags'] = $orderUpdateTags;
                        $url = '/admin/orders/' . $order['id'] . '.json';
                        $params = [
                            'order' => $updateOrder
                        ];
                        $result = $shopifyRequest->update($url, $params);
                        if (!isset($result['order'])) {
                            $eror = [];
                            $eror[$key] = $email;
                            $updateOrderErrors[] = $eror;
                            \Log::info(["order_update_error" => [$email, $result]]);
                        } else {
                            \Log::info(["order_update_success" => $email]);
                            $successOrder = $updateOrder;
                            $successOrder['search_email'] = $searchCustomer['email'];
                            $successOrder['email'] = $email;
                            $updateOrders[] = $successOrder;
                        }
                        break;
                    }
                }

                if (!$subscriptionOrder) {
                    $tempOrder = [];
                    $tempOrder['email'] = $email;
                    $tempOrder['tags'] = implode(',', $tagging);
                    $orderNotFounds[] = $tempOrder;
                }
            }
        }

        //Generate Excel sheets        
        \Log::info("Generate csv");

        $createFileFolder = 'projects/fuegobox/tagging-customer';

        $fileName = 'updated-customer';
        $storePath = $createFileFolder . '/success';
        ExcelController::create($fileName, $updatedCustomers, $storePath, 'xlsx');

        $fileName = 'updated-orders';
        $storePath = $createFileFolder . '/success';
        ExcelController::create($fileName, $updateOrders, $storePath, 'xlsx');

        $fileName = 'failed-customer';
        $storePath = $createFileFolder . '/error';
        ExcelController::create($fileName, $updateCustomerErrors, $storePath, 'xlsx');

        $fileName = 'failed-order';
        $storePath = $createFileFolder . '/error';
        ExcelController::create($fileName, $updateOrderErrors, $storePath, 'xlsx');

        $fileName = 'not-found-order';
        $storePath = $createFileFolder . '/error';
        ExcelController::create($fileName, $orderNotFounds, $storePath, 'xlsx');
        \Log::info("script end");

        dd("stop please");
    }
}
