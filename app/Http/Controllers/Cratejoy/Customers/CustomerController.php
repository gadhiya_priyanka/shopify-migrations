<?php

namespace App\Http\Controllers\Cratejoy\Customers;

use App\Helpers\Helper;
use App\Events\CustomerMigrated;
use App\Models\Schedule;
use App\Http\Controllers\Shopify\Api\CustomerController as ShopifyCustomerController;
use App\Http\Controllers\Cratejoy\CratejoyController;
use App\Models\ProjectCustomer;
use App\Models\ProjectCustomerSetting as CustomerSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    public $cjRequest, $pageId;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create(CustomerSetting $customerSetting)
    {
        $schedule = $customerSetting->schedule;
        $schedule->status = 'inprogress';
        $schedule->save();

        $project = $customerSetting->project;
        $this->cjRequest = new CratejoyController($project);
        $shopifyRequest = new ShopifyCustomerController($project->shopify_details);

        $lastCustomer = $customerSetting->customers()->latest()->first();
        $this->pageId = ($lastCustomer) ? $lastCustomer->extra['page_id'] + 1 : 1;

        $options = [];
        $options['with'] = 'addresses,subscriptions';
        $options['page'] = $this->pageId;
        $options['limit'] = 50;
        $migrateCustomers = $this->cjRequest->getCustomers($options);

        foreach ($migrateCustomers as $migrateCustomer) {
            $shopifyCustomer = $this->getShopifyCustomerObject($migrateCustomer);
            $result = $shopifyRequest->create($shopifyCustomer);
            $this->insertCustomerInDB($result, $migrateCustomer, $customerSetting);
        }

        if ($migrateCustomers) {
            $schedule = $customerSetting->schedule;
            $schedule->status = 'paused';
            $schedule->save();
        } else {
            event(new CustomerMigrated($customerSetting));
            $schedule = $customerSetting->schedule;
            $schedule->status = 'completed';
            $schedule->save();
        }
    }

    public function insertCustomerInDB($result, $migrateCustomer, $customerSetting)
    {
        $extra = [];
        $extra['type'] = (isset($result['customer'])) ? 'success' : 'error';
        $extra['customer_id'] = $migrateCustomer['id'];
        $extra['page_id'] = $this->pageId;

        $export = [];
        $export['customer_id'] = $migrateCustomer['id'];
        if (isset($result['customer'])) {
            $export['shopify_id'] = $result['customer']['id'];
        } else {
            $export['error'] = Helper::shopifyError($result);
        }

        $projectCustomer = new ProjectCustomer;
        $projectCustomer->id = \UUID::uuid4()->toString();
        $projectCustomer->project_id = $customerSetting->project->id;
        $projectCustomer->customer = (isset($result['customer'])) ? $result['customer'] : ['error' => $result];
        $projectCustomer->extra = $extra;
        $projectCustomer->export = $export;

        $customerSetting->customers()->save($projectCustomer);
        unset($result, $migrateCustomer, $customerSetting);
    }

    public function getShopifyCustomerObject($migrateCustomer)
    {
        $shopifyCustomer = [];
//        $shopifyCustomer['email'] = $migrateCustomer['email'];
        $shopifyCustomer['email'] = 'priyanka+' . $migrateCustomer['id'] . '@wolfpointagency.com';
        $shopifyCustomer['first_name'] = $migrateCustomer['first_name'];
        $shopifyCustomer['last_name'] = $migrateCustomer['last_name'];
        $shopifyCustomer['tags'] = $this->getTags($migrateCustomer);
        $shopifyCustomer['addresses'] = $this->getCustomerAddresses($migrateCustomer['addresses']);

        unset($migrateCustomer);
        return $shopifyCustomer;
    }

    public function getTags($migrateCustomer)
    {
        $customerTags = [];
        $customerTags[] = 'migrated';
        $customerTags[] = 'cj-import';
        $customerTags[] = 'cj-' . $migrateCustomer['id'];
        $customerTags[] = 'subscription_status-' . $migrateCustomer['subscription_status'];
        $customerTags[] = 'num_orders-' . $migrateCustomer['num_orders'];
        $customerTags[] = 'num_subscriptions-' . $migrateCustomer['num_subscriptions'];
        foreach ($migrateCustomer['subscriptions'] as $subcription) {
            $customerTags[] = 'subscription_id-' . $subcription['id'];
        }

        unset($migrateCustomer);
        return implode(', ', $customerTags);
    }

    public function getCustomerAddresses($addresses)
    {
        $shopifyAddresses = [];
        foreach ($addresses as $address) {
            $customerAddress = $address['address'];

            $shopifyAddress = [];
            $shopifyAddress['address1'] = $customerAddress['street'];
            $shopifyAddress['address2'] = $customerAddress['unit'];
            $shopifyAddress['company'] = $customerAddress['company'];
            $shopifyAddress['city'] = $customerAddress['city'];
            $shopifyAddress['country_code'] = $customerAddress['country'];
            if ($customerAddress['country']) {
                $shopifyAddress['province_code'] = $customerAddress['state'];
            }
            $shopifyAddress['zip'] = $customerAddress['zip_code'];
            $shopifyAddress['phone'] = $customerAddress['phone_number'];
            if ($address['is_primary']) {
                $shopifyAddress['default'] = true;
            }
            $shopifyAddresses[] = $shopifyAddress;
        }

        unset($address, $shopifyAddress, $addresses);
        return $shopifyAddresses;
    }
}
