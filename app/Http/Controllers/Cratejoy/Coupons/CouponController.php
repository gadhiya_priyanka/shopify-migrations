<?php

namespace App\Http\Controllers\Cratejoy\Coupons;

use Exception;
use App\Http\Controllers\Core\ExcelController;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Http\Controllers\Cratejoy\CratejoyController;
use App\Models\ProjectCollectionSetting as CollectionSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CouponController extends Controller
{

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create(CollectionSetting $collectionSetting)
    {
        dd("fuego CJ coupons update");
        \Log::info("Script Start");
        $filePath = resource_path('assets/fuego/discounts/shopify_coupons.xlsx');
        $shopifyDiscounts = ExcelController::view($filePath);
//        $result = ExcelController::view($filePath);
//        dd($result);
//        $shopifyDiscounts = $result[0];
//dd($shopifyDiscounts);
        $shopifyDiscounts = array_slice($shopifyDiscounts, 93);
//        dd($shopifyDiscounts);
        $project = $collectionSetting->project;
        $shopifyRequest = new ExternalApiRequestController($project->shopify_details);

        $failedUpdates = $notFounds = [];

        foreach ($shopifyDiscounts as $shopifyDiscount) {
            $couponCode = $shopifyDiscount['coupon'];
            $discount = \DB::table('fuego_discounts')->where('discount_code_code', $couponCode)->first();
            if ($discount) {
                if ($discount->price_rule_title != $discount->discount_code_code) {
                    $priceRule = [];
                    $priceRule['id'] = $discount->price_rule_id;
                    $priceRule['title'] = $discount->discount_code_code;

                    $url = 'admin/price_rules/' . $discount->price_rule_id . '.json';
                    $paramas = [
                        'price_rule' => $priceRule
                    ];
                    $result = $shopifyRequest->update($url, $paramas);
                    if (!isset($result['price_rule'])) {
                        sleep(1);
                        $result = $shopifyRequest->update($url, $paramas);
                        if (!isset($result['price_rule'])) {
                            $failedUpdate = [];
                            $failedUpdate = $shopifyDiscount;
                            $failedUpdates[] = $failedUpdate;
                        }
                    }
                }
            } else {
                $notFound = [];
                $notFound = $shopifyDiscount;
                $notFounds[] = $notFound;
            }
        }

        \Log::info("Generate CSV");
        $fileName = 'shopify-coupons-not-founds';
        $storePath = 'fuego-box/new-coupons';
        $fileType = 'csv';
        ExcelController::create($fileName, $notFounds, $storePath, $fileType);

        $fileName = 'shopify-coupons-failed-update';
        $storePath = 'fuego-box/new-coupons';
        $fileType = 'csv';
        ExcelController::create($fileName, $failedUpdates, $storePath, $fileType);

        \Log::info("Script End");
        dd("done");
        \Log::info("Script Start");


        for ($i = 1; $i <= 3; $i++) {
            \Log::info(["page_id" => $i]);
            $url = 'admin/price_rules.json';
            $params = [
                'limit' => 250,
                'page' => $i,
                'fields' => 'id,title'
            ];
            $result = $shopifyRequest->view($url, $params);
            $priceRules = $result['price_rules'];
            foreach ($priceRules as $priceRule) {
                \Log::info(["price_rule_title" => $priceRule['title']]);
                $url = 'admin/price_rules/' . $priceRule['id'] . '/discount_codes.json';
                $result = $shopifyRequest->view($url);

                if (!isset($result['discount_codes'])) {
                    sleep(1);
                    $result = $shopifyRequest->view($url);
                }

                $discounts = $result['discount_codes'];
                if ($result['discount_codes']) {
                    foreach ($discounts as $discount) {
                        $shopifyDiscount = [];
                        $shopifyDiscount['price_rule_id'] = $priceRule['id'];
                        $shopifyDiscount['price_rule_title'] = $priceRule['title'];
                        $shopifyDiscount['discount_code_id'] = $discount['id'];
                        $shopifyDiscount['discount_code_code'] = $discount['code'];

                        \DB::table('fuego_discounts')->insert($shopifyDiscount);
                    }
                } else {
                    $shopifyDiscount = [];
                    $shopifyDiscount['price_rule_id'] = $priceRule['id'];
                    $shopifyDiscount['price_rule_title'] = $priceRule['title'];
                    \DB::table('fuego_discounts')->insert($shopifyDiscount);
                }
            }
            sleep(1);
        }
        \Log::info("Script End");
    }
}
