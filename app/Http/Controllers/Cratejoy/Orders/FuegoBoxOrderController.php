<?php

namespace App\Http\Controllers\Cratejoy\Orders;

use App\Helpers\Helper;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Models\CustomGuzzleHttp;
use App\Models\Project;
use App\Http\Controllers\Cratejoy\CratejoyController;
use App\Http\Controllers\Core\ExcelController;
use App\Models\ProjectOrderSetting as OrderSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FuegoBoxOrderController extends Controller
{
    public $cjOrderController, $cjRequest, $db;

    public function __construct()
    {
        set_time_limit(0);
        dd("Stop all process");
    }

//    public function create(OrderSetting $orderSetting)
    public function createOld()
    {
        $projectName = 'fuegobox';
        $folderName = 'gifts';
        $logFileName = 'gift-order';

        \Log::useDailyFiles(storage_path() . '/logs/' . $projectName . '/' . $folderName . '/' . $logFileName);
        \Log::info('Start Script');
        $project = Project::find('8e790f16-ae91-4dd6-936f-ec52ad6af9a7');
        $cjRequest = new CratejoyController($project);
//        dd("fuegobox order create changes");
        $filePath = resource_path('assets/fuego-box/gift-order/unclaimed-gifts.csv');
        $giftOrders = ExcelController::view($filePath);

//        $filePath = resource_path('assets/fuego-box/gift-order/success-orders.xlsx');
//        $shopifyOrders = ExcelController::view($filePath);

        $finalGiftOrders = [];

        foreach ($giftOrders as $giftOrder) {
            $orderId = $giftOrder['gift_sending_order_id'];
            \Log::info(['gift_order_id' => $orderId]);
            $cjOrder = $cjRequest->getOrderByID($orderId);
            if (is_string($cjOrder)) {
                sleep(1);
                $cjOrder = $cjRequest->getOrderByID($orderId);
            }
            $giftOrder['sender_email'] = $cjOrder['customer']['email'];
            $finalGiftOrders[] = $giftOrder;
        }

        $fileName = 'unclaimed-gifts';
        $storePath = 'fuegobox/gifts';
        ExcelController::create($fileName, $finalGiftOrders, $storePath);
        \Log::info('End Script');
    }

    public function create()
    {
        dd("Stop Migrated");
        \Log::info("Script Start");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $this->db = $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $data = [];
        $data['api_key'] = 'fuego3';
        $data['api_password'] = '8uEmZHQjGFd7z7zH';
        $data['api_url'] = 'http://api.cratejoy.com';
        $this->cjRequest = new ExternalApiRequestController($data);

        $shopifyData = [];
        $shopifyData['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $shopifyData['api_password'] = '979469234d2e0e72d764cf980b255300';
        $shopifyData['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($shopifyData);
        $this->cjOrderController = new OrderController();

        $failedOrders = $db->table('failed_orders')->select('cj_order_id')->pluck('cj_order_id')->toArray();
        $pendingOrders = $db->table('cj_sp_orders')->whereNull('sp_order_id')->whereNotIn('cj_order_id', $failedOrders)->take(50)->get()->toArray();

        foreach ($pendingOrders as $pendingOrder) {
            $orderId = $pendingOrder->cj_order_id;
            \Log::info(["cj_order_id" => $orderId]);

            $url = 'v1/orders/' . $orderId;
            $params = [
                'with' => 'gifts,customer,products,order_gift_info,coupons,cart,sent_gift,received_gift,ship_address,bill_address,transactions'
            ];
            $migrateOrder = $this->cjRequest->view($url, $params);
            $shopifyOrder = $this->getShopifyOrderObject($migrateOrder);

            if (!$shopifyOrder) {
                \Log::info(["error_order_id - $orderId" => 'instance_error']);
                $failedOrder = [];
                $failedOrder['cj_order_id'] = $orderId;
                $failedOrder['error'] = 'instance_error';
                $this->db->table('failed_orders')->insert($failedOrder);
                sleep(2);
                continue;
            }

            $url = 'admin/orders.json';
            $params = [
                'order' => $shopifyOrder
            ];
            $result = $shopifyRequest->create($url, $params);
            if (isset($result['order'])) {
                \Log::info(["success_order_id - $orderId" => $result['order']['id']]);
                $cjOrder = $db->table('cj_orders')->where('cj_order_id', $orderId)->first();

                $migratedOrder = $result['order'];

                $insertOrder = [];
                $insertOrder['sp_customer_id'] = $migratedOrder['customer']['id'];
                $insertOrder['sp_email'] = $migratedOrder['customer']['email'];
                $insertOrder['sp_order_id'] = $migratedOrder['id'];
                $insertOrder['sp_financial_status'] = $migratedOrder['financial_status'];
                $insertOrder['sp_fulfillment_status'] = $migratedOrder['fulfillment_status'];
                $insertOrder['sp_placed_at'] = $migratedOrder['processed_at'];
                $insertOrder['sp_placed_date'] = date('Y-m-d', strtotime($migratedOrder['processed_at']));
                $insertOrder['sp_order_sub_total'] = $migratedOrder['subtotal_price'];
                $insertOrder['sp_order_total_price'] = $migratedOrder['total_price'];
                $insertOrder['app_id'] = $migratedOrder['app_id'];
                $insertOrder['browser_ip'] = $migratedOrder['browser_ip'];
                $insertOrder['is_deleted'] = 0;
                $insertOrder['is_migrated'] = 1;
                $insertOrder['migrated_app'] = 1;
                $insertOrder['cj_customer_id'] = $cjOrder->cj_customer_id;
                $db->table('shopify_orders')->insert($insertOrder);

                $db->table('cj_sp_orders')->where('id', $pendingOrder->id)->update(['sp_order_id' => $migratedOrder['id']]);
            } else {
                \Log::info(["error_order_id - $orderId" => $result]);

                $failedOrder = [];
                $failedOrder['cj_order_id'] = $orderId;
                $failedOrder['error'] = $result;
                $db->table('failed_orders')->insert($failedOrder);
            }
            sleep(2);
        }
        \Log::info("Script End");
        dd("done");
    }

    public function getShopifyOrderObject($migrateOrder)
    {
        //product list for migrate order
        $products = ($migrateOrder['products']) ? $migrateOrder['products'] : $migrateOrder['gifts'];
        $migratedEmail = $migrateOrder['customer']['email'];
        $dbCustomer = $this->db->table('cj_sp_customers')->where('cj_customer_email', $migratedEmail)->first();
        $spCustomerEmail = ($dbCustomer->sp_customer_email) ? $dbCustomer->sp_customer_email : $migratedEmail;
        $shopifyOrder = [];

        $shopifyOrder['email'] = str_contains($spCustomerEmail, ',') ? $migratedEmail : $spCustomerEmail;
        $shopifyOrder['line_items'] = $this->getLineItems($products, $migrateOrder['id']);

        if (!$shopifyOrder['line_items']) {
            return FALSE;
        }
        $shopifyOrder['tags'] = $this->cjOrderController->getTags($migrateOrder);

        $shopifyOrder['financial_status'] = $migrateOrder['financial_status'];
        $shopifyOrder['fulfillment_status'] = 'fulfilled';

        $shopifyOrder['created_at'] = Helper::shopifyDateFormate($migrateOrder['placed_at']);

        if (isset($migrateOrder['ship_address'])) {
            $shopifyOrder['shipping_address'] = $this->cjOrderController->getAddress($migrateOrder['ship_address']);
        }

        if (isset($migrateOrder['bill_address'])) {
            $shopifyOrder['billing_address'] = $this->cjOrderController->getAddress($migrateOrder['bill_address']);
        }

        $shopifyOrder['shipping_lines'] = $this->cjOrderController->getShippings($migrateOrder['total_shipping']);

        $shopifyOrder['tax_lines'] = $this->cjOrderController->getTaxLines($migrateOrder['cart']);
        $shopifyOrder['discount_codes'] = $this->cjOrderController->getDiscountCodes($migrateOrder['coupons']);
        $shopifyOrder['transactions'] = $this->cjOrderController->getTransactions($migrateOrder['transactions']);

        unset($migrateOrder, $products);
        return $shopifyOrder;
    }

    public function getLineItems($products, $orderId)
    {
        $lineItems = [];
        foreach ($products as $product) {
            $qty = (int) (isset($product['quantity']) ? $product['quantity'] : '1');
            $variants = (isset($product['instance']['variants'])) ? $product['instance']['variants'] : [];
            $variantTitle = [];
            foreach ($variants as $variant) {
                $variantTitle[] = $variant['variant_value']['value'];
            }
            $variantTitle = implode(' / ', $variantTitle);

            try {
                $productId = $product['instance']['product_id'];
            } catch (\Exception $ex) {
                return FALSE;
                \Log::info(["instance_error_order_id - $orderId" => json_encode($products)]);
                \Log::info(["instance_error_order_id - $orderId" => $ex->getMessage()]);

                $tempFailed = $this->db->table('temp_failed')->where('cj_order_id', $orderId)->first();
                if ($tempFailed) {
                    $count = $tempFailed->count + 1;

                    if ($count == 4) {
                        $failedOrder = [];
                        $failedOrder['cj_order_id'] = $orderId;
                        $failedOrder['error'] = 'instance_error';
                        $this->db->table('failed_orders')->insert($failedOrder);
                        \Log::info("Script End");
                        dd("stop");
                    }
                    $this->db->table('temp_failed')->where('id', $tempFailed->id)->update(['count' => $count]);
                } else {
                    $temp = [];
                    $temp['cj_order_id'] = $orderId;
                    $temp['count'] = 1;
                    $this->db->table('temp_failed')->insert($temp);
                }
                \Log::info("Script End");
                dd("stop");
            }

            $url = 'v1/products/' . $productId;
            $migrateProduct = $this->cjRequest->view($url);
            if (!isset($migrateProduct['id'])) {
                sleep(1);
                $migrateProduct = $this->cjRequest->view($url);
            }
            $lineItem = [];
            $lineItem['title'] = $migrateProduct['name'];
            $lineItem['sku'] = $product['instance']['sku'];
            $lineItem['variant_title'] = $variantTitle;
            $lineItem['price'] = ($product['price'] / $qty) / 100;
            $lineItem['quantity'] = $qty;
            $lineItems[] = $lineItem;
        }

        return $lineItems;
    }

    public function boxTagReportForAlen()
    {
        dd("stop boxTagReportForAlen");
        $task = 'task-18897';
        $fileName = 'Sep20FuegoRebill.csv';

        //connect database
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        //fuegobox api request
        $data = [];
        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);

        //get order records which need to proceed
        $lastRecord = $db->table($task)->orderBy('page_id', 'desc')->get()->first();
        $pageId = ($lastRecord) ? $lastRecord->page_id + 1 : 1;
        $size = 100;
        $skip = ($pageId - 1) * $size;
        $filePath = resource_path('assets/projects/fuego-box/' . $task . '/' . $fileName);

        $orders = ExcelController::view($filePath, $size, $skip);

        foreach ($orders as $order) {
            $orderId = (int) $order['id'];
            $url = 'admin/orders/' . $orderId . '.json';
            $shopifyOrder = $shopifyRequest->view($url);
            $shopifyCustomer = $shopifyOrder['order']['customer'];

            $url = 'admin/customers/' . $shopifyCustomer['id'] . '/orders.json';
            $params = [
                'limit' => 250,
                'fields' => 'id, tags, processed_at',
                'status' => 'any'
            ];
            $result = $shopifyRequest->view($url, $params);
            $customerOrders = $result['orders'];
            $alenOrder = $lastOrder = NULL;

            foreach ($customerOrders as $customerOrder) {
                if ($alenOrder) {
                    $lastOrder = $customerOrder;
                    break;
                }

                if ($customerOrder['id'] == $orderId) {
                    $alenOrder = TRUE;
                }
            }

            $dbOrder = [];
            $dbOrder['name'] = $order['name'];
            $dbOrder['email'] = $order['email'];
            $dbOrder['order_id'] = $orderId;
            $dbOrder['order_tags'] = $order['tags'];
            $dbOrder['customer_tags'] = $shopifyCustomer['tags'];
            $dbOrder['customer_order_count'] = $shopifyCustomer['orders_count'];
            $dbOrder['last_order_tags'] = isset($lastOrder['tags']) ? $lastOrder['tags'] : '';
            $dbOrder['last_order_id'] = isset($lastOrder['id']) ? $lastOrder['id'] : '';
            $dbOrder['page_id'] = $pageId;
            $db->table($task)->insert($dbOrder);
        }
        dd("Done");
    }

    public function cjOrderBackToDB()
    {
        dd("stop start cjOrderBackToDB");
        //connect database
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);
        $lastRecord = $db->table('cj_orders')->orderBy('page_id', 'desc')->get()->first();
        $pageId = ($lastRecord) ? $lastRecord->page_id + 1 : 1;
        \Log::info("start script $pageId");
        //fuegobox api request
        $data = [];
        $data['api_key'] = 'fuego3';
        $data['api_password'] = '8uEmZHQjGFd7z7zH';
        $data['api_url'] = 'http://api.cratejoy.com/';
        $cjRequest = new ExternalApiRequestController($data);
        $url = 'v1/orders';
        $params = [
            'page' => $pageId,
            'limit' => 300,
//            'with' => 'gifts,customer,products,order_gift_info,coupons,cart,sent_gift,received_gift,ship_address,bill_address,transactions'
        ];
        $result = $cjRequest->view($url, $params);
        $orders = $result['results'];
        foreach ($orders as $order) {
            $dbOrder = [];
            $dbOrder['cj_customer_id'] = $order['customer']['id'];
            $dbOrder['cj_email'] = $order['customer']['email'];
            $dbOrder['cj_order_id'] = $order['id'];
            $dbOrder['cj_financial_status'] = $order['financial_status'];
            $dbOrder['cj_fulfillment_status'] = $order['fulfillment_status'];
            $dbOrder['cj_placed_at'] = $order['placed_at'];
            $dbOrder['cj_placed_date'] = date('Y-m-d', strtotime($order['placed_at']));
            $dbOrder['cj_order_sub_total'] = $order['sub_total'];
            $dbOrder['cj_order_total'] = $order['total'];
            $dbOrder['cj_order_total_price'] = $order['total_price'];
            $dbOrder['page_id'] = $pageId;
            $db->table('cj_orders')->insert($dbOrder);
        }
        \Log::info("end script $pageId");
        dd("done");
    }

    public function shopifyOrderFulfilled()
    {
        dd("stop start shopifyOrderFulfilled");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $lastRecord = $db->table('shopify_orders')->orderBy('page_id', 'desc')->get()->first();
        $pageId = ($lastRecord) ? $lastRecord->page_id + 1 : 1;
        \Log::info("start script $pageId");

        $task = 'task-15596';
        $fileName = 'updated_orders.csv';
        $filePath = resource_path('assets/projects/fuego-box/' . $task . '/' . $fileName);
        $size = 100;
        $skip = ($pageId - 1) * $size;
        $orders = ExcelController::view($filePath, $size, $skip);

        //fuegobox api request
        $data = [];
        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);

        foreach ($orders as $order) {
            $orderId = (int) $order['id'];
            $url = 'admin/orders/' . $orderId . '.json';
            $params = [
                'fields' => 'id, customer, processed_at, total_price, subtotal_price, financial_status, fulfillment_status'
            ];
            $result = $shopifyRequest->view($url, $params);
            if (isset($result['order'])) {
                $shopifyOrder = $result['order'];
                $dbOrder = [];
                $dbOrder['sp_customer_id'] = $shopifyOrder['customer']['id'];
                $dbOrder['sp_email'] = $shopifyOrder['customer']['email'];
                $dbOrder['sp_order_id'] = $shopifyOrder['id'];
                $dbOrder['sp_financial_status'] = $shopifyOrder['financial_status'];
                $dbOrder['sp_fulfillment_status'] = $shopifyOrder['fulfillment_status'];
                $dbOrder['sp_placed_at'] = $shopifyOrder['processed_at'];
                $dbOrder['sp_placed_date'] = date('Y-m-d', strtotime($shopifyOrder['processed_at']));
                $dbOrder['sp_order_sub_total'] = $shopifyOrder['subtotal_price'];
                $dbOrder['sp_order_total_price'] = $shopifyOrder['total_price'];
                $dbOrder['page_id'] = $pageId;
                $db->table('shopify_orders')->insert($dbOrder);
            } else {
                $dbOrder = [];
                $dbOrder['sp_email'] = $order['email'];
                $dbOrder['sp_order_id'] = $orderId;
                $dbOrder['page_id'] = $pageId;
                $db->table('shopify_orders')->insert($dbOrder);
            }
        }
        \Log::info("end script $pageId");
        dd("done");
    }

    public function pendingOrderList()
    {
        dd("stop Cratejoy\Orders >> FuegoBoxOrderController >> pendingOrderList");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $filePath = resource_path('assets/projects/fuego-box/task-15596/delete-extra-order.xlsx');
        $orders = ExcelController::view($filePath);
        foreach ($orders as $order) {
            $id = (int) $order['id'];
            $db->table('pending_orders')->where('id', $id)->delete();
        }
        dd("done");


        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);
        $orders = $db->table('shopify_orders')->orderBy('id', 'asc')->get()->toArray();
        \Log::info("Start script");
        foreach ($orders as $order) {
            \Log::info(["order_id" => $order->sp_order_id]);
            $searchEmail = $db->table('cj_not_have_emails')->where('email', $order->sp_email)->first();
            $searchEmail = ($searchEmail) ? $searchEmail->cj_email : $order->sp_email;
            $searchEmail = ($searchEmail) ?: $order->sp_email;

            $cjOrders = $db->table('cj_orders')->where(['cj_email' => $searchEmail, 'cj_placed_date' => $order->sp_placed_date])->get()->toArray();
            foreach ($cjOrders as $cjOrder) {
                $pendingOrder = [];
                $pendingOrder['sp_email'] = $order->sp_email;
                $pendingOrder['sp_order_id'] = $order->sp_order_id;
                $pendingOrder['sp_placed_at'] = $order->sp_placed_at;
                $pendingOrder['sp_placed_date'] = $order->sp_placed_date;
                $pendingOrder['cj_email'] = $cjOrder->cj_email;
                $pendingOrder['cj_order_id'] = $cjOrder->cj_order_id;
                $pendingOrder['cj_placed_at'] = $cjOrder->cj_placed_at;
                $pendingOrder['cj_placed_date'] = $cjOrder->cj_placed_date;
                $pendingOrder['cj_financial_status'] = $cjOrder->cj_financial_status;
                $pendingOrder['cj_fulfillment_status'] = $cjOrder->cj_fulfillment_status;
                $db->table('pending_orders')->insert($pendingOrder);
            }

            if (!$cjOrders) {
                $pendingOrder = [];
                $pendingOrder['sp_email'] = $order->sp_email;
                $pendingOrder['sp_order_id'] = $order->sp_order_id;
                $pendingOrder['sp_placed_at'] = $order->sp_placed_at;
                $pendingOrder['sp_placed_date'] = $order->sp_placed_date;
                $db->table('pending_orders')->insert($pendingOrder);
            }
        }
        \Log::info("End script");
        dd("stop");
    }

    public function shopifyOrderBackUp()
    {
        dd("stop take backup");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $lastRecord = $db->table('shopify_orders')->orderBy('page_id', 'desc')->get()->first();
        $pageId = ($lastRecord) ? $lastRecord->page_id + 1 : 1;
        \Log::info(["start script" => $pageId]);
        $data = [];
        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);
        $limit = $pageId + 50;
        for ($i = $pageId; $i <= $limit; $i++) {
            \Log::info(["start page_id" => $pageId]);
            $url = 'admin/orders.json';
            $params = [
                'page' => $pageId,
                'limit' => 250,
                'status' => 'any',
                'fields' => 'id, customer, financial_status, fulfillment_status, processed_at, subtotal_price, total_price, app_id, browser_ip'
            ];
            $result = $shopifyRequest->view($url, $params);
            $orders = $result['orders'];
            foreach ($orders as $order) {
                $dbOrder = [];
                $dbOrder['sp_customer_id'] = isset($order['customer']) ? $order['customer']['id'] : NULL;
                $dbOrder['sp_email'] = isset($order['customer']) ? $order['customer']['email'] : NULL;
                $dbOrder['sp_order_id'] = $order['id'];
                $dbOrder['sp_financial_status'] = $order['financial_status'];
                $dbOrder['sp_fulfillment_status'] = $order['fulfillment_status'];
                $dbOrder['sp_placed_at'] = $order['processed_at'];
                $dbOrder['sp_placed_date'] = date('Y-m-d', strtotime($order['processed_at']));
                $dbOrder['sp_order_sub_total'] = $order['subtotal_price'];
                $dbOrder['sp_order_total_price'] = $order['total_price'];
                $dbOrder['app_id'] = $order['app_id'];
                $dbOrder['browser_ip'] = $order['browser_ip'];
                $dbOrder['page_id'] = $pageId;
                $db->table('shopify_orders')->insert($dbOrder);
            }
            \Log::info(["end page_id" => $pageId]);
            $pageId++;
        }
        \Log::info(["end script" => $pageId]);
    }

    public function deletePendingOrders()
    {
        dd("stop Cratejoy\Orders >> FuegoBoxOrderController >> deletePendingOrders");
        \Log::info("Start script");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $shopifyOrders = $db->table('pending_orders')->whereNotNull('cj_order_id')->get()->toArray();

        foreach ($shopifyOrders as $shopifyOrder) {
            \Log::info(['order_id' => $shopifyOrder->sp_order_id]);
            $updateOrder = [];
            $updateOrder['is_deleted'] = 1;
            $db->table('shopify_orders')->where('sp_order_id', $shopifyOrder->sp_order_id)->update($updateOrder);
        }
    }

    public function cjVsSp()
    {
        dd("compare");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

//        $page = 298;
//        $fileName = 'success-orders-' . $page . '-2.csv';
        $fileName = 'remaing-orders.xlsx';

        \Log::info(["start file" => $fileName]);
//        $filePath = resource_path('assets/projects/fuego-box/success-orders/' . $fileName);
        $filePath = resource_path('assets/projects/fuego-box/success-orders/Extra/' . $fileName);
        try {
            $successOrders = ExcelController::view($filePath);
        } catch (\Exception $ex) {
            \Log::info(["error $fileName file" => $ex->getMessage()]);
            $insertFailedPage = [];
            $insertFailedPage['page'] = $page;
            $db->table('failed_page_ids')->insert($insertFailedPage);
            $successOrders = [];
        }

        foreach ($successOrders as $successOrder) {
            $spOrderId = (int) $successOrder['shopify_order_id'];
//            $cjOrderId = (int) $successOrder['crate_joy_order_id'];
            $orderTags = explode(',', $successOrder['crate_joy_order_id']);
            $orderTags = array_map('trim', $orderTags);
            foreach ($orderTags as $orderTag) {
                if (str_contains($orderTag, 'cj-')) {
                    $cjOrderId = (int) str_replace('cj-', '', $orderTag);
                    break;
                }
            }

            $shopifyOrder = $db->table('shopify_orders')->where(['is_deleted' => 0, 'sp_order_id' => $spOrderId])->first();
            if ($shopifyOrder) {
                $updateOrder = [];
                $updateOrder['is_migrated'] = 1;
                $db->table('shopify_orders')->where(['is_deleted' => 0, 'sp_order_id' => $spOrderId])->update($updateOrder);

                $cjOrder = $db->table('cj_sp_orders')->where(['cj_order_id' => $cjOrderId])->first();
                $spOrderIds = ($cjOrder->sp_order_id) ? explode(',', $cjOrder->sp_order_id) : [];
                $spOrderIds = array_map('trim', $spOrderIds);
                if (!in_array($spOrderId, $spOrderIds)) {
                    $spOrderIds[] = $spOrderId;
                    $db->table('cj_sp_orders')->where(['cj_order_id' => $cjOrderId])->update(['sp_order_id' => implode(', ', $spOrderIds)]);
                }
            }
        }
        \Log::info(["end file" => $fileName]);
    }

    public function cjVsSpLog()
    {
        dd("Stop cjVsSpLog");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $fileName = 'orders-298-1-2018-04-30.log';
        $filePath = resource_path('assets/projects/fuego-box/success-order-log/' . $fileName);

        $file = fopen($filePath, "r");
        $text = fread($file, filesize($filePath));
        $lines = explode(PHP_EOL, $text);
        \Log::info(["start file" => $fileName]);
        foreach ($lines as $key => $line) {
            if (str_contains(trim($line), 'order success')) {
                $prevKey = $key - 3;

                $cjOrderId = $this->getAfterKeyword($lines[$prevKey], '=>', ',');
                $spOrderId = $this->getAfterKeyword($line, '=>', ',');

                $shopifyOrder = $db->table('shopify_orders')->where(['is_deleted' => 0, 'sp_order_id' => $spOrderId])->first();
                if ($shopifyOrder) {
                    $updateOrder = [];
                    $updateOrder['is_migrated'] = 1;
                    $db->table('shopify_orders')->where(['is_deleted' => 0, 'sp_order_id' => $spOrderId])->update($updateOrder);

                    $cjOrder = $db->table('cj_sp_orders')->where(['cj_order_id' => $cjOrderId])->first();
                    $spOrderIds = ($cjOrder->sp_order_id) ? explode(',', $cjOrder->sp_order_id) : [];
                    $spOrderIds = array_map('trim', $spOrderIds);
                    if (!in_array($spOrderId, $spOrderIds)) {
                        $spOrderIds[] = $spOrderId;
                        $db->table('cj_sp_orders')->where(['cj_order_id' => $cjOrderId])->update(['sp_order_id' => implode(', ', $spOrderIds)]);
                    }
                }
            }
        }
        \Log::info(["end file" => $fileName]);
    }

    public function deleteOrdersLog()
    {
        dd("stop deleteOrdersLog");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $fileName = 'orders-258-1-2018-02-09.log';
        $filePath = resource_path('assets/projects/fuego-box/success-order-log/' . $fileName);

        $file = fopen($filePath, "r");
        $text = fread($file, filesize($filePath));
        $lines = explode(PHP_EOL, $text);
        \Log::info(["start file" => $fileName]);
        foreach ($lines as $key => $line) {
            if (str_contains(trim($line), 'order success')) {
                $spOrderId = $this->getAfterKeyword($line, '=>', ',');

                $shopifyOrder = $db->table('shopify_orders')->where(['is_deleted' => 0, 'sp_order_id' => $spOrderId])->first();

                if ($shopifyOrder) {
                    $updateOrder = [];
                    $updateOrder['is_migrated'] = 1;
                    $updateOrder['is_deleted'] = 1;
                    $db->table('shopify_orders')->where(['is_deleted' => 0, 'sp_order_id' => $spOrderId])->update($updateOrder);
                }
            }
        }
        \Log::info(["end file" => $fileName]);
    }

    public function getAfterKeyword($str, $keyword, $remove = NULL)
    {
        $str = trim($str);
        $t = trim(substr($str, strpos($str, "=>") + 2));

        if ($remove) {
            $t = trim($t, ',');
        }
        return $t;
    }

    public function deleteRecord()
    {
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $orders = $db->table('cj_sp_orders')->where('sp_order_id', 'LIKE', '%,%')->get()->toArray();
        $notDeleteList = $db->table('two_emails')->pluck('cj_order_id')->toArray();

        \Log::info("Start script of delete order");
        $data = [];
        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);

        foreach ($orders as $order) {
            \Log::info(["order_id" => $order->cj_order_id]);

            $deletedOrders = explode(',', $order->sp_order_id);
            $deletedOrders = array_map('trim', $deletedOrders);
            $deletedOrders = array_unique($deletedOrders);

            foreach ($deletedOrders as $key => $deletedOrder) {
                $url = 'admin/orders/' . $deletedOrder . '.json';
                $result = $shopifyRequest->view($url);

                if (is_string($result) && str_contains($result, 'Not Found')) {
                    unset($deletedOrders[$key]);
                }
            }

            $db->table('cj_sp_orders')->where('id', $order->id)->update(['sp_order_id' => implode(', ', $deletedOrders)]);
        }

        \Log::info("End script of delete order");
        dd("Done");
    }

    public function spOrderTransactions()
    {
//        dd("testing");
        \Log::info("Start script of update sp partial refund order");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $data = [];
        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);

        $cjOrderList = $db->table('cj_orders')->where('cj_financial_status', 'voided')->pluck('cj_order_id')->toArray();

//        $spOrderList = $db->table('sp_transactions')->select('sp_order_id')->distinct()->pluck('sp_order_id')->toArray();
        $orders = $db->table('cj_sp_orders')->whereNotNull('sp_order_id')->whereIn('cj_order_id', $cjOrderList)->get()->toArray();

        foreach ($orders as $order) {
            $orderId = $order->sp_order_id;
            $url = 'admin/orders/' . $orderId . '/transactions.json';
            $result = $shopifyRequest->view($url);
            $transactions = $result['transactions'];
            foreach ($transactions as $transaction) {
                $newOrder = [];
                $newOrder['sp_order_id'] = $transaction['order_id'];
                $newOrder['kind'] = $transaction['kind'];
                $newOrder['status'] = $transaction['status'];
                $newOrder['amount'] = $transaction['amount'];
                $newOrder['source_name'] = $transaction['source_name'];
                $newOrder['redund_done'] = 0;
                $db->table('sp_transactions')->insert($newOrder);
            }
        }
        \Log::info("End script of update sp partial refund order");
    }

    public function cjOrderTransactions()
    {
        \Log::info("Start script of update cj partial refund order");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $data = [];
        $data['api_key'] = 'fuego3';
        $data['api_password'] = '8uEmZHQjGFd7z7zH';
        $data['api_url'] = 'http://api.cratejoy.com';
        $shopifyRequest = new ExternalApiRequestController($data);

        $spOrderList = $db->table('sp_transactions')->where('redund_done', 0)->pluck('sp_order_id')->toArray();

        $orders = $db->table('cj_sp_orders')->whereIn('sp_order_id', $spOrderList)->get()->toArray();

        foreach ($orders as $order) {
            $orderId = $order->cj_order_id;
            $url = 'v1/orders/' . $orderId;
            $params = [
                'with' => 'transactions'
            ];
            $result = $shopifyRequest->view($url, $params);
            $transactions = $result['transactions'];

            foreach ($transactions as $transaction) {
                if ($transaction['result'] == 2 || $transaction['result'] == 4) {
                    $newOrder = [];
                    $newOrder['cj_order_id'] = $transaction['order_id'];
                    $newOrder['sp_order_id'] = $order->sp_order_id;
                    $newOrder['kind'] = ($transaction['result'] == 2) ? 'sale' : 'refund';
                    $newOrder['status'] = 'success';
                    $newOrder['amount'] = $transaction['total'] / 100;
                    $newOrder['source_name'] = $transaction['store_id'];
                    $db->table('cj_transactions')->insert($newOrder);
                }
            }
        }
        \Log::info("End script of update cj partial refund order");
    }

    public function listPendingOrders()
    {
        \Log::info("Start script pending orders");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $cjOrderList = $db->table('cj_orders')->where('cj_financial_status', 'pending')->pluck('cj_order_id')->toArray();
        $orders = $db->table('cj_sp_orders')->whereNull('sp_order_id')->whereIn('cj_order_id', $cjOrderList)->get()->toArray();

        foreach ($orders as $order) {
            \Log::info(['order_id' => $order->cj_order_id]);
            $db->table('cj_sp_orders')->where('id', $order->id)->update(['sp_order_id' => 'pending_status']);
        }
        \Log::info("End script pending orders");
    }

    public function voidedTransactions()
    {
//        dd("Delete voided orders");

        \Log::info("Start voided order list");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $data = [];
        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);

        $cjOrderList = $db->table('cj_orders')->where('cj_financial_status', 'voided')->pluck('cj_order_id')->toArray();
        $spOrderList = $db->table('cj_sp_orders')->whereNotNull('sp_order_id')->whereIn('cj_order_id', $cjOrderList)->pluck('sp_order_id')->toArray();
        $orders = $db->table('shopify_orders')->where(['is_deleted' => 0, 'is_migrated' => 1, 'migrated_app' => 1])->whereIN('sp_order_id', $spOrderList)->get()->toArray();

        foreach ($orders as $order) {
            $orderId = $order->sp_order_id;
            $url = 'admin/orders/' . $orderId . '/transactions.json';
            $result = $shopifyRequest->view($url);
            $transactions = $result['transactions'];
            foreach ($transactions as $transaction) {
                $newOrder = [];
                $newOrder['sp_order_id'] = $transaction['order_id'];
                $newOrder['kind'] = $transaction['kind'];
                $newOrder['status'] = $transaction['status'];
                $newOrder['amount'] = $transaction['amount'];
                $newOrder['source_name'] = $transaction['source_name'];
                $newOrder['redund_done'] = 0;
                $db->table('deleted_order_transactions')->insert($newOrder);
            }
        }
        \Log::info("End voided order list");
        dd("done");
    }

    public function customerCjEmailCompare()
    {
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);
        \Log::info("script start");
        $customers = $db->table('cj_email_compare')->whereNull('sp_email')->get()->toArray();

        $skipCustomers = $db->table('cj_orders')->select('cj_email')->distinct()->pluck('cj_email')->toArray();
        $searchCustomers = $db->table('shopify_orders')->whereNotIn('sp_email', $skipCustomers)->where(['migrated_app' => 1])->select('sp_email')->distinct()->pluck('sp_email')->toArray();

        foreach ($customers as $customer) {
            foreach ($searchCustomers as $searchCustomer) {
                similar_text($customer->cj_email, $searchCustomer, $percent);
                if ($percent >= 90) {
                    $orders = $db->table('shopify_orders')->where(['sp_email' => $searchCustomer, 'is_deleted' => 0, 'migrated_app' => 1])->get()->toArray();

                    $updateCustomer = [];
                    $updateCustomer['sp_email'] = $searchCustomer;
                    $updateCustomer['sp_order_count'] = count($orders);
                    $updateCustomer['is_change'] = 1;
                    $db->table('cj_email_compare')->where('id', $customer->id)->update($updateCustomer);
                }
            }
        }
        \Log::info("script done");
    }

    public function compareCjvsSp()
    {
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $orders = $db->table('cj_sp_all_customers')->whereNull('cj_migrated_order_count')->take(1500)->get()->toArray();

        \Log::info("Script start");
        foreach ($orders as $order) {
            $cjOrders = $db->table('cj_sp_all_customers_backup')->where('cj_customer_id', $order->cj_customer_id)->first();
            $spOrders = $db->table('shopify_orders')->where(['cj_customer_id' => $order->cj_customer_id, 'is_migrated' => 1, 'migrated_app' => 1, 'is_deleted' => 0])->get()->toArray();

            $updateCustomer = [];
            $updateCustomer['cj_migrated_order_count'] = ($cjOrders) ? $cjOrders->cj_remaining_order_count : '0';
            $updateCustomer['cj_remaining_order_count'] = $order->cj_order_count - $updateCustomer['cj_migrated_order_count'];
            if ($spOrders) {
                $spEmails = [];
                $spCustomerIds = [];
                foreach ($spOrders as $spOrder) {
                    if (!in_array(trim($spOrder->sp_email), $spEmails)) {
                        $spEmails[] = trim($spOrder->sp_email);
                        $spCustomerIds[] = trim($spOrder->sp_customer_id);
                    }
                }

                $spEmails = implode(', ', $spEmails);
                $spCustomerIds = implode(', ', $spCustomerIds);

                $updateCustomer['sp_customer_id'] = $spCustomerIds;
                $updateCustomer['sp_customer_email'] = $spEmails;
                $updateCustomer['sp_migrated_order_count'] = count($spOrders);
            }
            $db->table('cj_sp_all_customers')->where('id', $order->id)->update($updateCustomer);
        }
        \Log::info("Script end");
        dd("done stop it");
    }

    public function deleteCustomers()
    {
        dd("All deleted");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $data = [];
        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);

        $logFileName = 'delete-customers.log';

        $logPath = storage_path('logs/fuego-solved/delete-customers/' . $logFileName);
        \Log::useDailyFiles($logPath);
        \Log::info("Start delete customers");
        $deleteCustomers = $db->table('shopify_orders')->where('is_deleted', 1)->select('sp_customer_id')->distinct()->pluck('sp_customer_id')->toArray();

        foreach ($deleteCustomers as $deleteCustomer) {
            \Log::info(["delete_customer_id" => $deleteCustomer]);
            $url = 'admin/customers/' . $deleteCustomer . '.json';
            $params = [
                'fields' => 'orders_count, id, email'
            ];
            $result = $shopifyRequest->view($url, $params);
            $customer = $result['customer'];

            if ($customer['orders_count'] == 0) {
                \Log::info(["deleted_customer" => json_encode($customer)]);
                $shopifyRequest->delete($url);

                $insertCustomer = [];
                $insertCustomer['customer_id'] = $customer['id'];
                $insertCustomer['customer_email'] = $customer['email'];
                $insertCustomer['orders_count'] = $customer['orders_count'];
                $db->table('deleted_customers')->insert($insertCustomer);
            }
        }
        \Log::info("End  delete customers");
    }

    public function compareCjvsSp1()
    {
        dd("compare");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $data = [];
        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);

        $orders = $db->table('cj_sp_customers')->whereNull('sp_customer_email')->get()->toArray();

        \Log::info("Script start");
        foreach ($orders as $order) {

            $url = 'admin/customers/search.json';
            $params = [
                'query' => 'email:' . $order->cj_customer_email
            ];
            $result = $shopifyRequest->view($url, $params);

            if (isset($result['customers'][0])) {
                $updateCustomer = [];
                $updateCustomer['sp_customer_id'] = $result['customers'][0]['id'];
                $updateCustomer['sp_customer_email'] = $result['customers'][0]['email'];
                $updateCustomer['sp_migrated_order_count'] = 0;
                $updateCustomer['search_api'] = 1;
                $db->table('cj_sp_customers')->where('id', $order->id)->update($updateCustomer);
            }
        }
        \Log::info("Script end");
    }

    public function deleteOrders()
    {
        \Log::info("Start script delete voided orders");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $data = [];
        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);
        $orders = $db->table('deleted_order_transactions')->select('sp_order_id')->distinct()->get()->toArray();

        foreach ($orders as $order) {
            $spOrderId = $order->sp_order_id;
            \Log::info(["order_id" => $spOrderId]);
            $url = 'admin/orders/' . $spOrderId . '.json';
            $result = $shopifyRequest->delete($url);
            if (is_string($result)) {
                \Log::info(["error_order_id $spOrderId" => $result]);
            }
            $db->table('shopify_orders')->where('sp_order_id', $spOrderId)->update(['is_deleted' => 1]);
            $cjVsSpOrder = $db->table('cj_sp_orders')->where('sp_order_id', $spOrderId)->first();
            $db->table('cj_sp_orders')->where('id', $cjVsSpOrder->id)->update(['sp_order_id' => NULL]);
        }
        \Log::info("End script delete voided orders");
    }

    public function createTransactions()
    {
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);
        $orders = $db->table('sp_transactions')->where(['redund_done' => 0])->get()->toArray();

        \Log::info("Start script of update sp refunded order");
        $data = [];
        $data['api_key'] = '7949e29bbfdd752434fe09bc7e3d28ef';
        $data['api_password'] = '979469234d2e0e72d764cf980b255300';
        $data['api_url'] = 'https://fuego-box.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);

        foreach ($orders as $order) {
            $orderId = $order->sp_order_id;
            \Log::info(["order_id" => $orderId]);

            $transactions = $db->table('cj_transactions')->where(['sp_order_id' => $orderId, 'kind' => 'refund'])->get()->toArray();

            foreach ($transactions as $transaction) {
                if ($transaction->amount > 0) {
                    $newTranscation = [];
                    $newTranscation['amount'] = $transaction->amount;
                    $newTranscation['kind'] = 'refund';
                    $newTranscation['status'] = 'success';
                    $params = [
                        'transaction' => $newTranscation
                    ];
                    $url = 'admin/orders/' . $orderId . '/transactions.json';
                    $result = $shopifyRequest->create($url, $params);
                    $spTransaction = $result['transaction'];
                } else {
                    $spTransaction = [];
                    $spTransaction['order_id'] = $orderId;
                    $spTransaction['kind'] = 'refund';
                    $spTransaction['status'] = 'success';
                    $spTransaction['amount'] = 0;
                    $spTransaction['source_name'] = '1968708';
                }

                $newOrder = [];
                $newOrder['sp_order_id'] = $spTransaction['order_id'];
                $newOrder['kind'] = $spTransaction['kind'];
                $newOrder['status'] = $spTransaction['status'];
                $newOrder['amount'] = $spTransaction['amount'];
                $newOrder['source_name'] = $spTransaction['source_name'];
                $newOrder['redund_done'] = 1;
                $db->table('sp_transactions')->insert($newOrder);
            }
            $db->table('sp_transactions')->where('id', $order->id)->update(['redund_done' => 1]);
        }
        \Log::info("End script of update sp refunded order");
    }
}

//special order id [order migrated then customer payment in cj for this orders]
//shopify => cratejoy
//422022283306 => 1441987808
//440457068586 => 1488519003
//deleted pending orders
//634145833002
//634124337194
//455262011434



    