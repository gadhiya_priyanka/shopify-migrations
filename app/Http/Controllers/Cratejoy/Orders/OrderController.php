<?php

namespace App\Http\Controllers\Cratejoy\Orders;

use App\Http\Controllers\Shopify\Api\ProductVariantController;
use App\Events\OrderMigrated;
use App\Helpers\Helper;
use App\Models\Schedule;
use App\Http\Controllers\Shopify\Core\OrderController as CoreOrderController;
use App\Http\Controllers\Shopify\Api\OrderController as ShopifyOrderController;
use App\Http\Controllers\Cratejoy\CratejoyController;
use App\Models\ProjectOrderSetting as OrderSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public $cjRequest, $proVarRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function delete(OrderSetting $orderSetting)
    {
        dd("order delete script");
        \Log::info("Script Start");
        $project = $orderSetting->project;
        $shopifyRequest = new ShopifyOrderController($project->shopify_details);

        $result = $shopifyRequest->get();

        $orders = $result['orders'];

        foreach ($orders as $order) {
            $shopifyRequest->delete($order['id']);
        }
        \Log::info("Script End");
        dd("Deleted");
    }

    public function deleteUsingSheet(OrderSetting $orderSetting)
    {
        dd("order delete sheet");
        $filePath = resource_path('assets/projects/migration-tool/cj/orders/project_orders.csv');
        $shopifyOrders = \App\Http\Controllers\Core\ExcelController::view($filePath);

        $project = $orderSetting->project;
        $shopifyRequest = new ShopifyOrderController($project->shopify_details);
        \Log::info("Script Start");
        foreach ($shopifyOrders as $shopifyOrder) {
            $deleteOrder = json_decode($shopifyOrder['json'], true);
            $result = $shopifyRequest->delete($deleteOrder['shopify_id']);
            if (is_string($result)) {
                \Log::info([$deleteOrder['shopify_id'] => $result]);
            }
        }
        \Log::info("Script End");
        dd("Stop");
    }

    public function create(OrderSetting $orderSetting)
    {
        $schedule = $orderSetting->schedule;
        $schedule->status = 'inprogress';
        $schedule->save();

        $this->orderSetting = $orderSetting;
        $lastOrder = $orderSetting->orders()->latest()->first();

        $project = $orderSetting->project;
        $this->cjRequest = new CratejoyController($project);

        $coreOrderController = new CoreOrderController($orderSetting);

        //for search product variants 
        $this->proVarRequest = new ProductVariantController($project->shopify_details);

        $pageId = ($lastOrder) ? $lastOrder->extra['page_id'] + 1 : 1;
        $options = [];
        $options['with'] = 'gifts,customer,products,order_gift_info,coupons,cart,sent_gift,received_gift,ship_address,bill_address,transactions';
        $options['page'] = $pageId;
        $options['limit'] = 100;
        $migrateOrders = $this->cjRequest->getOrders($options);

        if (is_array($migrateOrders)) {
            foreach ($migrateOrders as $migrateOrder) {
                \Log::info(["order_id" => $migrateOrder['id']]);
                $extra = [];
                $extra['page_id'] = $pageId;
                $extra['order_id'] = $migrateOrder['id'];

                $export = [];
                $export['order_id'] = $migrateOrder['id'];

                $shopifyOrder = $this->getShopifyOrderObject($migrateOrder);
                $shopifyOrder['extra'] = $extra;
                $shopifyOrder['export'] = $export;
                $shopifyOrder['customer'] = $this->getShopifyCustomerObject($migrateOrder);

                $coreOrderController->create($shopifyOrder, $migrateOrder);
            }
        }

        if ($migrateOrders) {
            $schedule = $orderSetting->schedule;
            $schedule->status = 'paused';
            $schedule->save();
        } else {
            event(new OrderMigrated($orderSetting));
            $schedule = $orderSetting->schedule;
            $schedule->status = 'completed';
            $schedule->save();
        }
    }

    public function getShopifyCustomerObject($migrateOrder)
    {
        $cjCustomer = $this->cjRequest->getCustomerByID($migrateOrder['customer']['id']);

        $customerTags = [];
        $customerTags[] = 'migrated';
        $customerTags[] = 'cj-import';
        $customerTags[] = 'cj-' . $migrateOrder['customer']['id'];
        $customerTags[] = 'subscription_status-' . $cjCustomer['subscription_status'];
        $customerTags[] = 'num_orders-' . $cjCustomer['num_orders'];
        $customerTags[] = 'num_subscriptions-' . $cjCustomer['num_subscriptions'];
        foreach ($cjCustomer['subscriptions'] as $subcription) {
            $customerTags[] = 'subscription_id-' . $subcription['id'];
        }

        $shopifyCustomer = [];
        $shopifyCustomer['first_name'] = $migrateOrder['customer']['first_name'];
        $shopifyCustomer['last_name'] = $migrateOrder['customer']['last_name'];
        $shopifyCustomer['email'] = 'priyanka+' . $migrateOrder['customer']['id'] . '@wolfpointagency.com';
        $shopifyCustomer['tags'] = implode(', ', $customerTags);

        unset($migrateOrder, $cjCustomer, $customerTags);
        return $shopifyCustomer;
    }

    public function getShopifyOrderObject($migrateOrder)
    {
        //product list for migrate order
        $products = ($migrateOrder['products']) ? $migrateOrder['products'] : $migrateOrder['gifts'];

        $shopifyOrder = [];
//        $shopifyOrder['email'] = $migrateOrder['customer']['email'];
        $shopifyOrder['email'] = 'priyanka+' . $migrateOrder['customer']['id'] . '@wolfpointagency.com';
        $shopifyOrder['line_items'] = $this->getLineItems($products);

        $shopifyOrder['tags'] = $this->getTags($migrateOrder);

        $shopifyOrder['financial_status'] = $this->orderSetting->mapping['financial_statuses'][$migrateOrder['financial_status']];
        $shopifyOrder['fulfillment_status'] = $this->orderSetting->mapping['fulfillment_statuses'][$migrateOrder['fulfillment_status']];

        $shopifyOrder['created_at'] = Helper::shopifyDateFormate($migrateOrder['placed_at']);

        if (isset($migrateOrder['ship_address'])) {
            $shopifyOrder['shipping_address'] = $this->getAddress($migrateOrder['ship_address']);
        }

        if (isset($migrateOrder['bill_address'])) {
            $shopifyOrder['billing_address'] = $this->getAddress($migrateOrder['bill_address']);
        }

        $shopifyOrder['shipping_lines'] = $this->getShippings($migrateOrder['total_shipping']);
        if ($migrateOrder['gifts']) {
            $shopifyOrder['note'] = $migrateOrder['gifts'][0]['gift']['message'];
        }
        $shopifyOrder['tax_lines'] = $this->getTaxLines($migrateOrder['cart']);
        $shopifyOrder['discount_codes'] = $this->getDiscountCodes($migrateOrder['coupons']);
        $shopifyOrder['transactions'] = $this->getTransactions($migrateOrder['transactions']);

        unset($migrateOrder, $products);
        return $shopifyOrder;
    }

    public function getTransactions($transactions)
    {
        $orderTransactions = [];
        foreach ($transactions as $transaction) {
            if (($transaction['result'] == 2 || $transaction['result'] == 4) && $transaction['total'] > 0) {
                $orderTransaction = [];
                $orderTransaction['status'] = 'success';
                $orderTransaction['amount'] = $transaction['total'] / 100;
                if ($transaction['result'] == 4) {
                    $orderTransaction['kind'] = "refund";
                }
                $orderTransactions[] = $orderTransaction;
            }
        }

        unset($transactions, $transaction);
        return $orderTransactions;
    }

    public function getDiscountCodes($coupons)
    {
        $discountCodes = [];

        foreach ($coupons as $coupon) {
            $discountCode = [];
            $discountCode['amount'] = ($coupon['discount']) ? ($coupon['discount'] / 100) : '0';
            $discountCode['code'] = $coupon['code'];
            $discountCode['type'] = 'fixed_amount';
            $discountCodes[] = $discountCode;
        }

        unset($discountCode, $coupons);
        return $discountCodes;
    }

    public function getTaxLines($cart)
    {
        $taxLines = [];

        if ($cart) {
            foreach ($cart['tax_rates'] as $cartTaxRate) {
                $taxLine = [];
                if ($cartTaxRate['name']) {
                    $taxLine['title'] = $cartTaxRate['name'];
                }
                $taxLine['price'] = $cartTaxRate['price'] / 100;
                $taxLine['rate'] = $cartTaxRate['rate'];

                $taxLines[] = $taxLine;
            }
        }

        unset($cart, $taxLine);
        return $taxLines;
    }

    public function getShippings($totalShipping)
    {
        $shippingLines = [];
        if ($totalShipping > 0) {
            $shippingLine = [];
            $shippingLine['code'] = 'Shipping Charge';
            $shippingLine['title'] = 'Shipping';
            $shippingLine['source'] = 'shopify';
            $shippingLine['price'] = $totalShipping / 100;
            $shippingLines[] = $shippingLine;
        }

        unset($shippingLine, $totalShipping);
        return $shippingLines;
    }

    public function getAddress($address)
    {
        $orderAddress = [];
        if ($address['street']) {
            $orderAddress['address1'] = $address['street'];
        }

        if ($address['unit']) {
            $orderAddress['address2'] = $address['unit'];
        }

        if ($address['company']) {
            $orderAddress['company'] = $address['company'];
        }

        if ($address['city']) {
            $orderAddress['city'] = $address['city'];
        }

        if ($address['state']) {
            $orderAddress['province_code'] = $address['state'];
        }

        if ($address['country']) {
            $orderAddress['country_code'] = $address['country'];
        }

        if ($address['zip_code']) {
            $orderAddress['zip'] = $address['zip_code'];
        }

        if ($address['phone_number']) {
            $orderAddress['phone'] = $address['phone_number'];
        }

        if ($address['to']) {
            $orderAddress['name'] = $address['to'];
        }

        unset($address);
        return $orderAddress;
    }

    public function getTags($migrateOrder)
    {
        $orderTags = [];
        $orderTags[] = 'migrated';
        $orderTags[] = 'cj-import';
        $orderTags[] = 'cj-' . $migrateOrder['id'];
        $orderTags[] = 'financial_status-' . $migrateOrder['financial_status'];
        $orderTags[] = 'fulfillment_status-' . $migrateOrder['fulfillment_status'];
        if ($migrateOrder['gifts']) {
            $orderTags[] = 'gift-order';
        } else {
            $products = $migrateOrder['products'];
            if ($products) {
                $subscriptionProduct = FALSE;
                foreach ($products as $product) {
                    if ($product['subscription_id']) {
                        $subscriptionProduct = TRUE;
                        $orderTags[] = 'subscription-order';
                        $orderTags[] = 'subscription-id-' . $product['subscription_id'];
                    }
                }
                if (!$subscriptionProduct) {
                    $orderTags[] = 'one-time-order';
                }
            }
        }

        if ($migrateOrder['sent_gift']) {
            $orderTags[] = 'gift-sender';
        }

        if ($migrateOrder['received_gift']) {
            $orderTags[] = 'gift-received';
        }

        unset($migrateOrder, $subscriptionProduct, $products);
        return implode(',', $orderTags);
    }

    public function getLineItems($products)
    {
        $lineItems = [];

        foreach ($products as $product) {
            $variantId = NULL;
            $productSku = trim($product['instance']['sku']);
            $query = "sku:" . $productSku;
            $result = $this->proVarRequest->search($query);
            if ($result['data']['shop']['productVariants']['edges']) {
                $id = $result['data']['shop']['productVariants']['edges'][0]['node']['id'];
                $variantId = trim(strrchr($id, "/"), '/');
            }

            $qty = (int) (isset($product['quantity']) ? $product['quantity'] : '1');
            if ($variantId) {
                $lineItem = [];
                $lineItem['variant_id'] = $variantId;
                $lineItem['price'] = ($product['price'] / $qty) / 100;
                $lineItem['quantity'] = $qty;
                $lineItems[] = $lineItem;
            } else {
                $variants = (isset($product['instance']['variants'])) ? $product['instance']['variants'] : [];
                $variantTitle = [];
                foreach ($variants as $variant) {
                    $variantTitle[] = $variant['variant_value']['value'];
                }
                $variantTitle = implode(' / ', $variantTitle);

                $migrateProduct = $this->cjRequest->getProductByID($product['instance']['product_id']);
                if (!isset($migrateProduct['id'])) {
                    sleep(1);
                    $migrateProduct = $this->cjRequest->getProductByID($product['instance']['product_id']);
                }
                $lineItem = [];
                $lineItem['title'] = $migrateProduct['name'];
                $lineItem['sku'] = $product['instance']['sku'];
                $lineItem['variant_title'] = $variantTitle;
                $lineItem['price'] = ($product['price'] / $qty) / 100;
                $lineItem['quantity'] = $qty;
                $lineItems[] = $lineItem;
            }
        }

        unset($lineItem, $products, $variantId, $productSku, $result, $id, $qty, $variants, $variantTitle, $migrateProduct, $product);
        return $lineItems;
    }
}
