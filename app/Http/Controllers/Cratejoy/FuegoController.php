<?php

namespace App\Http\Controllers\Cratejoy;

use App\Models\CustomGuzzleHttp;
use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FuegoController extends Controller
{
    public $cjRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create()
    {
        dd("Hey its start fuegobox order migrate");
        \Log::info("Start Script");
        $data = [];
        $data['api_key'] = 'fuego3';
        $data['api_password'] = '8uEmZHQjGFd7z7zH';
        $data['api_url'] = 'http://api.cratejoy.com';
        $this->cjRequest = $cjRequest = new ExternalApiRequestController($data);

        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'fuego_box';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $orders = $db->table('missing_orders_both')->whereNull('sp_order_id')->get()->toArray();
        foreach ($orders as $order) {
            \Log::info(['order_id' => $order->cj_order_id]);
            $url = '/v1/orders/' . $order->cj_order_id;
            $params = [
                'with' => 'gifts,customer,products,order_gift_info,coupons,cart,sent_gift,received_gift,ship_address,bill_address'
            ];
            $migrateOrder = $cjRequest->view($url, $params);
            $products = ($migrateOrder['products']) ? $migrateOrder['products'] : $migrateOrder['gifts'];
            foreach ($products as $product) {
                $insertOrder = [];
                $insertOrder['cj_order_id'] = $order->cj_order_id;
                $insertOrder['sku'] = $product['instance']['sku'];
                $db->table('missing_order_sku')->insert($insertOrder);
            }
        }

        \Log::info("End Script");
        dd("stop");
    }

    public function getShopifyOrderObject($migrateOrder)
    {
        //product list for migrate order
        $products = ($migrateOrder['products']) ? $migrateOrder['products'] : $migrateOrder['gifts'];

        $shopifyOrder = [];
        $shopifyOrder['email'] = $migrateOrder['customer']['email'];
        $shopifyOrder['line_items'] = $this->getLineItems($products);

        $shopifyOrder['tags'] = $this->getTags($migrateOrder);

        $shopifyOrder['financial_status'] = $migrateOrder['financial_status'];
        $shopifyOrder['fulfillment_status'] = 'fulfilled';

        $shopifyOrder['created_at'] = Helper::shopifyDateFormate($migrateOrder['placed_at']);

        if (isset($migrateOrder['ship_address'])) {
            $shopifyOrder['shipping_address'] = $this->getAddress($migrateOrder['ship_address']);
        }

        if (isset($migrateOrder['bill_address'])) {
            $shopifyOrder['billing_address'] = $this->getAddress($migrateOrder['bill_address']);
        }

        $shopifyOrder['shipping_lines'] = $this->getShippings($migrateOrder['total_shipping']);

        $shopifyOrder['tax_lines'] = $this->getTaxLines($migrateOrder['cart']);
        $shopifyOrder['discount_codes'] = $this->getDiscountCodes($migrateOrder['coupons']);
        $shopifyOrder['transactions'] = $this->getTransactions($migrateOrder);

        unset($migrateOrder, $products);
        return $shopifyOrder;
    }

    public function getLineItems($products)
    {
        $lineItems = [];

        foreach ($products as $product) {
            $variants = (isset($product['instance']['variants'])) ? $product['instance']['variants'] : [];
            $variantTitle = [];
            foreach ($variants as $variant) {
                $variantTitle[] = $variant['variant_value']['value'];
            }
            $variantTitle = implode(' / ', $variantTitle);

            $migrateProduct = $this->getProductByID($product['instance']['product_id']);
            if (!isset($migrateProduct['id'])) {
                sleep(1);
                $migrateProduct = $this->getProductByID($product['instance']['product_id']);
            }
            $qty = (int) (isset($product['quantity']) ? $product['quantity'] : '1');

            $lineItem = [];
            $lineItem['title'] = $migrateProduct['name'];
            $lineItem['sku'] = $product['instance']['sku'];
            $lineItem['variant_title'] = $variantTitle;
            $lineItem['price'] = ($product['price'] / $qty) / 100;
            $lineItem['quantity'] = $qty;
            $lineItems[] = $lineItem;
        }

        return $lineItems;
    }

    public function getProductByID($productId)
    {
        $url = 'v1/products/' . $productId;
        return $this->cjRequest->view($url);
    }

    public function getTags($migrateOrder)
    {
        $orderTags = [];
        $orderTags[] = 'migrated';
        $orderTags[] = 'Cj Import';
        $orderTags[] = 'cj-' . $migrateOrder['id'];
        $orderTags[] = 'financial_status-' . $migrateOrder['financial_status'];
        $orderTags[] = 'fulfillment_status-' . $migrateOrder['fulfillment_status'];
        if ($migrateOrder['gifts']) {
            $orderTags[] = 'gift-order';
        } else {
            if ($migrateOrder['products']) {
                if ($migrateOrder['products'][0]['subscription_id']) {
                    $orderTags[] = 'subscription-order';
                } else {
                    $orderTags[] = 'one-time-order';
                }
            }
        }

        if ($migrateOrder['sent_gift']) {
            $orderTags[] = 'gift-sender';
        }

        if ($migrateOrder['received_gift']) {
            $orderTags[] = 'gift-received';
        }

        unset($migrateOrder);
        return implode(',', $orderTags);
    }

    public function getAddress($address)
    {
        $orderAddress = [];
        if ($address['street']) {
            $orderAddress['address1'] = $address['street'];
        }

        if ($address['unit']) {
            $orderAddress['address2'] = $address['unit'];
        }

        if ($address['company']) {
            $orderAddress['company'] = $address['company'];
        }

        if ($address['city']) {
            $orderAddress['city'] = $address['city'];
        }

        if ($address['state']) {
            $orderAddress['province_code'] = $address['state'];
        }

        if ($address['country']) {
            $orderAddress['country_code'] = $address['country'];
        }

        if ($address['zip_code']) {
            $orderAddress['zip'] = $address['zip_code'];
        }

        if ($address['phone_number']) {
            $orderAddress['phone'] = $address['phone_number'];
        }

        if ($address['to']) {
            $orderAddress['name'] = $address['to'];
        }

        unset($address);
        return $orderAddress;
    }

    public function getShippings($totalShipping)
    {
        $shippingLines = [];
        if ($totalShipping > 0) {
            $shippingLine = [];
            $shippingLine['code'] = 'Shipping Charge';
            $shippingLine['title'] = 'Shipping';
            $shippingLine['source'] = 'shopify';
            $shippingLine['price'] = $totalShipping / 100;
            $shippingLines[] = $shippingLine;
        }

        unset($shippingLine, $totalShipping);
        return $shippingLines;
    }

    public function getTaxLines($cart)
    {
        $taxLines = [];

        if ($cart) {
            foreach ($cart['tax_rates'] as $cartTaxRate) {
                $taxLine = [];
                if ($cartTaxRate['name']) {
                    $taxLine['title'] = $cartTaxRate['name'];
                }
                $taxLine['price'] = $cartTaxRate['price'] / 100;
                $taxLine['rate'] = $cartTaxRate['rate'];

                $taxLines[] = $taxLine;
            }
        }

        unset($cart, $taxLine);
        return $taxLines;
    }

    public function getDiscountCodes($coupons)
    {
        $discountCodes = [];

        foreach ($coupons as $coupon) {
            $discountCode = [];
            $discountCode['amount'] = ($coupon['discount']) ? ($coupon['discount'] / 100) : '0';
            $discountCode['code'] = $coupon['code'];
            $discountCode['type'] = 'fixed_amount';
            $discountCodes[] = $discountCode;
        }

        unset($discountCode, $coupons);
        return $discountCodes;
    }

    public function getTransactions($migrateOrder)
    {
        $transactions = [];

        //transaction amount
        if ($migrateOrder['total'] > 0) {
            $transaction = [];
            $transaction['status'] = 'success';
            $transaction['amount'] = $migrateOrder['total'] / 100;
            $transactions[] = $transaction;
        }

        //refunded amount
        if ($migrateOrder['refunded_amount'] > 0) {
            $transaction = [];
            $transaction['status'] = 'success';
            $transaction['amount'] = $migrateOrder['refunded_amount'] / 100;
            $transaction['kind'] = "refund";
            $transactions[] = $transaction;
        }

        unset($migrateOrder, $transaction);
        return $transactions;
    }
}
