<?php

namespace App\Http\Controllers\Cratejoy\Redirects;

use App\Http\Controllers\Cratejoy\CratejoyController;
use App\Http\Controllers\Core\ExcelController;
use App\Events\RedirectMigrated;
use App\Http\Controllers\Shopify\Core\RedirectController as CoreRedirectController;
use App\Models\ProjectRedirectSetting as RedirectSetting;
use App\Http\Controllers\Core\CommonFunctionsController;
use App\Http\Controllers\Shopify\ShopifyController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RedirectController extends Controller
{
    public $commonFunctions, $shopify, $project, $externalRedirect;

    public function __construct()
    {
        set_time_limit(0);
        $this->commonFunctions = new CommonFunctionsController;
        $this->shopify = new ShopifyController;
    }

    public function create(RedirectSetting $redirectSetting)
    {
        $this->externalRedirect = $redirectSetting->project->redirectSettings()->where(['mapping->external' => true])->first();

        $schedule = $redirectSetting->schedule;
        $schedule->status = 'inprogress';
        $schedule->save();

        $this->project = $redirectSetting->project;
        $lastRedirect = $redirectSetting->redirects()->latest()->first();

        $page = ($lastRedirect) ? $lastRedirect->extra['page_id'] + 1 : 1;
        $filePath = storage_path($redirectSetting->file_paths['input']['file_path']);
        $skip = ($page - 1) * 100;
        $migrateRedirects = ExcelController::view($filePath, 100, $skip);

        $coreRedirectController = new CoreRedirectController($redirectSetting);

        //convert into shopify urls from platform urls
        foreach ($migrateRedirects as $migrateRedirect) {

            $shopifyRedirect = $this->getShopifyRedirectObject($migrateRedirect);
            $shopifyRedirect['extra']['page_id'] = $page;
            $coreRedirectController->create($shopifyRedirect);
        }

        if ($migrateRedirects) {
            $schedule = $redirectSetting->schedule;
            $schedule->status = 'paused';
            $schedule->save();
        } else {
            event(new RedirectMigrated($redirectSetting));
            $schedule = $redirectSetting->schedule;
            $schedule->status = 'completed';
            $schedule->save();

//            $this->commonFunctions->startNextRedirectMigration($redirectSetting);
        }
    }

    public function getShopifyRedirectObject($migrateRedirect)
    {
        //Get Redirect Target Url
        $url = $migrateRedirect['path'];
        $targetUrls = [];

        $search = $this->commonFunctions->getRedirectSearchTermFromUrl($url);
        $isAssetUrl = $this->commonFunctions->isAssetUrl($search);

        if ($search && !$isAssetUrl) {
            $with = 'links';
            //Search url from cratejoy id urls.
            if (!$targetUrls && is_numeric($search)) {

                $productId = $search;

                $cjRequest = new CratejoyController($this->project);
                $product = $cjRequest->getProductByID($productId);
                if (!isset($product['errors']) && !is_string($product)) {
                    $searchResults = $this->shopify->getSearchProducts($this->project, $product['slug'], $with);
                    $targetUrls = $searchResults['links'];
                }
                unset($cjRequest, $product);
            }

            //Search url from common urls. 
            if (!$targetUrls) {
                $basicUrl = $this->commonFunctions->searchBasicUrl($url);
                if ($basicUrl) {
                    $targetUrls[] = $basicUrl;
                }
            }

            //Search url from store predefine urls.
            if (!$targetUrls && $this->externalRedirect) {
                $resultUrl = $this->commonFunctions->getRedirectTargetUrl($this->externalRedirect, $search);
                if ($resultUrl) {
                    $targetUrls[] = $resultUrl;
                }
            }

            //Search Collection page url
            if (!$targetUrls && str_contains($url, 'shop') && !str_contains($url, 'product')) {
                $searchResults = $this->shopify->getSearchCollections($this->project, $search, $with);
                $targetUrls = $searchResults['links'];
            }

            //Search page Url
            if (!$targetUrls && !str_contains($url, 'product') && !str_contains($url, 'blog')) {
                $searchResults = $this->shopify->getSearchPages($this->project, $search, $with);
                $targetUrls = $searchResults['links'];
            }

            //Product page url
            if (!$targetUrls) {
                if (!str_contains($url, 'blog')) {
                    $searchResults = $this->shopify->getSearchProducts($this->project, $search, $with);
                    $targetUrls = $searchResults['links'];
                }
            }

            //Blog & Article page url
            if (!$targetUrls) {
                $searchResults = $this->shopify->getSearchBlogs($this->project, $search, $with);
                $targetUrls = $searchResults['links'];

                if (!$targetUrls) {
                    $searchResults = $this->shopify->getSearchArticles($this->project, $search, $with);
                    $targetUrls = $searchResults['links'];
                }
            }
        } else {
            $targetUrls[] = config('app.shopify_root_url');
        }

        if (!$targetUrls) {
            $targetUrls[] = '';
        }

        $shopifyRedirect = [];
        $shopifyRedirect['path'] = $migrateRedirect['path'];
        $shopifyRedirect['target'] = $targetUrls;
        $shopifyRedirect['extra']['type'] = ($isAssetUrl) ? 'asset' : NULL;
        return $shopifyRedirect;
    }
}
