<?php

namespace App\Http\Controllers\Cratejoy;

use App\Http\Controllers\Cratejoy\Products\ProductController;
use App\Http\Controllers\Core\CommonFunctionsController;
use App\Models\Project;
use App\Http\Controllers\Core\ExternalApiRequestController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CratejoyController extends Controller
{
    public $client, $project;

    public function __construct(Project $project)
    {
        $this->project = $project;
        $this->apiRequest = new ExternalApiRequestController($project->platform_details);
        $this->commonFunctions = new CommonFunctionsController;
    }

    //=========================== PRODUCT FUNCTIONS

    public function getProductCount($productType = NULL)
    {
        $params = [];
        $params['limit'] = 1;
        if ($productType) {
            if ($productType == 'subscription') {
                $params['product_type'] = 0;
            }
            if ($productType == 'one_time') {
                $params['product_type'] = 1;
            }
            if ($productType == 'gift') {
                $params['product_type'] = 2;
            }
        }

        $url = 'v1/products/';
        $results = $this->apiRequest->view($url, $params);
        return $results['count'];
    }

    public function getProductByID($productId)
    {
        $url = 'v1/products/' . $productId;
        return $this->apiRequest->view($url);
    }

    public function countTotalProducts($productType = FALSE)
    {
        //product_type = 0 => for subscriber product
        //product_type = 1 => for one-time product
        //product_type = 2 => for gift product

        $url = ($productType) ? 'v1/products/?product_type=' . $productType . '&limit=1' : 'v1/products/?limit=1';
        $products = $this->apiRequest->view($url);
        return $products['count'];
    }

    public function getProducts($options = [])
    {
        $params = [];
        if (isset($options['product_type'])) {
            $params['product_type'] = $options['product_type'];
        }

        if (isset($options['limit'])) {
            $params['limit'] = $options['limit'];
        }

        if (isset($options['page'])) {
            $params['page'] = $options['page'];
        }

        if (isset($options['with'])) {
            $params['with'] = $options['with'];
        }

        $url = 'v1/products/';
        $results = $this->apiRequest->view($url, $params);

        return $results['results'];
    }

    public function generateProductTags($migrateProduct)
    {
        $productTags = [];
        $productTags[] = "migrated";
        $productTags[] = "cj-import";
        $productTags[] = "cj-" . $migrateProduct['id'];

        $tags = $migrateProduct['taxonomy'];
        foreach ($tags as $tag) {
            $productTags[] = $tag['slug'];
        }
        if ($migrateProduct['product_type'] == "subscription") {
            $productTags[] = 'subscription-product';
        }
        if ($migrateProduct['deleted']) {
            $productTags[] = 'deleted-product';
        }
        if ($migrateProduct['ship_option'] == "flat") {
            $productTags[] = 'flat-ship-price-' . ($migrateProduct['flat_ship_price'] / 100);
        }

        return implode(',', $productTags);
    }

    public function generateProductVariants($migrateProduct)
    {
        $productVariants = [];

        $instances = $migrateProduct['instances'];
        foreach ($instances as $instance) {

            $url = $instance['url'] . 'inventory';
            $inventory = $this->apiRequest->view($url);
            $instance = $inventory['instance'];
            $variantOptions = $inventory['instance']['variants'];

            //generate product variant
            $productVariant = [];
            foreach ($variantOptions as $key => $variantOption) {
                $newKey = $key + 1;
                $productVariant['option' . $newKey] = trim($variantOption['variant_value']['value']);
            }
            $productVariant['sku'] = $instance['sku'];
            $productVariant['grams'] = $instance['ship_weight'];
            $productVariant['inventory_quantity'] = $inventory['quantity_on_hand'];
            if ($inventory['track_inventory']) {
                $productVariant['inventory_management'] = 'shopify';
            }
            $productVariant['inventory_policy'] = ($inventory['out_of_stock_purchases']) ? 'continue' : 'deny';
            $productVariant['fulfillment_service'] = 'manual';
            $productVariant['price'] = ($instance['price']) / 100;
            $productVariant['compare_at_price'] = '';
            $productVariant['requires_shipping'] = ($migrateProduct['ship_option'] == "pricedin" || $migrateProduct['product_type'] == "gift_card") ? 'false' : 'true';
            $productVariant['taxable'] = 'false';
            $productVariant['barcode'] = '';
            $productVariants[] = $productVariant;
        }

        return $productVariants;
    }

    public function generateSubscribeProductVariants($migrateProduct, $migrateInvalidProduct)
    {
        //check in cratejoy with perfect sku match with variant name
        $productVariants = [];

        //get variants
        $migrateVariants = $migrateProduct['variants'];
        $variants = $this->getCJVariants($migrateVariants, $migrateInvalidProduct);

        //subscription variant price
        $subscriptions = $migrateProduct['gift_terms'];
        $subscribePrices = $migrateProduct['giftinstances'][0]['term_prices'];

        $variantPrice = $this->getSubscribeProductPrice($subscriptions, $subscribePrices);

        //subscriptions variants
        $instances = $migrateProduct['instances'];
        foreach ($instances as $key => $instance) {
            if (isset($variants[$key])) {
                $url = $instance['url'];
                $instance = $this->apiRequest->view($url);

                //generate product variant
                $productVariant = [];

                if ($migrateVariants) {

                    foreach ($variants[$key] as $variantKey => $value) {
                        $newKey = $variantKey + 1;
                        $productVariant['option' . $newKey] = trim($value);
                    }
                }

                $productVariant['sku'] = $instance['sku'];
                $productVariant['inventory_policy'] = 'continue';
                $productVariant['fulfillment_service'] = 'manual';
                $productVariant['price'] = $variantPrice / 100;
                $productVariant['requires_shipping'] = ($migrateProduct['ship_option'] == "pricedin") ? 'false' : 'true';
                $productVariant['taxable'] = 'false';
                $productVariant['barcode'] = '';
                $productVariants[] = $productVariant;
            }
        }

        return $productVariants;
    }

    public function generateSubscribeTermsVariants($migrateProduct, $migrateInvalidProduct)
    {
        //check in cratejoy with perfect sku match with variant name
        $productVariants = [];
        $instance = $migrateProduct['instances'][0];
        $url = $instance['url'];
        $instance = $this->apiRequest->view($url);
        $variantSku = $instance['sku'];
        $billing = $migrateProduct['billing'];
        $shipFrequency = ($billing['rebill_months']) ?: $billing['rebill_weeks'];

        //get variants
        $variants = $migrateProduct['subscription_types'][0]['terms'];
        foreach ($variants as $variant) {
            $productVariant = [];
            $productVariant['option1'] = $variant['name'];
            $productVariant['sku'] = $variantSku;
            $productVariant['inventory_policy'] = 'continue';
            $productVariant['fulfillment_service'] = 'manual';
            $productVariant['price'] = $this->getVarinatPrice($variant, $migrateProduct['base_term_prices']);
            $productVariant['requires_shipping'] = ($migrateProduct['ship_option'] == "pricedin") ? 'false' : 'true';
            $productVariant['taxable'] = 'false';
            $productVariant['barcode'] = '';
            $productVariant['metafields'] = $this->getVariantMetafields($variant, $shipFrequency);
            $productVariants[] = $productVariant;
        }
        return $productVariants;
    }

    public function getVarinatPrice($variant, $baseTermPrices)
    {
        $termIds = array_column($baseTermPrices, 'term_id');
        $key = array_search($variant['id'], $termIds);
        if (is_numeric($key)) {
            return $baseTermPrices[$key]['price'] / 100;
        } else {
            \Log::info(['price_not_found_of_variant' => $variant]);
        }
    }

    public function getVariantMetafields($variant, $shipFrequency)
    {
        $metaFields = [];
        $metaField = [];
        $metaField['key'] = 'charge_internal_frequency';
        $metaField['value'] = $variant['num_cycles'] * $shipFrequency;
        $metaField['value_type'] = 'string';
        $metaField['namespace'] = 'subscription';
        $metaFields[] = $metaField;

        $metaField = [];
        $metaField['key'] = 'description';
        $metaField['value'] = $variant['description'];
        $metaField['value_type'] = 'string';
        $metaField['namespace'] = 'subscription';
        $metaFields[] = $metaField;

        $metaField = [];
        $metaField['key'] = 'is_hidden';
        $metaField['value'] = ($variant['enabled']) ? 1 : 0;
        $metaField['value_type'] = 'integer';
        $metaField['namespace'] = 'subscription';
        $metaFields[] = $metaField;
        return $metaFields;
    }

    public function getSubscribeProductPrice($subscriptions, $subscribePrices)
    {
        $terms = array_column($subscribePrices, 'term_id');

        foreach ($subscriptions as $subscription) {
            if ($subscription['enabled']) {
                $variantKey = array_search($subscription['id'], $terms);
                if (is_numeric($variantKey)) {
                    $variantPrice = $subscribePrices[$variantKey]['price'];
                }
                break;
            }
        }

        return isset($variantPrice) ? $variantPrice : 0;
    }

    public function getCJVariants($migrateVariants, $migrateInvalidProduct)
    {
//        foreach ($migrateVariants as $key => $variant) {
//            $chunkArray = [];
//            foreach ($variant['values'] as $value) {
//                $chunkArray[] = $value['value'];
//            }
//
//            if ($key == 0) {
//                $collection = collect($chunkArray);
//            } else {
//                $collection = $collection->crossJoin($chunkArray);
//                $collection = collect($collection);                
//            }
//        }
//        
//        dd($collection);

        foreach ($migrateVariants as $key => $variant) {
            $variants = [];
            foreach ($variant['values'] as $valueKey => $value) {
                $multiply = $valueKey + 1;
                if ($key == 0) {
                    $variants[][] = $value['value'];
                } else {
                    foreach ($tempVariants as $tempKey => $tempVariant) {
                        $tempVariant[] = $value['value'];
                        $pos = $tempKey * $multiply + $valueKey;
                        array_splice($variants, $pos, 0, [$tempVariant]);
                    }
                }
            }

            $tempVariants = $variants;
        }

        if ($migrateInvalidProduct && isset($variants) && count($migrateVariants) > 3) {
            $updatedVariants = [];
            foreach ($variants as $variant) {
                $updatedVariants[] = array_slice($variant, 0, 3);
            }
            $collection = collect($updatedVariants);
            $variants = $collection->uniqueStrict();
        }
        return isset($variants) ? $variants : [];
    }

    public function generateProductOptions($variants)
    {
        $productOptions = [];
        foreach ($variants as $variant) {

            //generate product option
            $productOption = [];
            $productOption['name'] = $variant['name'];
            $productOptions[] = $productOption;
        }

        return $productOptions;
    }

    public function generateSubscribeProductOptions()
    {
        $productOptions = [];
        $productOption = [];
        $productOption['name'] = 'Term';
        $productOptions[] = $productOption;
        return $productOptions;
    }

    public function generateProductImages($migrateProduct)
    {
        $productImages = [];
        $images = $migrateProduct['images'];

        foreach ($images as $image) {
            $url = 'https:' . $image['url'];

            //generate product image obejct
            $url = $this->commonFunctions->generateProductImageSrc($url, $this->project->id);
            if ($url) {
                $productImage = [];
                $productImage['src'] = $url;
                $productImage['alt'] = $migrateProduct['name'];
                $productImage['position'] = $image['weight'];
                $productImages[] = $productImage;
            }
        }

        return $productImages;
    }

    //=========================== CUSTOMER FUNCTIONS    
    public function getCustomers($options = [])
    {
        $params = [];
        if (isset($options['limit'])) {
            $params['limit'] = $options['limit'];
        }

        if (isset($options['page'])) {
            $params['page'] = $options['page'];
        }

        if (isset($options['with'])) {
            $params['with'] = $options['with'];
        }

        $url = 'v1/customers/';
        $results = $this->apiRequest->view($url, $params);

        return $results['results'];
    }

    public function getCustomerCount()
    {
        $params = [];
        $params['limit'] = 1;

        $url = 'v1/customers/';
        $results = $this->apiRequest->view($url, $params);
        return $results['count'];
    }

    public function getCustomerByID($customerId)
    {
        $with = 'subscriptions';
        $params = [];
        $params['with'] = $with;

        $url = 'v1/customers/' . $customerId;
        return $this->apiRequest->view($url, $params);
    }

    //=========================== ORDER FUNCTIONS
    public function getOrders($options = [])
    {
        $params = [];
        if (isset($options['limit'])) {
            $params['limit'] = $options['limit'];
        }

        if (isset($options['page'])) {
            $params['page'] = $options['page'];
        }

        if (isset($options['with'])) {
            $params['with'] = $options['with'];
        }

        $url = 'v1/orders/';
        $results = $this->apiRequest->view($url, $params);

        return (isset($results['results']) ? $results['results'] : $results);
    }

    public function getOrderByID($orderId)
    {
        $url = 'v1/orders/' . $orderId;
        return $this->apiRequest->view($url);
    }

    public function getOrderCount()
    {
        $params = [];
        $params['limit'] = 1;

        $url = 'v1/orders/';
        $results = $this->apiRequest->view($url, $params);
        return $results['count'];
    }
}
