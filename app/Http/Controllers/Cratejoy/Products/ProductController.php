<?php

namespace App\Http\Controllers\Cratejoy\Products;

use File;
use App\Helpers\Helper;
use App\Events\ProductMigrated;
use App\Models\Schedule;
use App\Http\Controllers\Shopify\Core\ProductController as CoreProductController;
use App\Http\Controllers\Cratejoy\CratejoyController;
use App\Models\ProjectProductSetting as ProductSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public $cjRequest, $productSetting;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function index()
    {
        
    }

    public function create(ProductSetting $productSetting)
    {
        $schedule = $productSetting->schedule;
        $schedule->status = 'inprogress';
        $schedule->save();

        $this->productSetting = $productSetting;
        $lastProduct = $productSetting->products()->latest()->first();

        $project = $productSetting->project;
        $this->cjRequest = new CratejoyController($project);

        $coreProductController = new CoreProductController($productSetting);

        $options = [];
        $productType = $productSetting->mapping['product_type'];
        if ($productType != 'all') {
            if ($productType == 'subscription') {
                $options['product_type'] = 0;
            }
            if ($productType == 'one_time') {
                $options['product_type'] = 1;
            }
            if ($productType == 'gift') {
                $options['product_type'] = 2;
            }
        }

        $pageId = ($lastProduct) ? $lastProduct->extra['page_id'] + 1 : 1;
        $options['page'] = $pageId;
        $options['limit'] = 10;
        $migrateProducts = $this->cjRequest->getProducts($options);

        foreach ($migrateProducts as $migrateProduct) {
            $shopifyProduct = [];
            $shopifyProduct = $this->getShopifyProductObject($migrateProduct);

            $shopifyProduct['extra'] = [
                'product_id' => $migrateProduct['id'],
                'page_id' => $pageId
            ];
            $shopifyProduct['export'] = [
                'product_id' => $migrateProduct['id']
            ];
            $coreProductController->create($shopifyProduct, $migrateProduct);
        }

        if ($migrateProducts) {
            $schedule = $productSetting->schedule;
            $schedule->status = 'paused';
            $schedule->save();
        } else {
            event(new ProductMigrated($productSetting));
            $schedule = $productSetting->schedule;
            $schedule->status = 'completed';
            $schedule->save();
        }
    }

    public function getShopifyProductObject($migrateProduct)
    {
        //shopify product object
        $migrateInvalidProduct = $this->productSetting->mapping['migrate_invalid_product'];
        $migrateInvalidProduct = ($migrateInvalidProduct == 'true') ? TRUE : FALSE;

        $desc = $this->productSetting->mapping['desc'];
        $desc = str_replace('{{ product.', '$migrateProduct[\'', $desc);
        $desc = str_replace(' }}', '\']', $desc);
        $desc = str_replace('{{product.', '$migrateProduct[\'', $desc);
        $desc = str_replace('}}', '\']', $desc);

        $shopifyDescription = eval($desc);

        $shopifyDescription = ($shopifyDescription) ? $shopifyDescription : '';

        $shopifyProduct = [];
        if ($this->productSetting->mapping['migrate_with_current_handle']) {
            $shopifyProduct['handle'] = $migrateProduct['slug'];
        }
        $shopifyProduct['title'] = $migrateProduct['name'];
        $shopifyProduct['body_html'] = $shopifyDescription;
        $shopifyProduct['vendor'] = '';
        $shopifyProduct['product_type'] = ($migrateProduct['deleted']) ? 'deleted' : '';
        $shopifyProduct['tags'] = $this->cjRequest->generateProductTags($migrateProduct);
        $shopifyProduct['published'] = ($migrateProduct['deleted']) ? 'false' : $migrateProduct['visible'];
        if ($migrateProduct['product_type'] == "subscription") {
            $shopifyProduct['options'] = $this->cjRequest->generateSubscribeProductOptions();
        } else {
            $shopifyProduct['options'] = $this->cjRequest->generateProductOptions($migrateProduct['variants']);
        }
        if (count($shopifyProduct['options']) > 3) {
            if ($migrateInvalidProduct) {
                $shopifyProduct['options'] = array_slice($shopifyProduct['options'], 0, 3);
            }
        }

        if ($migrateProduct['product_type'] == "subscription") {
//            $shopifyProduct['variants'] = $this->cjRequest->generateSubscribeProductVariants($migrateProduct, $migrateInvalidProduct);
            $shopifyProduct['variants'] = $this->cjRequest->generateSubscribeTermsVariants($migrateProduct, $migrateInvalidProduct);
        } else {
            $shopifyProduct['variants'] = $this->cjRequest->generateProductVariants($migrateProduct);
        }
        $shopifyProduct['images'] = $this->cjRequest->generateProductImages($migrateProduct);

        //There is No SEO description.
        $shopifyProduct['metafields_global_title_tag'] = $migrateProduct['name'];
        $shopifyProduct['metafields_global_description_tag'] = strip_tags($migrateProduct['description']);

        return $shopifyProduct;
    }
}
