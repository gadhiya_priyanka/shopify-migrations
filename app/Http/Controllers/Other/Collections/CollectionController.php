<?php

namespace App\Http\Controllers\Other\Collections;

use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Helpers\Helper;
use App\Http\Controllers\Core\ExcelController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CollectionController extends Controller
{

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create()
    {
        dd("Stop again");
        $taskName = 'task-17939';
        $fileName = 'SiteCategories751.csv';
        $filePath = resource_path('assets/projects/natura-brasil/' . $taskName . '/' . $fileName);
        $migrateCollections = ExcelController::view($filePath);

        //api data
        $data = []; // api of natuara brasil
        $data['api_key'] = 'dc1909608162a17372fcf1c409fc50ab';
        $data['api_password'] = '43fcb2cf9309865e5523e4ba1b96c71b';
        $data['api_url'] = 'https://natura-br.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);

        foreach ($migrateCollections as $migrateCollection) {
            $shopifyCollection = $this->getShopifyCollectionObject($migrateCollection);
            $params = [
                'smart_collection' => $shopifyCollection
            ];
            $url = '/admin/smart_collections.json';
            $collection = $shopifyRequest->create($url, $params);
            if (!isset($collection['smart_collection'])) {
                \Log::info(["collection " . $migrateCollection['category_name'] => $collection]);
            }
        }
        dd("Stop");
    }

    public function getShopifyCollectionObject($migrateCollection)
    {
        $rules = [];
        $rule = [];
        $rule['column'] = 'tag';
        $rule['relation'] = 'equals';
        $rule['condition'] = Helper::handleizedString($migrateCollection['category_name']);
        $rules[] = $rule;

        $shopifyCollection = [];
        $shopifyCollection['title'] = $migrateCollection['category_name'];
        $shopifyCollection['rules'] = $rules;
        $shopifyCollection['published'] = true;
        return $shopifyCollection;
    }
}
