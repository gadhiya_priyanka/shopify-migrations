<?php

namespace App\Http\Controllers\Other;

use App\Models\CustomGuzzleHttp;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OtherController extends Controller
{
    public $db, $project;

    public function __construct(Project $project)
    {
        $this->project = $project;
        $this->db = CustomGuzzleHttp::connectExternalDatabase($project->platform_details);
    }

    public function getOrders($options = [])
    {
        $search = (isset($options['search'])) ? $options['search'] : [];
        $query = $this->db->table('order_sales_list')->where($search);
        if (isset($options['from'])) {
            $query = $query->offset($options['from']);
        }
        if (isset($options['size'])) {
            $query = $query->limit($options['size']);
        }

        return $query->orderBy('sales_order', 'asc')->get()->toArray();
    }

    public function getItems($migrateOrder)
    {
        return $this->db->table('orders')->where(['sales_order' => $migrateOrder->sales_order])->get()->toArray();
    }

    public function getLineItems($products)
    {
        $lineItems = [];

        foreach ($products as $product) {
            if ($product->quantity != 0) {
                $variantId = NULL;
                $variant = $this->db->table('shopify_product')->where(['name' => $product->item])->first();
                if ($variant) {
                    $variantId = $variant->variant_id;
                }

                if ($variantId) {
                    $lineItem = [];
                    $lineItem['variant_id'] = $variantId;
                    $lineItem['quantity'] = abs($product->quantity);
                    $lineItems[] = $lineItem;
                } else {

                    $lineItem = [];
                    $lineItem['title'] = $product->item;
                    $lineItem['price'] = abs($product->item_rate);
                    $lineItem['quantity'] = abs($product->quantity);
                    $lineItems[] = $lineItem;
                }
            }
        }

        return $lineItems;
    }

    public function getLocationId($item)
    {
        return $this->db->table('locations')->where(['name' => $item->location])->first();
    }
}
