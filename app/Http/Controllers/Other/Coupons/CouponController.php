<?php

namespace App\Http\Controllers\Other\Coupons;

use App\Http\Controllers\Core\ExternalApiRequestController;
use App\Helpers\Helper;
use App\Http\Controllers\Core\ExcelController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CouponController extends Controller
{

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create()
    {
        dd("Stop again");
        $taskName = 'task-17938';
        $fileName = 'Promotions324.csv';
        $filePath = resource_path('assets/projects/natura-brasil/' . $taskName . '/' . $fileName);
        $migrateCoupons = ExcelController::view($filePath);
        dd($migrateCoupons);
        //api data
        $data = []; // api of natuara brasil
        $data['api_key'] = 'dc1909608162a17372fcf1c409fc50ab';
        $data['api_password'] = '43fcb2cf9309865e5523e4ba1b96c71b';
        $data['api_url'] = 'https://natura-br.myshopify.com';
        $shopifyRequest = new ExternalApiRequestController($data);

        foreach ($migrateCoupons as $migrateCoupon) {
            $shopifyPriceRule = $this->getShopifyPriceRuleObject($migrateCoupon);



            $shopifyCoupon = $this->getShopifyCouponObject($migrateCoupon);
            $params = [
                'smart_coupon' => $shopifyCoupon
            ];
            $url = '/admin/smart_coupons.json';
            $coupon = $shopifyRequest->create($url, $params);
            if (!isset($coupon['smart_coupon'])) {
                \Log::info(["coupon " . $migrateCoupon['category_name'] => $coupon]);
            }
        }
        dd("Stop");
    }

    public function getShopifyPriceRuleObject($migrateCoupon)
    {
        $targeType = ($migrateCoupon['discount_rate'] == "-100.00%") ? 'shipping_line' : 'line_item';
        $valueType = (str_contains($migrateCoupon['discount_rate'], '%')) ? 'percentage' : 'fixed_amount';
        $oncePerCustomer = ($migrateCoupon['apply_customer_criteria'] == '1') ? 'true' : 'false';
        
        $shopifyPriceRule = [];
        $shopifyPriceRule['title'] = $migrateCoupon['coupon_code'];
        $shopifyPriceRule['target_type'] = $targeType;
        $shopifyPriceRule['target_selection'] = 'all';
        $shopifyPriceRule['allocation_method'] = ($targeType == "shipping_line") ? 'each' : 'across';
        $shopifyPriceRule['value_type'] = $valueType;
        $shopifyPriceRule['value'] = (int) $migrateCoupon['discount_rate'];
        $shopifyPriceRule['once_per_customer'] = $oncePerCustomer;
//        $shopifyPriceRule['usage_limit'] = $metaData->usage_limit_per_user;
        $shopifyPriceRule['customer_selection'] = 'all';
//        $shopifyPriceRule['prerequisite_saved_search_ids'] = '';
//        $shopifyPriceRule['prerequisite_customer_ids'] = '';
        if ($minimumAmount) {
            $shopifyPriceRule['prerequisite_subtotal_range'] = [
                'greater_than_or_equal_to' => $minimumAmount
            ];
        }
        if ($maximumAmount) {
            $shopifyPriceRule['prerequisite_shipping_price_range'] = [
                'less_than_or_equal_to' => $maximumAmount
            ];
        }
        $shopifyPriceRule['entitled_product_ids'] = '';
        $shopifyPriceRule['entitled_variant_ids'] = '';
        $shopifyPriceRule['entitled_collection_ids'] = '';
        $shopifyPriceRule['entitled_country_ids'] = '';
        $shopifyPriceRule['starts_at'] = Helper::shopifyDateFormate(date('Y-m-d'));
        if ($expiryDate) {

            if (date("Y-m-d", strtotime($expiryDate)) < date('Y-m-d')) {
                $shopifyPriceRule['ends_at'] = Helper::shopifyDateFormate(date('Y-m-d'));
            } else {
                $shopifyPriceRule['ends_at'] = Helper::shopifyDateFormate($expiryDate);
            }
        }
        return $shopifyPriceRule;
    }

    public function getShopifyCouponObject($migrateCoupon)
    {
        $rules = [];
        $rule = [];
        $rule['column'] = 'tag';
        $rule['relation'] = 'equals';
        $rule['condition'] = Helper::handleizedString($migrateCoupon['category_name']);
        $rules[] = $rule;

        $shopifyCoupon = [];
        $shopifyCoupon['title'] = $migrateCoupon['category_name'];
        $shopifyCoupon['rules'] = $rules;
        $shopifyCoupon['published'] = true;
        return $shopifyCoupon;
    }
}
