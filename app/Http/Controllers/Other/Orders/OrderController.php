<?php

namespace App\Http\Controllers\Other\Orders;

use App\Helpers\Helper;
use App\Events\OrderMigrated;
use App\Http\Controllers\Shopify\Core\OrderController as CoreOrderController;
use App\Models\ProjectOrderSetting as OrderSetting;
use App\Models\CustomGuzzleHttp;
use App\Http\Controllers\Core\ExcelController;
use App\Http\Controllers\Other\OtherController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public $orderSetting, $otherRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function dbEntry()
    {
        dd("it is dbEntry stop");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'natura_brasil';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);
        $pageId = 1;

        \Log::info("script start $pageId");
        $orders = $db->table('orders')
                ->select('internal_id', $db->raw('count(internal_id) as count'))
                ->whereNull('sales_order')
                ->groupBy('internal_id')
                ->get()->toArray();        

        foreach ($orders as $order) {
            $dbOrder = [];
            $dbOrder['sales_order'] = $order->internal_id;
            $dbOrder['count'] = (int) $order->count;
            $dbOrder['page_id'] = $pageId;
            $db->table('order_without_sales_list')->insert($dbOrder);
        }


        \Log::info("script end $pageId");

        dd("done");
    }

    public function dbEntryMain()
    {
        dd("it dbEntryMain is stop");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'natura_brasil';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        $lastOrder = $db->table('orders')->orderBy('page_id', 'desc')->first();
        $pageId = ($lastOrder) ? $lastOrder->page_id + 1 : 1;

//        $filePath = resource_path('assets/projects/natura-brasil/task-17936/WPCustomExportResults405.csv');
        $filePath = resource_path('assets/projects/natura-brasil/task-17936/WPCustomExportResults268_NEW_8.6.18 -original.csv');
//        $pageId = 12;
        $limit = 500;
//        $limit = 1000;
        $skip = ($pageId - 1) * $limit;
        \Log::info("start $pageId $limit");
        $orders = ExcelController::view($filePath, $limit, $skip);
//        dd($orders);
        foreach ($orders as $order) {
//            $pageId = 6;
            $salesOrder = NULL;
            if (strpos($order['created_from'], "#")) {
                $salesOrder = substr($order['created_from'], strpos($order['created_from'], "#") + 1);
            }

            $dbOrder = [];
            $dbOrder['internal_id'] = $order['internal_id'];
            $dbOrder['created_from'] = $order['created_from'];
            $dbOrder['sales_order'] = $salesOrder;
            $dbOrder['email'] = $order['email'];
            $dbOrder['shipping_addressee'] = $order['shipping_addressee'];
            $dbOrder['shipping_address_1'] = $order['shipping_address_1'];
            $dbOrder['shipping_address_2'] = $order['shipping_address_2'];
            $dbOrder['shipping_address_3'] = $order['shipping_address_3'];
            $dbOrder['shipping_city'] = $order['shipping_city'];
            $dbOrder['shipping_country'] = $order['shipping_country'];
            $dbOrder['shipping_country_code'] = $order['shipping_country_code'];
            $dbOrder['shipping_state_province'] = $order['shipping_stateprovince'];
            $dbOrder['shipping_zip'] = $order['shipping_zip'];
            $dbOrder['shipping_phone'] = $order['shipping_phone'];
            $dbOrder['shipping_attention'] = $order['shipping_attention'];
            $dbOrder['billing_addressee'] = $order['billing_addressee'];
            $dbOrder['billing_address_1'] = $order['billing_address_1'];
            $dbOrder['billing_address_2'] = $order['billing_address_2'];
            $dbOrder['billing_address_3'] = $order['billing_address_3'];
            $dbOrder['billing_city'] = $order['billing_city'];
            $dbOrder['billing_country'] = $order['billing_country'];
            $dbOrder['billing_country_code'] = $order['billing_country_code'];
            $dbOrder['billing_state_province'] = $order['billing_stateprovince'];
            $dbOrder['billing_zip'] = $order['billing_zip'];
            $dbOrder['billing_phone'] = $order['billing_phone'];
            $dbOrder['billing_attention'] = $order['billing_attention'];
            $dbOrder['date'] = $order['date'];
            $dbOrder['date_created'] = $order['date_created'];
            $dbOrder['shipping_cost'] = $order['shipping_cost'];
            $dbOrder['amount_transaction_tax_total'] = $order['amount_transaction_tax_total'];
            $dbOrder['amount_discount'] = $order['amount_discount'];
            $dbOrder['coupon_code'] = $order['coupon_code'];
            $dbOrder['amount_transaction_total'] = $order['amount_transaction_total'];
            $dbOrder['item'] = $order['item'];
            $dbOrder['item_rate'] = $order['item_rate'];
            $dbOrder['item_note'] = $order['item_note'];
            $dbOrder['page_id'] = $pageId;
            $dbOrder['document_number'] = $order['document_number'];
            $dbOrder['status'] = $order['status'];
            $dbOrder['name'] = $order['name'];
            $dbOrder['location'] = $order['location'];
            $dbOrder['quantity'] = (int) $order['quantity'];
            $dbOrder['created_by'] = $order['created_by'];
            $dbOrder['sales_channel'] = $order['sales_channel'];
            $db->table('orders')->insert($dbOrder);
        }

        \Log::info("end $pageId");

        dd("done");
    }

    public function create(OrderSetting $orderSetting)
    {
        dd("Natura Brasil Orders");
        $schedule = $orderSetting->schedule;
        $schedule->status = 'inprogress';
        $schedule->save();

        $this->orderSetting = $orderSetting;
        $lastOrder = $orderSetting->orders()->latest()->first();

        $project = $orderSetting->project;
        $this->otherRequest = new OtherController($project);

        $coreOrderController = new CoreOrderController($orderSetting);

        $options = [];
        $options['from'] = 0;
        $options['size'] = 50;
        if ($lastOrder) {
            $options['search'] = [['sales_order', '>', $lastOrder->extra['sales_order_id']]];
        }
        $migrateOrders = $this->otherRequest->getOrders($options);

        foreach ($migrateOrders as $migrateOrder) {
            $extra = [];
            $extra['sales_order_id'] = $migrateOrder->sales_order;

            $export = [];
            $export['sales_order_id'] = $migrateOrder->sales_order;


            $shopifyOrder = $this->getShopifyOrderObject($migrateOrder);
            $extra['order_id'] = $export['order_id'] = $shopifyOrder['internal_id'];

            unset($shopifyOrder['internal_id']);
            $shopifyOrder['extra'] = $extra;
            $shopifyOrder['export'] = $export;

            $coreOrderController->create($shopifyOrder, $migrateOrder);
        }

        if ($migrateOrders) {
            $schedule = $orderSetting->schedule;
            $schedule->status = 'paused';
            $schedule->save();
        } else {
            event(new OrderMigrated($orderSetting));
            $schedule = $orderSetting->schedule;
            $schedule->status = 'completed';
            $schedule->save();
        }
    }

    public function getShopifyOrderObject($migrateOrder)
    {
        $items = $this->otherRequest->getItems($migrateOrder);
        $lineItems = $this->otherRequest->getLineItems($items);
        $location = $this->otherRequest->getLocationId($items[0]);
        $shopifyOrder = [];
//        $shopifyOrder['email'] = $migrateOrder['email'];
        $shopifyOrder['email'] = ($items[0]->email) ?: 'anonymous@natura.net';
        $shopifyOrder['line_items'] = $lineItems;
        if ($location && $location->location_id) {
            $shopifyOrder['location_id'] = $location->location_id;
        }
        $shopifyOrder['tags'] = $this->getTags($items[0]);

        $shopifyOrder['financial_status'] = 'paid';
        $shopifyOrder['fulfillment_status'] = 'fulfilled';

        $shopifyOrder['created_at'] = Helper::shopifyDateFormate($items[0]->date_created);

        $shopifyOrder['shipping_address'] = $this->getAddress($items[0], 'shipping');
        $shopifyOrder['billing_address'] = $this->getAddress($items[0], 'billing');

        $shopifyOrder['shipping_lines'] = $this->getShippings($items[0]->shipping_cost);
        $shopifyOrder['tax_lines'] = $this->getTaxLines($items[0]->amount_transaction_tax_total);

        $shopifyOrder['discount_codes'] = $this->getDiscountCodes($items);
        $shopifyOrder['transactions'] = $this->getTransactions($items[0]->amount_transaction_total);

//        $shopifyOrder['invalid_quantity'] = (count($items) > 0 && count($lineItems) > 0) ? FALSE : TRUE;
        $shopifyOrder['internal_id'] = $items[0]->internal_id;
        return $shopifyOrder;
    }

    public function getTransactions($totalAmount)
    {
        $transactions = [];

        if ($totalAmount > 0) {
            $transaction = [];
            $transaction['status'] = 'success';
            $transaction['amount'] = (float) $totalAmount;
            $transactions[] = $transaction;
        }

        return $transactions;
    }

    public function getTaxLines($totalTax)
    {
        $taxLines = [];

        if ($totalTax) {
            $taxLine = [];
            $taxLine['title'] = 'Tax';
            $taxLine['price'] = (float) $totalTax;
//        $taxLine['rate'] = $cartTaxRate['rate'];
            $taxLines[] = $taxLine;
        }
        return $taxLines;
    }

    public function getShippings($totalShipping)
    {
        $shippingLines = [];
        if ($totalShipping > 0) {
            $shippingLine = [];
            $shippingLine['code'] = 'Shipping Charge';
            $shippingLine['title'] = 'Shipping';
            $shippingLine['source'] = 'shopify';
            $shippingLine['price'] = (float) $totalShipping;
            $shippingLines[] = $shippingLine;
        }

        return $shippingLines;
    }

    public function getAddress($address, $type)
    {
        $orderAddress = [];

        $addressee = $type . '_addressee';
        $address1 = $type . '_address_1';
        $address2 = $type . '_address_2';
        $address3 = $type . '_address_3';
        $city = $type . '_city';
        $stateProvince = $type . '_state_province';
        $countryCode = $type . '_country_code';
        $zip = $type . '_zip';
        $phone = $type . '_phone';

        if ($address->$addressee || $address->$address1) {
            $orderAddress['address1'] = ($address->$addressee && $address->$addressee != $address->name) ? $address->$addressee . ' ' : '';
            $orderAddress['address1'] = $orderAddress['address1'] . $address->$address1;
        }

        if ($address->$address2 || $address->$address3) {
            $orderAddress['address2'] = ($address->$address2) ? $address->$address2 . ' ' : '';
            $orderAddress['address2'] = $orderAddress['address2'] . $address->$address3;
        }

        if ($address->$city) {
            $orderAddress['city'] = $address->$city;
        }

        if ($address->$stateProvince) {
            $orderAddress['province_code'] = $address->$stateProvince;
        }

        if ($address->$countryCode) {
            $orderAddress['country_code'] = $address->$countryCode;
        }

        if ($address->$zip) {
            $orderAddress['zip'] = $address->$zip;
        }

        if ($address->$phone) {
            $orderAddress['phone'] = $address->$phone;
        }

        if ($address->name) {
            $orderAddress['name'] = $address->name;
        }

        return $orderAddress;
    }

    public function getTags($item)
    {
        $orderTags = [];
        $orderTags[] = 'migrated';
        $orderTags[] = 'netsuite-' . $item->internal_id;
        if ($item->location) {
            $orderTags[] = 'location-' . $item->location;
        }
        if (!$item->email) {
            $orderTags[] = 'anonymous-order';
        }
        if ($item->status) {
            $orderTags[] = 'status-' . $item->status;
        }
        if ($item->sales_channel) {
            $orderTags[] = 'sales-channel-' . $item->sales_channel;
        }
        if ($item->sales_order) {
            $orderTags[] = 'sales-order-id-' . $item->sales_order;
        }

        return implode(',', $orderTags);
    }

    public function getDiscountCodes($discounts)
    {
        $discountCodes = [];
        $totalDiscounts = 0;
        foreach ($discounts as $discount) {
            $totalDiscounts = $totalDiscounts + $discount->amount_discount;
        }

        if ($totalDiscounts > 0) {
            $discountCode = [];
            $discountCode['amount'] = (float) $totalDiscounts;
            $discountCode['code'] = $discounts[0]->coupon_code;
            $discountCode['type'] = 'fixed_amount';
            $discountCodes[] = $discountCode;
        }
        return $discountCodes;
    }
}
