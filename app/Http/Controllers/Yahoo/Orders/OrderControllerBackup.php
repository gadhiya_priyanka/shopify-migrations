<?php

namespace App\Http\Controllers\Yahoo\Orders;

use Config;
use Exception;
use App\Helpers\Helper;
use App\Models\ProjectOrderSetting as OrderSetting;
use App\Models\ProjectOrder;
use App\Http\Controllers\Shopify\Api\ProductController as ShopifyApiProductController;
use App\Http\Controllers\Shopify\Api\OrderController as ShopifyApiOrderController;
use App\Models\CustomGuzzleHttp;
use App\Http\Controllers\Core\ExcelController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public $db, $sheetId, $shopifyProductRequest;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create(OrderSetting $orderSetting)
    {
        //1 & 57

        $filePath = resource_path('assets/repair-universe/project_orders.csv');
        $successOrders = ExcelController::view($filePath);

        $newOrders = [];
        foreach ($successOrders as $successOrder) {

            $successOrder = json_decode($successOrder['success'], TRUE);
//            dd($successOrder);
            $newOrder = [];
            $newOrder['yahoo_order_id'] = $successOrder['order_id'];
            $newOrder['yahoo_order_number'] = $successOrder['order_number'];
            $newOrder['shopify_order_id'] = $successOrder['success'];
            $newOrder['shopify_order_number'] = $successOrder['shopify_order_number'];
            $newOrder['email'] = $successOrder['email'];
            $newOrders[] = $newOrder;
            unset($successOrder, $filePath);
        }

        $fileName = 'migration-success-order';
        $storePath = 'repair-universe/orders';
        ExcelController::create($fileName, $newOrders, $storePath, 'xlsx')->download('xlsx');
        dd("done");
        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'repairs_universe';
        $data['db_username'] = 'root';
        $data['db_password'] = 'localhost';
        $db = CustomGuzzleHttp::connectExternalDatabase($data);
        $orders = $db->table('project_orders')->get()->toArray();
        foreach ($orders as $order) {
            $extra = json_decode($order->extra, TRUE);
            $errorReport = [];
            $errorReport['yahoo_order_id'] = $extra['order_id'];
            $errorReport['yahoo_order_number'] = $extra['order_number'];
            $errorReport['email'] = $extra['email'];
//            $errorReport['error'] = $error['extra']['error'];
            $errorReport['error'] = "{\"errors\":{\"customer\":[\"Email is invalid\"]}}\n";
            $errorReports[] = $errorReport;
        }

        $fileName = 'migration-failed-order';
        $data = $errorReports;
        $storePath = 'repair-universe/orders';

        ExcelController::create($fileName, $data, $storePath, 'xlsx')->download('xlsx');
        dd($orders);


        $errors = $orderSetting->orders()->where([['extra', 'LIKE', '%"type": "error",%']])->get()->toArray();
        dd($errors);
        $errorReports = [];
        foreach ($errors as $error) {
            $errorReport = [];
            $errorReport['yahoo_order_id'] = $error['extra']['order_id'];
            $errorReport['yahoo_order_number'] = $error['extra']['order_number'];
            $errorReport['email'] = $error['extra']['email'];
//            $errorReport['error'] = $error['extra']['error'];
            $errorReport['error'] = "{\"errors\":{\"customer\":[\"Email is invalid\"]}}\n";
            $errorReports[] = $errorReport;
        }
        $fileName = 'failed-order';
        $data = $errorReports;
        $storePath = 'repair-universe/orders';

        ExcelController::create($fileName, $data, $storePath, 'xlsx')->download('xlsx');
        dd($errors);
        $this->sheetId = 41;
        $migrated = $orderSetting->orders()->where(['extra->sheet_id' => $this->sheetId])->get()->toArray();
        $total = count($migrated);
        $i = (int) ($total / 100);

        $lastOrder = end($migrated);

//        $lastOrderId = $orderSetting->orders()->latest()->first();
//        $this->sheetId = ($lastOrderId) ? $lastOrderId->extra['sheet_id'] + 1 : 57;
//        $this->sheetId = 40;
//        dd($lastOrderId);
//
//        $this->getSheetId();

        $projectName = 'repair-univse';
        $folderName = 'orders';
        $logFileName = 'order-' . $this->sheetId . '.log';

        \Log::useDailyFiles(storage_path() . '/logs/' . $projectName . '/' . $folderName . '/' . $logFileName);
        \Log::info("Script Start $this->sheetId");

        $data = [];
        $data['db_host'] = '127.0.0.1';
        $data['db_port'] = '3306';
        $data['db_name'] = 'repairs_universe';
        $data['db_username'] = 'root';
        $data['db_password'] = '23b4afb60c6aa3d05b475e33fc6c3423cebb2cd59a180066';

        $this->db = CustomGuzzleHttp::connectExternalDatabase($data);

        $project = $orderSetting->project;
        $shopifyOrderRequest = new ShopifyApiOrderController($project->shopify_details);
        $this->shopifyProductRequest = new ShopifyApiProductController($project->shopify_details);

//        $i = 0;
        do {
            \Log::info(["sheet_index" => $i]);
            $skip = $i * 100;
            $filePath = resource_path('assets/repair-universe/order/order-' . $this->sheetId . '.xlsx');
            $migrateOrders = ExcelController::view($filePath, 100, $skip);
//            $migrateOrders = $migrateOrders[0];
            foreach ($migrateOrders as $migrateOrder) {
                $convertedEmail = $successOrder = $failedOrder = FALSE;
                $shopifyOrder = $this->generateShopifyOrderController($migrateOrder);
                $result = $shopifyOrderRequest->create($shopifyOrder);

                if (is_string($result)) {
                    $failedOrder = TRUE;
                    if (str_contains($result, '502 Bad Gateway') || !str_contains($result, '422 Unprocessable Entity')) {
                        sleep(1);
                        $result = $shopifyOrderRequest->create($shopifyOrder);
                        if (isset($result['order'])) {
                            $successOrder = TRUE;
                            $failedOrder = FALSE;
                        }
                    }

                    if ($failedOrder) {
                        $result = strtolower($result);
                        if (str_contains($result, "email")) {
                            sleep(1);
                            $convertedEmail = Helper::convertValidEmail($shopifyOrder['email']);
                            $shopifyOrder['email'] = $convertedEmail;
                            $result = $shopifyOrderRequest->create($shopifyOrder);
                            if (isset($result['order'])) {
                                $successOrder = TRUE;
                                $failedOrder = FALSE;
                            }
                        }
                    }
                } else {
                    $successOrder = TRUE;
                }

                $this->insertOrderInDB($result, $migrateOrder, $orderSetting, $convertedEmail);

                if ($successOrder) {
                    if ($migrateOrder['statusmark'] == 'Cancelled') {
                        $cancelOrder = $shopifyOrderRequest->cancel($result['order']['id']);
                        if (is_string($cancelOrder)) {
                            sleep(1);
                            $cancelOrder = $shopifyOrderRequest->cancel($result['order']['id']);
                        }
                        $fulFilledOrder = $shopifyOrderRequest->fulfillments($result['order']['id']);
                        if (is_string($fulFilledOrder)) {
                            sleep(1);
                            $fulFilledOrder = $shopifyOrderRequest->fulfillments($result['order']['id']);
                        }
                    }
                }
                unset($migrateOrder, $shopifyOrder, $result);
            }

            \Log::info(["sheet_index_end" => $i]);

            if (!$migrateOrders) {
                \Log::info(["sheet_done" => $i]);
                $i = 115;
            }

            $i++;
        } while ($i < 100);

        \Log::info("Script End $this->sheetId");
    }

    public function getSheetId()
    {
        try {
            $filePath = resource_path('assets/repair-universe/order/order-' . $this->sheetId . '.xlsx');
            ExcelController::view($filePath, 10, 0);
        } catch (Exception $ex) {
            $this->sheetId++;
            return $this->getSheetId();
        }

        return TRUE;
    }

    public function insertOrderInDB($result, $migrateOrder, $orderSetting, $convertedEmail)
    {
        $email = ($migrateOrder['ship_email']) ? $migrateOrder['ship_email'] : $migrateOrder['bill_email'];

        $extra = [];
        $extra['type'] = (isset($result['order'])) ? 'success' : 'error';
        $extra['order_id'] = $migrateOrder['id'];
        $extra['order_number'] = $migrateOrder['proid'];
        if (isset($result['order'])) {
            $extra['success'] = $result['order']['id'];
            $extra['shopify_order_number'] = $result['order']['name'];
        } else {
            $error = Helper::shopifyError($result);
            $extra['error'] = Helper::shopifyError($result);
            $extra['order_detail'] = $migrateOrder;
        }

        $extra['email'] = $email;
        $extra['convert_email'] = ($convertedEmail) ? $convertedEmail : $email;
        $extra['sheet_id'] = $this->sheetId;

        $projectOrder = new ProjectOrder;
        $projectOrder->id = \UUID::uuid4()->toString();
        $projectOrder->project_id = $orderSetting->project->id;
        $projectOrder->order = (isset($result['order'])) ? $result['order'] : ['error' => $error];
        $projectOrder->extra = $extra;

        $orderSetting->orders()->save($projectOrder);
    }

    public function generateShopifyOrderController($migrateOrder)
    {
        $email = ($migrateOrder['ship_email']) ? $migrateOrder['ship_email'] : $migrateOrder['bill_email'];
//        $email = "mahesh@wolfpointagency.com";
        $shopifyOrder = [];
        $shopifyOrder['email'] = $email;
        $shopifyOrder['line_items'] = $this->getLineItems($migrateOrder);
        $shopifyOrder['tags'] = $this->getTags($migrateOrder);
        $shopifyOrder['created_at'] = Helper::shopifyDateFormate($migrateOrder['date']);

        $shopifyOrder['financial_status'] = 'paid';
        if ($migrateOrder['statusmark'] != 'Cancelled') {
            $shopifyOrder['fulfillment_status'] = 'fulfilled';
        }
        $shopifyOrder['shipping_address'] = $this->getAddress($migrateOrder, "ship");
        $shopifyOrder['billing_address'] = $this->getAddress($migrateOrder, "bill");

        $shopifyOrder['shipping_lines'] = $this->getShippings($migrateOrder);
        $shopifyOrder['tax_lines'] = $this->getTaxLines($migrateOrder);
        $shopifyOrder['transactions'] = $this->getTransactions($migrateOrder['totalamount']);

        return $shopifyOrder;
    }

    public function getTransactions($totalAmount)
    {
        $transactions = [];

        if ($totalAmount > 0) {
            $transaction = [];
            $transaction['status'] = 'success';
            $transaction['amount'] = $totalAmount;
            $transactions[] = $transaction;
        }

        return $transactions;
    }

    public function getTaxLines($migrateOrder)
    {
        $taxLines = [];

        if ($migrateOrder['vwtax']) {
            $taxLine = [];
            $taxLine['title'] = 'Tax';
            $taxLine['price'] = $migrateOrder['vwtax'];
//            $taxLine['rate'] = 1;
            $taxLines[] = $taxLine;
        }
        return $taxLines;
    }

    public function getShippings($migrateOrder)
    {
        $shippingLines = [];
        if ($migrateOrder['vwshipping'] > 0) {
            $shippingLine = [];
            $shippingLine['code'] = 'Shipping Charge';
            $shippingLine['title'] = $migrateOrder['shipping'];
            $shippingLine['source'] = 'shopify';
            $shippingLine['price'] = $migrateOrder['vwshipping'];
            $shippingLines[] = $shippingLine;
        }

        return $shippingLines;
    }

    public function getAddress($migrateOrder, $type)
    {
        $shippingAdress = [];
        $shippingAdress['name'] = $migrateOrder[$type . '_name'];
        $shippingAdress['address1'] = $migrateOrder[$type . '_address1'];
        $shippingAdress['address2'] = $migrateOrder[$type . '_address2'];
        $shippingAdress['company'] = $migrateOrder[$type . '_company'];
        $shippingAdress['city'] = $migrateOrder[$type . '_city'];
        $shippingAdress['country_code'] = $migrateOrder[$type . '_countrycode'];
        if ($migrateOrder[$type . '_countrycode']) {
            $shippingAdress['province_code'] = $migrateOrder[$type . '_state'];
        }
        $shippingAdress['zip'] = $migrateOrder[$type . '_zip'];
        $shippingAdress['phone'] = $migrateOrder[$type . '_phone'];
        return $shippingAdress;
    }

    public function getLineItems($migrateOrder)
    {
        $orderLineItems = [];
        $temp = '';
        for ($i = 1; $i <= 20; $i++) {
            $key = 'itemdata_' . $i;
            $item = $migrateOrder[$key];
            if ($item) {
                $segements = explode('|', $item);

                if (count($segements) != '6') {
                    if ($temp) {
                        $temp = $temp . ',' . $item;
                    } else {
                        $temp = $temp . $item;
                    }
                    continue;
                } else {
                    if ($i > 1) {
                        $prevKey = $i - 1;
                        $test = explode('|', $migrateOrder['itemdata_' . $prevKey]);

                        if (count($test) < 6) {
                            $temp = $temp . ',' . $item;
                            $segements = explode('|', $temp);
                            $temp = '';
                        }
                    }
                }

                $variant = $this->db->table('shopify_product')->where('sku', $segements[2])->first();

                if (!$variant) {
                    $product = $this->createTempProduct($segements);

                    $result = $this->shopifyProductRequest->create($product);
                    if (!isset($result['product'])) {
                        sleep(1);
                        $result = $this->shopifyProductRequest->create($product);
                    }

                    $resultProduct = $result['product'];

                    $migrateProduct = [];
                    $migrateProduct['product_handle'] = $resultProduct['handle'];
                    $migrateProduct['product_id'] = $resultProduct['id'];
                    $migrateProduct['variant_id'] = $resultProduct['variants'][0]['id'];
                    $migrateProduct['sku'] = $resultProduct['variants'][0]['sku'];
                    $this->db->table('shopify_product')->insert($migrateProduct);

                    $variant = $this->db->table('shopify_product')->where('sku', $segements[2])->first();
                }

                $lineItem = [];
                $lineItem['variant_id'] = $variant->variant_id;
                $lineItem['quantity'] = (int) $segements[3];
                $orderLineItems[] = $lineItem;
            }
        }
        return $orderLineItems;
    }

    public function getTags($migrateOrder)
    {
        $orderTags = [];
        $orderTags[] = 'migration-app';
        $orderTags[] = 'yahoo-b-' . strtolower($migrateOrder['id']);
        $orderTags[] = 'yahoo-f-' . strtolower($migrateOrder['proid']);
        $orderTags[] = 'ship-state-' . strtolower($migrateOrder['shipstate']);
        $orderTags[] = 'card-' . strtolower($migrateOrder['card']);
        $orderTags[] = 'ip-' . strtolower($migrateOrder['ipaddr']);
        $orderTags[] = 'statusmark-' . strtolower($migrateOrder['statusmark']);

        return implode(', ', $orderTags);
    }

    public function createTempProduct($segements)
    {
        $productVariant = [];
        $productVariant['sku'] = $segements[2];
        $productVariant['price'] = $segements[4];

        $product = [];
        $product['title'] = $segements[0];
        $product['handle'] = $segements[1];
        $product['variants'] = [];
        $product['variants'][] = $productVariant;
        $product['tags'] = 'product-deleted';
        $product['product_type'] = 'product-deleted';
        $product['published'] = false;
        return $product;
    }
}
