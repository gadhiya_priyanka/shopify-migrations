<?php

namespace App\Http\Controllers\Core;

use App\Models\ProjectRedirectSetting as RedirectSetting;
use Image;
use Exception;
use App\Models\Platform;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommonFunctionsController extends Controller
{

    public function __construct()
    {
        
    }

    public function isAssetUrl($url)
    {
        $assets = ['.js', '.css', '.pdf', '.png', '.gif', '.jpg', '.svg', '.jpeg', '.ico'];
        return str_contains($url, '.');
    }

    //Model functions
    public function getPlatform($projectName)
    {
        return Platform::where(['name' => $projectName])->first();
    }

    // Redirect301 Functions
    public function getRedirectSearchUrl($url)
    {
        //get url path only
        return str_replace(['.php', '.html', '.htm'], '', trim(parse_url($url, PHP_URL_PATH), '/'));
    }

    public function getRedirectSearchQueryFromUrl($url)
    {
        // get query parameter
        return parse_url($url, PHP_URL_QUERY);
    }

    public function getRedirectSearchTermFromUrl($url)
    {
        $segments = explode('/', $this->getRedirectSearchUrl($url));
        return strtolower(end($segments));
    }

    public function getRedirectTargetUrl(RedirectSetting $redirectSetting, $search)
    {
        $targetUrl = $redirectSetting->redirects()->where(['redirect->source_handle' => $search])->first();
        return ($targetUrl) ? $targetUrl->redirect['target'] : NULL;
    }

    public function searchBasicUrl($url)
    {
        $url = strtolower($url);

        //Admin Url
        $basicUrl = $this->checkAdminUrl($url);

        //Account Url
        if (!$basicUrl) {
            $basicUrl = $this->checkAccountPageUrl($url);
        }

        //Root Url
        if (!$basicUrl) {
            $basicUrl = $this->checkRootPageUrl($url);
        }

        //Cart Page Url
        if (!$basicUrl) {
            $basicUrl = $this->checkCartPageUrl($url);
        }

        //Search Page Url
        if (!$basicUrl) {
            $basicUrl = $this->checkSearchPageUrl($url);
        }

        //Login Page Url
        if (!$basicUrl) {
            $basicUrl = $this->checkLoginPageUrl($url);
        }

        //Register Page Url
        if (!$basicUrl) {
            $basicUrl = $this->checkRegisterPageUrl($url);
        }

        return ($basicUrl) ? $basicUrl : NULL;
    }

    public function checkAccountPageUrl($url)
    {
        if (str_contains($url, 'account') || str_contains($url, 'checkout') || str_contains($url, 'customer') || str_contains($url, 'profile') || str_contains($url, 'user') || str_contains($url, 'order/')) {
            return config('app.shopify_account_url');
        }
    }

    public function checkAdminUrl($url)
    {
        return (str_contains($url, 'admin')) ? config('app.shopify_admin_url') : NULL;
    }

    public function checkRootPageUrl($url)
    {
        return (str_contains($url, 'newsletter')) ? config('app.shopify_root_url') : NULL;
    }

    public function checkCartPageUrl($url)
    {
        return (str_contains($url, 'cart')) ? config('app.shopify_cart_url') : NULL;
    }

    public function checkSearchPageUrl($url)
    {
        return (str_contains($url, 'search')) ? config('app.shopify_search_url') : NULL;
    }

    public function checkLoginPageUrl($url)
    {
        return (str_contains($url, 'login') || str_contains($url, 'signup')) ? config('app.shopify_login_url') : NULL;
    }

    public function checkRegisterPageUrl($url)
    {
        return (str_contains($url, 'register')) ? config('app.shopify_register_url') : NULL;
    }

    public function startNextRedirectMigration($redirectSetting)
    {
        $redirectSetting = $redirectSetting->project->redirectSettings()->with('schedule')
                ->whereHas('schedule', function ($query) {
                    $query->where('status', 'queued');
                })->first();

        if ($redirectSetting) {
            $schedule = $redirectSetting->schedule;
            $schedule->status = 'todo';
            $schedule->save();
        }
    }

    public function generateProductImageSrc($url, $projectId)
    {
        try {
            $imageInfo = getimagesize($url);
        } catch (Exception $e) {
            return NULL;
        }

        $width = $imageInfo[0];
        $height = $imageInfo[1];

        if ($width > '4472') {
            $segements = explode('/', $url);
            $name = end($segements);

            $directoryPath = 'public/images/projects/' . $projectId;
            if (!file_exists(storage_path('app/' . $directoryPath))) {
                \Storage::makeDirectory($directoryPath);
            }
            $publicPath = 'app/' . $directoryPath . '/' . time() . $name;
            $path = storage_path($publicPath);

            $imgWidth = '4472';
            $imgHeight = 4472 - ($width - $height);

            if ($imgHeight > $imgWidth) {
                $difference = abs(($width - $height));
                $imgWidth = $imgWidth - $difference;
                $imgHeight = $imgHeight - $difference;
            }

            $img = Image::make($url);
            $img->resize($imgWidth, $imgHeight);
            $img->save($path);

            $publicPath = str_replace('app/public', 'storage', $publicPath);
            $url = url($publicPath);
        }
        return $url;
    }
}
