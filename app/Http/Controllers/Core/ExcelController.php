<?php

namespace App\Http\Controllers\Core;

use Excel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExcelController extends Controller
{

    public static function view($filePath, $size = 0, $skip = 0)
    {
        return Excel::load($filePath, function($reader) use ($size, $skip) {
                if ($size == 0 && $skip == 0) {
                    $reader->get();
                } else {
                    $reader->skipRows($skip)->takeRows($size);
                }
            })->get()->toArray();
    }

    public static function create($fileName, $data, $storePath, $fileType = 'xlsx')
    {
        return Excel::create($fileName, function($excel) use($data) {

                $excel->sheet('Sheet 1', function($sheet) use($data) {
                    $sheet->setAutoSize(true);
                    $sheet->with($data);
                });
            })->store($fileType, storage_path($storePath));
    }

    public static function advanceCreate($fileName, $data, $storePath, $fileType = 'xlsx')
    {
        return Excel::create($fileName, function($excel) use($data) {
                foreach ($data as $index => $d) {
                    $no = $index + 1;
                    $excel->sheet('Sheet ' . $no, function($sheet) use($d) {
                        $sheet->setAutoSize(true);
                        $sheet->with($d);
                    });
                }
            })->store($fileType, storage_path($storePath));
    }
}
