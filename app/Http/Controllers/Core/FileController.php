<?php

namespace App\Http\Controllers\Core;

//use App\Models\Redirect301;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FileController extends Controller
{

    public function download($model, $id, $type, $fileType)
    {
        $model = '\\App\\Models\\' . $model;
        $model = $model::find($id);

        $filePath = $model->file_paths[$type][$fileType];
        $path = storage_path($filePath);

        $fileName = ($type == "input") ? $model->file_paths[$type]['file_name'] : $fileType . '-' . time() . '.' . pathinfo($path, PATHINFO_EXTENSION);
        return response()->download($path, $fileName);
    }

    public function downloadSampleFiles($model)
    {
        $fileName = 'sample-' . strtolower($model) . '.xlsx';
        $filePath = 'app/sample-files/' . $fileName;
        $path = storage_path($filePath);
        return response()->download($path, $fileName);
    }
}
