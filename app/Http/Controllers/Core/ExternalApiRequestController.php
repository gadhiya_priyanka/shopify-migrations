<?php

namespace App\Http\Controllers\Core;

use Exception;
use App\Models\CustomGuzzleHttp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExternalApiRequestController extends Controller
{
    public $client;

    public function __construct($data)
    {
        $this->client = CustomGuzzleHttp::connectExternalAPI($data);
    }

    public function view($url, $params = [])
    {
        try {
            $result = $this->client->request('GET', $url, ['query' => $params]);
        } catch (Exception $e) {

            $error = $e->getMessage();
            $retry = $this->checkValidError($error);
            if ($retry) {
                try {
                    $result = $this->client->request('GET', $url, ['query' => $params]);
                } catch (Exception $e) {

                    return $e->getMessage();
                }
            } else {
                return $e->getMessage();
            }
        }

        return json_decode($result->getBody(), true);
    }

    public function create($url, $params = [])
    {
        try {
            $result = $this->client->request('POST', $url, ['json' => $params]);
        } catch (Exception $e) {

            $error = $e->getMessage();
            $retry = $this->checkValidError($error);
            if ($retry) {
                try {
                    $result = $this->client->request('POST', $url, ['json' => $params]);
                } catch (Exception $e) {

                    return $e->getMessage();
                }
            } else {
                return $e->getMessage();
            }
        }

        return json_decode($result->getBody(), true);
    }

    public function update($url, $params = [])
    {
        try {
            $result = $this->client->request('PUT', $url, ['json' => $params]);
        } catch (Exception $e) {

            $error = $e->getMessage();
            $retry = $this->checkValidError($error);
            if ($retry) {
                try {
                    $result = $this->client->request('PUT', $url, ['json' => $params]);
                } catch (Exception $e) {

                    return $e->getMessage();
                }
            } else {
                return $e->getMessage();
            }
        }

        return json_decode($result->getBody(), true);
    }

    public function delete($url, $params = [])
    {
        try {
            $result = $this->client->request('DELETE', $url, ['json' => $params]);
        } catch (Exception $e) {

            $error = $e->getMessage();
            $retry = $this->checkValidError($error);
            if ($retry) {
                try {
                    $result = $this->client->request('DELETE', $url, ['json' => $params]);
                } catch (Exception $e) {

                    return $e->getMessage();
                }
            } else {
                return $e->getMessage();
            }
        }

        return json_decode($result->getBody(), true);
    }

    public function graphQL($url, $query = NULL)
    {
        $params = [
            'body' => $query,
            'headers' => [
                'Content-Type' => 'application/graphql'
            ]
        ];
        try {
            $result = $this->client->request('POST', $url, $params);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $retry = $this->checkValidError($error);
            if ($retry) {
                try {
                    $result = $this->client->request('POST', $url, $params);
                } catch (Exception $e) {
                    return $e->getMessage();
                }
            } else {
                return $e->getMessage();
            }
        }

        return json_decode($result->getBody(), true);
    }

    public function checkValidError($error)
    {
        if (str_contains($error, '502 Bad Gateway') || !str_contains($error, '422 Unprocessable Entity')) {
//            \Log::info(["External API Request Controller Error" => $error]);
            sleep(1);
            return TRUE;
        }
        return FALSE;
    }
}
