<?php

namespace App\Http\Controllers\Core;

use Exception;
use App\Models\CustomGuzzleHttp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestConnectionCollection extends Controller
{

    public function __construct()
    {
        set_time_limit(0);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('pages.test-connection');
        }

        $this->validate($request, $this->validationRule(), [], $this->validationMessages());

        $data = [];
        $data['db_host'] = $request->db_host;
        $data['db_port'] = $request->db_port;
        $data['db_name'] = $request->db_name;
        $data['db_username'] = $request->db_username;
        $data['db_password'] = $request->db_password;

        $db = CustomGuzzleHttp::connectExternalDatabase($data);

        try {
            $tables = $db->select('SHOW TABLES');
        } catch (Exception $ex) {
            return redirect()->back()->withInput()->withErrors(['platform_database' => 'Sorry!! We could not access database.']);
        }

        return redirect()->route('test-connection')->with('message', 'Database connection successfully!!');
    }

    public function validationRule()
    {
        return [
            'db_host' => 'required|max:50',
            'db_port' => 'required|max:4',
            'db_name' => 'required|max:20',
            'db_username' => 'required|max:50',
            'db_password' => 'required|max:50',
        ];
    }

    public function validationMessages()
    {
        return [
            'db_host' => 'database host',
            'db_port' => 'database port',
            'db_name' => 'database name',
            'db_username' => 'database username',
            'db_password' => 'database password',
        ];
    }
}
