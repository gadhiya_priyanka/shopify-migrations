<?php

namespace App\Http\Controllers\Django\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function getShopifyProductObject($migrateProduct)
    {
        $shopifyProduct = [];
        $shopifyProduct['handle'] = '';
        $shopifyProduct['title'] = '';
        $shopifyProduct['body_html'] = '';
        $shopifyProduct['vendor'] = '';
        $shopifyProduct['product_type'] = '';
        $shopifyProduct['tags'] = '';
        $shopifyProduct['published'] = 'true/false';
        $shopifyProduct['variants'] = [
            [
                'option1' => 'test',
                'sku' => '4548',
                'grams' => '',
                'inventory_quantity' => '',
                'inventory_management' => 'shopify/ignore field',
                'inventory_policy' => 'deny/continue',
                'fulfillment_service' => 'manual/gift_card',
                'price' => '25.54',
                'compare_at_price' => '',
                'requires_shipping' => 'true/false',
                'taxable' => 'true/false',
                'barcode' => ''
            ]
        ];
        $shopifyProduct['images'] = [
            'position',
            'src',
            'alt',
            'variant_ids' //check 
        ];

        $shopifyProduct['metafields_global_title_tag'] = '';
        $shopifyProduct['metafields_global_description_tag'] = '';
    }
}
