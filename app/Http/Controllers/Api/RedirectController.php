<?php

namespace App\Http\Controllers\Api;

use App\Models\ProjectRedirectSetting;
use App\Models\ProjectRedirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RedirectController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!isset($request->redirect_setting_id) || !isset($request->source) || !isset($request->target) || !isset($request->source_handle) || !isset($request->target_handle)) {
            return response()->json(['errors' => ['redirect' => 'Required parameter missing or invalid.']]);
        }

        $redirectSetting = ProjectRedirectSetting::where(['id' => $request->redirect_setting_id, 'mapping->external' => true])->first();
        if (!$redirectSetting) {
            return response()->json(['errors' => ['redirect' => 'Invalid redirect setting id.']]);
        }

        $redirectJson = [
            'source' => strtolower($request->source),
            'target' => strtolower($request->target),
            'source_handle' => strtolower($request->source_handle),
            'target_handle' => strtolower($request->target_handle)
        ];
        $redirect = new ProjectRedirect;
        $redirect->id = \UUID::uuid4()->toString();
        $redirect->project_id = $redirectSetting->project->id;
        $redirect->redirect = $redirectJson;
        $redirectSetting->redirects()->save($redirect);

        $response = [
            'id' => $redirect->id,
            'project_id' => $redirect->project_id,
            'redirect_setting_id' => $redirect->project_redirect_setting_id,
            'redirect' => $redirect->redirect
        ];
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
