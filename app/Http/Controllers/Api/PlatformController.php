<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\JsonResource;
use App\Models\Platform;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlatformController extends Controller
{

    public function __construct()
    {
        
    }

    public function view(Platform $platform)
    {
        //with settings
        $platform->setting;
        return new JsonResource($platform);
    }
}
