<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\JsonResource;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{

    public function __construct()
    {
        
    }

    public function view(Request $request, Project $project)
    {
        $with = $request->with;
        $with = explode(',', $with);
        $with = array_map('trim', $with);

        $project->platform;
        
        if (in_array('products', $with)) {
            $project->products;
        }

        if (in_array('collections', $with)) {
            $project->collections;
        }

        if (in_array('customers', $with)) {
            $project->customers;
        }

        if (in_array('orders', $with)) {
            $project->orders;
        }

        return new JsonResource($project);
    }
}
