<?php

namespace App\Http\Controllers\Api;

use App\Models\ProjectRedirectSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RedirectSettingController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!isset($request->project_id)) {
            return response()->json(['errors' => ['redirect-setting' => 'Required parameter missing or invalid.']]);
        }

        $project = \App\Models\Project::find($request->project_id);
        if (!$project) {
            return response()->json(['errors' => ['redirect-setting' => 'Invalid project id.']]);
        }
        $redirectSetting = ProjectRedirectSetting::where(['mapping->external' => true, 'project_id' => $request->project_id])->first();
        if (!$redirectSetting) {
            $mapping = [];
            $mapping['external'] = TRUE;

            $redirectSetting = new ProjectRedirectSetting;
            $redirectSetting->id = \UUID::uuid4()->toString();
            $redirectSetting->project_id = $request->project_id;
            $redirectSetting->mapping = $mapping;
            $redirectSetting->save();
        }

        $response = [
            'id' => $redirectSetting->id,
            'project_id' => $redirectSetting->project_id
        ];

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
