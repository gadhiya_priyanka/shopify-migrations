<?php

namespace App\Rules;

use App\Http\Controllers\Core\ExcelController;
use Illuminate\Contracts\Validation\Rule;

class RedirectRule implements Rule
{

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $fileName = $value->getClientOriginalName();
        $fileName = trim(preg_replace('/[^A-Za-z0-9\-.]/', '', str_replace(' ', '-', strtolower(trim($fileName)))));
        $fileName = \UUID::uuid4()->toString() . time() . $fileName;
        $filePath = $value->storeAs('temp', $fileName);
        $storagePath = storage_path('app/' . $filePath);

        $platformUrls = ExcelController::view($storagePath, 1);
        \Storage::delete($filePath);

        $key = ($platformUrls) ? key($platformUrls[0]) : '';
        return $key === 'path';
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The upload file must have valid format.';
    }
}
