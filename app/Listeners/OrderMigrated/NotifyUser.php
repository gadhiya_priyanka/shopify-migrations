<?php

namespace App\Listeners\OrderMigrated;

use App\Http\Controllers\Core\ExcelController;
use App\Notifications\NotifyOrderSettingMigrated;
use App\Events\OrderMigrated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyUser
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderMigrated  $event
     * @return void
     */
    public function handle(OrderMigrated $event)
    {
        $orderSetting = $event->orderSetting;
        $user = $orderSetting->project->user;

        $successes = $this->getSpecificOrders($orderSetting, 'success');
        $errors = $this->getSpecificOrders($orderSetting, 'failed');

        $outputs = [];
        if ($successes) {
            $outputs['success'] = $successes;
        }

        if ($errors) {
            $outputs['failed'] = $errors;
        }

        $filePaths = [];
        $filePaths['outputs'] = $outputs;
        $orderSetting->file_paths = $filePaths;
        $orderSetting->save();

        $user->notify(new NotifyOrderSettingMigrated($orderSetting));
    }

    public function getSpecificOrders($orderSetting, $type)
    {
        $limit = config('app.write_excel_limit');
        $typeOrder = $type . 'Orders';
        $orders = collect($orderSetting->$typeOrder()->pluck('export')->toArray())->chunk($limit)->toArray();
        if ($orders) {
            $fileName = $type . '-orders-' . $orderSetting->id;
            $storePath = 'app/' . $orderSetting->project_id . '/orders/outputs';
            $fileType = 'xlsx';
            ExcelController::advanceCreate($fileName, $orders, $storePath, $fileType);

            return $storePath . '/' . $fileName . '.' . $fileType;
        }
        return FALSE;
    }
}
