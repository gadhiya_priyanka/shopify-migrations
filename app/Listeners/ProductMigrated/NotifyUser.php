<?php

namespace App\Listeners\ProductMigrated;

use App\Http\Controllers\Core\ExcelController;
use App\Notifications\NotifyProductSettingMigrated;
use App\Events\ProductMigrated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyUser implements ShouldQueue
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductMigrated  $event
     * @return void
     */
    public function handle(ProductMigrated $event)
    {
        $productSetting = $event->productSetting;
        $user = $productSetting->project->user;

        $successes = $this->getSpecificProducts($productSetting, 'success');
        $errors = $this->getSpecificProducts($productSetting, 'failed');

        $outputs = [];
        if ($successes) {
            $outputs['success'] = $successes;
        }

        if ($errors) {
            $outputs['failed'] = $errors;
        }

        $filePaths = [];
        $filePaths['outputs'] = $outputs;
        $productSetting->file_paths = $filePaths;
        $productSetting->save();

        $user->notify(new NotifyProductSettingMigrated($productSetting));
    }

    public function getSpecificProducts($productSetting, $type)
    {
        $limit = config('app.write_excel_limit');
        $typeRedirect = $type . 'Products';
        $products = collect($productSetting->$typeRedirect()->pluck('export')->toArray())->chunk($limit)->toArray();

        if ($products) {
            $fileName = $type . '-products-' . $productSetting->id;
            $storePath = 'app/' . $productSetting->project_id . '/products/outputs';
            $fileType = 'xlsx';
            ExcelController::advanceCreate($fileName, $products, $storePath, $fileType);
            return $storePath . '/' . $fileName . '.' . $fileType;
        }
        return FALSE;
    }
}
