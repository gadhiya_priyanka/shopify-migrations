<?php

namespace App\Listeners\CustomerMigrated;

use App\Http\Controllers\Core\ExcelController;
use App\Notifications\NotifyCustomerSettingMigrated;
use App\Events\CustomerMigrated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyUser
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CustomerMigrated  $event
     * @return void
     */
    public function handle(CustomerMigrated $event)
    {
        $customerSetting = $event->customerSetting;
        $user = $customerSetting->project->user;

        $successes = $this->getSpecificCustomers($customerSetting, 'success');
        $errors = $this->getSpecificCustomers($customerSetting, 'failed');

        $outputs = [];
        if ($successes) {
            $outputs['success'] = $successes;
        }

        if ($errors) {
            $outputs['failed'] = $errors;
        }

        $filePaths = [];
        $filePaths['outputs'] = $outputs;
        $customerSetting->file_paths = $filePaths;
        $customerSetting->save();

        $user->notify(new NotifyCustomerSettingMigrated($customerSetting));
    }

    public function getSpecificCustomers($customerSetting, $type)
    {
        $limit = config('app.write_excel_limit');
        $typeCustomer = $type . 'Customers';
        $customers = collect($customerSetting->$typeCustomer()->pluck('export')->toArray())->chunk($limit)->toArray();
        if ($customers) {
            $fileName = $type . '-customers-' . $customerSetting->id;
            $storePath = 'app/' . $customerSetting->project_id . '/customers/outputs';
            $fileType = 'xlsx';
            ExcelController::advanceCreate($fileName, $customers, $storePath, $fileType);

            return $storePath . '/' . $fileName . '.' . $fileType;
        }
        return FALSE;
    }
}
