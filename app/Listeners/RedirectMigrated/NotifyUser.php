<?php

namespace App\Listeners\RedirectMigrated;

use App\Http\Controllers\Core\ExcelController;
use App\Notifications\NotifyRedirectSettingMigrated;
use App\Events\RedirectMigrated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyUser implements ShouldQueue
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RedirectMigrated  $event
     * @return void
     */
    public function handle(RedirectMigrated $event)
    {
        $redirectSetting = $event->redirectSetting;
        $user = $redirectSetting->project->user;

        $successes = $this->getSpecificRedirects($redirectSetting, 'success');
        $errors = $this->getSpecificRedirects($redirectSetting, 'failed');
        $assets = $this->getSpecificRedirects($redirectSetting, 'asset');

        $outputs = [];
        if ($successes) {
            $outputs['success'] = $successes;
        }

        if ($errors) {
            $outputs['failed'] = $errors;
        }

        if ($assets) {
            $outputs['asset'] = $assets;
        }

        $filePaths = [];
        $filePaths = $redirectSetting->file_paths;
        $filePaths['outputs'] = $outputs;
        $redirectSetting->file_paths = $filePaths;
        $redirectSetting->save();

        $user->notify(new NotifyRedirectSettingMigrated($redirectSetting));
    }

    public function getSpecificRedirects($redirectSetting, $type)
    {
        $limit = config('app.write_excel_limit');
        $typeRedirect = $type . 'Redirects';
        $redirects = collect($redirectSetting->$typeRedirect()->pluck('export')->toArray())->chunk($limit)->toArray();
        if ($redirects) {
            $fileName = $type . '-redirects-' . $redirectSetting->id;
            $storePath = 'app/' . $redirectSetting->project_id . '/redirects/outputs';
            $fileType = 'xlsx';
            ExcelController::advanceCreate($fileName, $redirects, $storePath, $fileType);

            return $storePath . '/' . $fileName . '.' . $fileType;
        }
        return FALSE;
    }
}
