<?php

namespace App\Listeners\SchedulSaved;

use App\Events\ScheduleSaved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class StartMigration implements ShouldQueue
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ScheduleSaved  $event
     * @return void
     */
    public function handle(ScheduleSaved $event)
    {
        $migrations = [
            'Redirects',
            'Products',
//            'Collections',
//            'Customers',
//            'Orders'
        ];
        $schedule = $event->schedule;

        if (in_array($schedule->shopifyMigration->type, $migrations)) {
            $validStatus = [
                'todo',
                'paused'
            ];

            if (in_array($schedule->status, $validStatus)) {
                $setting = $schedule->schedulable_type::find($schedule->schedulable_id);
                $platformName = $setting->project->platform->name;
                $folderName = $schedule->shopifyMigration->type;
                $controllerName = substr($folderName, 0, -1) . 'Controller';

                $controllerPath = '\App\Http\Controllers\\' . $platformName . '\\' . $folderName . '\\' . $controllerName;

                $controller = new $controllerPath;
                $controller->create($setting);
            }
        }
    }
}
