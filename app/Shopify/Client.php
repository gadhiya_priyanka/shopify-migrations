<?php

namespace App\Shopify;

use GuzzleHttp\Client as GuzzleClient;
use Psr\Http\Message\ResponseInterface;

class Client
{
    /**
     * @var string
     */
    protected $domain;

    /**
     * @var null|string
     */
    protected $accessToken;

    /**
     * @var CallLimit
     */
    protected $callLimit;

    /**
     * @var null|object
     */
    protected $httpClient;

    /**
     * Client constructor.
     * @param string $domain
     * @param null|string $accessToken
     * @param CallLimit|null $callLimit
     * @param null|object $httpClient
     */
    public function __construct($domain, $accessToken = null, CallLimit $callLimit = null, $httpClient = null)
    {
        $this->domain = $domain;
        $this->accessToken = $accessToken;
        $this->callLimit = $callLimit ?: new CallLimitTemporary();        
        $this->httpClient = $httpClient ?: new GuzzleClient($this->tokenHeader());
    }

    /**
     * @param $args
     * @return bool|mixed|ResponseInterface
     */
    public function get($args)
    {
        $args['verb'] = 'get';

        return $this->http($args);
    }

    /**
     * @param $args
     * @return bool|mixed|ResponseInterface
     */
    public function post($args)
    {
        $args['verb'] = 'post';

        return $this->http($args);
    }

    /**
     * @param $args
     * @return bool|mixed|ResponseInterface
     */
    public function put($args)
    {
        $args['verb'] = 'put';

        return $this->http($args);
    }

    /**
     * @param $args
     * @return bool|mixed|ResponseInterface
     */
    public function delete($args)
    {
        $args['verb'] = 'delete';

        return $this->http($args);
    }

    /**
     * @param $args
     * @return bool|mixed|ResponseInterface
     */
    public function http($args)
    {
        $endpoint = $this->endpoint($args['path'], $this->pluck('query', $args));

        $options = $this->prepare($this->pluck('options', $args, []));

        $response = $this->makeRequest($args['verb'], $endpoint, $options);

        return $this->parse($response, $this->pluck('extract', $args));
    }

    /**
     * @param $verb
     * @param $endpoint
     * @param $options
     * @return mixed
     */
    protected function makeRequest($verb, $endpoint, $options)
    {
        $this->callLimit->wait($this->callLimit->queue($this->domain));
        
        $response = $this->httpClient()->$verb($endpoint, $options);

        $header = ($headers = $response->getHeader('X-Shopify-Shop-Api-Call-Limit')) ? $headers[0] : '';
        $this->callLimit->processResponse($this->domain, $header);

        return $response;
    }

    /**
     * @param $key
     * @param $items
     * @param mixed $default
     * @return mixed
     */
    protected function pluck($key, $items, $default = false)
    {
        return (isset($items[$key])) ? $items[$key] : $default;
    }

    /**
     * @param string $path
     * @param bool $query
     * @return string
     */
    public function endpoint($path, $query = false)
    {
        $url = "https://{$this->domain}/admin/" . trim($path, '/');

        if (!$query) {
            return $url;
        }

        return $url . '?' . urldecode(http_build_query($query, '', '&'));
    }

    /**
     * @param ResponseInterface $response
     * @param bool $extract
     * @return bool|mixed|ResponseInterface
     */
    public function parse(ResponseInterface $response, $extract = false)
    {
        $response = json_decode($response->getBody(), true);

        if (!$extract) {
            return $response;
        }

        $extract = is_array($extract) ? $extract[0] : $extract;
        $extract = isset($response[$extract]) ? $response[$extract] : false;
        return $extract;
    }

    /**
     * @param array $options
     * @return array
     */
    protected function prepare(array $options = [])
    {
        return $options ? ['json' => $options] : $options;
    }

    /**
     * @return object|GuzzleClient
     */
    public function httpClient()
    {
        return $this->httpClient;
    }

    /**
     * @return array
     */
    public function tokenHeader()
    {
        return [
            'defaults' => [
                'headers' => ['content-type' => 'application/json']
            ],
            'auth' => [
                'c88cad54a110a2b52b657819d2aeb737',
                '9fa89918e2508856c2be712416ad357b'
            ]
        ];
        return ($token = $this->getAccessToken()) ? ['headers' => ['X-Shopify-Access-Token' => $token]] : [];
    }

    /**
     * @return null|string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }
}
