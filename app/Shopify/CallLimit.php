<?php

namespace App\Shopify;

abstract class CallLimit
{
    protected $header = 'X-Shopify-Shop-Api-Call-Limit';

    protected $limit = 40;

    protected $waitTime = 0.5;

    /**
     * @param string $domain
     * @param array $queue
     * @return void
     */
    abstract protected function setQueue($domain, $queue);

    /**
     * @param string $domain
     * @return array|null
     */
    abstract protected function getQueue($domain);

    /**
     * @param string $domain
     * @return bool|float
     */
    public function queue($domain)
    {
        $queue = $this->getQueue($domain) ?: [];

        $queue['queued'][] = $this->now();
        $this->setQueue($domain, [
            'queued'     => $queue['queued'],
            'call_limit' => isset($queue['call_limit']) ? $queue['call_limit'] : 0,
        ]);

        return $this->waitTime($domain);
    }

    /**
     * @param string $domain
     * @param string $header
     */
    public function processResponse($domain, $header)
    {
        list($calls) = array_map('intval', explode('/', $header));

        if ($queue = $this->getQueue($domain)) {
            array_shift($queue['queued']);
        }

        $this->setQueue($domain, [
            'call_limit' => $calls,
            'queued'     => $queue ? $queue['queued'] : []
        ]);
    }

    /**
     * @param string $domain
     * @return bool|float
     */
    public function waitTime($domain)
    {
        $queue = $this->getQueue($domain);

        if ($queue['call_limit'] + 1 < $this->limit) {
            return false;
        }

        $time = $this->waitTime * count($queue['queued']);

        return $time > 0 ? $time : 0;
    }

    /**
     * @param bool|float|int $seconds
     */
    public function wait($seconds)
    {
        if (is_numeric($seconds)) {
            usleep($seconds * 1000000);
        }
    }

    /**
     * @return float
     */
    protected function now()
    {
        return microtime(true);
    }
}
