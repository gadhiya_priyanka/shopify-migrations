<?php

namespace App\Shopify\Api;

class Theme extends Resource
{
    public function get($query = false)
    {
        return $this->client->get([
            'path'    => 'themes.json',
            'query'   => $query,
            'extract' => 'themes',
        ]);
    }
}
