<?php

namespace App\Shopify\Api;

use App\Shopify\Client;

abstract class Resource
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var int
     */
    protected $recordsPerPage = 250;

    /**
     * Resource constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return bool|int
     */
    public function pages()
    {
        if (! method_exists($this, 'count')) {
            return false;
        }

        return (int) ceil($this->count() / (float) $this->recordsPerPage);
    }
}
