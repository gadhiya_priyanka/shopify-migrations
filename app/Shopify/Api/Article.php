<?php

namespace App\Shopify\Api;

class Article extends Resource
{
    public function all($blog, $query = false)
    {
        return $this->client->get([
            'path'    => "blogs/{$blog}/articles.json",
            'query'   => $query,
            'extract' => 'articles',
        ]);
    }

    public function count($blog, $query = false)
    {
        return $this->client->get([
            'path'    => "blogs/{$blog}/articles/count.json",
            'query'   => $query,
            'extract' => 'count',
        ]);
    }

    public function get($blog, $id, $query = false)
    {
        return $this->client->get([
            'path'    => "blogs/{$blog}/articles/{$id}.json",
            'query'   => $query,
            'extract' => 'article',
        ]);
    }

    public function create($blog, array $article)
    {
        return $this->client->post([
            'path'    => "blogs/{$blog}/articles.json",
            'options' => ['article' => $article],
            'extract' => 'article',
        ]);
    }

    public function update($blog, $id, array $article)
    {
        return $this->client->put([
            'path'    => "blogs/{$blog}/articles/{$id}.json",
            'options' => ['article' => $article],
            'extract' => 'article',
        ]);
    }

    public function delete($blog, $id)
    {
        return $this->client->delete([
            'path'    => "blogs/{$blog}/articles/{$id}.json",
        ])->getStatusCode();
    }

    public function authors()
    {
        return $this->client->get([
            'path'    => 'articles/authors.json',
            'extract' => 'authors',
        ]);
    }

    public function tags($blog, $query = false)
    {
        return $this->client->get([
            'path'    => "blogs/{$blog}/articles/tags.json",
            'query'   => $query,
            'extract' => 'tags',
        ]);
    }
}
