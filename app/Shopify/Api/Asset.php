<?php

namespace App\Shopify\Api;

class Asset extends Resource
{
    public function all($theme, $query = false)
    {
        return $this->client->get([
            'path'    => "themes/{$theme}/assets.json",
            'query'   => $query,
            'extract' => 'assets',
        ]);
    }

    public function get($theme, $query = false)
    {
        return $this->client->get([
            'path'    => "themes/{$theme}/assets.json",
            'query'   => $query,
            'extract' => 'asset',
        ]);
    }

    public function create($theme, array $asset)
    {
        return $this->client->put([
            'path'    => "themes/{$theme}/assets.json",
            'options' => ['asset' => $asset],
            'extract' => 'asset',
        ]);
    }

    public function update($theme, array $asset)
    {
        return $this->create($theme, $asset);
    }

    public function delete($theme, array $query)
    {
        return $this->client->delete([
            'path'  => "themes/{$theme}/assets.json",
            'query' => $query,
        ])->getStatusCode();
    }
}
