<?php

namespace App\Shopify\Api;

class ScriptTag extends Resource
{
    public function all($query = false)
    {
        return $this->client->get([
            'path'    => 'script_tags.json',
            'query'   => $query,
            'extract' => 'script_tags',
        ]);
    }

    public function get($id, $query = false)
    {
        return $this->client->get([
            'path'    => "script_tags/{$id}.json",
            'query'   => $query,
            'extract' => 'script_tag',
        ]);
    }

    public function count($query = [])
    {
        return $this->client->get([
            'path'    => 'script_tags/count.json',
            'query'   => $query,
            'extract' => 'count',
        ]);
    }

    public function create(array $scriptTag)
    {
        return $this->client->post([
            'path'    => 'script_tags.json',
            'options' => ['script_tag' => $scriptTag],
            'extract' => 'script_tag',
        ]);
    }

    public function update($id, array $scriptTag)
    {
        return $this->client->put([
            'path'    => "script_tags/{$id}.json",
            'options' => ['script_tag' => $scriptTag],
            'extract' => 'script_tag',
        ]);
    }

    public function delete($id)
    {
        return $this->client->delete([
            'path' => "script_tags/{$id}.json"
        ])->getStatusCode();
    }
}
