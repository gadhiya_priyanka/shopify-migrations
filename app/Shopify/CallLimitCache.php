<?php

namespace App\Shopify;

use Illuminate\Contracts\Cache\Repository as Cache;
use App\Shopify\CallLimit;

class CallLimitCache extends CallLimit
{
    /**
     * @var Cache
     */
    protected $cache;

    /**
     * CallLimitCache constructor.
     * @param Cache $cache
     */
    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param string $domain
     * @param array $queue
     */
    public function setQueue($domain, $queue)
    {
        $this->cache->forever("api.limit.{$domain}", $queue);
    }

    /**
     * @param string $domain
     * @return array|null
     */
    protected function getQueue($domain)
    {
        return $this->cache->get("api.limit.{$domain}");
    }
}
