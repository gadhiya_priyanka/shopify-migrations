<?php

namespace App\Shopify;

class CallLimitTemporary extends CallLimit
{

    protected $domains = [];

    /**
     * @param string $domain
     * @return array|null
     */
    protected function getQueue($domain)
    {
        return isset($this->domains[$domain]) ? $this->domains[$domain] : null;
    }

    /**
     * @param string $domain
     * @param array $queue
     */
    protected function setQueue($domain, $queue)
    {
        \Log::info(['call_limit' => $queue['call_limit']]);
        $this->domains[$domain] = $queue;
    }
}
