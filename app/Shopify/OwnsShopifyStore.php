<?php

namespace App\Shopify;

trait OwnsShopifyStore
{
    public function shopOwner($shop)
    {
        return static::whereDomain($shop)->first();
    }

    public function isActive()
    {
        return $this->installed;
    }

    public function install()
    {
        return $this->update(['installed' => 1]);
    }

    public function uninstall()
    {
        //return $this->update(['installed' => 0]);
        return $this->delete();
    }

    public function setAccessTokenAttribute($value)
    {
        //$this->attributes['access_token'] = encrypt($value);
        $this->attributes['access_token'] = $value;
    }

    public function getAccessTokenAttribute()
    {
        //return decrypt($this->attributes['access_token']);
        return $this->attributes['access_token'];
    }
}
