<?php

namespace App\Shopify;

use Exception;
use App\Shopify\Client;
use App\Shopify\Oauth;

class ShopifyManager
{

    public function client()
    {
        return $this->resolve(Client::class);
    }

    public function api($resource)
    {
        $fullyQualified = "\\App\\Shopify\\Api\\{$resource}";

        if (!class_exists($fullyQualified)) {
            throw new Exception("Shopify API class ['{$fullyQualified}'] does not exist");
        }

        return $this->resolve($fullyQualified);
    }

    public function oauth()
    {
        return $this->resolve(Oauth::class);
    }

    protected function resolve($class, $parameters = [])
    {
        return app($class, $parameters);
    }
}
