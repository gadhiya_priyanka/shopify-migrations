<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
    public $incrementing = FALSE;
    protected $fillable = [
        'id',
        'name',
        'slug'
    ];

    public function setting()
    {
        return $this->hasOne(PlatformSetting::class);
    }

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function scopeShopify()
    {
        return $this->with('setting')->where('name', 'Shopify')->first();
    }
}
