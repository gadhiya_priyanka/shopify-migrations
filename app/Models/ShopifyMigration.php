<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopifyMigration extends Model
{
    public $incrementing = FALSE;
    protected $fillable = [
        'id',
        'type'
    ];

    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }
}
