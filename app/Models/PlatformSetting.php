<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlatformSetting extends Model
{
    public $incrementing = FALSE;
    protected $fillable = [
        'id',
        'platform_id',
        'shopify_project',
        'shopify_product',
        'shopify_blog',
        'shopify_customer',
        'shopify_coupon',
        'shopify_order'
    ];
    protected $casts = [
        'shopify_project' => 'array',
        'shopify_product' => 'array',
        'shopify_blog' => 'array',
        'shopify_customer' => 'array',
        'shopify_coupon' => 'array',
        'shopify_order' => 'array',
    ];

    public function platform()
    {
        return $this->belongsTo(Platform::class);
    }
}
