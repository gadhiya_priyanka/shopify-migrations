<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectCollection extends Model
{
    public $incrementing = FALSE;
    protected $fillable = [
        'id',
        'project_id',
        'project_collection_setting_id',
        'collection',
        'extra',
        'export'
    ];
    protected $casts = [
        'collection' => 'array',
        'extra' => 'array',
        'export' => 'array'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function collectionSetting()
    {
        return $this->belongsTo(ProjectCollectionSetting::class);
    }
}
