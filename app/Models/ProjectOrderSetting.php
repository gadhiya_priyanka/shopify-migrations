<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectOrderSetting extends Model
{
    public $incrementing = FALSE;
    protected $fillable = [
        'id',
        'project_id',
        'mapping',
        'file_paths',
        'import_type'
    ];
    protected $casts = [
        'mapping' => 'array',
        'file_paths' => 'array'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function orders()
    {
        return $this->hasMany(ProjectOrder::class);
    }

    public function schedule()
    {
        return $this->morphOne(Schedule::class, 'schedulable');
    }

    public function successOrders()
    {
        return $this->orders()->where(['extra->type' => 'success'])->oldest();
    }

    public function failedOrders()
    {
        return $this->orders()->where(['extra->type' => 'error'])->oldest();
    }
}
