<?php

namespace App\Models;

use App\Events\ScheduleSaved;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    public $incrementing = FALSE;
    protected $fillable = [
        'id',
        'shopify_migration_id',
        'schedulable_id',
        'schedulable_type',
        'status'
    ];
    
    protected $dispatchesEvents = [
        'saved' => ScheduleSaved::class,
    ];
        
    public function shopifyMigration()
    {
        return $this->belongsTo(ShopifyMigration::class);
    }

    public function schedulable()
    {
        return $this->morphTo();
    }
}
