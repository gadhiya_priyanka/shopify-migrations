<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectRedirectSetting extends Model
{
    public $incrementing = FALSE;
    protected $fillable = [
        'id',
        'project_id',
        'mapping',
        'file_paths'
    ];
    protected $casts = [
        'mapping' => 'array',
        'file_paths' => 'array'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function redirects()
    {
        return $this->hasMany(ProjectRedirect::class);
    }

    public function schedule()
    {
        return $this->morphOne(Schedule::class, 'schedulable');
    }

    public function successRedirects()
    {
        return $this->redirects()->where(['extra->type' => 'success'])->oldest();
    }

    public function failedRedirects()
    {
        return $this->redirects()->where(['extra->type' => 'error'])->oldest();
    }

    public function assetRedirects()
    {
        return $this->redirects()->where(['extra->type' => 'asset'])->oldest();
    }
}
