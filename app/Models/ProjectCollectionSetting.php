<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectCollectionSetting extends Model
{
    public $incrementing = FALSE;
    protected $fillable = [
        'id',
        'project_id',
        'mapping',
        'file_paths',
        'import_type'
    ];
    protected $casts = [
        'mapping' => 'array',
        'file_paths' => 'array'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function collections()
    {
        return $this->hasMany(ProjectCollection::class);
    }

    public function schedule()
    {
        return $this->morphOne(Schedule::class, 'schedulable');
    }
}
