<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectRedirect extends Model
{
    public $incrementing = FALSE;
    protected $fillable = [
        'id',
        'project_id',
        'project_redirect_setting_id',
        'redirect',
        'extra',
        'export'
    ];
    protected $casts = [
        'redirect' => 'array',
        'extra' => 'array',
        'export' => 'array'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function redirectSetting()
    {
        return $this->belongsTo(ProjectRedirectSetting::class);
    }
}
