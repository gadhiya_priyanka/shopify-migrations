<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public $incrementing = FALSE;
    protected $fillable = [
        'id',
        'user_id',
        'platform_id',
        'name',
        'platform_details',
        'shopify_details'
    ];
    protected $casts = [
        'platform_details' => 'array',
        'shopify_details' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function platform()
    {
        return $this->belongsTo(Platform::class);
    }

    public function productSettings()
    {
        return $this->hasMany(ProjectProductSetting::class);
    }

    public function collectionSettings()
    {
        return $this->hasMany(ProjectCollectionSetting::class);
    }

    public function customerSettings()
    {
        return $this->hasMany(ProjectCustomerSetting::class);
    }

    public function orderSettings()
    {
        return $this->hasMany(ProjectOrderSetting::class);
    }

    public function redirectSettings()
    {
        return $this->hasMany(ProjectRedirectSetting::class);
    }

    public function products()
    {
        return $this->hasMany(ProjectProduct::class);
    }

    public function collections()
    {
        return $this->hasMany(ProjectCollection::class);
    }

    public function customers()
    {
        return $this->hasMany(ProjectCustomer::class);
    }

    public function orders()
    {
        return $this->hasMany(ProjectOrder::class);
    }
}
