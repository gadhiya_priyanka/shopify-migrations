<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectCustomer extends Model
{
    public $incrementing = FALSE;
    protected $fillable = [
        'id',
        'project_id',
        'project_customer_setting_id',
        'customer',
        'extra',
        'export'
    ];
    protected $casts = [
        'customer' => 'array',
        'extra' => 'array',
        'export' => 'array'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function customerSetting()
    {
        return $this->belongsTo(ProjectCustomerSetting::class);
    }
}
