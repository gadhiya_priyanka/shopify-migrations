<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectCustomerSetting extends Model
{
    public $incrementing = FALSE;
    protected $fillable = [
        'id',
        'project_id',
        'mapping',
        'file_paths',
        'import_type'
    ];
    protected $casts = [
        'mapping' => 'array',
        'file_paths' => 'array'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function customers()
    {
        return $this->hasMany(ProjectCustomer::class);
    }

    public function schedule()
    {
        return $this->morphOne(Schedule::class, 'schedulable');
    }

    public function successCustomers()
    {
        return $this->customers()->where(['extra->type' => 'success'])->oldest();
    }

    public function failedCustomers()
    {
        return $this->customers()->where(['extra->type' => 'error'])->oldest();
    }
}
