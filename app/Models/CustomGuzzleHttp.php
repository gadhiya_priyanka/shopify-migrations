<?php

namespace App\Models;

use Config;
use DB;
use GuzzleHttp\Client;

class CustomGuzzleHttp
{

    public static function connectExternalAPI($data)
    {

        //test later 
//        new Client([
//                'headers' => ['content-type' => 'application/json']
//        ]);
        return new Client([
            'defaults' => [
                'headers' => ['content-type' => 'application/json']
            ],
            'auth' => [
                $data['api_key'],
                $data['api_password']
            ],
            'base_uri' => $data['api_url']
        ]);
    }

    public static function connectExternalDatabase($data)
    {
        Config::set('database.connections.mysql_external.host', $data['db_host']);
        Config::set('database.connections.mysql_external.port', $data['db_port']);
        Config::set('database.connections.mysql_external.database', $data['db_name']);
        Config::set('database.connections.mysql_external.username', $data['db_username']);
        Config::set('database.connections.mysql_external.password', $data['db_password']);
        return DB::connection('mysql_external');
    }

    public static function connectExternalDatabase2($data)
    {
        Config::set('database.connections.mysql_external2.host', $data['db_host']);
        Config::set('database.connections.mysql_external2.port', $data['db_port']);
        Config::set('database.connections.mysql_external2.database', $data['db_name']);
        Config::set('database.connections.mysql_external2.username', $data['db_username']);
        Config::set('database.connections.mysql_external2.password', $data['db_password']);
        return DB::connection('mysql_external2');
    }

    public static function connectTest()
    {
        return new Client([
            'defaults' => [
                'headers' => ['content-type' => 'application/json']
            ]
        ]);
    }
}
