<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectProductSetting extends Model
{
    public $incrementing = FALSE;
    protected $fillable = [
        'id',
        'project_id',
        'mapping',
        'file_paths',
        'import_type'
    ];
    protected $casts = [
        'mapping' => 'array',
        'file_paths' => 'array'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function products()
    {
        return $this->hasMany(ProjectProduct::class);
    }

    public function schedule()
    {
        return $this->morphOne(Schedule::class, 'schedulable');
    }

    public function successProducts()
    {
        return $this->products()->where(['extra->type' => 'success'])->oldest();
    }

    public function failedProducts()
    {
        return $this->products()->where(['extra->type' => 'error'])->oldest();
    }
}
