<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectOrder extends Model
{
    public $incrementing = FALSE;
    protected $fillable = [
        'id',
        'project_id',
        'project_order_setting_id',
        'order',
        'extra',
        'export'
    ];
    protected $casts = [
        'order' => 'array',
        'extra' => 'array',
        'export' => 'array'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function orderSetting()
    {
        return $this->belongsTo(ProjectOrderSetting::class);
    }
}
