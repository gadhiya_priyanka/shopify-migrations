<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectProduct extends Model
{
    public $incrementing = FALSE;
    protected $fillable = [
        'id',
        'project_id',
        'project_product_setting_id',
        'product',
        'extra',
        'export'
    ];
    protected $casts = [
        'product' => 'array',
        'extra' => 'array',
        'export' => 'array'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function productSetting()
    {
        return $this->belongsTo(ProjectProductSetting::class);
    }
}
