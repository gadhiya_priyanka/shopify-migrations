@extends('layouts.app')

@section('page-title')
Redirects - Create
@endsection

@section('page-css')
@endsection

@section('content')
<?php
$projects = \Auth::user()->projects()->latest()->get();

?>
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h4 class="page-title">Redirect</h4>
                <ol class="breadcrumb">
                </ol>
            </div>           
        </div>        

        <!-- Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-8 col-md-push-2">
                            <form class="form-horizontal" role="form" method="POST" action="{{ route('redirect-create') }}" enctype="multipart/form-data">  
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Project</label>
                                    <div class="col-sm-10">                                       
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id') == $project->id) selected @endif>{{ $project->name }}</option>                                            
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('file_path') ? ' has-error' : '' }}">
                                    <label for="file_path" class="col-md-2 control-label">Upload File</label>
                                    <div class="col-md-10">
                                        <input id="file_path" type="file" class="filestyle"  data-iconname="fa fa-cloud-upload" name="file_path" value="{{ old('file_path') }}">
                                        <span class="help-block">
                                            <small>
                                                Download our 
                                                <a href="{{ route('sample-file-download', ['model' => 'redirect']) }}">
                                                    Xlsx template
                                                </a>
                                                to see an example of the required format.    
                                            </small>
                                        </span>

                                        @if ($errors->has('file_path'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('file_path') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
                                            Cancel
                                        </button>
                                    </div>
                                </div>                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script src="{{ asset('js/bootstrap-filestyle.min.js') }}"></script>
@endsection
