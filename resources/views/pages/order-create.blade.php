@extends('layouts.app')

@section('page-title')
Order - {{Request::is('*create*') ? 'Create' : 'Update'}}
@endsection

@section('content')
<?php
$shopifyOrderSettings = \App\Models\Platform::shopify()->setting->shopify_order;
$projects = \Auth::user()->projects()->with('platform')->latest()->get();
?>
<div class="content" id="order">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h4 class="page-title">Order</h4>
                <ol class="breadcrumb">
                </ol>
            </div>           
        </div>        

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <!-- Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-8 col-md-push-2">
                            <form class="form-horizontal" role="form" method="POST" action="{{ (isset($orderSetting)) ? route('order-update', ['orderSetting' => $orderSetting->id]) : route('order-create') }}">  
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}" >
                                    <label for="project_id" class="col-sm-2 control-label">Project</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id" v-model='project_id' v-on:change='getProjectDetails'>
                                            @foreach($projects as $project)                                            
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($orderSetting->project->id) ? $orderSetting->project->id : '')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                @if(isset($orderSetting))                                
                                <cratejoy-order v-if="platforms.cratejoy" :shopify_settings="{{ json_encode($shopifyOrderSettings) }}" :platform_id=platform_id :old="{{ json_encode(Session::getOldInput()) }}" :errors="{{ $errors }}" :ordersetting="{{$orderSetting}}"></cratejoy-order>
                                @else
                                <cratejoy-order v-if="platforms.cratejoy" :shopify_settings="{{ json_encode($shopifyOrderSettings) }}" :platform_id=platform_id :old="{{ json_encode(Session::getOldInput()) }}" :errors="{{ $errors }}"></cratejoy-order>
                                @endif

                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            {{ (isset($orderSetting)) ? 'Update' : 'Create' }}
                                        </button>
                                        <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
                                            Cancel
                                        </button>
                                    </div>
                                </div>                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript"  src="{{ asset('js/order.js')}}"></script>
@endsection