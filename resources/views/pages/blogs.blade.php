@php
$blogs = \App\Models\Blog::all();
@endphp

@foreach ($blogs as $blog)
<h1>{{ $blog->name }}</h1>
<p>{{ $blog->description }}</p>
@endforeach