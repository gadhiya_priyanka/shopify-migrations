@extends('layouts.app')
@section('page-title')
Blog Create
@endsection


@section('page-css')
<link href="{{asset('css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="content" id="blog">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h4 class="page-title">Blog</h4>
                <ol class="breadcrumb">
                </ol>
            </div>   

        </div>        
        @include ('pages.blogs')

        <!-- Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-8 col-md-push-2">
                            <form method="POST" action="/blogs" class="form-horizontal" role="form" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)">

                                <div class="form-group" >
                                    <label for="name" class="col-sm-2 control-label">Blog Name</label>
                                    <div class="col-sm-10">   
                                        <input type="text" id="name" name="name" class="form-control" v-model="form.name">
                                        <span class="help-block" v-if="form.errors.has('name')">
                                            <strong v-text="form.errors.get('name')"></strong>
                                        </span>            
                                    </div>
                                </div>

                                <div class="form-group" >
                                    <label for="description" class="col-sm-2 control-label">Blog Description</label>
                                    <div class="col-sm-10">   
                                        <input type="text" id="description" name="description" class="form-control" v-model="form.description">
                                        <span class="help-block" v-if="form.errors.has('description')">
                                            <strong v-text="form.errors.get('description')"></strong>
                                        </span>            
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light" :disabled="form.errors.any()">
                                            Create
                                        </button>            
                                    </div>
                                </div> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript"  src="{{ asset('js/blog.js')}}"></script>
@endsection