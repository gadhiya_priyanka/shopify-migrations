@extends('layouts.app')

@section('page-title')
Test Database
@endsection

@section('content')

<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h4 class="page-title">Test Connection</h4>
                <ol class="breadcrumb">
                </ol>
            </div>           
        </div>        

        <!-- Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-8 col-md-push-2">                               
                            @if ($errors->has('platform_database'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('platform_database') }}</strong>
                            </div>
                            @endif
                            @if (session('message'))
                            <div class="alert alert-success">
                                <strong>{{ session('message') }}</strong>
                            </div>
                            @endif

                            <form class="form-horizontal" role="form" method="POST" action="{{ route('test-connection') }}">  
                                {{ csrf_field() }}

                                <!--<h4 class="m-t-20 header-title"><b>Database details</b></h4>-->
                                <p class="text-muted font-18 m-b-30">Test remote SQL connectivity EASILY!. </p>

                                <div class="form-group{{ $errors->has('db_host') ? ' has-error' : '' }}">
                                    <label for="db_host" class="col-md-2 control-label">Host</label>
                                    <div class="col-md-10">
                                        <input id="db_host" type="text" class="form-control" placeholder="XXX.XX.XX.XXX" name="db_host" value="{{ old('db_host') }}">
                                        @if ($errors->has('db_host'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('db_host') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('db_port') ? ' has-error' : '' }}">
                                    <label for="db_port" class="col-md-2 control-label">Port</label>
                                    <div class="col-md-10">
                                        <input id="db_port" type="text" class="form-control" placeholder="3306" name="db_port" value="{{ old('db_port') }}">
                                        @if ($errors->has('db_port'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('db_port') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('db_name') ? ' has-error' : '' }}">
                                    <label for="db_name" class="col-md-2 control-label">Name</label>
                                    <div class="col-md-10">
                                        <input id="db_name" type="text" class="form-control" placeholder="db_name" name="db_name" value="{{ old('db_name') }}">
                                        @if ($errors->has('db_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('db_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('db_username') ? ' has-error' : '' }}">
                                    <label for="db_username" class="col-md-2 control-label">Username</label>
                                    <div class="col-md-10">
                                        <input id="db_username" type="text" class="form-control" placeholder="username" name="db_username" value="{{ old('db_username') }}">
                                        @if ($errors->has('db_username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('db_username') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('db_password') ? ' has-error' : '' }}">
                                    <label for="db_password" class="col-md-2 control-label">Password</label>
                                    <div class="col-md-10">
                                        <input id="db_password" type="password" class="form-control" placeholder="*****" name="db_password" value="{{ old('db_password') }}">
                                        @if ($errors->has('db_password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('db_password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Test
                                        </button>
                                        <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
                                            Cancel
                                        </button>
                                    </div>
                                </div> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
