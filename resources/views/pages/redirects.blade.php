@extends('layouts.app')

@section('page-title')
Redirects
@endsection

@section('page-css')
<link href="{{asset('css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('css/sweetalert.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h4 class="page-title">Redirects</h4>
                <ol class="breadcrumb">
                </ol>
            </div>
            <div class="col-sm-12 col-md-6  text-right">
                <a href="{{ route('redirect-create') }}" class="btn btn-default btn-md waves-effect waves-light m-b-30" data-animation="fadein" data-plugin="custommodal" 
                   data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-plus m-r-5"></i> Redirect</a>
            </div>            
        </div>  

        <!-- Content -->
        <div class="row">
            <div class="col-lg-12">
                @if (session('message'))
                <div class="alert alert-success">
                    <strong>{{ session('message') }}</strong>
                </div>
                @endif
                @if (session('error'))
                <div class="alert alert-danger">
                    <strong>{{ session('error') }}</strong>
                </div>
                @endif
                <div class="card-box table-responsive">                    
                    <table id="datatable" class="table table-striped table-bordered table-actions-bar">
                        <thead>
                            <tr>
                                <th>Project Name</th>                                
                                <th>Input File</th>
                                <th>Progress bar</th>
                                <th>Status</th>
                                <th>Source Urls</th>
                                <th>Redirect Urls</th>
                                <th>Output File</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($projects as $project)
                            @forelse($project->redirectSettings as $redirectSetting)

                            @if(!$redirectSetting->mapping['external'])                            
                            @php
                            $progress = (int)(($redirectSetting->redirects->count() * 100) / ($redirectSetting->file_paths['input']['total_urls']));
                            $style = "style=width:".$progress."%; visibility: visible; animation-name: animationProgress;";
                            @endphp

                            <tr>
                                <td>{{ $project->name }}</td>
                                <td>
                                    <a href="{{ route('file-download', ['model' => 'ProjectRedirectSetting', 'id' => $redirectSetting, 'type' => 'input', 'file_type' => 'file_path' ]) }}">
                                        {{ $redirectSetting->file_paths['input']['file_name'] }}
                                    </a>
                                </td>
                                <td>                                    
                                    <span class="text-custom pull-right">{{$progress}}%</span>
                                    <div class="progress"> 
                                        <div class="progress-bar progress-bar-custom progress-animated wow animated animated" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" {{$style}}>
                                        </div>
                                    </div>    
                                </td>
                                <td>
                                    @if($redirectSetting->schedule->status == "queued")
                                    <span class="label label-pink">In Queue</span>
                                    @elseif($redirectSetting->schedule->status == "todo")
                                    <span class="label label-primary">To Do</span>
                                    @elseif($redirectSetting->schedule->status == "inprogress")
                                    <span class="label label-purple">In Progress</span>
                                    @elseif($redirectSetting->schedule->status == "paused")
                                    <span class="label label-warning">Paused</span>
                                    @elseif($redirectSetting->schedule->status == "completed")
                                    <span class="label label-success">Completed</span>
                                    @endif
                                </td>
                                <td>
                                    {{$redirectSetting->file_paths['input']['total_urls']}}
                                </td>

                                <td>
                                    Success : {{$redirectSetting->successRedirects()->count()}}
                                    <br/>
                                    Assets : {{ $redirectSetting->assetRedirects()->count() }}
                                    <br/>
                                    Fail : {{$redirectSetting->failedRedirects()->count() }}
                                </td>
                                <td>
                                    @if($redirectSetting->schedule->status == "completed")
                                    @foreach ($redirectSetting->file_paths['outputs'] as $key => $file)
                                    {{$key}} - 
                                    <a href="{{ route('file-download', [ 'model' => 'ProjectRedirectSetting', 'id' => $redirectSetting, 'type' => 'outputs' , 'file_type' => $key ]) }}" class="table-action-btn">
                                        <i class="glyphicon glyphicon-download-alt"></i>
                                    </a>
                                    <br/>
                                    @endforeach
                                    @endif
                                </td>
                                <td>                                                                          
                                    @if ($redirectSetting->schedule->status != "inprogress") 
                                    <form action="{{ route('redirect-delete', ['redirectSetting' => $redirectSetting->id]) }}" method="post" class="table-action-btn" id="redirect_delete_{{$redirectSetting->id}}">
                                        {{ method_field('delete') }}
                                        {{ csrf_field() }}
                                        <a href="javascript:void(0)" class="table-action-btn" onclick='submit_redirect_delete_form("{{$redirectSetting->id}}")'><i class="md md-close"></i></a>
                                    </form> 
                                    @else
                                    -
                                    @endif
                                </td>
                            </tr> 
                            @endif
                            @empty
                            <tr>
                                <td>{{ $project->name }}</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                            </tr>  
                            @endforelse                      
                            @endforeach     
                        </tbody>
                    </table>
                </div>
            </div>
        </div>              
    </div>
</div>
@endsection

@section('page-js')
<script src="{{ asset('js/sweetalert.min.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap.js') }}"></script>
@endsection

@section('js')
<script type="text/javascript">
                                            $('#datatable').dataTable();
                                            function submit_redirect_delete_form(redirect_id) {
                                                swal({
                                                    title: "Are you sure?",
                                                    type: "warning",
                                                    showCancelButton: true,
                                                    confirmButtonColor: "#DD6B55",
                                                    confirmButtonText: "Yes, delete it!",
                                                    closeOnConfirm: false
                                                }, function () {
                                                    $('#redirect_delete_' + redirect_id).submit();
                                                });
                                            }
</script>
@endsection
