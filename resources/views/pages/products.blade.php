@extends('layouts.app')

@section('page-title')
Products
@endsection

@section('page-css')
<link href="{{asset('css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h4 class="page-title">Product</h4>
                <ol class="breadcrumb">
                </ol>
            </div>
            <div class="col-sm-12 col-md-6  text-right">
                <a href="{{ route('product-create') }}" class="btn btn-default btn-md waves-effect waves-light m-b-30">
                    <i class="fa fa-plus m-r-5"></i> Product
                </a>
            </div>            
        </div>  

        <!-- Content -->
        <div class="row">
            <div class="col-lg-12">
                @if (session('message'))
                <div class="alert alert-success">
                    <strong>{{ session('message') }}</strong>
                </div>
                @endif
                <div class="card-box table-responsive">                    
                    <table id="datatable" class="table table-striped table-bordered table-actions-bar">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Platform</th>
                                <th>Progress bar</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($projects as $project)
                            @foreach($project->productSettings()->latest()->get() as $productSetting)

                            @php
                            $progress = (int)(($productSetting->products->count() * 100) / ($productSetting->mapping['product_count']));
                            $style = "style=width:".$progress."%; visibility: visible; animation-name: animationProgress;";
                            @endphp

                            <tr>
                                <td>{{ $project->name }}</td>
                                <td>{{ $project->platform->name }}</td>
                                <td>                                    
                                    <span class="text-custom pull-right">{{$progress}}%</span>
                                    <div class="progress"> 
                                        <div class="progress-bar progress-bar-custom progress-animated wow animated animated" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" {{$style}}>
                                        </div>                                        
                                    </div>    
                                </td>
                                <td>
                                    @if($productSetting->schedule->status == "queued")
                                    <span class="label label-pink">In Queue</span>
                                    @elseif($productSetting->schedule->status == "todo")
                                    <span class="label label-primary">To Do</span>
                                    @elseif($productSetting->schedule->status == "inprogress")
                                    <span class="label label-purple">In Progress</span>
                                    @elseif($productSetting->schedule->status == "paused")
                                    <span class="label label-warning">Paused</span>
                                    @elseif($productSetting->schedule->status == "completed")
                                    <span class="label label-success">Completed</span>
                                    @endif
                                </td>
                                <td> 
                                    
                                    <a href="{{ route('product-update', ['productSetting' => $productSetting->id]) }}" class="table-action-btn"><i class="md md-edit"></i></a>                                    
                                    
                                </td>
                            </tr>  
                            @endforeach 
                            @endforeach     
                        </tbody>
                    </table>
                </div>
            </div>
        </div>              
    </div>
</div>
@endsection

@section('page-js')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap.js') }}"></script>
<script type="text/javascript">
    $('#datatable').dataTable();
</script>
@endsection
