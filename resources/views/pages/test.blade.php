<form role="form" action="" method="post" class="registration-form">

    <fieldset>
        {{ csrf_field() }}
<input type="hidden" name="_method" value="PATCH">
        <div class="form-top">

            <div class="form-top-left">
                <h3>Insight Contributor Account Info</h3>

            </div>
            <div class="form-top-right">

            </div>
        </div>

        <div class="form-bottom" style="height: 400px">
            <!--Name-->
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                <div class="col-md-8 col-md-offset-2">

                    <input id="name" type="text" class="form-control" name="name"
                           placeholder="Full Name (e.g. John Doe)" value="{{ old('name') }}">
                    <br>

                    @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                        <h3> name is required</h3>
                    </span>
                    @endif
                </div>
            </div>
            <!--Email-->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                <div class="col-md-8 col-md-offset-2">
                    <input id="email" type="email" class="form-control" name="email"
                           placeholder="Primary Email Address (e.g.Jdoe@gmail.com)"
                           value="{{ old('email') }}"><br>
                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <!--Password-->
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <!-- <label for="password" class="col-md-4 control-label">Password</label>-->
                <div class="col-md-8 col-md-offset-2">
                    <input id="password" type="password" class="form-control"
                           placeholder="Password (at least 6 character)" name="password"><br>
                    @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <!--PasswordConfirm-->
            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <div class="col-md-8 col-md-offset-2">
                    <input id="password-confirm" type="password" class="form-control"
                           placeholder="Confirm Password" name="password_confirmation"><br>

                    @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                        <h3> password mismatch</h3>
                    </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="form-top" style="margin-top: 10px">

            <div class="form-top-left">
                <h3>Professional Information</h3>
            </div>

        </div>

        <div class="form-bottom" style="height: 460px">
            <!--Primary Industry(single value)-->
            <div class="form-group{{ $errors->has('industry') ? ' has-error' : '' }}">

                <div class="col-md-8 col-md-offset-2"><br>
                    <select class="form-control selectpicker" name="industry" id="industry">
                        <option selected disabled>Primary Industry</option>
                        <option>Art</option>
                        <option>Business</option>
                        <option>Law</option>
                        <option>Media</option>
                        <option>Medicine</option>
                        <option>Education</option>
                        <option>Technology</option>
                        <option> Science</option>
                        <option>Service</option>
                        <option>Other</option>
                    </select>


                    @if ($errors->has('industry'))
                    <span class="help-block">
                        <strong>{{ $errors->first('industry') }}</strong>
                    </span>
                    @endif
                </div>
            </div>


            <!--Primary Job Function (single value)-->
            <div class="form-group{{ $errors->has('job_function') ? ' has-error' : '' }}">

                <div class="col-md-8 col-md-offset-2"><br>
                    <select class="form-control selectpicker" name="job_function"
                            id="job_function">
                        <option selected disabled>Primary Job Function</option>
                        @foreach($professions as $profession)
                        <option @if ($profession->id == old('job_function_id')) selected
                                 @endif value="{{ $profession->id }}">{{ $profession->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('job_function'))
                    <span class="help-block">
                        <strong>{{ $errors->first('job_function') }}</strong>
                    </span>@endif



                </div>
            </div>

            <!--Add relative experience (multi tag)-->
            <div id="tags" class="form-group" style="margin-top: 30px">

                <div class="col-md-8 col-md-offset-2"><br>


                    <select id="test" style="width: 100%;margin-left: 10%;" name="tags[]"
                            multiple>

                        <option value="root" disabled="disabled">Tags</option>
                        <option value="level11" parent="root" disabled="disabled">Subjects
                        </option>
                        <option value="level12" parent="root" disabled="disabled">Grades
                        </option>
                        <option value="level13" parent="root" disabled="disabled">Relationship
                            Management
                        </option>
                        <option value="level14" parent="root" disabled="disabled">Classroom
                            Management & Design
                        </option>
                        <option value="level15" parent="root" disabled="disabled">Curricula &
                            Resources
                        </option>
                        <option value="level16" parent="root" disabled="disabled">Professional
                            Growth & Career Management
                        </option>
                        <option value="level17" parent="root" disabled="disabled">More</option>
                        @foreach($tags as $tag)

                        @if($tag->category =='Subjects')
                        <option value='{{ $tag->id }}'
                                parent="level11"> {{$tag->name}}</option>
                        @endif
                        @if($tag->category =='Grades')
                        <option value='{{ $tag->id }}'
                                parent="level12"> {{$tag->name}}</option>
                        @endif
                        @if($tag->category =='Relationship Management')
                        <option value='{{ $tag->id }}'
                                parent="level13"> {{$tag->name}}</option>
                        @endif
                        @if($tag->category =='Classroom Management & Design')
                        <option value='{{ $tag->id }}'
                                parent="level14"> {{$tag->name}}</option>
                        @endif
                        @if($tag->category =='Curricula & Resources')
                        <option value='{{ $tag->id }}'
                                parent="level15"> {{$tag->name}}</option>
                        @endif
                        @if($tag->category =='Professional Growth & Career Management')
                        <option value='{{ $tag->id }}'
                                parent="level16"> {{$tag->name}}</option>
                        @endif
                        @if($tag->category =='More')
                        <option value='{{ $tag->id }}'
                                parent="level17"> {{$tag->name}}</option>
                        @endif
                        @endforeach

                    </select>
                    @if ($errors->has('tags'))
                    <span class="help-block">
                        <strong>{{ $errors->first('tags') }}</strong>
                    </span>
                    @endif
                </div>
            </div>


            <!--Bio-->
            <div class="form-group{{ $errors->has('bio') ? ' has-error' : '' }}">
                <!--  <label for="bio" class="col-md-4 control-label">Short Bio</label> -->

                <div class="col-md-8 col-md-offset-2"><br>
                    <textarea id="bio" class="form-control" placeholder="Brief profile bio"
                              name="bio">{{ old(nl2br('bio')) }}</textarea><br>


                    @if ($errors->has('bio'))
                    <span class="help-block">
                        <strong>{{ $errors->first('bio') }}</strong>

                    </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="form-top" style="margin-top: 10px">

            <div class="form-top-left">
                <h3> Agreements </h3>
            </div>

            <div class="form-top-right">
            </div>
        </div>

        <div class="form-bottom">

            <!--Terms-->
            <h2 class="section-heading">Cypress Community Principles</h2>
            <p class="lead">
                <br>
                Teachers value each other for their expertise.<br><br>

                Teachers believe in the power of collaboration and will work together to engage
                in
                open and honest dialogue, provide guidance and mentorship, and create content
                that
                supports growth and success for fellow teachers.<br><br>

                Teachers will respect each other and be mindful of what they post. We encourage,
                open and honest communication, a diversity of perspectives, and thoughtful
                disagreement. Harassment, disrespect, and inappropriate content are not
                tolerated.<br><br>

                Teachers will actively engage in fostering a positive community of learning and
                growth.<br><br>

                Teachers are the most significant influence on a student’s academic achievement
                and
                will support fellow teachers as agents of change and innovators of
                education.<br><br>
            </p>

            

            <!--Signup botton-->
            <button type="submit" id="submit" class="btn btn-default"
                    style="background-color: #a5d5a7">
                <i class="fa fa-btn fa-user"></i> Sign me up!
            </button>

            <button type="button" class="btn btn-default" style="background-color: #a5d5a7">
                <a class="btn btn-link" href="{{ url('/') }}" style="color: whitesmoke">
                    Cancel </a>
            </button>


        </div>
    </fieldset>

</form>