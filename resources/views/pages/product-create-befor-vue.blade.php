@extends('layouts.app')

@section('page-title')
Product - {{Request::is('*create*') ? 'Create' : 'Update'}}
@endsection

@section('content')
<?php
$projects = \Auth::user()->projects()->with('platform')->latest()->get();

?>
<div class="content" id="product">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h4 class="page-title">Product</h4>
                <ol class="breadcrumb">
                </ol>
            </div>           
        </div>        

        <!-- Content -->
        <div class="row">
            <div class="col-lg-12">
                @if ($errors->any())
                <div class="alert alert-danger">

                    {{-- dd($errors->messages()) --}}
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <div class="card-box">
                    <div class="row">
                        <div class="col-md-8 col-md-push-2">
                            <form class="form-horizontal" role="form" method="POST" action="{{ (isset($productSetting)) ? route('product-update', ['productSetting' => $productSetting->id]) : route('product-create') }}">  
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}" >
                                    <label for="project_id" class="col-sm-2 control-label">Project</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id" v-model='project_id' v-on:change='getProjectDetails'>
                                            @foreach($projects as $project)                                            
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($productSetting->project->id) ? $productSetting->project->id : '')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}" >
                                    <label for="desc" class="col-sm-2 control-label">Description</label>
                                    <div class="col-sm-10">   
                                        <textarea class="form-control" name="desc" id="desc" rows="5" v-pre>{{ old('desc', (isset($productSetting->mapping['desc']) ? $productSetting->mapping['desc'] : '')) }}</textarea>
                                        @if ($errors->has('desc'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('desc') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                @if(isset($productSetting))
                                <cratejoy-product v-cloak v-if="platforms.cratejoy" :productsetting="{{$productSetting}}" :old="{{ json_encode(Session::getOldInput()) }}" :errors="{{ $errors }}"></cratejoy-product>
                                @else
                                <cratejoy-product v-cloak v-if="platforms.cratejoy" :old="{{ json_encode(Session::getOldInput()) }}" :errors="{{ $errors }}"></cratejoy-product>
                                @endif

                                <div class="form-group">
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" id="migrate_with_current_handle" name="migrate_with_current_handle" value='true' @if(old('migrate_with_current_handle', (isset($productSetting->mapping['migrate_with_current_handle']) ? $productSetting->mapping['migrate_with_current_handle'] : '')) == 'true') checked @endif>
                                               <label for="migrate_with_current_handle">
                                            Migrate with current product handle?
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group" v-if="is_remigrate" v-cloak>
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" id="is_remigrate" name="is_remigrate" value='true' @if(old('is_remigrate', (isset($productSetting->mapping['is_remigrate']) ? $productSetting->mapping['is_remigrate'] : '')) == 'true') checked @endif>
                                               <label for="is_remigrate">
                                            Continue Migrate?
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group" v-cloak>
                                    <div class="col-sm-12 col-md-6  text-left">
                                        <a @click="addMetafield" class="btn btn-default btn-md waves-effect waves-light">
                                            <i class="fa fa-plus m-r-5"></i> Metafield
                                        </a>
                                    </div>        
                                </div>                

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}" v-for="metafield, index in metafields" v-cloak>
                                    <div class="col-md-4">                                        
                                        <input :id='"metafields["+index+"][key]"' type="text" class="form-control" v-model="metafield.key" :name='"metafields["+index+"][key]"' placeholder="key">
                                        
                                        @if ($errors->has('metafields.+$index+.key'))
                                        <span class="help-block">                                        
                                            <strong v-pre>{{ $errors->first('metafields.+index+.key') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-7">
                                        <select :id='"metafields["+index+"][value]"' class="form-control" v-model="metafield.value" :name='"metafields["+index+"][value]"'>                                        
                                            <option value="name">name</option>                                        
                                            <option value="description">description</option>                                        
                                            <option value="slug">slug</option>                                        
                                        </select>
                                    </div>
                                    <div class="col-md-1">
                                        <a v-on:click="cancelMetafield(index)" class="table-action-btn"><i class="md md-close"></i></a>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            {{ (isset($productSetting)) ? 'Update' : 'Create' }}
                                        </button>
                                        <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
                                            Cancel
                                        </button>
                                    </div>
                                </div>                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script>
    var metafields = {!! json_encode(old('metafields', [])) !!}
</script>
<script type="text/javascript"  src="{{ asset('js/product.js')}}"></script>
@endsection