@extends('layouts.app')

@section('page-title')
Projects
@endsection

@section('page-css')
<link href="{{asset('css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h4 class="page-title">Projects</h4>
                <ol class="breadcrumb">
                </ol>
            </div>
            <div class="col-sm-12 col-md-6  text-right">
                <a href="{{ route('project-create') }}" class="btn btn-default btn-md waves-effect waves-light m-b-30" data-animation="fadein" data-plugin="custommodal" 
                   data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-plus m-r-5"></i> Project</a>
            </div>            
        </div>  

        <!-- Content -->
        <div class="row">
            <div class="col-lg-12">
                @if (session('message'))
                <div class="alert alert-success">
                    <strong>{{ session('message') }}</strong>
                </div>
                @endif
                <div class="card-box table-responsive">                    
                    <table id="datatable" class="table table-striped table-bordered table-actions-bar">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Platform</th>
                                <th>Platform Website Url</th>
                                <th>Shopify Url</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($projects as $project)
                            <tr>
                                <td>{{ $project->name }}</td>
                                <td>{{$project->platform->name}}</td>
                                <td>{{$project->platform_details['url']}}</td>
                                <td>{{$project->shopify_details['api_url']}}</td>
                                <td> <a href="{{ route('project-update', ['project' => $project->id]) }}" class="table-action-btn"><i class="md md-edit"></i></a></td>
                            </tr>  
                            @endforeach     
                        </tbody>
                    </table>
                </div>
            </div>
        </div>              
    </div>
</div>
@endsection

@section('page-js')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap.js') }}"></script>
<script type="text/javascript">
    $('#datatable').dataTable();
</script>
@endsection
