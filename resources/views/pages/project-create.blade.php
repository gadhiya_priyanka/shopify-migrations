@extends('layouts.app')

@section('page-title')
Project - {{Request::is('*create*') ? 'Create' : 'Update'}}
@endsection

@section('content')
<?php
$platforms = \App\Models\Platform::latest()->get();

?>
<div class="content" id="project">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h4 class="page-title">Project</h4>
                <ol class="breadcrumb">
                </ol>
            </div>           
        </div>        

        <!-- Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-8 col-md-push-2">   
                            @if ($errors->has('shopify_api'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('shopify_api') }}</strong>                                
                            </div>
                            @endif

                            @if ($errors->has('platform_database'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('platform_database') }}</strong>
                            </div>
                            @endif

                            @if ($errors->has('platform_api'))
                            <div class="alert alert-danger">
                                <strong>{{ $errors->first('platform_api') }}</strong>
                            </div>
                            @endif

                            <form class="form-horizontal" role="form" method="POST" action="{{ (isset($project)) ? route('project-update', ['project' => $project->id]) : route('project-create') }}">  
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-2 control-label">Project Name</label>
                                    <div class="col-md-10">
                                        <input id="name" type="text" class="form-control" name="name" placeholder="xyz" value="{{ old('name', (isset($project)) ? $project->name : '') }}">
                                        @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <hr>
                                <h4 class="m-t-0 header-title text-center m-b-20"><b>Source Store Details</b></h4>

                                <div class="form-group{{ $errors->has('platform_id') ? ' has-error' : '' }}">
                                    <label for="platform_id" class="col-sm-2 control-label">Platform</label>
                                    <div class="col-md-10">
                                        <select id="platform_id" class="form-control" name="platform_id" v-model='platform_id' v-on:change='getPlatformSettings'>                                        
                                            @foreach($platforms as $platform)
                                            <option value="{{ $platform->id }}"  @if(old('platform_id', (isset($project->platform_id) ? $project->platform_id : '')) == $platform->id) selected @endif>{{ $platform->name }}</option>
                                            @endforeach         
                                        </select>

                                        @if ($errors->has('platform_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('platform_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('platform_details.url') ? ' has-error' : '' }}" v-if="platform_details.url">
                                    <label for="platform_details[url]" class="col-md-2 control-label"> URL</label>
                                    <div class="col-md-10">
                                        <input id="platform_details[url]" type="text" class="form-control" placeholder="http://yourdomain.com/" name="platform_details[url]" value="{{ old('platform_details.url', (isset($project)) ? $project->platform_details['url'] : '') }}">
                                        @if ($errors->has('platform_details.url'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('platform_details.url') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <h4 class="m-t-20 header-title" v-if="platform_details.db_host"><b>Database details</b></h4>
                                <p class="text-muted font-18 m-b-30" v-if="platform_details.db_host">Provide your server database details to access your data automatically. See document how to connect your remote database.</p>

                                <div class="form-group{{ $errors->has('platform_details.db_host') ? ' has-error' : '' }}" v-if="platform_details.db_host">
                                    <label for="platform_details[db_host]" class="col-md-2 control-label">Host</label>
                                    <div class="col-md-10">
                                        <input id="platform_details[db_host]" type="text" class="form-control" placeholder="XXX.XX.XX.XXX" name="platform_details[db_host]" value="{{ old('platform_details.db_host', (isset($project->platform_details['db_host'])) ? $project->platform_details['db_host'] : '') }}">
                                        @if ($errors->has('platform_details.db_host'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('platform_details.db_host') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('platform_details.db_port') ? ' has-error' : '' }}" v-if="platform_details.db_port">
                                    <label for="platform_details[db_port]" class="col-md-2 control-label">Port</label>
                                    <div class="col-md-10">
                                        <input id="platform_details[db_port]" type="text" class="form-control" placeholder="3306" name="platform_details[db_port]" value="{{ old('platform_details.db_port', (isset($project->platform_details['db_port'])) ? $project->platform_details['db_port'] : '') }}">
                                        @if ($errors->has('platform_details.db_port'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('platform_details.db_port') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('platform_details.db_name') ? ' has-error' : '' }}" v-if="platform_details.db_name">
                                    <label for="platform_details[db_name]" class="col-md-2 control-label">Name</label>
                                    <div class="col-md-10">
                                        <input id="platform_details[db_name]" type="text" class="form-control" placeholder="db_name" name="platform_details[db_name]" value="{{ old('platform_details.db_name', (isset($project->platform_details['db_name'])) ? $project->platform_details['db_name'] : '') }}">
                                        @if ($errors->has('platform_details.db_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('platform_details.db_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('platform_details.db_username') ? ' has-error' : '' }}" v-if="platform_details.db_username">
                                    <label for="platform_details[db_username]" class="col-md-2 control-label">Username</label>
                                    <div class="col-md-10">
                                        <input id="platform_details[db_username]" type="text" class="form-control" placeholder="username" name="platform_details[db_username]" value="{{ old('platform_details.db_username', (isset($project->platform_details['db_username'])) ? $project->platform_details['db_username'] : '') }}">
                                        @if ($errors->has('platform_details.db_username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('platform_details.db_username') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('platform_details.db_password') ? ' has-error' : '' }}" v-if="platform_details.db_password">
                                    <label for="platform_details[db_password]" class="col-md-2 control-label">Password</label>
                                    <div class="col-md-10">
                                        <input id="platform_details[db_password]" type="password" class="form-control" placeholder="*****" name="platform_details[db_password]" value="{{ old('platform_details.db_password', (isset($project->platform_details['db_password'])) ? $project->platform_details['db_password'] : '') }}">
                                        @if ($errors->has('platform_details.db_password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('platform_details.db_password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <h4 class="m-t-20 header-title" v-if="platform_details.api_url"><b>Api details</b></h4>
                                <p class="text-muted font-18 m-b-30" v-if="platform_details.api_url">Provide your api details to access your data automatically.</p>

                                <div class="form-group{{ $errors->has('platform_details.api_url') ? ' has-error' : '' }}" v-if="platform_details.api_url">
                                    <label for="platform_details[api_url]" class="col-md-2 control-label">Api Path</label>
                                    <div class="col-md-10">
                                        <input id="platform_details[api_url]" type="text" class="form-control" placeholder="http://yourdomain.com/store" name="platform_details[api_url]" value="{{ old('platform_details.api_url', (isset($project->platform_details['api_url'])) ? $project->platform_details['api_url'] : '') }}">
                                        @if ($errors->has('platform_details.api_url'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('platform_details.api_url') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('platform_details.api_key') ? ' has-error' : '' }}" v-if="platform_details.api_key">
                                    <label for="platform_details[api_key]" class="col-md-2 control-label">Key</label>
                                    <div class="col-md-10">
                                        <input id="platform_details[api_key]" type="text" class="form-control" placeholder="username" name="platform_details[api_key]" value="{{ old('platform_details.api_key', (isset($project->platform_details['api_key'])) ? $project->platform_details['api_key'] : '') }}">
                                        @if ($errors->has('platform_details.api_key'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('platform_details.api_key') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('platform_details.api_password') ? ' has-error' : '' }}" v-if="platform_details.api_password">
                                    <label for="platform_details[api_password]" class="col-md-2 control-label">Password</label>
                                    <div class="col-md-10">
                                        <input id="platform_details[api_password]" type="password" class="form-control" placeholder="*****" name="platform_details[api_password]" value="{{ old('platform_details.api_password', (isset($project->platform_details['api_password'])) ? $project->platform_details['api_password'] : '') }}">
                                        @if ($errors->has('platform_details.api_password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('platform_details.api_password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <hr>
                                <h4 class="m-t-0 header-title text-center m-b-20"><b>Shopify Store Details</b></h4>

                                <div class="form-group{{ $errors->has('shopify_details.api_url') ? ' has-error' : '' }}">
                                    <label for="shopify_details[domain]" class="col-md-2 control-label">Store Url</label>
                                    <div class="col-md-10">
                                        <input id="shopify_details[domain]" type="text" class="form-control" placeholder="https://yourdomain.myshopify.com" name="shopify_details[api_url]" value="{{ old('shopify_details.api_url', (isset($project)) ? $project->shopify_details['api_url'] : '') }}">
                                        @if ($errors->has('shopify_details.api_url'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('shopify_details.api_url') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('shopify_details.api_key') ? ' has-error' : '' }}">
                                    <label for="shopify_details[api_key]" class="col-md-2 control-label">Private APP Key</label>
                                    <div class="col-md-10">
                                        <input id="shopify_details[api_key]" type="text" class="form-control" placeholder="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" name="shopify_details[api_key]" value="{{ old('shopify_details.api_key', (isset($project)) ? $project->shopify_details['api_key'] : '') }}">
                                        @if ($errors->has('shopify_details.api_key'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('shopify_details.api_key') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('shopify_details.api_password') ? ' has-error' : '' }}">
                                    <label for="shopify_details[api_password]" class="col-md-2 control-label">Private APP Password</label>
                                    <div class="col-md-10">
                                        <input id="shopify_details[api_password]" type="password" class="form-control" placeholder="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" name="shopify_details[api_password]" value="{{ old('shopify_details.api_password', (isset($project)) ? $project->shopify_details['api_password'] : '') }}">
                                        @if ($errors->has('shopify_details.api_password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('shopify_details.api_password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            {{ (isset($project)) ? "Update" : "Create" }}
                                        </button>
                                        <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
                                            Cancel
                                        </button>
                                    </div>
                                </div> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript"  src="{{ asset('js/project.js')}}"></script>
@endsection