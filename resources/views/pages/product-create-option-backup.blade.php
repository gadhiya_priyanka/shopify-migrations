@extends('layouts.app')

@section('page-title')
Product - {{Request::is('*create*') ? 'Create' : 'Update'}}
@endsection

@section('content')
<?php
$projects = \Auth::user()->projects()->latest()->get();

?>
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h4 class="page-title">Create Product</h4>
                <ol class="breadcrumb">
                </ol>
            </div>           
        </div>        

        <!-- Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-8 col-md-push-2">
                            <form class="form-horizontal" role="form" method="POST" action="{{ route('301-redirect-create') }}" enctype="multipart/form-data">  
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Project</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Handle</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Title</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Body(html)</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Vendor</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Type</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Tags</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Published</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Option1 Name</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Option1 Value</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Option2 Name</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Option2 Value</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Option3 Name</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Option3 Value</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Variant SKU</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Variant Grams</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Variant Inventory Tracker</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Variant Inventory Qty</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Variant Inventory Policy</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Variant Fulfillment Service</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Variant Price</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Variant Compare At Price</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Variant Requires Shipping</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>                                
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Variant Taxable</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Variant Barcode</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Image Src</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Image Position</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Image Alt Text</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Gift Card</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">SEO Title</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">SEO Description</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Google Shopping / Google Product Category</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Google Shopping / Gender</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>                                
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Google Shopping / Age Group</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Google Shopping / MPN</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Google Shopping / AdWords Grouping</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Google Shopping / AdWords Labels</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Google Shopping / Condition</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Google Shopping / Custom Product</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Google Shopping / Custom Label 0</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Google Shopping / Custom Label 1</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Google Shopping / Custom Label 2</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Google Shopping / Custom Label 3</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Google Shopping / Custom Label 4</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Variant Image</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Variant Weight Unit</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                    <label for="project_id" class="col-sm-2 control-label">Variant Tax Code</label>
                                    <div class="col-sm-10">
                                        <select id="project_id" class="form-control" name="project_id">                                       
                                            @foreach($projects as $project)
                                            <option value="{{ $project->id }}"  @if(old('project_id', (isset($project->id) ? $project->id : 'fresh')) == $project->id) selected @endif>{{ $project->name }}</option>
                                            @endforeach       
                                        </select>

                                        @if ($errors->has('project_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('project_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
                                            Cancel
                                        </button>
                                    </div>
                                </div>                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection