@extends('layouts.account')

@section('content')
<div class="wrapper-page">
    <div class=" card-box">
        <div class="panel-heading">
            <h3 class="text-center"> Reset Password </h3>
        </div>

        <div class="panel-body">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <form method="POST" action="{{ route('password.email') }}" role="form" class="text-center">
                {{ csrf_field() }}
                
                <div class="form-group m-b-0">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address">
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>                        
                </div>
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-default w-sm waves-effect waves-light">
                        Reset
                    </button> 
                </span>
            </form>
        </div>
    </div>


</div>
@endsection
