<table>
    <tr>
        <td>Products of {{ strtoupper($data->project->name) }} project has been migrated.</td>
    </tr>
    <tr>
        <td>{{ $data->successProducts()->count() }} products successfully migrated.</td>
    </tr>
    <tr>
        <td>{{ $data->failedProducts()->count() }} products failed to migrate.</td>
    </tr>
</table>
