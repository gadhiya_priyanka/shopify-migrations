<table>
    <tr>
        <td>Orders of {{ strtoupper($data->project->name) }} project has been migrated.</td>
    </tr>
    <tr>
        <td>{{ $data->successOrders()->count() }} orders successfully migrated.</td>
    </tr>
    <tr>
        <td>{{ $data->failedOrders()->count() }} orders failed to migrate.</td>
    </tr>
</table>
