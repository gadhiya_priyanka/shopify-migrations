<table>
    <tr>
        <td>Customers of {{ strtoupper($data->project->name) }} project has been migrated.</td>
    </tr>
    <tr>
        <td>{{ $data->successCustomers()->count() }} customers successfully migrated.</td>
    </tr>
    <tr>
        <td>{{ $data->failedCustomers()->count() }} customers failed to migrate.</td>
    </tr>
</table>
