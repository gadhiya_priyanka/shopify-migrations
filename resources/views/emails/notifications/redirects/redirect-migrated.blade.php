{{ $data['message'] }}
<table>
    <tr>
        <td>Redirects of {{ strtoupper($data->project->name) }} project has been migrated.</td>
    </tr>
    <tr>
        <td>{{ $data->successRedirects()->count() + $data->assetRedirects()->count() }} redirects successfully migrated.</td>
    </tr>
    <tr>
        <td>{{ $data->failedRedirects()->count() }} redirects failed to migrate.</td>
    </tr>
</table>
