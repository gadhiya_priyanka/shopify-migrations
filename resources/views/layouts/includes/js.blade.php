<!-- jQuery  -->
<script>
    var resizefunc = [];
</script>

<script src="{{ asset('js/detect.js') }}"></script>
<script src="{{ asset('js/fastclick.js') }}"></script>
<script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('js/waves.js') }}"></script>
<script src="{{ asset('js/wow.min.js') }}"></script>
<script src="{{ asset('js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('js/jquery.scrollTo.min.js') }}"></script>

<!--<script src="{{ asset('js/jquery.peity.min.js') }}"></script>

<script src="{{ asset('js/jquery.sparkline.min.js') }}"></script>


<script src="{{ asset('js/jquery.dashboard_3.js') }}"></script>-->

<script src="{{ asset('js/jquery.core.js') }}"></script>
<script src="{{ asset('js/jquery.app.js') }}"></script>
