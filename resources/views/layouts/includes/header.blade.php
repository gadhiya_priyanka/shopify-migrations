<!-- =========== views/layouts/includes/header ==============-->
<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left">
        <div class="text-center">
            <a href="{{ route('dashboard') }}" class="logo"><i class="icon-magnet icon-c-logo"></i><span>Migrati<i class="md md-album"></i>n</span></a>
        </div>
    </div>

    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="">                
                <div class="pull-left">
                    <button class="button-menu-mobile open-left">
                        <i class="ion-navicon"></i>
                    </button>
                    <span class="clearfix"></span>
                </div>
                <ul class="nav navbar-nav navbar-right pull-right">                    
                    <li class="hidden-xs">
                        <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                    </li>   
                    <li class="dropdown">
                        <a href="{{ route('dashboard') }}" class="dropdown-toggle profile profile-icon" data-toggle="dropdown" aria-expanded="true">
                            <div class="iconbox bg-custom">
                                <span>{{ Auth::user()->name[0] }}</span>
                            </div>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                    <i class="ti-power-off m-r-5"></i> Logout
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </a>
                            </li>
                        </ul>
                    </li>                                        
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>