<!-- =========== views/layouts/includes/sidebar ==============-->
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
                <li>
                    <a href="{{ route('dashboard') }}" class="waves-effect {{ Request::is('dashboard*') ? 'active' : '' }}"><i class="ti-home"></i> <span> Dashboard </span> </a>
                </li>
                <li>
                    <a href="{{ route('projects') }}" class="waves-effect {{ Request::is('projects*') ? 'active' : '' }}"><i class="icon-folder-alt"></i> <span> Projects </span> </a>
                </li> 
                <li>                    
                    <a href="{{ route('products') }}" class="waves-effect {{ Request::is('products*') ? 'active' : '' }}"><i class="icon-tag"></i> <span> Products </span> </a>                    
                </li> 
                <!--                <li>
                                    <a href="#" class="waves-effect"><i class="icon-layers"></i> <span> Collections </span> </a>
                                </li>                
                                <li>
                                    <a href="#" class="waves-effect"><i class="ti-files"></i> <span> Pages </span> </a>
                                </li>                
                                <li>
                                    <a href="#" class="waves-effect"><i class="ti-rss"></i> <span> Blogs </span> </a>
                                </li>                -->
                <li>                    
                    <a href="{{ route('customers') }}" class="waves-effect"><i class="ti-user"></i> <span> Customers </span> </a>                    
                </li>
                <li>                   
                    <a href="{{ route('orders') }}" class="waves-effect"><i class="ti-shopping-cart-full"></i> <span> Orders </span> </a>                    
                </li>
                <li>
                    <a href="{{ route('redirects') }}" class="waves-effect {{ Request::is('redirects*') ? 'active' : '' }}"><i class="ti-share"></i> <span> Redirects </span> </a>
                </li>                
                <li>
                    <a href="#" class="waves-effect"><i class="ti-check-box"></i> <span> Completed Migration </span> </a>
                </li>                
                <li>
                    <a href="{{ route('test-connection') }}" class="waves-effect"><i class="ti-server"></i> <span> Test DB Connection </span> </a>
                </li>                
<!--                <li>
                    <a href="#" class="waves-effect"><i class="fa fa-wrench"></i> <i class="ti-settings"></i><span> Settings </span> </a>
                </li>                -->
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>