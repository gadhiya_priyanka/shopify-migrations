<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('page-title')</title>

        <!-- Favicon -->  
        <link rel="shortcut icon" href="{{asset('storage/favicon.jpg')}}" type="image/png" />

        <!-- Styles -->           
        @yield('css')
        @yield('page-css')
        @include('layouts.includes.css')

    </head>
    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            @include('layouts.includes.header')            
            <!-- Top Bar End -->

            @auth
            <!-- ========== Left Sidebar Start ========== -->
            @include('layouts.includes.sidebar')            
            <!-- Left Sidebar End --> 
            @endauth  


            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->                      
            <div class="content-page">
                @yield('content')                
            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
        </div>
        <!-- END wrapper -->

        <!-- JS -->
        <script src="{{ asset('js/modernizr.min.js') }}"></script>
        <script src="{{ asset('js/main-app.js') }}"></script>          
        @yield('page-js')
        @include('layouts.includes.js')
        @yield('js')
    </body>

    <!-- Mirrored from coderthemes.com/ubold_1.5/light/dashboard_3.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 30 Apr 2016 06:04:53 GMT -->
</html>
