/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Vue.component('cratejoy-product', require('./components/CratejoyProduct.vue'));

var project_id = $("#project_id").val();

Vue.prototype.$defaultPlatforms = function () {
    this.platforms['drupal'] = false
    this.platforms['django'] = false
    this.platforms['magento'] = false
    this.platforms['shopify'] = false
    this.platforms['yahoo'] = false
    this.platforms['cratejoy'] = false
    this.platforms['wordpress'] = false
    this.is_remigrate = false
}

var app = new Vue({
    el: '#product',
    data: {
        project_id: project_id,
        platforms: {
            drupal: false,
            django: false,
            magento: false,
            shopify: false,
            yahoo: false,
            cratejoy: false,
            wordpress: false
        },
        is_remigrate: false,        
        metafields: metafields
    },
    created: function () {
        this.getProjectDetails();
    },
    methods: {
        getProjectDetails: function () {
            axios.get('/api/user')
                    .then(response => {
                        console.log(response.data);
                    });
            axios.get('/api/projects/' + this.project_id + '?with=products')
                    .then(function (response) {
                        app.$defaultPlatforms();
                        var value = response.data.platform.slug;
                        var has_products = response.data.products.length;
                        if (has_products) {
                            app.is_remigrate = false;
                        }
                        app.platforms[value] = true;
                    }, function (response) {
                        console.log(response.statusText);
                    });
        },
        addMetafield: function () {
            this.metafields.push(Vue.util.extend({}, this.metafields));
        },
        cancelMetafield: function (index) {
            this.metafields.splice(index, 1);
        }
    }
});
