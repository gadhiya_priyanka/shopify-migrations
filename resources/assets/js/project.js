//platform id
var platform_id = $("#platform_id").val();

Vue.prototype.$defaultPlatformSettings = function () {
    this.platform_details['url'] = false
    this.platform_details['domain'] = false
    this.platform_details['db_host'] = false
    this.platform_details['db_port'] = false
    this.platform_details['db_name'] = false
    this.platform_details['db_username'] = false
    this.platform_details['db_password'] = false
    this.platform_details['api_url'] = false
    this.platform_details['api_key'] = false
    this.platform_details['api_password'] = false
}

var app = new Vue({
    el: '#project',
    data: {
        platform_id: platform_id,
        settings: [],
        platform_details: {
            url: false,
            domain: false,
            db_host: false,
            db_port: false,
            db_name: false,
            db_username: false,
            db_password: false,
            api_url: false,
            api_key: false,
            api_password: false
        }
    },
    created: function () {
        this.getPlatformSettings();
    },
    methods: {
        getPlatformSettings: function () {
            this.$defaultPlatformSettings();
            axios.get('/api/platforms/' + this.platform_id)
                    .then(function (response) {
                        this.settings = response.data.setting.shopify_project;
                        this.settings.forEach(function (value, key) {
                            app.platform_details[value] = true;
                        });
                    }, function (response) {
                        console.log(response.statusText);
                    });
        }
    }
});

