Vue.component('cratejoy-order', require('./components/CratejoyOrder.vue'));

var project_id = $("#project_id").val();

Vue.prototype.$defaultPlatforms = function () {
    this.platforms['drupal'] = false
    this.platforms['django'] = false
    this.platforms['magento'] = false
    this.platforms['shopify'] = false
    this.platforms['yahoo'] = false
    this.platforms['cratejoy'] = false
    this.platforms['wordpress'] = false
    this.is_remigrate = false
}

var app = new Vue({
    el: '#order',
    data: {
        project_id: project_id,
        platform_id: null,
        platforms: {
            drupal: false,
            django: false,
            magento: false,
            shopify: false,
            yahoo: false,
            cratejoy: false,
            wordpress: false
        },
        is_remigrate: false
    },
    created: function () {
        this.getProjectDetails();
    },
    methods: {
        getProjectDetails: function () {
            axios.get('/api/projects/' + this.project_id + '?with=orders')
                    .then(function (response) {
                        app.$defaultPlatforms();
                        app.platform_id = response.data.platform.id
                        var value = response.data.platform.slug;
                        var has_orders = response.data.orders.length;
                        if (has_orders) {
                            app.is_remigrate = true;
                        }
                        app.platforms[value] = true;
                    }, function (response) {
                        console.log(response.statusText);
                    });
        }
    }
});
