<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function () {

    Route::group(['namespace' => 'Api'], function () {

        Route::group(['prefix' => 'platforms'], function () {
            Route::get('/{platform}', 'PlatformController@view');
        });

        Route::group(['prefix' => 'redirects'], function () {
            Route::post('/create', 'RedirectController@store');
        });

        Route::group(['prefix' => 'redirect-settings'], function () {
            Route::post('/create', 'RedirectSettingController@store');
        });

        Route::group(['prefix' => 'projects'], function () {
            Route::get('/{project}', 'ProjectController@view');
        });
    });
});
