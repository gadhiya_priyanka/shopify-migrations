<?php
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/test', 'TestController@test');

//Route::get('/natura/order/migrate/{orderSetting}' , 'Other\Orders\OrderController@create');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::redirect('/', '/dashboard');
    Route::redirect('/home', '/dashboard');

    Route::view('/dashboard', 'dashboard')->name('dashboard');

    Route::match(['get', 'post'], '/test-connection', 'Core\TestConnectionCollection@create')->name('test-connection');

    Route::group(['namespace' => 'Website'], function () {

        Route::group(['prefix' => 'redirects'], function () {

            Route::get('/', 'RedirectController@index')->name('redirects');
            Route::match(['get', 'post'], '/create', 'RedirectController@create')->name('redirect-create');
            Route::delete('/delete/{redirectSetting}', 'RedirectController@delete')->name('redirect-delete');
        });

        Route::group(['prefix' => 'projects'], function () {

            Route::get('/', 'ProjectController@index')->name('projects');
            Route::match(['get', 'post'], '/create', 'ProjectController@create')->name('project-create');
            Route::match(['get', 'post'], '/update/{project}', 'ProjectController@update')->name('project-update');
        });

        Route::group(['prefix' => 'products'], function () {

            Route::get('/', 'ProductController@index')->name('products');
            Route::match(['get', 'post'], '/create', 'ProductController@create')->name('product-create');
            Route::match(['get', 'post'], '/update/{productSetting}', 'ProductController@update')->name('product-update');
            Route::delete('/delete/{productSetting}', 'ProductController@delete')->name('product-delete');
        });

        Route::group(['prefix' => 'customers'], function () {

            Route::get('/', 'CustomerController@index')->name('customers');
            Route::match(['get', 'post'], '/create', 'CustomerController@create')->name('customer-create');
            Route::match(['get', 'post'], '/update/{customerSetting}', 'CustomerController@update')->name('customer-update');
            Route::delete('/delete/{customerSetting}', 'CustomerController@delete')->name('customer-delete');
        });

        Route::group(['prefix' => 'orders'], function () {

            Route::get('/', 'OrderController@index')->name('orders');
            Route::match(['get', 'post'], '/create', 'OrderController@create')->name('order-create');
            Route::match(['get', 'post'], '/update/{orderSetting}', 'OrderController@update')->name('order-update');
            Route::delete('/delete/{orderSetting}', 'OrderController@delete')->name('order-delete');
        });
    });

    Route::group(['namespace' => 'Core', 'prefix' => 'files'], function () {
        Route::group(['prefix' => 'downloads'], function () {
            Route::group(['prefix' => '{model}'], function () {
                Route::get('/', 'FileController@downloadSampleFiles')->name('sample-file-download');
                Route::get('/{id}/{type}/{fileType}', 'FileController@download')->name('file-download');
            });
        });
    });
});

