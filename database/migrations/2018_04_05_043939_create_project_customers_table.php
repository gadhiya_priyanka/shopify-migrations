<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_customers', function (Blueprint $table) {
            $table->uuid('id')->primary()->unique();
            $table->uuid('project_id');
            $table->uuid('project_customer_setting_id');
            $table->json('customer');
            $table->json('extra')->nullable();
            $table->json('export')->nullable();
            $table->timestamps();
            
            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('project_customer_setting_id')->references('id')->on('project_customer_settings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('project_customers_project_id_foreign');
        $table->dropForeign('project_customers_project_customer_setting_id_foreign');
        Schema::dropIfExists('project_customers');
    }
}
