<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectRedirectSettingsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_redirect_settings', function (Blueprint $table) {
            $table->uuid('id')->primary()->unique();
            $table->uuid('project_id');
            $table->json('mapping')->nullable();
            $table->json('file_paths')->nullable();
//            $table->string('input_file_path', 255);
//            $table->string('input_file_name', 255);
//            $table->string('output_file_path', 255)->nullable();
//            $table->string('output_file_name', 255)->nullable();
//            $table->integer('total_platform_urls')->nullable();
//            $table->integer('total_shopify_urls')->nullable();
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('project_redirect_settings_project_id_foreign');
        Schema::dropIfExists('project_redirect_settings');
    }
}
