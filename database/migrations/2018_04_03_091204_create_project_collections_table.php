<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_collections', function (Blueprint $table) {
            $table->uuid('id')->primary()->unique();
            $table->uuid('project_id');
            $table->uuid('project_collection_setting_id');
            $table->json('collection');
            $table->json('extra')->nullable();
            $table->json('export')->nullable();
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('project_collection_setting_id')->references('id')->on('project_collection_settings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('project_collections_project_id_foreign');
        $table->dropForeign('project_collections_project_collection_setting_id_foreign');
        Schema::dropIfExists('project_collections');
    }
}
