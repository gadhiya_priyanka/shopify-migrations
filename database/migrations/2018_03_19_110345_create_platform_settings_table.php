<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlatformSettingsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('platform_settings', function (Blueprint $table) {
            $table->uuid('id')->primary()->unique();
            $table->uuid('platform_id');
            $table->json('shopify_project')->nullable();
            $table->json('shopify_product')->nullable();
            $table->json('shopify_blog')->nullable();
            $table->json('shopify_customer')->nullable();
            $table->json('shopify_coupon')->nullable();
            $table->json('shopify_order')->nullable();
            $table->timestamps();

            $table->foreign('platform_id')->references('id')->on('platforms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('platform_settings_platform_id_foreign');
        Schema::dropIfExists('platform_settings');
    }
}
