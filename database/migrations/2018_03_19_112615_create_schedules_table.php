<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->uuid('id')->primary()->unique();
            $table->uuid('shopify_migration_id');
            $table->morphs('schedulable');
            $table->enum('status', ['queued', 'todo', 'inprogress', 'paused', 'completed']);
            $table->timestamps();

            $table->foreign('shopify_migration_id')->references('id')->on('shopify_migrations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('schedules_shopify_migration_id_foreign');
        Schema::dropIfExists('schedules');
    }
}
