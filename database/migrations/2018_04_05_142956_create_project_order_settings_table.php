<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectOrderSettingsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_order_settings', function (Blueprint $table) {
            $table->uuid('id')->primary()->unique();
            $table->uuid('project_id');
            $table->json('mapping')->nullable();
            $table->json('file_paths')->nullable();
            $table->enum('import_type', ['file', 'api', 'database']);
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('project_order_settings_project_id_foreign');
        Schema::dropIfExists('project_order_settings');
    }
}
