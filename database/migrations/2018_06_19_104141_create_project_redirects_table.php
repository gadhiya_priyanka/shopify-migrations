<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectRedirectsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_redirects', function (Blueprint $table) {
            $table->uuid('id')->primary()->unique();
            $table->uuid('project_id');
            $table->uuid('project_redirect_setting_id');
            $table->json('redirect');
            $table->json('extra')->nullable();
            $table->json('export')->nullable();
//            $table->longText('path');
//            $table->longText('target');
//            $table->json('extra')->nullable();
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('project_redirect_setting_id')->references('id')->on('project_redirect_settings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_redirects');
    }
}
