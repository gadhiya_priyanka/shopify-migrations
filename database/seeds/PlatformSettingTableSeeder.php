<?php

use App\Models\PlatformSetting;
use App\Models\Platform;
use Illuminate\Database\Seeder;

class PlatformSettingTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $apiProjectSettings = ['url', 'api_url', 'api_key', 'api_password'];
        $dbProjectSettings = ['url', 'db_host', 'db_port', 'db_name', 'db_username', 'db_password'];

        //Django        
        $settings = [];

        $setting = [];
        $setting['name'] = 'Django';
        $setting['project'] = ['url'];
        $setting['product'] = [];
        $setting['blog'] = [];
        $setting['customer'] = [];
        $setting['coupon'] = [];
        $setting['order'] = [];
        $settings[] = $setting;

        //Magento        
        $setting = [];
        $setting['name'] = 'Magento';
        $setting['project'] = $dbProjectSettings;
        $setting['product'] = [];
        $setting['blog'] = [];
        $setting['customer'] = [];
        $setting['coupon'] = [];
        $setting['order'] = [];
        $settings[] = $setting;

        //Cratejoy        
        $setting = [];
        $setting['name'] = 'Cratejoy';
        $setting['project'] = $apiProjectSettings;
        $setting['product'] = [];
        $setting['blog'] = [];
        $setting['customer'] = [];
        $setting['coupon'] = [];
        $setting['order'] = [];
        $settings[] = $setting;

        //WordPress 
        $extraSettings = ['table_prefix'];
        $newSettings = [];
        $newSettings = array_merge($dbProjectSettings, $extraSettings);
        $setting = [];
        $setting['name'] = 'WordPress';
        $setting['project'] = $newSettings;
        $setting['product'] = [];
        $setting['blog'] = [];
        $setting['customer'] = [];
        $setting['coupon'] = [];
        $setting['order'] = [];
        $settings[] = $setting;

        //Yahoo        
        $setting = [];
        $setting['name'] = 'Yahoo';
        $setting['project'] = ['url'];
        $setting['product'] = [];
        $setting['blog'] = [];
        $setting['customer'] = [];
        $setting['coupon'] = [];
        $setting['order'] = [];
        $settings[] = $setting;

        //Drupal        
        $setting = [];
        $setting['name'] = 'Drupal';
        $setting['project'] = ['url'];
        $setting['product'] = [];
        $setting['blog'] = [];
        $setting['customer'] = [];
        $setting['coupon'] = [];
        $setting['order'] = [];
        $settings[] = $setting;

        //Shopify        
        $setting = [];
        $setting['name'] = 'Shopify';
        $setting['project'] = $apiProjectSettings;
        $setting['product'] = [];
        $setting['blog'] = [];
        $setting['customer'] = [];
        $setting['coupon'] = [];
        $setting['order'] = [];
        $settings[] = $setting;

        foreach ($settings as $data) {
            $platform = Platform::where('name', $data['name'])->first();

            $platformSetting = new PlatformSetting;
            $platformSetting->id = \UUID::uuid4()->toString();
            $platformSetting->shopify_project = $data['project'];
            $platformSetting->shopify_product = $data['product'];
            $platformSetting->shopify_blog = $data['blog'];
            $platformSetting->shopify_customer = $data['customer'];
            $platformSetting->shopify_coupon = $data['coupon'];
            $platformSetting->shopify_order = $data['order'];
            $platform->setting()->save($platformSetting);
        }
    }
}
