<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PlatformTableSeeder::class);
        $this->call(PlatformSettingTableSeeder::class);
        $this->call(ShopifyMigrationTableSeeder::class);
    }
}
