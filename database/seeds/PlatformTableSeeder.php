<?php

use App\Models\Platform;
use Illuminate\Database\Seeder;

class PlatformTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $platforms = [];

        $platform = [];
        $platform['name'] = 'Django';
        $platform['slug'] = 'django';
        $platforms[] = $platform;

        $platform = [];
        $platform['name'] = 'Magento';
        $platform['slug'] = 'magento';
        $platforms[] = $platform;

        $platform = [];
        $platform['name'] = 'Cratejoy';
        $platform['slug'] = 'cratejoy';
        $platforms[] = $platform;

        $platform = [];
        $platform['name'] = 'WordPress';
        $platform['slug'] = 'wordpress';
        $platforms[] = $platform;

        $platform = [];
        $platform['name'] = 'Yahoo';
        $platform['slug'] = 'yahoo';
        $platforms[] = $platform;

        $platform = [];
        $platform['name'] = 'Drupal';
        $platform['slug'] = 'drupal';
        $platforms[] = $platform;


        $platform = [];
        $platform['name'] = 'Shopify';
        $platform['slug'] = 'shopify';
        $platforms[] = $platform;

        foreach ($platforms as $data) {
            $platform = new Platform;
            $platform->id = \UUID::uuid4()->toString();
            $platform->name = $data['name'];
            $platform->slug = $data['slug'];
            $platform->save();
        }
    }
}
