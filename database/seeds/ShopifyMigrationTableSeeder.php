<?php

use App\Models\ShopifyMigration;
use Illuminate\Database\Seeder;

class ShopifyMigrationTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shopifyMigrations = [
            'Products',
            'Collections',
            'Customers',            
            'Coupons',
            'Orders',
            'Blogs',
            'Pages',
            'Redirects',            
        ];

        foreach ($shopifyMigrations as $type) {
            $shopifyMigration = new ShopifyMigration;
            $shopifyMigration->id = \UUID::uuid4()->toString();
            $shopifyMigration->type = $type;
            $shopifyMigration->save();
        }
    }
}
